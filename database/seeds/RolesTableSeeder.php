<?php
/**
 * Nov 9, 2018, 9:16 AM
 * Developed by Korn <kornthebkk@gmail.com>
 */

use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * pattern for set name : <name_of_module>/<name_of_permission>
         */

        /**
         * User
         */
        $role = new Role();
        $role->name = 'user/access';
        $role->description = 'User Access';
        $role->sort_order = 1;
        $role->save();

        $role = new Role();
        $role->name = 'user/modify';
        $role->description = 'User Modify';
        $role->sort_order = 2;
        $role->save();

        /**
         * Filemanager
         */
        $role = new Role();
        $role->name = 'filemanager/access';
        $role->description = 'Filemanager Access';
        $role->sort_order = 3;
        $role->save();

        $role = new Role();
        $role->name = 'filemanager/modify';
        $role->description = 'Filemanager Modify';
        $role->sort_order = 4;
        $role->save();

        /**
         * Article
         */
        $role = new Role();
        $role->name = 'article_category/access';
        $role->description = 'Article Caregory Access';
        $role->sort_order = 5;
        $role->save();

        $role = new Role();
        $role->name = 'article_category/modify';
        $role->description = 'Article Caregory Modify';
        $role->sort_order = 6;
        $role->save();

        $role = new Role();
        $role->name = 'article/access';
        $role->description = 'Article Access';
        $role->sort_order = 7;
        $role->save();

        $role = new Role();
        $role->name = 'article/modify';
        $role->description = 'Article Modify';
        $role->sort_order = 8;
        $role->save();


        $role = new Role();
        $role->name = 'banner/access';
        $role->description = 'Banner Access';
        $role->sort_order = 9;
        $role->save();

        $role = new Role();
        $role->name = 'banner/modify';
        $role->description = 'Banner Modify';
        $role->sort_order = 10;
        $role->save();

        $role = new Role();
        $role->name = 'event/access';
        $role->description = 'Event Access';
        $role->sort_order = 11;
        $role->save();

        $role = new Role();
        $role->name = 'event/modify';
        $role->description = 'Event Modify';
        $role->sort_order = 12;
        $role->save();

        $role = new Role();
        $role->name = 'exchange_program/access';
        $role->description = 'Exchange_program Access';
        $role->sort_order = 13;
        $role->save();

        $role = new Role();
        $role->name = 'exchange_program/modify';
        $role->description = 'Exchange_program Modify';
        $role->sort_order = 14;
        $role->save();

        $role = new Role();
        $role->name = 'exchange_category/access';
        $role->description = 'Exchange program Caregory Access';
        $role->sort_order = 29;
        $role->save();

        $role = new Role();
        $role->name = 'exchange_category/modify';
        $role->description = 'Exchange program Caregory Modify';
        $role->sort_order = 30;
        $role->save();


        $role = new Role();
        $role->name = 'co_operative/access';
        $role->description = 'Co_Operative Access';
        $role->sort_order = 15;
        $role->save();

        $role = new Role();
        $role->name = 'co_operative/modify';
        $role->description = 'Co_Operative Modify';
        $role->sort_order = 16;
        $role->save();

        $role = new Role();
        $role->name = 'student_portfolio/access';
        $role->description = 'Student Portfolio Access';
        $role->sort_order = 17;
        $role->save();

        $role = new Role();
        $role->name = 'student_portfolio/modify';
        $role->description = 'Student Portfolio Modify';
        $role->sort_order = 18;
        $role->save();

        $role = new Role();
        $role->name = 'video/access';
        $role->description = 'Video Access';
        $role->sort_order = 19;
        $role->save();

        $role = new Role();
        $role->name = 'video/modify';
        $role->description = 'Video Modify';
        $role->sort_order = 20;
        $role->save();

        $role = new Role();
        $role->name = 'ebochure/access';
        $role->description = 'Ebochure Access';
        $role->sort_order = 21;
        $role->save();

        $role = new Role();
        $role->name = 'ebochure/modify';
        $role->description = 'Ebochure Modify';
        $role->sort_order = 22;
        $role->save();
        
        $role = new Role();
        $role->name = 'admission/access';
        $role->description = 'Admission Access';
        $role->sort_order = 23;
        $role->save();

        $role = new Role();
        $role->name = 'admission/modify';
        $role->description = 'Admission Modify';
        $role->sort_order = 24;
        $role->save();

        $role = new Role();
        $role->name = 'banner_highlight/access';
        $role->description = 'Banner Highlight Access';
        $role->sort_order = 25;
        $role->save();

        $role = new Role();
        $role->name = 'banner_highlight/modify';
        $role->description = 'Banner Highlight Modify';
        $role->sort_order = 26;
        $role->save();

        $role = new Role();
        $role->name = 'finance/access';
        $role->description = 'Fanance Access';
        $role->sort_order = 27;
        $role->save();

        $role = new Role();
        $role->name = 'finance/modify';
        $role->description = 'Fanance Modify';
        $role->sort_order = 28;
        $role->save();

      

        

    }
}
