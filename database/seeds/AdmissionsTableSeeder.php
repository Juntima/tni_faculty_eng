<?php

use Illuminate\Database\Seeder;

use App\Admission;
use App\AdmissionDescription;
use App\Language;

class AdmissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker\Generator $faker)
    {
        $languages          = Language::all();//_โหลดภาษาทั้งหมด

        for ($i = 1; $i <= 20; $i++)
        {
            $id                  = $i;

            $item                = new Admission();
            $item->publish_start = date('Y-m-d H:i:s');
            $item->viewed        = $faker->numberBetween(100, 10000);
            $item->created_at    = date('Y-m-d H:i:s');
            $item->updated_at    = date('Y-m-d H:i:s');
            $item->save();
            unset($item);

             //ลูปเพิ่มแถวตามจำนวนภาษาที่มี
             foreach ($languages as $language)
             {
                $name        = $faker->sentence;
                $description = $faker->sentence;

                $item_description                   = new AdmissionDescription(); //_Model
                $item_description->admission_id     = $id;
                $item_description->language_id      = $language->id;
                $item_description->name             = '(' . $language->code . ')' . $name;
                $item_description->description      = '(' . $language->code . ')' . $description;
                $item_description->link             = $faker->url;
                $item_description->meta_title       = '(' . $language->code . ')' . $name;
                $item_description->meta_description = '(' . $language->code . ')' . $description;
                $item_description->save();

                unset($item_description);
            }//_end foreach
        }//_end for

    }
}
