<?php
/**
 * Nov 9, 2018, 9:16 AM
 * Developed by Korn <kornthebkk@gmail.com>
 */

use App\Article;
use App\ArticleCategory;
use App\ArticleToCategory;
use Illuminate\Database\Seeder;

class ArticleToCategoriesTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ArticleCategory::all();

        $items = Article::all();

        foreach ($items as $item) {
            $related = $this->random_categories($categories);
            if ($related) {
                foreach ($related as $article_category_id) {
                    $related_obj = new ArticleToCategory();
                    $related_obj->article_id = $item->id;
                    $related_obj->article_category_id = $article_category_id;
                    $related_obj->save();
                    unset($related_obj);
                }
            }
        }
    }

    private function random_categories($categories)
    {
        $index_last = count($categories) - 1;
        $random_begin = rand(0, $index_last);
        // echo $random_begin; die;
        $random_end = rand($random_begin, $index_last);
        // echo $random_begin . ', ' . $random_end; die;

        $arr_related = [];
        for ($i = $random_begin; $i < $random_end; $i++) {
            $arr_related[] = $categories[$i]->id;
        }
        return $arr_related;
    }
}
