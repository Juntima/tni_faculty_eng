<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExchangeCategoryDescriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exchange_category_descriptions', function (Blueprint $table) {
            $table->integer('exchange_category_id')->unsigned()->index();
            $table->integer('language_id')->unsigned()->index();
            $table->string('name', 255);
            $table->text('description')->nullable();
            $table->string('meta_title', 255);
            $table->string('meta_description')->nullable();
            $table->string('meta_keyword')->nullable();

            $table->primary(['exchange_category_id', 'language_id'], 'exchange_category_descriptions_primary');

            $table->foreign('language_id')
                ->references('id')->on('languages')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('exchange_category_id')
                ->references('id')->on('exchange_categories')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exchange_category_descriptions');
    }
}
