<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleToCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article_to_categories', function (Blueprint $table) {
            $table->integer('article_id')->unsigned();
            $table->integer('article_category_id')->unsigned();

            $table->primary(['article_id', 'article_category_id'], 'article_to_categories_primary');

            $table->foreign('article_id')
                ->references('id')->on('articles')
                ->onDelete('cascade');

            $table->foreign('article_category_id')
                ->references('id')->on('article_categories')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article_to_categories');
    }
}
