<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentPortfoliosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create('student_portfolios', function (Blueprint $table) {
             $table->increments('id');
             $table->string('image', 255)->nullable();
             $table->dateTime('publish_start');
             $table->dateTime('publish_stop')->nullable();
             $table->integer('viewed')->unsigned()->default(0)->nullable();
             $table->boolean('status')->default(1)->nullable();
             $table->timestamps();
         });
     }
 
     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('student_portfolios');
     }
 }
 