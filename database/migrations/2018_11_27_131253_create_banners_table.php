<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 128);
            $table->string('image', 255);
            $table->string('link', 255)->nullable();
            $table->string('background_color', 6)->nullable()->default('FFFFFF');
            $table->dateTime('publish_start');
            $table->dateTime('publish_stop')->nullable();
            $table->integer('sort_order')->default(0)->unsigned();
            $table->boolean('status')->default(1);
            $table->timestamps();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners');
    }
}
