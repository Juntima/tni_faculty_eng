<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExchangeToCategoriesTable extends Migration
{
   /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exchange_to_categories', function (Blueprint $table) {
            $table->integer('exchange_program_id')->unsigned();
            $table->integer('exchange_category_id')->unsigned();

            $table->primary(['exchange_program_id', 'exchange_category_id'], 'exchange_to_categories_primary');

            $table->foreign('exchange_program_id')
                ->references('id')->on('exchange_programs')
                ->onDelete('cascade');

            $table->foreign('exchange_category_id')
                ->references('id')->on('exchange_categories')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exchange_to_categories');
    }
}
