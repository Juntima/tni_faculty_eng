<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticleCategoryDescriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article_category_descriptions', function (Blueprint $table) {
            $table->integer('article_category_id')->unsigned()->index();
            $table->integer('language_id')->unsigned()->index();
            $table->string('name', 255);
            $table->text('description')->nullable();
            $table->string('meta_title', 255);
            $table->string('meta_description')->nullable();
            $table->string('meta_keyword')->nullable();

            $table->primary(['article_category_id', 'language_id'], 'article_category_descriptions_primary');

            $table->foreign('language_id')
                ->references('id')->on('languages')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('article_category_id')
                ->references('id')->on('article_categories')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article_category_descriptions');
    }
}
