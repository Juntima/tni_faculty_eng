<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmissionDescriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admission_descriptions' , function(Blueprint $table){
            $table->integer('admission_id')->unsigned()->index();
            $table->integer('language_id')->unsigned()->index();
            $table->string('name',255);
            $table->text('description')->nullable();
            $table->string('link',255);
            $table->text('tag')->nullable();
            $table->string('meta_title',255)->nullable();
            $table->string('meta_description',255)->nullable();
            $table->string('meta_keyword',255)->nullable();
            $table->timestamps();

            $table->primary(['admission_id','language_id'],'admission_descriptions_primary');

            $table->foreign('language_id')
                ->references('id')->on('languages')
                ->onDelete('restrict')
                ->onUpdate('cascade');
            
            $table->foreign('admission_id')
                ->references('id')->on('admissions')
                ->onDelete('cascade');//_edit from restrict
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admission_descriptions');
    }
}
