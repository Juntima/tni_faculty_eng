<?php
/**
 * Nov 9, 2018, 9:16 AM
 * Developed by Korn <kornthebkk@gmail.com>
 */

/**
* Front-end section
*/
Route::get(Config::get(''), 'Main\HomeController@index');
Route::get(Config::get('url.main.language'), 'Main\LanguageController@index');

Route::get(Config::get('url.main.major')   , 'Main\FacultyController@index');  // หลักสูตร 
Route::get(Config::get('url.main.major_ae')   , 'Main\MajorAeController@major_ae');
Route::get(Config::get('url.main.major_le')   , 'Main\MajorLeController@major_le');
Route::get(Config::get('url.main.major_ce')   , 'Main\MajorCeController@major_ce');
Route::get(Config::get('url.main.major_ie')   , 'Main\MajorIeController@major_ie');
Route::get(Config::get('url.main.major_ee')   , 'Main\MajorEeController@major_ee');
Route::get(Config::get('url.main.major_met')   , 'Main\MajorMetController@major_met');
Route::get(Config::get('url.main.major_dge')   , 'Main\MajorDgeController@major_dge');


Route::get(Config::get('url.main.message_eng_dean'), 'Main\HistoryController@index');// เกี่ยวกับ 
Route::get(Config::get('url.main.mission_and_vision'), 'Main\HistoryController@mission');// เกี่ยวกับ 
Route::get(Config::get('url.main.management_structure'), 'Main\HistoryController@structure');// เกี่ยวกับ 

Route::get(Config::get('url.main.eng_lab_research_room'), 'Main\HistoryController@eng_lab_research_room');
// Route::get(Config::get('url.main.eng_research_room'), 'Main\HistoryController@eng_research_room');

Route::get(Config::get('url.main.support'), 'Main\MemberController@index');
Route::get(Config::get('url.main.contact')   , 'Main\ContactController@index');


Route::get(Config::get('url.main.news'), 'Main\NewsController@index');
Route::get(Config::get('url.main.news').'/{id}/detail', 'Main\NewsController@detail')->where('id','[0-9]+');
// Route::get(Config::get('url.main.newdetail'), 'Main\AllNewsController@detail');
Route::get(Config::get('url.main.tags'), 'Main\NewsController@tag'); 

Route::get(Config::get('url.main.events'), 'Main\EventController@index');
Route::get(Config::get('url.main.events').'/{id}/detail', 'Main\EventController@detail')->where('id','[0-9]+');

Route::get(Config::get('url.main.ebrochures'), 'Main\EbrochureController@index');

Route::get(Config::get('url.main.student_portfolios'), 'Main\StudentPortfolioController@index');
Route::get(Config::get('url.main.student_portfolios').'/{id}/detail', 'Main\StudentPortfolioController@detail')->where('id','[0-9]+');

Route::get(Config::get('url.main.academic_services'), 'Main\AcademicServiceController@index');
Route::get(Config::get('url.main.academic_services').'/{id}/detail', 'Main\AcademicServiceController@detail')->where('id','[0-9]+');

Route::get(Config::get('url.main.researches'), 'Main\ResearchController@index');
Route::get(Config::get('url.main.researches').'/{id}/detail', 'Main\ResearchController@detail')->where('id','[0-9]+');

Route::get(Config::get('url.main.services'), 'Main\ServiceController@index');
Route::get(Config::get('url.main.services').'/{id}/detail', 'Main\ServiceController@detail')->where('id','[0-9]+');

// Route::get(Config::get('url.main.cooperatives'), 'Main\CoOperativeController@index');
// Route::get(Config::get('url.main.cooperatives').'/{id}/detail', 'Main\CoOperativeController@detail')->where('id','[0-9]+');


// Route::get(Config::get('url.main.exchange_programs'), 'Main\ExchangeProgramController@index');
// Route::get(Config::get('url.main.exchange_programs').'/{id}/detail', 'Main\ExchangeProgramController@detail')->where('id','[0-9]+');


Route::get(Config::get('url.main.tni_channel'), 'Main\VideoController@index');
Route::get(Config::get('url.main.tni_channel').'/{id}/detail', 'Main\VideoController@detail')->where('id','[0-9]+');

Route::get(Config::get('url.main.finances'), 'Main\FinanceController@index');
Route::get(Config::get('url.main.finances').'/{id}/detail', 'Main\FinanceController@detail')->where('id','[0-9]+');

//_hotline
Route::get(Config::get('url.main.hotline'), 'Main\HotlineController@index');
Route::get( Config::get('url.main.querysend_ajax') , function () {
    return view('main.hotline.querysend_ajax');
});


/**
* Backoffice section
*/

// ต้องเข้าสู่ระบบ และต้องเป็น admin
Route::group(['middleware' => 'auth', 'middleware' => 'auth.admin'], function () {

    // Default
    Route::get(Config::get('url.backoffice.backoffice'), function () {
        return redirect(Config::get('url.backoffice.dashboard'));
    });

    // Switch Language
    Route::get(Config::get('url.backoffice.language'), 'Backoffice\LanguageController@index');

    // Filemanager
    Route::get(Config::get('url.backoffice.filemanager'), 'Backoffice\FilemanagerController@index');
    Route::post(Config::get('url.backoffice.filemanager') . '/folder', 'Backoffice\FilemanagerController@folder');
    Route::post(Config::get('url.backoffice.filemanager') . '/upload', 'Backoffice\FilemanagerController@upload');
    Route::post(Config::get('url.backoffice.filemanager') . '/delete', 'Backoffice\FilemanagerController@delete');

    // Dashboard
    Route::get(Config::get('url.backoffice.dashboard'), 'Backoffice\DashboardController@index');

    // User
    Route::resource(Config::get('url.backoffice.users'), 'Backoffice\UserController');

    // User Profile
    Route::get(Config::get('url.backoffice.profile'), 'Backoffice\ProfileController@edit');
    Route::put(Config::get('url.backoffice.profile'), 'Backoffice\ProfileController@update');

    // Artcile Category
    Route::resource(Config::get('url.backoffice.article_categories'), 'Backoffice\ArticleCategoryController');

    // Artcile
    Route::get(Config::get('url.backoffice.articles') . '/touch/{id}', 'Backoffice\ArticleController@touch')
        ->where('id', '[0-9]+');
    Route::resource(Config::get('url.backoffice.articles'), 'Backoffice\ArticleController');

    // Banner
    Route::get(Config::get('url.backoffice.banners') . '/touch/{id}', 'Backoffice\BannerController@touch')
        ->where('id', '[0-9]+');
    Route::resource(Config::get('url.backoffice.banners'), 'Backoffice\BannerController');

    //BannerHighlight
    Route::get(Config::get('url.backoffice.banners_highlight') . '/touch/{id}', 'Backoffice\BannerHighlightController@touch')
        ->where('id','[0-9]+');
    Route::resource(Config::get('url.backoffice.banners_highlight'), 'Backoffice\BannerHighlightController');

    // Event
    Route::get(Config::get('url.backoffice.events') . '/touch/{id}', 'Backoffice\EventController@touch')
        ->where('id', '[0-9]+');
    Route::resource(Config::get('url.backoffice.events'), 'Backoffice\EventController');

    //Exchange Program category
    Route::resource(Config::get('url.backoffice.exchange_categories'), 'Backoffice\ExchangeCategoryController');

    // Exchange Program
    Route::get(Config::get('url.backoffice.exchange_programs') . '/touch/{id}', 'Backoffice\ExchangeProgramController@touch')
        ->where('id', '[0-9]+');
    Route::resource(Config::get('url.backoffice.exchange_programs'), 'Backoffice\ExchangeProgramController');
    
    //CoOperative
    Route::get(Config::get('url.backoffice.cooperatives') . '/touch/{id}', 'Backoffice\CoOperativeController@touch')
    ->where('id', '[0-9]+');
    Route::resource(Config::get('url.backoffice.cooperatives'), 'Backoffice\CoOperativeController');

    // Student Portfolio
    Route::get(Config::get('url.backoffice.student_portfolios') . '/touch/{id}', 'Backoffice\StudentPortfolioController@touch')
        ->where('id', '[0-9]+');
    Route::resource(Config::get('url.backoffice.student_portfolios'), 'Backoffice\StudentPortfolioController');

    // Video
    Route::get(Config::get('url.backoffice.videos') . '/touch/{id}', 'Backoffice\VideoController@touch')
        ->where('id', '[0-9]+');
    Route::resource(Config::get('url.backoffice.videos'), 'Backoffice\VideoController');

    // E-bochure
    Route::get(Config::get('url.backoffice.ebochures') . '/touch/{id}', 'Backoffice\EbochureController@touch')
        ->where('id', '[0-9]+');
    Route::resource(Config::get('url.backoffice.ebochures'), 'Backoffice\EbochureController');
    

    //_Admission
     Route::get(Config::get('url.backoffice.admissions') . '/touch/{id}', 'Backoffice\AdmissionController@touch')->where('id','[0-9]+');
     Route::resource(Config::get('url.backoffice.admissions'), 'Backoffice\AdmissionController');

      //_Accounting and Finance
      Route::get(Config::get('url.backoffice.finances') . '/touch/{id}', 'Backoffice\FinanceController@touch')->where('id','[0-9]+');
      Route::resource(Config::get('url.backoffice.finances'), 'Backoffice\FinanceController');

    //_DOCUMENT File manager
    Route::get(Config::get('url.backoffice.filemanager_doc') ,'Backoffice\FileManagerDocumentController@index' );
    Route::post(Config::get('url.backoffice.filemanager_doc') . '/folder', 'Backoffice\FileManagerDocumentController@folder');
    Route::post(Config::get('url.backoffice.filemanager_doc') . '/upload', 'Backoffice\FileManagerDocumentController@upload');
    Route::post(Config::get('url.backoffice.filemanager_doc') . '/delete', 'Backoffice\FileManagerDocumentController@delete');

     





});
