<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ebochure extends Model
{
protected $guarded = ['id'];
    private $table_description = 'ebochure_descriptions';


    public function ebochureDescriptions()
        {
            return $this->hasMany(EbochureDescription::class);
        }

    public function scopeOrder($query)
        {
            return $this->orderBy('updated_at', 'DESC');
        }


    public function scopeLanguage($query, $language_id)
        {
            return $this->join($this->table_description, $this->table_description . '.ebochure_id', 'id')->where('language_id', $language_id);
        }
    /**
     * สำหรับดึงข้อมูลที่ publish ของ Front-End
     */
    public function scopePublishWeb($query)
    {
        return $query->where('status', 1)
            ->where('publish_start', '<=', Carbon::now())
            ->where(function ($query) {
                $query->where('publish_stop', '>=', Carbon::now())
                    ->orWhere('publish_stop', '=', null);
            });
    }

    

}
