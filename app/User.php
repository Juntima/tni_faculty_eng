<?php
/**
 * Nov 9, 2018, 9:16 AM
 * Developed by Korn <kornthebkk@gmail.com>
 */

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;
     
    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = ['last_login'];

    // public function articles()
    // {
    //     return $this->hasMany('App\Article');
    // }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * @param string|array $roles
     */
    public function authorizeRoles($roles, $abort = true)
    {
        if (is_array($roles)) {

            if ($this->hasAnyRole($roles)) {
                return true;
            } else {
                if ($abort) {
                    abort(401, 'This action is unauthorized.');
                } else {
                    
                    return false;
                }
            }
        }

        if ($this->hasRole($roles)) {
            return true;
        } else {
            if ($abort) {
                abort(401, 'This action is unauthorized.');
            } else {
                return false;
            }
        }
    }

    /**
     * Check multiple roles
     * @param array $roles
     */
    public function hasAnyRole($roles)
    {
        return null !== $this->roles()->whereIn('name', $roles)->first();
    }

    /**
     * Check one role
     * @param string $role
     */
    public function hasRole($role)
    {
        return null !== $this->roles()->where('name', $role)->first();
    }

    






}
