<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ExchangeProgramDescription extends Model
{
    private $table_main = 'exchange_programs'; //ตารางที่เก็บข้อมูลหลัก
    
    
    public $timestamps = false; // Disable Laravel's Eloquent timestamps
    
    protected $fillable = [
        'exchange_program_id', 'language_id', 'name', 'description', 'tag', 'meta_title', 'meta_description', 'meta_keyword',
    ];

    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    public function exchangeProgram()
    {
        return $this->belongsTo(ExchangeProgram::class);
    }

    public function scopeLanguage($query, $language_id)
    {
        return $query->where('language_id', $language_id);
    }

    /**
     * ค้นหารตามชื่อ
     */
    public function scopeSearchKeyword($query, $name)
    {
        $query->where('name', 'LIKE', '%' . $name . '%');

        return $query;
    }

    public function scopeSearchCategory($query, $exchange_category_id)
    {
        $arr_exchange_program_id = ExchangeToCategory::where('exchange_category_id', $exchange_category_id)->pluck('exchange_program_id')->toArray();
        return $query->whereIn('exchange_program_id', $arr_exchange_program_id);
    }


    /**
     * เรียงตามวันที่อัปเดทล่าสุด
     */
    public function scopeOrder($query)
    {
        return $query->join($this->table_main, $this->table_main . '.id', 'exchange_program_id')->orderBy($this->table_main . '.updated_at', 'DESC');
    }

    /**
     * สำหรับดึงข้อมูลที่ publish ของ Front-End
     */
    public function scopePublishWeb($query)
    {
        return $query->join($this->table_main, $this->table_main . '.id', '=', 'exchange_program_id')
            ->where($this->table_main . '.status', 1)
            ->where('publish_start', '<=', Carbon::now())
            ->where(function ($query) {
                $query->where('publish_stop', '>=', Carbon::now())
                    ->orWhere('publish_stop', '=', null);
            })
            ->orderBy($this->table_main . '.publish_start', 'DESC');
            //->orderBy($this->table_main . '.updated_at', 'DESC');
    }


}
