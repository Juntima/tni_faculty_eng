<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class EbochureDescription extends Model
{
    private $table_main = 'ebochures'; //ตารางที่เก็บข้อมูลหลัก
    
    
    public $timestamps = false; // Disable Laravel's Eloquent timestamps
    
    protected $fillable = [
        'ebochure_id', 'language_id', 'name', 'description','link', 'tag', 'meta_title', 'meta_description', 'meta_keyword',
    ];

    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    public function ebochure()
    {
        return $this->belongsTo(Ebochure::class);
    }

    public function scopeLanguage($query, $language_id)
    {
        return $query->where('language_id', $language_id);
    }

    /**
     * ค้นหารตามชื่อ
     */
    public function scopeSearchKeyword($query, $name)
    {
        $query->where('name', 'LIKE', '%' . $name . '%');

        return $query;
    }

    

    /**
     * เรียงตามวันที่อัปเดทล่าสุด
     */
    public function scopeOrder($query)
    {
        return $query->join($this->table_main, $this->table_main . '.id', 'ebochure_id')->orderBy($this->table_main . '.updated_at', 'DESC');
    }

    /**
     * สำหรับดึงข้อมูลที่ publish ของ Front-End
     */
    public function scopePublishWeb($query)
    {
        return $query->join($this->table_main, $this->table_main . '.id', '=', 'ebochure_id')
            ->where($this->table_main . '.status', 1)
            ->where('publish_start', '<=', Carbon::now())
            ->where(function ($query) {
                $query->where('publish_stop', '>=', Carbon::now())
                    ->orWhere('publish_stop', '=', null);
            })
            ->orderBy($this->table_main . '.publish_start', 'DESC');
    }

}

