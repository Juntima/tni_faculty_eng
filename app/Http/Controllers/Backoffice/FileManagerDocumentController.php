<?php
/**
* MAR 6, 2018, 15:11PM
* Developed by Metha <fakestore@gmail.com>
*/

/**
 * Copy then Edited from FileManagerController.php
 */
namespace App\Http\Controllers\Backoffice;

use App\Pagination;
use Config;
use Illuminate\Http\Request;

class FileManagerDocumentController extends BackofficeController
{
    //_https://laravel.com/docs/5.4/requests
    
    protected $per_page          = 16;// จำนวนรูปหรือโฟลเดอร์ที่แสดงต่อหน้า
    private   $text_unauthorized = 'You are not authorized to use this function.';
    //private   $document_ext      = ['doc', 'docx', 'pdf', 'odt'];
    
    public function index(Request $request)
    {   
        if(!$request->user()->authorizeRoles('filemanager/access', false)){
            return '<div class="filemanager-unauthorized alert alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> ' . $this->text_unauthorized . '</div>';
        }
        //_Find which protocol to use to pass the full image link back
        
        $server = Config::get('app.url'); // die($server);

        if (isset($request->filter_name)) {
            $filter_name = rtrim(str_replace(array('*', '/', '\\'), '', $request->filter_name), '/');
        } else {
            $filter_name = '';
        }
        
        // Make sure we have the correct directory
        if (isset($request->directory)) {
            $directory = rtrim(Config::get('app.dir_document') . 'document/' .str_replace('*', '', $request->directory), '/');
        } else {
            $directory = Config::get('app.dir_document') . 'document';
        }
        
       
        if (isset($request->page)) {
            $page = $request->page;
        } else {
            $page = 1;
        }
       
        $directories      = array();
        $files            = array();
        $data['documents'] = array();
     
        if (substr(str_replace('\\', '/', realpath($directory) . '/' . $filter_name), 0, strlen(Config::get('app.dir_document').'document')) == str_replace('\\', '/', Config::get('app.dir_document').'document' ) ) {
            //_Get directories
            $directories = glob($directory . '/' . $filter_name . '*', GLOB_ONLYDIR);
           
            if (!$directories) {
                $directories = array();
            }

            //_Get files (https://www.w3schools.com/php/func_filesystem_glob.asp)
            $files = glob($directory . '/' . $filter_name . '*.{pdf,docx, doc ,xlsx,xls}', GLOB_BRACE);
            
            if (!$files) {
                $files = array();
            }
        }
        //dd($directories);
        //_Merge directories and files
        $documents = array_merge($directories, $files);
        
        //_Get total number of files and directories
        $document_total = count($documents);

        //_Split the array based on current page number and max number of items per page of 10
        $documents = array_splice($documents, ($page - 1) * $this->per_page, $this->per_page);
       
        //foreach ($documents as $document)
        foreach ($documents as $i => $document)
        {
            $name = str_split(basename($document), 14);
            
            if (is_dir($document))
            {
                $url = '';

                if (isset($request->target)) {
                    $url .= '&target=' . $request->target;
                }

                if (isset($request->thumb)) {
                    $url .= '&thumb=' . $request->thumb;
                }

                $data['documents'][] = array(
                    'thumb' => '',
                    'name' => implode(' ', $name),
                    'type' => 'directory',
                    'path' => substr($document, strlen(Config::get('app.dir_document'))),
                    'href' => Config::get('app.url') . 'backoffice/filemanager_doc/?directory='.urlencode( substr($document, strlen(Config::get('app.dir_document').'document/')) ) . $url,
                );
             
            } elseif (is_file($document)) {
                
                $file_name = implode('', $name);
                $arr_file  = explode('.', $file_name);//_file_name = name.ext
                //dd($file_name);
                if($arr_file[1] == 'pdf'){
                    $file_thumb = '<i class="fa fa-file-pdf-o"></i>';
                }elseif( ($arr_file[1] == 'doc')|| ($arr_file[1] == 'docx') ){
                    $file_thumb = '<i class="fa fa-file-word-o"></i>';
                }elseif( ($arr_file[1] == 'xls')|| ($arr_file[1] == 'xlsx') ){
                    $file_thumb = '<i class="fa fa-file-excel-o"></i>';    
                }else{
                    $file_thumb = '<i class="fa fa-file"></i>';
                }
                
                
                $data['documents'][] = array(
                    'thumb'=> $file_thumb,
                    'name' => $arr_file[0],
                    'type' => $arr_file[1],//'document',
                    'path' => substr($document, strlen(Config::get('app.dir_document'))),
                    'href' => $server . 'storage/' . substr($document, strlen(Config::get('app.dir_document'))),
                );
               
            }
        }//_endforeach 
        

        if (isset($request->directory)) {
            $data['directory'] = urlencode($request->directory);
        } else {
            $data['directory'] = '';
        }

        if (isset($request->filter_name)) {
            $data['filter_name'] = $request->filter_name;
        } else {
            $data['filter_name'] = '';
        }

        // Return the target ID for the file manager to set the value
        if (isset($request->target)) {
            $data['target'] = $request->target;
        } else {
            $data['target'] = '';
        }

        // Return the thumbnail for the file manager to show a thumbnail
        if (isset($request->thumb)) {
            $data['thumb'] = $request->thumb;
        } else {
            $data['thumb'] = '';
        }

        // Parent
        $url = '';

        if (isset($request->directory)) {
            $pos = strrpos($request->directory, '/');

            if ($pos) {
                $url .= '&directory=' . urlencode(substr($request->directory, 0, $pos));
            }
        }

        if (isset($request->target)) {
            $url .= '&target=' . $request->target;
        }

        if (isset($request->thumb)) {
            $url .= '&thumb=' . $request->thumb;
        }

        $data['parent'] = Config::get('app.url') . 'backoffice/filemanager_doc/?' . $url;

        //_Refresh
        $url = '';

        if (isset($request->directory)) {
            $url .= '&directory=' . urlencode($request->directory);
        }

        if (isset($request->target)) {
            $url .= '&target=' . $request->target;
        }

        if (isset($request->thumb)) {
            $url .= '&thumb=' . $request->thumb;
        }

        $data['refresh'] = Config::get('app.url') . 'backoffice/filemanager_doc/?' . $url;

        $url = '';

        if (isset($request->directory)) {
            $url .= '&directory=' . urlencode(html_entity_decode($request->directory, ENT_QUOTES, 'UTF-8'));
        }

        if (isset($request->filter_name)) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($request->filter_name, ENT_QUOTES, 'UTF-8'));
        }

        if (isset($request->target)) {
            $url .= '&target=' . $request->target;
        }

        if (isset($request->thumb)) {
            $url .= '&thumb=' . $request->thumb;
        }

        $pagination        = new Pagination();
        $pagination->total = $document_total;
        $pagination->page  = $page;
        $pagination->limit = $this->per_page;
        $pagination->url   = Config::get('app.url') . 'backoffice/filemanager_doc/?' . $url . '&page={page}';
        $data['pagination']      = $pagination->render();
        //dd($data);
        return view('backoffice.filemanager_doc.document' , $data);

    }

    
    public function upload(Request $request)
    {
        if(!$request->user()->authorizeRoles('filemanager/modify', false)){
            return $this->text_unauthorized;
        }

        $json = array();

        // Make sure we have the correct directory
        if (isset($request->directory)) {
            $directory = rtrim(Config::get('app.dir_document') . 'document/' .$request->directory, '/');
        } else {
            $directory = Config::get('app.dir_document') . 'document';
        }

        //_Check its a directory
        if (!is_dir($directory) || substr(str_replace('\\', '/', realpath($directory)), 0, strlen(Config::get('app.dir_document').'document')) != str_replace('\\', '/', Config::get('app.dir_document').'document')) {
            $json['error'] = 'error_directory';
        }
       

        if (!$json) {
            // Check if multiple files are uploaded or just one
            $files = array();

            // if (!empty($request->files['file']['name']) && is_array($this->request->files['file']['name'])) {
            if (!empty($_FILES['file']['name']) && is_array($_FILES['file']['name'])) {
                // if (!empty($request->file('file')) && is_array($request->file('file'))) {
                foreach (array_keys($request->file('file')) as $key) {
                    $files[] = array(
                        'name' => $_FILES['file']['name'][$key],
                        'type' => $_FILES['file']['type'][$key],
                        'tmp_name' => $_FILES['file']['tmp_name'][$key],
                        'error' => $_FILES['file']['error'][$key],
                        'size' => $_FILES['file']['size'][$key],
                    );
                }
            }

            foreach ($files as $file) {
                if (is_file($file['tmp_name'])) {
                    // Sanitize the filename
                    $filename = basename(html_entity_decode($file['name'], ENT_QUOTES, 'UTF-8'));

                    // Validate the filename length
                    if ((strlen($filename) < 3) || (strlen($filename) > 255)) {
                        $json['error'] = 'error_filename';
                    }
                   
                    $allowed = array(
                        'pdf' , 'docx' , 'doc' , 'xlsx' , 'xls'
                    );

                    if (!in_array(strtolower(substr(strrchr($filename, '.'), 1)), $allowed)) {
                        $json['error'] = 'error_filetype';
                    }
                    
                    $allowed = array(
                        'application/pdf',
                        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                        'application/msword',
                        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                        'application/vnd.ms-excel'
                    );

                    if (!in_array($file['type'], $allowed)) {
                        $json['error'] = 'error filetype. Please upload file pdf , docx , doc , xlsx , xls ';
                    }

                   

                    // Return any upload error
                    if ($file['error'] != UPLOAD_ERR_OK) {
                        $json['error'] = 'error_upload_' . $file['error'];
                    }

         
                }  

              
                else {
                    $json['error'] = 'error_upload';
                }

                 if (($file['size']) > 104857600) {
                        $json['error'] = 'File too large. Please upload file not more than 1MB';
                }


                if (!$json) {
                    move_uploaded_file($file['tmp_name'], $directory . '/' . $filename);
                }
            }
        }

        if (!$json) {
            $json['success'] = 'text_uploaded';
        }

        return $json;
    }
    
    public function folder(Request $request)
    {  
         if(!$request->user()->authorizeRoles('filemanager/modify', false)){
            return $this->text_unauthorized;
         }
        $json = array();

        //_Make sure we have the correct directory
        if (isset($request->directory)) {
            $directory = rtrim(Config::get('app.dir_document') . 'document/' . $request->directory, '/');
        } else {
            $directory = Config::get('app.dir_document') . 'document';
        }

        //_Check its a directory
        if ( !is_dir($directory) || substr(str_replace('\\', '/', realpath($directory)), 0, strlen(Config::get('app.dir_document').'document')) != str_replace('\\', '/', Config::get('app.dir_document').'document')) 
        {
            $json['error'] = 'error_directory';
        }
        
        //if ($request->server['REQUEST_METHOD'] == 'POST') {
        //_Sanitize the folder name
        $folder = basename(html_entity_decode($request->folder, ENT_QUOTES, 'UTF-8'));

        //_Validate the filename length
        if ((strlen($folder) < 3) || (strlen($folder) > 128)) {
            $json['error'] = 'error_folder';
        }

        //_Check if directory already exists or not
        if (is_dir($directory . '/' . $folder)) {
            $json['error'] = 'error_exists';
        }

        if (!isset($json['error'])) {
            mkdir($directory . '/' . $folder, 0777);
            chmod($directory . '/' . $folder, 0777);

            @touch($directory . '/' . $folder . '/' . 'index.html');

            $json['success'] = 'text_directory_success';
        }
      
        return $json;
    }

    public function delete(Request $request)
    {
        if(!$request->user()->authorizeRoles('filemanager/modify', false)){
            return $this->text_unauthorized;
        }


        $json = array();

        if (isset($request->path)) {
            $paths = $request->path;
        } else {
            $paths = array();
        }
        //var_dump($paths);
        // Loop through each path to run validations
        foreach ($paths as $path) {
            // Check path exsists
            if ($path == Config::get('app.dir_document') . 'document' || substr(str_replace('\\', '/', realpath(Config::get('app.dir_document') . $path)), 0, strlen(Config::get('app.dir_document').'document')) != str_replace('\\', '/', Config::get('app.dir_document').'document')) {
                $json['error'] = 'error_delete';

                break;
            }
        }

        if (!$json) {
            // Loop through each path
            foreach ($paths as $path) {
                $path = rtrim(Config::get('app.dir_document') . $path, '/');

                // If path is just a file delete it
                if (is_file($path)) {
                    unlink($path);
                    // $json['path'] = $path;

                    // If path is a directory beging deleting each file and sub folder
                } elseif (is_dir($path)) {
                    $files = array();

                    // Make path into an array
                    $path = array($path);

                    // While the path array is still populated keep looping through
                    while (count($path) != 0) {
                        $next = array_shift($path);

                        foreach (glob($next) as $file) {
                            // If directory add to path array
                            if (is_dir($file)) {
                                $path[] = $file . '/*';
                            }

                            // Add the file to the files to be deleted array
                            $files[] = $file;
                        }
                    }

                    // Reverse sort the file array
                    rsort($files);

                    foreach ($files as $file) {
                        // If file just delete
                        if (is_file($file)) {
                            // $json['debug'] = $file;
                            unlink($file);

                            // If directory use the remove directory function
                        } elseif (is_dir($file)) {
                            rmdir($file);
                        }
                    }
                }
            }

            $json['success'] = 'text_delete_success';
        }

        return $json;
    }
}
