<?php

namespace App\Http\Controllers\Backoffice;

use App\ExchangeCategory;
use App\ExchangeCategoryDescription;
use App\Http\Requests\Backoffice\ExchangeCategoryRequest;
use App\Image;
use Config;
use Illuminate\Http\Request;

class ExchangeCategoryController extends BackofficeController
{

    public function __construct()
    {
        BackofficeController::__construct();

        //initial data
        $this->data_common += ['exchange_program_page_active' => 'active'];
    }

    public function index(Request $request)
    {
        $request->user()->authorizeRoles('exchange_category/access');

        $find = $request->get('find');
        $this->data_common += ['find' => $find];

        $items = ExchangeCategoryDescription::with('exchangeCategory')
            ->language($this->language->id);

        if ($find) {
            $items = $items->search($find);
        }

        $items = $items->order()->paginate($this->per_page);
        // dd($items);

        // Map image fields
        $items->setCollection(
            $items->getCollection()
                ->map(function ($item, $key) {

                    if ($item->image) {
                        $item->image = Image::resize($item->image, 40, 40); //สร้าง cache image ปรับขนาดได้ตามต้องการที่นี่ ไฟล์จะเก็บไว้ที่ public/storage/image/cache
                    } else {
                        $item->image = Image::resize('no_image.png', 40, 40);
                    }

                    return $item;
                })
        );

        $this->data_common += ['items' => $items];

        $this->data_common += ['breadcrumb' => [
            trans('backoffice/exchange-categories.text_exchange_category_lists') => ['url' => '', 'active' => 'active'],
        ]];

        return $this->view('backoffice.exchange-categories.list');
    }

    public function create(Request $request)
    {
        $request->user()->authorizeRoles('exchange_category/modify');

        $this->data_common += [
            'breadcrumb' => [
                trans('backoffice/exchange-categories.text_exchange_category_lists') => ['url' => Config::get('url.backoffice.exchange_categories'), 'active' => ''],
                trans('backoffice/exchange-categories.text_create_exchange_category') => ['', 'active' => 'active'],
            ],
        ];

        return $this->view('backoffice.exchange-categories.create');
    }

    public function store(Request $req, ExchangeCategoryRequest $request)
    {
        $req->user()->authorizeRoles('exchange_category/modify');

        // dd($request->all());
        // dd($request->article_category_descriptions);

        // บันทึกเข้าตารางหลัก
        $category = new ExchangeCategory();
        $category->image = $request->image;
        $category->sort_order = (int) $request->sort_order;
        $category->status = $request->status;
        $category->save();

        $exchange_category_id = $category->id;
        unset($category);

        // บันทึกเข้าตารางภาษา
        foreach ($request->exchange_category_descriptions as $language_id => $value) {
            if($value['name'] != '')
            {
            $category_description = new ExchangeCategoryDescription();
            $category_description->exchange_category_id = $exchange_category_id;
            $category_description->language_id = $language_id;
            $category_description->name = $value['name'];
            $category_description->description = $value['description'];

            if(trim($value['meta_title']) != ''){
                $category_description->meta_title   = $value['meta_title'];
            }else{
                $category_description->meta_title   = $value['name'];
            }

            // $category_description->meta_title = $value['meta_title'];
            $category_description->meta_description = $value['meta_description'];
            $category_description->meta_keyword = $value['meta_keyword'];
            $category_description->save();

            unset($category_description); // clear memory
        }
    }

        return redirect(Config::get('url.backoffice.exchange_categories'))
            ->with('success', trans('backoffice/common.text_save_successful'));
    }

    public function show($id)
    {
        //
    }

    public function edit($id, Request $req)
    {
        $req->user()->authorizeRoles('exchange_category/modify');

        $item = ExchangeCategory::with('exchangeCategoryDescriptions')->findOrFail($id);
        // dd($item);

        //Prepare image field for hidden input (สำหรับเก็บลงฐานข้อมูล)
        if ($item->image) {
            $item->image = $item->image;
        } else {
            $item->image = '';
        }

        //Prepare thumb field for View (สำหรับแสดง)
        if ($item->image) {
            $item->thumb = Image::resize($item->image, 100, 100);
        } else {
            $item->thumb = Image::resize('no_image.png', 100, 100);
        }

        // เตรียมข้อมูลแต่ละภาษาสำหรับจะไปแสดงใน View
        $description = []; //สร้างฟิลด์ใหม่เพื่อแทรกเข้าในตัวแปร $item สำหรับเก็บข้อมูลแต่ละภาษา โดยใช้ array index เป็น language_id
        foreach ($item->exchangeCategoryDescriptions as $dest) {
            $description[$dest->language_id] = $dest;
        }
        $item->description = $description;
        // dd($item);

        $data = [
            'item' => $item,
            'breadcrumb' => [
                trans('backoffice/exchange-categories.text_exchange_category_lists') => ['url' => Config::get('url.backoffice.exchange_categories'), 'active' => ''],
                trans('backoffice/exchange-categories.text_edit_exchange_category') => ['', 'active' => 'active'],
            ],
        ];

        $this->data_common += $data;

        return $this->view('backoffice.exchange-categories.edit');
    }

    public function update(Request $req, ExchangeCategoryRequest $request, $exchange_category_id)
    {
        $req->user()->authorizeRoles('exchange_category/modify');

        // dd($request->all());

        // บันทึกเข้าตารางหลัก
        $item = ExchangeCategory::findOrFail($exchange_category_id);

        $item->image = $request->image;
        $item->sort_order = (int) $request->sort_order;
        $item->status = $request->status;
        $item->save();

        unset($item);

        // บันทึกเข้าตารางภาษา
        // ลบของเดิมในตาราง article_category_descriptions ก่อน ตาม article_category_id
       ExchangeCategoryDescription::where('exchange_category_id', $exchange_category_id)->delete();

        //แทรกข้อมูลภาาษาใหม่
        foreach ($request->exchange_category_descriptions as $language_id => $value) {
            if($value['name'] != '')
            {
            $category_description = new ExchangeCategoryDescription();
            $category_description->exchange_category_id = $exchange_category_id;
            $category_description->language_id = $language_id;
            $category_description->name = $value['name'];
            $category_description->description = $value['description'];
            
            if(trim($value['meta_title']) != ''){
                $category_description->meta_title   = $value['meta_title'];
            }else{
                $category_description->meta_title   = $value['name'];
            }

            $category_description->meta_description = $value['meta_description'];
            $category_description->meta_keyword = $value['meta_keyword'];
            $category_description->save();

            unset($category_description); // clear memory
        }
    }

        return redirect(Config::get('url.backoffice.exchange_categories'))
            ->with('success', trans('backoffice/common.text_save_successful'));
    }

    public function destroy(Request $req, $exchange_category_id)
    {
        $req->user()->authorizeRoles('exchange_category/modify');

        $item = ExchangeCategory::findOrFail($exchange_category_id);

        $item->delete();

        return redirect(Config::get('url.backoffice.exchange_categories'))
            ->with('success', trans('backoffice/common.text_delete_successful'));
    }

}
