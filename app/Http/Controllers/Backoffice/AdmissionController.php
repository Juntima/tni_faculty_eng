<?php
/* 
    Jan 16,2019
    developed by : Metha<fakestore@gmail.com>
*/

namespace App\Http\Controllers\Backoffice;

use Illuminate\Http\Request;
use App\Admission;
use App\AdmissionDescription;
use App\Http\Requests\Backoffice\AdmissionRequest;
use Config;
use Illuminate\Support\Facades\Auth;
use App\Image;
use App\User;

class AdmissionController extends BackofficeController
{
    //

    public function __construct()
    {
        BackofficeController::__construct();

        //initial data
        $this->data_common += [
            'admission_page_active' => 'active',
        ];
    }

    //_List page
    public function index(Request $request)
    {
        //_check role
        $request->user()->authorizeRoles('admission/access');

        $find  = $request->get('find');
        $items = AdmissionDescription::with('admission')->language($this->language->id);

        if ($find) {
            $items = $items->searchKeyword($find);
        }
        //dd($items->toSql());
        

        $items = $items->order()->paginate($this->per_page);

        $items->setCollection(
            $items->getCollection()
                ->map(function ($item, $key) {

                    if ($item->image && !preg_match('/^http:\/\//', $item->image) && !preg_match('/^https:\/\//', $item->image))  { //ต้องมีข้อมูล และต้องไม่ขึ้นต้นด้วย http://
                        $item->image = Image::resize($item->image, 40, 40);//สร้าง cache image ปรับขนาดได้ตามต้องการที่นี่ ไฟล์จะเก็บไว้ที่ public/storage/image/cache
                        //$item->image_highlight = Image::resize($item->image_highlight, 40, 40); 
                    } else {
                        $item->image = Image::resize('no_image.png', 40, 40);
                        //$item->image_highlight = Image::resize('no_image.png', 40, 40);
                    }

                    return $item;
                })
        );
        //_get ชื่อ user ที่สร้างและแก้ไขข่าว
        if(count($items)){
            foreach($items as $value)
            {
                $username[] = [
                    'created_by'=>User::find($value->created_by,['name']),
                    'updated_by'=>User::find($value->updated_by,['name'])
                ];
            }
        }else{
            $username = array();
        }

        $this->data_common += [
            'items' => $items,
            'find' => $find,
            'breadcrumb' => [
                trans('backoffice/admissions.text_admission_lists') => ['url' => '', 'active' => 'active'] //_parent menu/current menu
            ],
            'username'=>$username
        ];
        //dd($items);
        //exit;
        return $this->view('backoffice.admissions.list');
    }

    //_Create page
    public function create(Request $request)
    {
        //_check role
        $request->user()->authorizeRoles('admission/access');
        
        $this->data_common += [
            'breadcrumb' => [
                trans('backoffice/admissions.text_admission_lists') => ['url' => Config::get('url.backoffice.admissions'), 'active' => ''],
                trans('backoffice/admissions.text_create_admission') => ['', 'active' => 'active'],
            ],
        ];

        return $this->view('backoffice.admissions.create');
    }

    //_Insert
    public function store(Request $req ,AdmissionRequest $request)//EventRequest $request
    {
        $req->user()->authorizeRoles('admission/modify');

        // บันทึกเข้าตารางหลัก Admissions
        $item                  = new admission();
        $item->image           = $request->image;
        $item->publish_start   = $request->publish_start;
        $item->publish_stop    = $request->publish_stop;
        $item->status          = $request->status;
        $item->created_by      = Auth::id();
        $item->save();

        $admission_id = $item->id;

        // บันทึกเข้าตาราง event descriptions
        foreach ($request->admission_descriptions as $language_id => $value) 
        {
            //_language_id = 1 mean Thai , บังคับกรอกเฉพาะภาษาไทย
            if($value['name'] != '')
            {
                $item_description                   = new AdmissionDescription();
                $item_description->admission_id     = $admission_id;
                $item_description->language_id      = $language_id;
                $item_description->name             = $value['name'];              //ชื่อ
                $item_description->description      = $value['description'];       //รายละเอียด
                $item_description->link             = $value['link'];              //link
                //$item_description->meta_title = $value['meta_title'];
                if(trim($value['meta_title']) != ''){
                    $item_description->meta_title   = $value['meta_title'];
                }else{
                    $item_description->meta_title   = $value['name'];
                }
                $item_description->meta_description = $value['meta_description'];
                $item_description->meta_keyword     = $value['meta_keyword'];
                $item_description->tag              = $value['tag'];
                $item_description->save();

                unset($item_description); //_clear memory
            }
        }

        return redirect(Config::get('url.backoffice.admissions'))
            ->with('success', trans('backoffice/common.text_save_successful'));

    }

    //_Edit page
    public function edit($id , Request $request)
    {
        //_check role : can modify
        $request->user()->authorizeRoles('admission/modify');

        //_query
        $item = Admission::with('admissionDescriptions') //function in Admission model 
            ->findOrFail($id);
        
        if ($item->image) {
            $item->image = $item->image;
        } else {
            $item->image = '';
        }

        //_Prepare thumb field for View (สำหรับแสดง)
        if ($item->image) {
            $item->thumb = Image::resize($item->image, 100, 100);
        } else {
            $item->thumb = Image::resize('no_image.png', 100, 100);
        }

        //_สร้างฟิลด์ใหม่เพื่อแทรกเข้าในตัวแปร $item สำหรับเก็บข้อมูลแต่ละภาษา โดยใช้ array index เป็น language_id
        $description = []; 
        foreach ($item->admissionDescriptions as $dest) {
            $description[$dest->language_id] = $dest;
        }
        $item->description = $description;

        //_
        $this->data_common += [
            'item'       => $item ,
            'breadcrumb' => [
                trans('backoffice/admissions.text_admission_lists') => ['url' => Config::get('url.backoffice.admissions'), 'active' => ''],
                trans('backoffice/admissions.text_edit_admission') => ['', 'active' => 'active'],
            ],
        ];

        return $this->view('backoffice.admissions.edit');//_call edit page
    }


    //_Update
    public function update(Request $req, AdmissionRequest $request,$admission_id)
    {
        $req->user()->authorizeRoles('admission/modify');

        //_บันทึกเข้าตารางหลัก
        $item                = Admission::findOrFail($admission_id);
        $item->image         = $request->image;
        $item->publish_start = $request->publish_start;
        $item->publish_stop  = $request->publish_stop;
        $item->status        = $request->status;
        $item->updated_by    = Auth::id();
        $item->save();

        //_delete before insert
        AdmissionDescription::where('admission_id', $admission_id)->delete();

        //_บันทึกเข้าตาราง event descriptions
        foreach ($request->admission_descriptions as $language_id => $value)
        {
            //_language_id = 1 mean Thai , บังคับกรอกเฉพาะภาษาไทย
            if($value['name'] != '')
            {
                $item_description                   = new  AdmissionDescription();
                $item_description->admission_id     = $admission_id;
                $item_description->language_id      = $language_id;
                $item_description->name             = $value['name'];               //ชื่อ
                $item_description->description      = $value['description'];        //รายละเอียด
                $item_description->link             = $value['link'];               //link
                //$item_description->meta_title = $value['meta_title'];
                if(trim($value['meta_title']) != ''){
                    $item_description->meta_title   = $value['meta_title'];
                }else{
                    $item_description->meta_title   = $value['name'];
                }
                $item_description->meta_description = $value['meta_description'];
                $item_description->meta_keyword     = $value['meta_keyword'];
                $item_description->tag              = $value['tag'];
                $item_description->save();
            }    
            unset($item_description); // clear memory
        }

        return redirect(Config::get('url.backoffice.admissions'))
            ->with('success', trans('backoffice/common.text_save_successful'));

    }

    //_Delete
    public function destroy(Request $req, $id)
    {
        $req->user()->authorizeRoles('admission/modify');

        $item = Admission::findOrFail($id);

        $item->delete();

        return redirect(Config::get('url.backoffice.admissions'))
            ->with('success', trans('backoffice/common.text_delete_successful'));
    }

    //_อัปเดทวันที่ปัจจุบันที่ updated_at
    public function touch(Request $request,$id)
    {
        Admission::findOrFail($id)->touch();

        return redirect(Config::get('url.backoffice.admissions') )
            ->with('success', trans('backoffice/common.text_save_successful'));
    }
}

