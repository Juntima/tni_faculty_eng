<?php

namespace App\Http\Controllers\Backoffice;

use Illuminate\Http\Request;
use App\Image;
use Config;
use App\Banner;
use App\Http\Requests\Backoffice\BannerRequest;
use Illuminate\Support\Facades\Auth;
use App\User;


class BannerController extends BackofficeController
{

    public function __construct()
    {
        BackofficeController::__construct();

        //initial data
        $this->data_common += ['banner_page_active' => 'active'];
    }

    public function index(Request $request)
    {
        $request->user()->authorizeRoles('banner/access');

        $find = $request->get('find');
        $this->data_common += ['find' => $find];

        $items = Banner::where([]);

        if ($find) {
            $items = $items->search($find);
        }

        $items = $items->order()->paginate($this->per_page);
        // dd($items);

        // Map image fields
        $items->setCollection(
            $items->getCollection()
                ->map(function ($item, $key) {

                    if ($item->image) {
                        $item->image = Image::resize($item->image, 40, 40); //สร้าง cache image ปรับขนาดได้ตามต้องการที่นี่ ไฟล์จะเก็บไว้ที่ public/storage/image/cache
                    } else {
                        $item->image = Image::resize('no_image.png', 40, 40);
                    }

                    return $item;
                })
        );
        //_get ชื่อ user ที่สร้างและแก้ไขข่าว
        if(count($items)){
            foreach($items as $value)
            {
                $username[] = [
                    'created_by'=>User::find($value->created_by,['name']),
                    'updated_by'=>User::find($value->updated_by,['name'])
                ];
            }
        }else{
            $username = array();
        }

        $this->data_common += ['items' => $items];

        $this->data_common += ['breadcrumb' => [
            trans('backoffice/banners.text_banner_lists') => ['url' => '', 'active' => 'active'],
        ] ,
        'username'=>$username
    ];
       

        return $this->view('backoffice.banners.list');
    }

    public function create(Request $request)
    {
        $request->user()->authorizeRoles('banner/modify');

        $this->data_common += [
            'breadcrumb' => [
                trans('backoffice/banners.text_banner_lists') => ['url' => Config::get('url.backoffice.banners'), 'active' => ''],
                trans('backoffice/banners.text_create_banner') => ['', 'active' => 'active'],
            ],
        ];

        return $this->view('backoffice.banners.create');
    }

    public function store(Request $req, BannerRequest $request)
    {
        $req->user()->authorizeRoles('banner/modify');

        $item = new Banner();
        $item->name = $request->name;
        $item->image = $request->image;
        $item->background_color = $request->background_color;
        $item->publish_start = $request->publish_start;
        $item->publish_stop = $request->publish_stop;
        $item->sort_order = (int) $request->sort_order;
        $item->status = $request->status;
        $item->created_by = Auth::id();
        $item->save();

        return redirect(Config::get('url.backoffice.banners'))
            ->with('success', trans('backoffice/common.text_save_successful'));
    }

    public function show($id)
    {
        //
    }

    public function edit($id, Request $req)
    {
        $req->user()->authorizeRoles('banner/modify');

        $item = Banner::findOrFail($id);
        // dd($item);

        //Prepare image field for hidden input (สำหรับเก็บลงฐานข้อมูล)
        if ($item->image) {
            $item->image = $item->image;
        } else {
            $item->image = '';
        }

        //Prepare thumb field for View (สำหรับแสดง)
        if ($item->image) {
            $item->thumb = Image::resize($item->image, 100, 100);
        } else {
            $item->thumb = Image::resize('no_image.png', 100, 100);
        }

        $data = [
            'item' => $item,
            'breadcrumb' => [
                trans('backoffice/banners.text_banner_lists') => ['url' => Config::get('url.backoffice.banners'), 'active' => ''],
                trans('backoffice/banners.text_edit_banner') => ['', 'active' => 'active'],
            ],
        ];

        $this->data_common += $data;

        return $this->view('backoffice.banners.edit');
    }

    public function update(Request $req, BannerRequest $request, $id)
    {
        $req->user()->authorizeRoles('banner/modify');

        $item = Banner::findOrFail($id);

        $item->name = $request->name;
        $item->image = $request->image;
        $item->background_color = $request->background_color;
        $item->publish_start = $request->publish_start;
        $item->publish_stop = $request->publish_stop;
        $item->sort_order = (int) $request->sort_order;
        $item->status = $request->status;
        $item->updated_by = Auth::id();
        $item->save();

        return redirect(Config::get('url.backoffice.banners'))
            ->with('success', trans('backoffice/common.text_save_successful'));
    }

    public function destroy(Request $req, $id)
    {
        $req->user()->authorizeRoles('banner/modify');

        $item = Banner::findOrFail($id);

        $item->delete();

        return redirect(Config::get('url.backoffice.banners'))
            ->with('success', trans('backoffice/common.text_delete_successful'));
    }
}
