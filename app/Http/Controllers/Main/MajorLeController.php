<?php

namespace App\Http\Controllers\Main;

use Illuminate\Http\Request;
use config;

class MajorLeController extends MainController
{
    public function __construct()
    {
        MainController::__construct();
    }
    
    public function major_le()
    {
        $this->data_common += $this->load_struct_detail();
        $this->data_common += $this->load_name_course();
        $this->data_common += $this->load_subject_y1s1();
        $this->data_common += $this->load_subject_y1s2();
        $this->data_common += $this->load_subject_y2s1();
        $this->data_common += $this->load_subject_y2s2();
        $this->data_common += $this->load_subject_y3s1();
        $this->data_common += $this->load_subject_y3s2();
        $this->data_common += $this->load_subject_y4s1();
        $this->data_common += $this->load_subject_y4s2();
        $this->data_common += [
            'breadcrumb' => [
                trans('main/home.text_home') => ['url' => Config::get('url.main.home'), 'active' => ''],
                trans('main/major.text_major') => ['url' => Config::get('url.main.major'), 'active' => ''],
                trans('main/major_le.text_name') => ['', 'active' => 'active'],
            ],
        ];
        return $this->view('main.major.major_le');
    }

    
        private function load_name_course() // ชื่อปริญญาและสาขาวิชา 
        {
            
            for($i=1; $i<5; $i++)
            {
                $arr_temp = [
                    'id' => $i, 
                    'topic' => trans("main/major_le.text_topic$i"), 
                    'name' => trans("main/major_le.text_name$i"), 
                ];

                $data[] = $arr_temp;
            }
            return ['name_course' => $data];
        
        }
        
        private function load_struct_detail() // โครงสร้างหลักสูตร 
        {
            
            for($i=1; $i<12; $i++)
            {
                $arr_temp = [
                    'id' => $i, 
                    'name' => trans("main/major_le.text_couse_name$i"), 
                    'detail' => trans("main/major_le.text_couse_unit$i"),
                ];

                $data[] = $arr_temp;
            }
            return ['struct_detail' => $data];
        
        }

        private function load_subject_y1s1()  // แผนการศึกษา ปี1 เทอม 1 
        {
            for($i=1; $i<9; $i++)
            {
                $arr_temp = [
                    'id' => $i, 
                    'sub_id' => trans("main/major_le.sub_y1s1_id$i"), 
                    'sub_name' => trans("main/major_le.sub_y1s1_name$i"),   
                    'sub_unit' => trans("main/major_le.sub_y1s1_unit$i"),             
                ];

                $data[] = $arr_temp;
            }
            return ['subject_y1s1' => $data];

        }

        private function load_subject_y1s2()  // แผนการศึกษา ปี1 เทอม 2 
        {
            for($i=1; $i<9; $i++)
            {
                $arr_temp = [
                    'id' => $i, 
                    'sub_id' => trans("main/major_le.sub_y1s2_id$i"), 
                    'sub_name' => trans("main/major_le.sub_y1s2_name$i"),   
                    'sub_unit' => trans("main/major_le.sub_y1s2_unit$i"),             
                ];

                $data[] = $arr_temp;
            }
            return ['subject_y1s2' => $data];

        }

        private function load_subject_y2s1()  // แผนการศึกษา ปี2 เทอม 1
        {
            for($i=1; $i<9; $i++)
            {
                $arr_temp = [
                    'id' => $i, 
                    'sub_id' => trans("main/major_le.sub_y2s1_id$i"), 
                    'sub_name' => trans("main/major_le.sub_y2s1_name$i"),   
                    'sub_unit' => trans("main/major_le.sub_y2s1_unit$i"),             
                ];

                $data[] = $arr_temp;
            }
            return ['subject_y2s1' => $data];

        }


        private function load_subject_y2s2()  // แผนการศึกษา ปี2 เทอม 2
        {
            for($i=1; $i<9; $i++)
            {
                $arr_temp = [
                    'id' => $i, 
                    'sub_id' => trans("main/major_le.sub_y2s2_id$i"), 
                    'sub_name' => trans("main/major_le.sub_y2s2_name$i"),   
                    'sub_unit' => trans("main/major_le.sub_y2s2_unit$i"),             
                ];

                $data[] = $arr_temp;
            }
            return ['subject_y2s2' => $data];

        }

        private function load_subject_y3s1()  // แผนการศึกษา ปี3 เทอม 1
        {
            for($i=1; $i<8; $i++)
            {
                $arr_temp = [
                    'id' => $i, 
                    'sub_id' => trans("main/major_le.sub_y3s1_id$i"), 
                    'sub_name' => trans("main/major_le.sub_y3s1_name$i"),   
                    'sub_unit' => trans("main/major_le.sub_y3s1_unit$i"),             
                ];

                $data[] = $arr_temp;
            }
            return ['subject_y3s1' => $data];

        }


        private function load_subject_y3s2()  // แผนการศึกษา ปี3 เทอม 2
        {
            for($i=1; $i<9; $i++)
            {
                $arr_temp = [
                    'id' => $i, 
                    'sub_id' => trans("main/major_le.sub_y3s2_id$i"), 
                    'sub_name' => trans("main/major_le.sub_y3s2_name$i"),   
                    'sub_unit' => trans("main/major_le.sub_y3s2_unit$i"),             
                ];

                $data[] = $arr_temp;
            }
            return ['subject_y3s2' => $data];

        }

        private function load_subject_y4s1()  // แผนการศึกษา ปี4 เทอม 1
        {
            for($i=1; $i<2; $i++)
            {
                $arr_temp = [
                    'id' => $i, 
                    'sub_id' => trans("main/major_le.sub_y4s1_id$i"), 
                    'sub_name' => trans("main/major_le.sub_y4s1_name$i"),   
                    'sub_unit' => trans("main/major_le.sub_y4s1_unit$i"),             
                ];

                $data[] = $arr_temp;
            }
            return ['subject_y4s1' => $data];

        }


        private function load_subject_y4s2()  // แผนการศึกษา ปี4 เทอม 2
        {
            for($i=1; $i<6; $i++)
            {
                $arr_temp = [
                    'id' => $i, 
                    'sub_id' => trans("main/major_le.sub_y4s2_id$i"), 
                    'sub_name' => trans("main/major_le.sub_y4s2_name$i"),   
                    'sub_unit' => trans("main/major_le.sub_y4s2_unit$i"),             
                ];

                $data[] = $arr_temp;
            }
            return ['subject_y4s2' => $data];

        }

   

    
}


