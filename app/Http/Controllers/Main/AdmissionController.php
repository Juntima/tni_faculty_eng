<?php

namespace App\Http\Controllers\Main;

use Illuminate\Http\Request;
use App\AdmissionDescription;
use App\Admission;
use Carbon\Carbon;
use Config;

class AdmissionController extends MainController
{
    public function __construct()
    {
        MainController::__construct();
    }
    public function index()
    {
        $this->data_common += $this->load_admission();

        $this->data_common += [
            'breadcrumb' => [
                trans('main/home.text_home') => ['url' => Config::get('url.main.home'), 'active' => ''],
                trans('main/admission.text_admission') => ['', 'active' => 'active'],
            ],
        ];

        return $this->view('main.admission');
    }

    private function load_admission()
    {

        // ดึงรายการสมัครเรียน 5 รายการ 
        /* $admissions = AdmissionDescription::with('admission')
            ->language($this->language->id)
            ->paginate(5); */

        $admissions = AdmissionDescription::with('admission')
                        ->language($this->language->id)
                        ->publishWeb()//_เช็ควันเริ่มกับวันสิ้นสุด
                        ->get(); 
                        //->paginate();

        // ต้องเช็คอีก model นึง ถึงจะมี public start stop 
        return [
            'admissions' => $admissions
        ];

    }

    

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
