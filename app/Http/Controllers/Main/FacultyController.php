<?php

namespace App\Http\Controllers\Main;

use Illuminate\Http\Request;
//use App\Http\Controllers\Controller;
use Config;

class FacultyController extends MainController
{
    public function __construct()
    {
        MainController::__construct();
    }
    public function index()
    {
       
        $this->data_common += [
            'breadcrumb' => [
                trans('main/home.text_home') => ['url' => Config::get('url.main.home'), 'active' => ''],        
                trans('main/major.text_major') => ['', 'active' => 'active'],
            ],
        ];
        return $this->view('main.major.index');
    }

    
}