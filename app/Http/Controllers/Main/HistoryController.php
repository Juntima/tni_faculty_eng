<?php

namespace App\Http\Controllers\Main;

use Illuminate\Http\Request;
use App\Banner;
use App\Image;
use Config;
//use App\Http\Controllers\Controller;

class HistoryController extends MainController
{
    public function __construct()
    {
        MainController::__construct();
    }
    public function index()
    {    // home hero banner mockup
       
       
        $this->data_common += [
            //'item'       => $item ,
            'breadcrumb' => [
                trans('main/home.text_home') => ['url' => Config::get('url.main.home'), 'active' => ''],
                trans('main/history.text_intro') => ['', 'active' => 'active'],
            ],
        ];
       
        //dd( $this->data_common);
        return $this->view('main.abouts.message_eng_dean');
    }

    public function mission()
    {    // home hero banner mockup
       
       
        $this->data_common += [
            //'item'       => $item ,
            'breadcrumb' => [
                trans('main/home.text_home') => ['url' => Config::get('url.main.home'), 'active' => ''],
                trans('main/history.text_mission') => ['', 'active' => 'active'],
            ],
        ];
       
        //dd( $this->data_common);
        return $this->view('main.abouts.mission_and_vision');
    }

    public function structure()
    {    // home hero banner mockup
       
       
        $this->data_common += [
            //'item'       => $item ,
            'breadcrumb' => [
                trans('main/home.text_home') => ['url' => Config::get('url.main.home'), 'active' => ''],
                trans('main/history.text_manage_struct') => ['', 'active' => 'active'],
            ],
        ];
       
        //dd( $this->data_common);
        return $this->view('main.abouts.management_structure');
    }

    

    public function eng_lab_research_room()
    {       
        
        $this->data_common += [
            'breadcrumb' => [
                trans('main/home.text_home') => ['url' => Config::get('url.main.home'), 'active' => ''],
                trans('main/history.text_eng_lab_research_room') => ['', 'active' => 'active'],
            ],
        ];
    
        return $this->view('main.eng_lab_research_room');

    } 


    // public function eng_research_room()
    // {       
        
    //     $this->data_common += [
    //         'breadcrumb' => [
    //             trans('main/home.text_home') => ['url' => Config::get('url.main.home'), 'active' => ''],
    //             trans('main/history.text_research_room') => ['', 'active' => 'active'],
    //         ],
    //     ];
    
    //     return $this->view('main.eng_lab_research_room');

    // } 


    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
