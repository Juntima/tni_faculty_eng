<?php

namespace App\Http\Controllers\Main;

use App\Finance;
use App\FinanceDescription;
use Config;
use App\Image;
use App\Language;
use Illuminate\Http\Request;

class FinanceController extends MainController
{
    public function __construct()
    {
        MainController::__construct();
    }
    public function index()
    {
        $this->data_common += $this->load_finance();

        $this->data_common += [
            'breadcrumb' => [
                trans('main/home.text_home') => ['url' => Config::get('url.main.home'), 'active' => ''],
                trans('main/department.text_department') => ['url' => Config::get('url.main.department'), 'active' => ''],
                trans('main/department.text_finance') => ['', 'active' => 'active'],
            ],
        ];
        
        return $this->view('main.finances.index');
    }

    private function load_finance()
    {
        // ดึงข่าวล่าสุด 5 อันดับแรก
      
        $finances =FinanceDescription::with('finance')
                ->language($this->language->id)
                ->publishWeb()
                ->paginate(12); ///แสดงหน้าละ 12
        
            $finances->setCollection(
                $finances->getCollection()
                        ->map(function ($item, $key) {
    
                        if ($item->image) {
                            $item->image = Image::resize($item->image, 235, 165); //สร้าง cache image ปรับขนาดได้ตามต้องการที่นี่ ไฟล์จะเก็บไว้ที่ public/storage/image/cache ถ้ามีไฟล์อยู่โปรแกรมจะไม่สร้างให้ใหม่
                        } else {
                            $item->image = Image::resize('no_image.png', 235, 165);
                        }
    
                        return $item;
                    })
            );
    
            
        return [
            'finances' => $finances,
            //'first_event_images_blank' => Image::resize('no_image.png', 235, 165),
            //'first_article_images' => $first_article_images,
            //'first_event_images_blank' => Image::resize('no_image.png', 975, 475),
            
        ];
        
    }

    public function detail($id , Request $request)
    {
        
        //_increst field:Viewd number when click to see news detail
        //Event::findOrFail($id)->increment('viewed');
        $item_viewed = Finance::findOrFail($id);
        $item_viewed->timestamps = false; //_disable update field:updated_at
        $item_viewed->increment('viewed');
        unset($item_viewed); //_clear memory

        $item = Finance::with('financeDescriptions') //function in event model 
                    ->findOrFail($id);   
        /* if ($item->image) {
            $item->image = $item->image;
        } else {
            $item->image = '';
        } */
        if(is_null($item->image)){
            $item->image = '';
        }

        //_Prepare thumb field for View (สำหรับแสดง)
        if ($item->image) {
            $item->thumb = Image::resize($item->image, 235, 165);
        } else {
            //$item->thumb = Image::resize('no_image.png', 235, 165);
            $item->thumb = '';
        }

        $description = []; // สร้างฟิลด์ใหม่เพื่อแทรกเข้าในตัวแปร $item สำหรับเก็บข้อมูลแต่ละภาษา โดยใช้ array index เป็น language_id
        foreach ($item->financeDescriptions as $dest) {
            $description[$dest->language_id] = $dest;
        }
        $item->description = $description;

        $this->data_common += [
            'item' => $item,
            'breadcrumb' => [
                trans('main/home.text_home') => ['url' => Config::get('url.main.home'), 'active' => ''],
                trans('main/department.text_department') => ['url' => Config::get('url.main.department'), 'active' => ''],
                trans('main/department.text_finance') => ['url' => Config::get('url.main.finances'), 'active' => ''],
                $item->description[$this->language->id]->name => ['', 'active' => 'active'],
            ],
        ];
        
       
        return $this->view('main.finances.detail');
    }

    /* private function update_viewd($id)
    {
        //_increst Viewd number when click to see news detail
        $item = Event::findOrFail($id)
                    ->increment('viewed');
        //$item->save();
    } */

    /* private function load_detail_event_update($id)
    {   

        $event_detail = Event::with('eventDescriptions') //function in event model 
                ->findOrFail($id);     
        
            $event_detail->setCollection(
                $event_detail->getCollection()
                        ->map(function ($item, $key) {
    
                        if ($item->image) {
                            $item->image = Image::resize($item->image, 235, 165); //สร้าง cache image ปรับขนาดได้ตามต้องการที่นี่ ไฟล์จะเก็บไว้ที่ public/storage/image/cache ถ้ามีไฟล์อยู่โปรแกรมจะไม่สร้างให้ใหม่
                        } else {
                            $item->image = Image::resize('no_image.png', 235, 165);
                        }
    
                        return $item;
                    })
            );
    
            
        return [
            'events' => $events,
            'first_event_images_blank' => Image::resize('no_image.png', 235, 165),
            
        ];
        
    } */



    
}
