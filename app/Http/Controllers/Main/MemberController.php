<?php
namespace App\Http\Controllers\main;
use Illuminate\Http\Request;
use App\Banner;
use App\Image;
use Config;

class MemberController extends MainController
{
    public function __construct()
    {
        MainController::__construct();
    }
    public function index()
    {    // home hero banner mockup
       
        $this->data_common += [
            //'item'       => $item ,

            'breadcrumb' => [
                trans('main/home.text_home') => ['url' => Config::get('url.main.home'), 'active' => ''],
                trans('main/support.text_intro') => ['', 'active' => 'active'],
            ],
        ];
       
        //dd( $this->data_common);
        return $this->view('main.support');
    }


   
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
