<?php
/**
 * Nov 9, 2018, 9:16 AM
 * Developed by Korn <kornthebkk@gmail.com>
 */

namespace App\Http\Requests\Backoffice;

use Illuminate\Foundation\Http\FormRequest;


class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // return [];
        return [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'กรุณาใส่ชื่อ',
            'email.required' => 'กรุณาใส่อีเมล์',
            'email.email' => 'กรุณาใส่อีเมล์ให้ถูกต้อง',
            'email.unique' => 'มีผู้ใช้อีเมล์นี้แล้ว กรุณาเปลี่ยนอีเมล์ใหม่',
            'password.required' => 'กรุณาใส่หัสผ่าน',
            'password.min' => 'กรุณาใส่รหัสผ่านขั้นต่ำ 6 ตัวอักษร',
            'password.confirmed' => 'ยืนยันรหัสผ่านไม่ตรงกัน'
        ];
    }
}
