<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExchangeCategory extends Model
{
    private $table_description = 'exchange_category_descriptions'; //ตารางที่เก็บข้อมูลภาษา

    
    public function exchangeCategoryDescriptions()
    {
        return $this->hasMany(ExchangeCategoryDescription::class);
    }

    public function exchangePrograms()
    {
        return $this->belongsToMany(ExchangeProgram::class);
    }

}
