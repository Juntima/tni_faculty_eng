<?php
/**
 * Nov 9, 2018, 9:16 AM
 * Developed by Korn <kornthebkk@gmail.com>
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
    protected $table = 'role_user';   
}
