<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admission extends Model
{
    //_https://laravel.com/docs/5.4/eloquent

    protected $guarded           = ['id'];//_$guarded:contain the attributes that not want to be mass assignment
    private   $table_description = 'admission_descriptions';

    public function admissionDescriptions()
    {
        return $this->hasMany(AdmissionDescription::class);//_one-to-many or hasMany(App\AdmissionDescription)
    }

    public function scopeOrder($query)
    {
        return $this->orderBy('updated_at', 'DESC');
    }


    public function scopeLanguage($query, $language_id)
    {
        return $this->join($this->table_description, $this->table_description . '.admission_id', 'id')->where('language_id', $language_id);
    }
    
    /**
     * สำหรับดึงข้อมูลที่ publish ของ Front-End
     */
    public function scopePublishWeb($query)
    {
        return $query->where('status', 1)
            ->where('publish_start', '<=', Carbon::now())
            ->where(function ($query) {
                $query->where('publish_stop', '>=', Carbon::now())
                    ->orWhere('publish_stop', '=', null);
            });
    }
}
