@extends('main.layouts.app')

@section('htmlheader_title'){{ trans('main/home.text_htmltitle') }}@endsection

@section('htmlheader_description'){{ trans('main/common.htmlheader_description') }}@endsection

@section('htmlheader_keywords'){{ trans('main/common.htmlheader_keywords') }}@endsection


@section('content-header')
    <div id="header" class="nav-item">
        <!-- <div class="container">
            <div class="second-header">{{ trans('main/home.text_tni_channel') }}</div>
        </div> -->
        <div class="container">
            <div class="second-header">
                @if(@$breadcrumb && count($breadcrumb) > 0)
                <ul class="breadcrumb">
                    @if(@$breadcrumb)
                        @foreach(@$breadcrumb as $key => $value)
                            @if(@$value['active'] == 'active')
                                <li class="active">{{ $key }}</li>
                            @else
                                <li><a href="{{ url(@$value['url']) }}">{{ $key }}</a></li>
                            @endif
                        @endforeach
                    @endif
                </ul>
                @endif
            </div>

            <div class='poston'>
                <i class='fa fa-clock-o fa'></i> {{date('d-M-Y H:m:s', strtotime(@$item->publish_start)) }}
                    | 
                <i class="fa fa-eye"></i> {{ @$item->viewed }}
            </div>

        </div>
    </div><!-- End apply-now -->
@endsection

@section('main-content')
<div id="background-new-detail">
<div class="container ">
    <!--_SHOW Thumbnail image -->
    <!-- <div class='row'>
        <div class='col-lg-12' >
            @if(@$item->thumb)
                <div id="background-white">
                <img src="{{ $item->thumb }}" data-placeholder="{{ url('storage/image/cache/no_image-100x100.png') }}" />
                </div>
            @endif
        </div>
    </div> -->

    <!--_SHOW Detail -->
    <div class='row' >
        <div class='col-lg-12 detail_content' >
            <div id="background-white" >
                <div class="embed-responsive embed-responsive-16by9" id="tni_channel">
                    <?php echo @$item->description[$language->id]->video_link ; ?>
                </div> <br>
                {!! @$item->description[$language->id]->description  !!}
            </div>
        </div>
    </div>

    <div class='row'>
        <div class='col-lg-12'>
            <div id="social-share" >      
                <h5 style="color:#767676"><b>SHARE :</b>   {{-- icon share --}}
                    <span> &nbsp;
                        <a href="https://www.facebook.com/sharer.php?u={{ url(Config::get('url.main.cooperatives').'/'.$item->id.'/detail') }}" target='_blank'>
                            <i class="fa fa-facebook-square">     </i>
                        </a>   &nbsp;
                        <a href="https://twitter.com/share?url={{ url(Config::get('url.main.cooperatives').'/'.$item->id.'/detail') }}" target='_blank'>
                            <i class="fa fa-twitter-square">      </i>
                        </a>   &nbsp;
                    </span>    
                </h5>          
            </div> {{-- end social share --}}
        </div>
    </div>


</div>{{-- end containner --}}
<br>
</div>
@endsection


@section('style')
<link rel="stylesheet" href="{{ url('lib/swiper-4.4.0/css/swiper.min.css') }}">

@endsection

@section('script')

<script src="{{ url('lib/swiper-4.4.0/js/swiper.min.js') }}"></script>
 <!-- jQuery library -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

 <!-- Popper JS -->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

 <!-- Latest compiled JavaScript -->
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

 

@endsection

