@extends('main.layouts.app')

@section('htmlheader_title'){{ trans('main/home.text_htmltitle') }}@endsection

@section('htmlheader_description'){{ trans('main/common.htmlheader_description') }}@endsection

@section('htmlheader_keywords'){{ trans('main/common.htmlheader_keywords') }}@endsection



@section('content-header')

    <div id="header" class="nav-item">

            <!-- <div class="container">
                <div class="second-header">{{ trans('main/contact.text_contact') }}</div>
            </div> -->
            <div class="container">
                <div class="second-header">
                    @if(@$breadcrumb && count($breadcrumb) > 0)
                    <ul class="breadcrumb">
                        @if(@$breadcrumb)
                            @foreach(@$breadcrumb as $key => $value)
                                @if(@$value['active'] == 'active')
                                    <li class="active">{{ $key }}</li>
                                @else
                                    <li><a href="{{ url(@$value['url']) }}">{{ $key }}</a></li>
                                @endif
                            @endforeach
                        @endif
                    </ul>
                    @endif
                </div>
            </div>

    </div><!-- End apply-now -->
    <div id="home-course" class="container">

    </div>

    <div class="container">
            {{-- {{ url('image/catalog/mockup/mockup-banner-5.png') }} --}}
        <img src="{{ url('image/catalog/mockup/map.png') }}" class="img-fluid" style="width:100%;" />

        <div class="contact">
            <h3><b> {{ trans('main/contact.text_tni') }} </b></h3>
            <h4>
                   {{ trans('main/contact.text_address') }} <br>          
                   {{ trans('main/contact.text_phone') }} <br>
                    <a href="mailto:tniinfo@tni.ac.th"> {{ trans('main/contact.text_email') }} </a> <br>
                    <a href="https://www.tni.ac.th/" target="_blank"> {{ trans('main/contact.text_website') }} </a> <br>
            </h4>  
      
            <h3><b> {{ trans('main/contact.text_tni_en') }}  </b> </h3>
            <h4><?php echo trans('main/contact.text_map'); ?> </h4> 
        </div>

    </div>{{-- end containner --}}
        
@endsection


@section('main-content')
<br>
<div class="container">
    <div class="contact">
        <h3><b> {{ trans('main/contact.text_department_phonenumber') }} </b></h3>
        <table class="table table-striped ">

            <tbody style="text-indent:1.0em; font-size:1.5rem" id="table">

                <?php echo  trans('main/contact.text_tbody') ; ?>

            </tbody>

        </table>
    </div>
</div> {{-- end containner --}}

<br>
@endsection

@section('style')
<link rel="stylesheet" href="{{ url('lib/swiper-4.4.0/css/swiper.min.css') }}">


@endsection

@section('script')

<script src="{{ url('lib/swiper-4.4.0/js/swiper.min.js') }}"></script>
 <!-- jQuery library -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

 <!-- Popper JS -->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

 <!-- Latest compiled JavaScript -->
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

 <style>
        [id^='card'],[id^='accordion']{
          cursor:pointer;
        }
</style>



@endsection

