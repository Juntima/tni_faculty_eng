@extends('main.layouts.app')

@section('htmlheader_title'){{ trans('main/home.text_htmltitle') }}@endsection

@section('htmlheader_description'){{ trans('main/common.htmlheader_description') }}@endsection

@section('htmlheader_keywords'){{ trans('main/common.htmlheader_keywords') }}@endsection



@section('content-header')

    <div id="header" class="nav-item">

            <!-- <div class="container">
                <div class="second-header">{{ trans('main/history.text_facilities') }}</div>
            </div> -->
            <div class="container">
                <div class="second-header">
                    @if(@$breadcrumb && count($breadcrumb) > 0)
                    <ul class="breadcrumb">
                        @if(@$breadcrumb)
                            @foreach(@$breadcrumb as $key => $value)
                                @if(@$value['active'] == 'active')
                                    <li class="active">{{ $key }}</li>
                                @else
                                    <li><a href="{{ url(@$value['url']) }}">{{ $key }}</a></li>
                                @endif
                            @endforeach
                        @endif
                    </ul>
                    @endif
                </div>
            </div>

    </div><!-- End apply-now -->
    <div id="home-course" class="container">

    </div>

    
@endsection


@section('main-content')
<br>
<div class="container">
  <br>
    <h4> {{ trans('main/history.text_cgel_detail_room') }}</h4> <br>

    <div class="row" style="text-align:center;">
        <div class="col-12 col-md-12 col-lg-12">
                <img src="{{ url('img/history/labcgel/lab1.jpg') }}"  width="45%" style="margin:auto"/> &nbsp;
                <img src="{{ url('img/history/labcgel/lab2.jpg') }}"  width="45%" style="margin:auto"/> 
        </div> 
    </div>
    <br>
    <div class="row" style="text-align:center;">
        <div class="col-12 col-md-12 col-lg-12">
                <img src="{{ url('img/history/labcgel/lab3.jpg') }}"  width="45%" style="margin:auto"/> &nbsp;
                <img src="{{ url('img/history/labcgel/lab4.jpg') }}"  width="45%" style="margin:auto"/> 
        </div>
    </div> <br>
    <div class="row" style="text-align:center;">
        <div class="col-12 col-md-12 col-lg-12">
                
                <img src="{{ url('img/history/labcgel/lab5.jpg') }}"  width="50%" style="margin:auto"/> 
        </div>
    </div>
   
</div>{{-- end containner --}}
    

<br>
@endsection

@section('style')
<link rel="stylesheet" href="{{ url('lib/swiper-4.4.0/css/swiper.min.css') }}">
@endsection

@section('script')

<script src="{{ url('lib/swiper-4.4.0/js/swiper.min.js') }}"></script>
 <!-- jQuery library -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

 <!-- Popper JS -->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

 <!-- Latest compiled JavaScript -->
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

 {{-- <style>
        [id^='card'],[id^='accordion']{
          cursor:pointer;
        }
</style> --}}



@endsection

