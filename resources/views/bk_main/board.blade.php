@extends('main.layouts.app')

@section('htmlheader_title'){{ trans('main/home.text_htmltitle') }}@endsection

@section('htmlheader_description'){{ trans('main/common.htmlheader_description') }}@endsection

@section('htmlheader_keywords'){{ trans('main/common.htmlheader_keywords') }}@endsection

@section('content-header')

    <div id="header" class="nav-item">

            <div class="container">
                <!-- <div class="second-header">{{ trans('main/history.text_board_and_executives') }}</div> -->
                <div class="second-header">
                    @if(@$breadcrumb && count($breadcrumb) > 0)
                    <ul class="breadcrumb">
                        @if(@$breadcrumb)
                            @foreach(@$breadcrumb as $key => $value)
                                @if(@$value['active'] == 'active')
                                    <li class="active">{{ $key }}</li>
                                @else
                                    <li><a href="{{ url(@$value['url']) }}">{{ $key }}</a></li>
                                @endif
                            @endforeach
                        @endif
                    </ul>
                    @endif
                </div>
            </div>

    </div>
    
   <div id="home-course" class="container"> 

    </div> 


    <div class="container">
        <div class='row' id="card-board">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 " >
                <div class='row'id="card-board" >
                    <div class="col-xs-4 col-sm-6 col-md-6 col-lg-6">
                        <div class="history-item" id="card_board">
                            <a href="{{ url(Config::get('url.main.board')) }}" >
                                <div class="card text-white shake" >
                                    <div class="card-body board" id="board">
                                        <i class="fa fa-users fa-2x"></i><br>{{trans('main/history.text_board')}}
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                     <div class="col-xs-4 col-sm-6 col-md-6 col-lg-6" id="card_executive">
                        <div class="history-item" >
                            <a href="{{ url(Config::get('url.main.executive')) }}" >
                                <div class="card text-white shake" >
                                    <div class="card-body board" id="board">
                                        <i class="fas fa-user-friends fa-2x"></i><br>{{trans('main/history.text_executives')}}
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   </div>  
        

    


@endsection

@section('main-content')

<div class="container">

    <div id="history-line">{{trans('main/history.text_board')}}</div>
    <div id="history-second-line"></div>
 
    <div class="home-news-more">
        @if(count($board_image) > 0 )
            @for($i=0; $i<4; $i++)
                <div class="row">  
                    @for($i=0;$i<count($board_image);$i++)
    
                        <div class="home-board-item  col-sm-6 col-md-4 col-lg-3 col-xl-3" id="board">
                        
                            <a href="#">
                            
                                <div style="background-image: url( {{$board_image[$i]['image'] }} );position:relative;">
                                        <img src="{{ $board_image[$i]['image'] }}" class="img-fluid" style="visibility: hidden" />

                   
                                    <h1>
                                        <b style="font-size:22px;">{{ $board_image[$i]['name'] }}</b> <br>                               
                                        {{ $board_image[$i]['position'] }}      <br><br>                         
                                        {{-- <p style="font-size:17px;">{{ $board_image[$i]['graduate'] }} </p>       --}}
                                    </h1>    
                                </div>
                            </a>
                       
                        </div>        
                    @endfor
                </div>    
            @endfor   
        @endif
          
    </div>

</div>

@endsection

@section('style')
<link rel="stylesheet" href="{{ url('lib/swiper-4.4.0/css/swiper.min.css') }}">

@endsection

@section('script')
<script src="{{ url('lib/swiper-4.4.0/js/swiper.min.js') }}"></script>

@endsection


