@extends('main.layouts.app')

@section('htmlheader_title'){{ trans('main/home.text_htmltitle') }}@endsection

@section('htmlheader_description'){{ trans('main/common.htmlheader_description') }}@endsection

@section('htmlheader_keywords'){{ trans('main/common.htmlheader_keywords') }}@endsection


@section('content-header')
    <div id="header" class="nav-item">
        <div class="container">
            <div class="second-header">
                @if(@$breadcrumb && count($breadcrumb) > 0)
                <ul class="breadcrumb">
                    @if(@$breadcrumb)
                        @foreach(@$breadcrumb as $key => $value)
                            @if(@$value['active'] == 'active')
                                <li class="active">{{ $key }}</li>
                            @else
                                <li><a href="{{ url(@$value['url']) }}">{{ $key }}</a></li>
                            @endif
                        @endforeach
                    @endif
                </ul>
                @endif
            </div>

        </div>
    </div><!-- End apply-now -->
@endsection


<?php @include( public_path() . '/lib/PHPMailer_5.2.21') ?>

@section('main-content')
<div id="background-new-detail">
<div class="container ">
   
    <!--_INPUT ALERT -->
    <div class="row" style='z-index:666;;position:fixed;left:1em;width: 100%;'>
        <div class="col-sm-12">
            <div id='errorNotifyGroup' class='alert alert-danger ' style='opacity:1;top:0;display:none'>
                <span class='close custom-close'>&times;</span>
                <p><i class="fa fa-exclamation-circle" aria-hidden="true"></i> แจ้งเตือน</p>
                <span id='errorNotify' class='font_miniDanger' style='font-size:1em'></span>
            </div>
        </div>
    </div>

    <!--_SHOW Detail -->
    <div class='row' >
        <div class='col-sm-12 detail_content' >
            <div id="background-white" style='text-align:left;padding:1em;'>
           
                <form class="form-horizontal" role="form" method="post" action='#'>
                    
                    <div class="form-group">
                        <label for="txtInquirerName">{{ trans('main/hotline.text_name') }} : </label>							
                        <div class="input-group">
                            <input id="txtInquirerName" name="txtInquirerName" type="text" class="form-control" placeholder="{{ trans('main/hotline.text_name') }}" maxlength="100">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="txtInquirerEmail">{{ trans('main/hotline.text_email') }} : </label>							
                        <div class="input-group">
                            <input id="txtInquirerEmail" name="txtInquirerEmail" type="text" class="form-control" placeholder="{{ trans('main/hotline.text_email') }}" maxlength="150">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="txtInquirerPhone">{{ trans('main/hotline.text_tel') }} : </label>							
                        <div class="input-group">
                        <input id="txtInquirerPhone" name="txtInquirerPhone" type="text" class="form-control" placeholder="{{ trans('main/hotline.text_tel') }}" maxlength="100" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="txtAreInquirerQuery">{{ trans('main/hotline.text_message') }} : </label>							
                        <div class="input-group">
                            <textarea id='txtAreInquirerQuery' name='txtAreInquirerQuery' class="form-control customTextarea" placeholder="{{ trans('main/hotline.text_message') }}"></textarea>
                        </div>
                    </div>

                    <div class="form-group" style='text-align:right;font-size:0.9em'>
                        <span style='color:red'>* </span>{{ trans('main/hotline.text_info') }}
                    </div>
                    
                    <div class="form-group" style='text-align:center'>
                        <div class="input-group">
                            <button type="submit" id='submitBtn' class="btn btn-primary btn-lg" style='margin:auto'>
                            <!-- <i class="fa fa-spinner fa-pulse fa-1x fa-fw" aria-hidden="true"></i>   -->
                                {{ trans('main/hotline.text_sendbutton') }}
                            </button>
                        </div>
                    </div>

                    <div class="form-group" style='text-align:center'>
                        <span  id='notify_querysent'>
                            <div id='loading_img' style='position:relative;top:0.60em;' class='hidden'>
                                <i class="fa fa-spinner fa-pulse fa-1x fa-fw" aria-hidden="true"></i>
					         </div>
                        </span>
                    </div>

                </form>

            </div>
        </div>
    </div>


</div>{{-- end containner --}}
<br>
</div>
@endsection


@section('style')
<!-- <link rel="stylesheet" href="{{ url('lib/swiper-4.4.0/css/swiper.min.css') }}"> -->


@endsection

@section('script')

<!-- <script src="{{ url('lib/swiper-4.4.0/js/swiper.min.js') }}"></script> -->
 <!-- jQuery library -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

 <!-- Popper JS -->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

 <!-- Latest compiled JavaScript -->
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

<!--Custom JS -->
 <script src="{{ url('js/utils.js') }}"></script>

 <script>
    /* $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        }); */

	$(document).ready(function(){

		/*---------------------------------
         	CLICK (X) CLOSE BUTTON 
         	of Error notify (submit form)
      	------------------------------------*/   
       	$('#errorNotifyGroup .close').click(function(){
          $('#errorNotifyGroup').fadeOut('slow'); 
       	});

       	/*================
          	SUBMIT FORM
      	==================*/
		$('#submitBtn').click(function(){
             
			 
			 event.preventDefault();
			/*-----------------------------------------
              Varify input data (call fx in util.js)
            -------------------------------------------*/
            //_fn(must chk[selector1,selector2,...] ,except [ selector1 selector2 ... ])
            var errNotify = varifyBeforeSubmit("form input[type='text'],#txtAreInquirerQuery" 
            									,"");
            if(errNotify != '')
            {
              /*-----------------------------------------
                Warning when found something wrong
              -------------------------------------------*/
              $('#errorNotifyGroup').removeClass('hidden').fadeIn('slow');
              $('#errorNotify').html(errNotify);
            
              return false;

            }else{
                /*-----------------------------------
                    Email format check
                -------------------------------------*/
                if( !isEmail($('#txtInquirerEmail').val()) ) {
                  $("#frmgroup_txtInquirerEmail").addClass('has-error'); 

                  $('#errorNotifyGroup').removeClass('hidden').fadeIn('slow');
                  $('#errorNotify').html("*กรุณา" + $('#txtInquirerEmail').attr('placeholder') + "ให้ถูกต้อง");
                  return false;
                }
                
                /*-------------------------------------------------------
                    Submit button go!go!!go!!!
                --------------------------------------------------------*/
                $('#loading_img').removeClass('hidden');//_Show loading pic

                 //_call ajax
                $.ajax({
	                 url  :"{{ url(Config::get('url.main.querysend_ajax')) }}",
                     type :'get',
                     /* headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     }, */
	                 data : {'txtInquirerName':$('#txtInquirerName').val()
	                		,'txtInquirerEmail':$('#txtInquirerEmail').val()
	                		,'txtInquirerPhone':$('#txtInquirerPhone').val()
	                		,'txtAreInquirerQuery':$('#txtAreInquirerQuery').val()
	                		}
	            }).done(function(result){
	               $('#loading_img').addClass('hidden');//_Hide loading pic
	               $('#notify_querysent').html($.trim(result));
                   $('#alert_querysent').removeClass('hidden');//_show bootstrap's alert
                   
                   $("input[name^='txtInquirer'] , #txtAreInquirerQuery").val('');//_clear input
	            });
            }//#endif notify
		});
	});

</script>   
<style>
    .hidden{
        display:none;
    }

    input[name^='txtInquirer']
    ,#txtAreInquirerQuery
    {
        font-size:0.9em;
    }
  
</style> 

@endsection

