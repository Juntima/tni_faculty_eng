<?php
    $level_selected = $_GET['level_selected_param'];

//_เขียนแบบ heredoc ต้องเขียนชิด ห้ามมีย่อหน้า
//_%s คือ ค่าที่เราจะใส่ลง เรียวตามลำดับ ซึ่ง เราจะใช้ $fac_template กับฟังคชัน sprintf
//_ต.ย. sprintf($fac_template , ค่า สำหรับ %s ตัวที่1 ,  ค่า สำหรับ %s ตัวที่2 ,...)
//__* ถ้าจะใส่ % เช่น ข้างล่างนี้ เขียนเป็น 100%% ก็หมายถึง 100% นั้นแหละ

$fac_template = <<<EOT

                <div class="home-nav-pec-item col-sm-6 col-md-3 col-lg-3 col-xl-3" id="%s" >
                    <div id="%s">
                        <div style="background-image: #000;">                                  
                             <img src="%s" class="img-fluid" />                           
                        </div>
                    </div>
                    <a href="#"><h1>%s</h1></a>
                    <h2>%s</h2>
                </div>
            
        
EOT;
//_เอา ตย มาจากหน้านี้ https://www.w3schools.com/bootstrap4/tryit.asp?filename=trybs_scrollspy&stacked=h

    $fac_result = '';

    switch ($level_selected) {
        case 'level_bachelor_fac':
            $fac_result = sprintf($fac_template ,'fac_thumbnal_eng','fac_engineer','img/faculty/eng/ENG.jpg',trans('main/faculty_level.text_ENG'),trans('main/faculty_level.text_eng') ); 
            $fac_result.= sprintf($fac_template ,'fac_thumbnal_ba','ba','img/faculty/ba/BA_r.jpg',trans('main/faculty_level.text_BA'),trans('main/faculty_level.text_ba') ); 
            $fac_result.= sprintf($fac_template ,'fac_thumbnal_it','it','img/faculty/inter/IT-INTER_r.jpg',trans('main/faculty_level.text_IT'),trans('main/faculty_level.text_it') );  
            $fac_result.= sprintf($fac_template ,'fac_thumbnal_cgel','cgel','img/faculty/cgel/CGEL.jpg',trans('main/faculty_level.text_CGEL'),trans('main/faculty_level.text_cgel') );  
            break;

        case 'level_master_fac':
            $fac_result = sprintf($fac_template ,'fac_thumbnal_master1','fac_engineer','img/faculty/master/ENG_r.jpg',trans('main/faculty_level.text_ENG'),trans('main/faculty_level.text_eng') ); 
            $fac_result.= sprintf($fac_template ,'fac_thumbnal_master2','ba','img/faculty/master/BA_r.jpg',trans('main/faculty_level.text_BA'),trans('main/faculty_level.text_ba') ); 
            $fac_result.= sprintf($fac_template ,'fac_thumbnal_master3','it','img/faculty/master/IT_r.jpg',trans('main/faculty_level.text_IT'),trans('main/faculty_level.text_it') );
            break;

        // case 'level_inter_fac':           
        //     $fac_result = sprintf($fac_template , 'fac_thumbnal_inter1' ,'image/catalog/mockup/mockup-1.png',trans('main/faculty_level.text_BA'),trans('main/faculty_level.text_ba')); 
        //     $fac_result.= sprintf($fac_template , 'fac_thumbnal_inter2' ,'image/catalog/mockup/mockup-1.png',trans('main/faculty_level.text_ENG'),trans('main/faculty_level.text_eng') );
        //     $fac_result.= sprintf($fac_template , 'fac_thumbnal_inter3' ,'image/catalog/mockup/mockup-1.png',trans('main/faculty_level.text_IT'),trans('main/faculty_level.text_it')  );
        //     break;    

        default:
            $fac_result = sprintf($fac_template ,'fac_thumbnal_eng','fac_engineer','img/faculty/eng/ENG.jpg',trans('main/faculty_level.text_ENG'),trans('main/faculty_level.text_eng') ); 
            $fac_result.= sprintf($fac_template ,'fac_thumbnal_ba','ba','img/faculty/ba/BA_r.jpg',trans('main/faculty_level.text_BA'),trans('main/faculty_level.text_ba') ); 
            $fac_result.= sprintf($fac_template ,'fac_thumbnal_it','it','img/faculty/inter/IT-INTER_r.jpg',trans('main/faculty_level.text_IT'),trans('main/faculty_level.text_it') );  
            $fac_result.= sprintf($fac_template ,'fac_thumbnal_cgel','cgel','img/faculty/cgel/CGEL.jpg',trans('main/faculty_level.text_CGEL'),trans('main/faculty_level.text_cgel') ); 
            break;
    }

    echo $fac_result;
?>

   
<script>
        $(document).ready(function(){
          
          //_selects id that are preceded by 'fac_thumbnal'
          $("[id^='fac_thumbnal']").click(function(){
          //   alert("ff");
            faculty_selected = $(this).attr('id');//_ตัวแปรสำหรับส่งไปทำงานหน้าที่จะเรียกใน method Ajax
            
            $.ajax({
                //url : '/resources/views/main/fac_info_ajax.php'
                url : '{{ url(Config::get('url.main.fac_ajax')) }}'
              , type: 'get' //_เรียกหน้า fac_info_ajax.php แบบ post
              , data: {'faculty_selected_param':faculty_selected} //_ตัวแปรส่งไปหน้า fac_info_ajax.php
            }).done(function(result){
              //_display faculty info
              $('#fac_info').html($.trim(result));
            });
      
          });
        });
    </script>
