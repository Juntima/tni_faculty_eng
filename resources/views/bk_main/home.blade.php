@extends('main.layouts.app')

@section('htmlheader_title'){{ trans('main/home.text_htmltitle') }}@endsection

@section('htmlheader_description'){{ trans('main/common.htmlheader_description') }}@endsection

@section('htmlheader_keywords'){{ trans('main/common.htmlheader_keywords') }}@endsection



@section('content-header')
<div id="hero-banner">
        <div class="container">
            
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    
                    @foreach($home_hero_banner as $banner)
                        <div class="swiper-slide" data-background_color="{{ $banner->background_color }}">
                            @if($banner->link)
                            <a href="{{ $banner->link }}" target="_blank">
                                <img src="{{ $banner->image }}" class="img-fluid" alt="{{ $banner->title }}" />
                            </a>
                            @else
                            <img src="{{ $banner->image }}" class="img-fluid" alt="{{ $banner->title }}" />
                            @endif
                        </div>
                    @endforeach
    
                </div>
    
                <div class="swiper-pagination"></div>
            </div>
    
        </div>
    </div>
@endsection


@section('main-content')

<div id="home-course" class="container">
    <h3>{{ trans('main/home.text_course_heading') }}</h3>
    <div>
        {{-- <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> --}}
        <a href="{{ url(Config::get('url.main.faculty')) }}" class="shake">{{ trans('main/home.text_bechelor') }}</a>
        <a href="{{ url(Config::get('url.main.faculty')) }}?f=master" class="shake">{{ trans('main/home.text_master') }}</a>
        <a href="http://inter.tni.ac.th/" class="shake" target="_blank">{{ trans('main/home.text_international_program') }}</a>
        <a href="https://admission.tni.ac.th/home/2017/main/index.php?option=contents&category=70&id=61" class="shake" target="_blank">
            {{ trans('main/home.text_scholarship') }}
        </a>
        <a href="{{url(Config::get('url.main.exchange_programs'))}}" class="shake" target="_blank">{{ trans('main/home.text_exchange_program') }}</a>
        {{-- </div> --}}
    </div>
</div>

<div id="home-news-event" class="container">
    <div class="row">
        <div class="col-sm-12 col-md-8 col-lg-9 col-xl-9">
            <div id="home-news">
                <h1>{{ trans('main/home.text_highlight_activities') }}</h1>
                {{-- @if (count($banner_highlight) > 0) --}}
               
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                            
                            @if (count($banner_highlight) > 0)   {{--  ถ้ามีรายการ แสดงตาม DB  --}}

                                @foreach($banner_highlight as $highlight)   {{--  วนแสดง ปกรูปใหญ่ รูปแรก   --}}
                                    <div class="swiper-slide">
                                        @if($highlight->link)
                                            <a href="{{ $highlight->link }}" target="_blank">
                                                <img src="{{ $highlight->image }}" class="img-fluid" />
                                            </a>
                                        @else
                                            <img src="{{ $highlight->image }}" class="img-fluid" />
                                        @endif
                                    </div>
                                @endforeach
                            @else  {{--  ถ้าไม่มีรายการ แสดงภาพ no image  ปกรูปใหญ่ รูปแรก --}}
                                <div class="swiper-slide">
                                    <a href="#">
                                        <img src="{{ $first_article_images_blank }}" class="img-fluid" alt="image blank" />
                                    </a>
                                </div>
                            @endif
            
                        </div>
            
                        @if(count($banner_highlight) > 1) {{--  เอาจำนวนภาพมาวนสไลด์ ถ้าภาพมีมากกว่า1 ภาพให้สไลด์ --}}
                        <div class="swiper-pagination"></div>
                        <div class="swiper-button-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></div>
                        <div class="swiper-button-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></div>
                        @endif
                    </div>
                
                {{-- @endif --}}
                <div class="home-news-more">  {{--  highlight 4  รายการด้านล่าง --}}
                    @for($i=0; $i < count($articles); $i++)
                            <div class="home-news-item" >
                                <div>
                                    @if($articles[$i]->link )
                                        <a href="{{ $articles[$i]->link }}" target="_blank">
                                        
                                            <div style="background-image: url({{ $articles[$i]->image }})">
                                                <img src="{{ $articles[$i]->image }}" class="img-fluid" style="visibility: hidden" />
                                            </div>
                                                                    
                                        </a> 
                                    @else
                                        <a href="{{ url(Config::get('url.main.news').'/'.$articles[$i]->id.'/detail') }}">
                                        
                                            <div style="background-image: url({{ $articles[$i]->image }})">
                                                <img src="{{ $articles[$i]->image }}" class="img-fluid" style="visibility: hidden" />
                                            </div>
                                                                    
                                        </a> 
                                    @endif
                                </div>
                                @if($articles[$i]->link )
                                    <a href="{{ $articles[$i]->link }}" target="_blank">
                                        <h1>{{ $articles[$i]->name }}  </h1>
                                    </a>
                                @else
                                    <a href="{{ url(Config::get('url.main.news').'/'.$articles[$i]->id.'/detail') }}">
                                        <h1>{{ $articles[$i]->name }}  </h1>
    
                                    </a>
                                @endif
                            </div> 
                      

                    @endfor
                </div>

                <a href="{{ url(Config::get('url.main.news')) }}" class="btn-more pull-right">{{ trans('main/home.text_news_more' )}}</a>   {{--  ปุ่ม more  --}}
                
            </div>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-3 col-xl-3">
            <div id="home-event">
                <h1>{{ trans('main/home.text_event_update') }}</h1>
                @if(count($event_update) > 0)
                    <ul class="timeline">
                        @foreach($event_update as $event)
                        <li>
                            @if($event->link )
                                <a href="{{ $event->link }}" target="_blank">{{ $event->month }}</a>
                                <a href="{{ $event->link }}" target="_blank" class='truncate_text_event'>{{ $event->name }}<br>&nbsp;</a>
                            @else
                                <a href="{{ url(Config::get('url.main.events').'/'.$event->id.'/detail') }}">{{ $event->month }}</a>
                                <a href="{{ url(Config::get('url.main.events').'/'.$event->id.'/detail') }}" class='truncate_text_event'>{{ $event->name }}<br>&nbsp;</a>
                            @endif
                            <div class="timeline-day">{{ $event->day }}</div>
                        </li>
                        @endforeach
                    </ul>
                @else
                    <div style='text-align:center;margin-top:10px'><b>{{ trans('main/common.text_not_found_data') }}</b></div>
                @endif
                <a href="{{ url(Config::get('url.main.events')) }}" class="btn-more pull-right">{{ trans('main/home.text_event_more' )}}</a>
            </div>
        </div>
    </div>
</div>

<div id="home-nav-pec">
    <div class="container">
        <div class="row">
            @foreach($nav_pec as $pec)
            <div class="home-nav-pec-item col-sm-12 col-md-4 col-lg-4 col-xl-4">
                <div>
                    <!-- <div style="background-image: url({{ $pec->image }})">
                        @if($pec->link)
                        <a href="{{ $pec->link }}">
                            <img src="{{ $pec->image }}" class="img-fluid" style="visibility: hidden;"/>
                        </a>
                        @else
                        <img src="{{ $pec->image }}" class="img-fluid" />
                        @endif
                    </div> -->

                    @if($pec->link)
                        <a href="{{ $pec->link }}" >
                            <div style="background-image: url({{ $pec->image }})">
                                <img src="{{ $pec->image }}" class="img-fluid" style="visibility: hidden;"/>
                            </div>
                        </a>
                    @else
                        <div style="background-image: url({{ $pec->image }})">
                            <img src="{{ $pec->image }}" class="img-fluid" style="visibility: hidden;"/>
                        </div>
                    @endif


                </div>

                @if($pec->link)
                <a href="{{ $pec->link }}" ><h1>{{ $pec->name }}</h1></a>
                @else
                <h1>{{ $pec->name }}</h1>
                @endif
            </div>
            @endforeach
        </div>
    </div>
</div>

<div id="home-learn2know">
    <div class="container">
        <div>
            <h1>{{ trans('main/home.text_learn2know_heading') }}</h1>
            <h2>{{ trans('main/home.text_learn2know_subheading') }}</h2>
            <p>{{ trans('main/home.text_learn2know_p1') }}</p>
            <p>{{ trans('main/home.text_learn2know_p2') }}</p>
        </div>
        <a class="shake" href="http://learn2know.tni.ac.th/main/" target="_blank">{{ trans('main/home.text_click_here') }}</a>
    </div>
</div>

<div id="home-channel" class="container">
    <div class="row">
        <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
            <h1>{{ trans('main/home.text_tni_channel') }}</h1>
            @if(count($latest_video) > 0)
                <div class="video-item" >
                        {{-- <div style="background: rgba(0,0,0,0.8); position:absolute;height:100%; width:100%;z-index:1"></div> --}}
                    @foreach($latest_video as $video)
                        @if($video->link)
                            <div class="embed-responsive embed-responsive-16by9" id="video" >                         
                                {!!$video->link !!}                           
                            </div>
                                {{-- <img src="{{ $video->image }}" class="img-fluid" />--}}
                            <h2>{{ $video->name }}</h2>  
                        @else                           
                            <img src="{{ url('storage/image/cache/no_image-100x100.png') }}"  class='img-thumbnail'/>                           
                        @endif                 
                    @endforeach
                </div>
                <a href="https://www.youtube.com/channel/UCOMVmuT0gRQxBGAFBDgqBLg" target="_blank"  class="btn-more pull-right">{{ trans('main/home.text_more') }}</a>                       
            @endif

        </div>
        <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
            <h1>{{ trans('main/home.text_e_bochure') }}</h1>

            @if(count($ebrochures) > 0)

            <div class="video-item">
                @foreach($ebrochures as $ebochure)
                    <a href="{{  $ebochure->link }}" target="_blank">
                        <img src="{{ $ebochure->image }}" class="img-fluid" />
                        <h2>{{ $ebochure->name }}</h2>
                        <img src="{{ url('img/read-overlay.png') }}" class="play-overlay" />
                    </a>
                @endforeach
            </div>

            <a href="{{ url(Config::get('url.main.ebrochures')) }}" class="btn-more pull-right" >{{ trans('main/home.text_more') }}</a>
            
            @endif
        </div>
    </div>
</div>

@endsection

@section('style')
<link rel="stylesheet" href="{{ url('lib/swiper-4.4.0/css/swiper.min.css') }}">

@endsection

@section('script')
<script src="{{ url('lib/swiper-4.4.0/js/swiper.min.js') }}"></script>
<script>
var homeHeroSwiper = new Swiper('#hero-banner .swiper-container', {
    autoplay: {
        delay: 5000,
        disableOnInteraction: false
    },
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    on: {
        init: function() {
            var background_color = $('#hero-banner .swiper-wrapper>div:nth-child(1)').data('background_color');
            $('#hero-banner').css('background', background_color).addClass('transition-none');
        },
        slideChange: function(e) {
            var background_color = $('#hero-banner .swiper-wrapper>div:nth-child(' + (homeHeroSwiper.activeIndex + 1) + ')').data('background_color');
            $('#hero-banner').css('background', background_color).removeClass('transition-none').addClass('transition-slow');
        }
    }
});

var homeNewsSwiper = new Swiper('#home-news .swiper-container', {
    loop: true,
    effect: 'fade',
    autoplay: {
        delay: 10000,
        disableOnInteraction: false
    },
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
});
</script>
{{-- <script>
    $(document).ready(function() {
  $('#play-video').on('click', function(ev) {
    
    // var x = $("iframe").attr('src');
    // alert(x);
    
    
    $("iframe")[0].src += "&autoplay=1";
    ev.preventDefault();
 
  });
});
</script> --}}
@endsection