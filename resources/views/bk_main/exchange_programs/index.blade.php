@extends('main.layouts.app')

@section('htmlheader_title'){{ trans('main/home.text_htmltitle') }}@endsection

@section('htmlheader_description'){{ trans('main/common.htmlheader_description') }}@endsection

@section('htmlheader_keywords'){{ trans('main/common.htmlheader_keywords') }}@endsection



@section('content-header')
@php
    {{--_รับค่าตัวแปรที่ส่งมาตอนกดเปลี่ยนหน้า ใช้ที่ Jquery ด้านล่าง --}}
    $exchange_category_id = @$_GET['category'];
@endphp

    <div id="header" class="nav-item">

           <!-- <div class="container">
                <div class="second-header">{{ trans('main/home.text_exchange_program') }}</div>
            </div> -->
            <div class="container">
                <div class="second-header">
                    @if(@$breadcrumb && count($breadcrumb) > 0)
                    <ul class="breadcrumb">
                        @if(@$breadcrumb)
                            @foreach(@$breadcrumb as $key => $value)
                                @if(@$value['active'] == 'active')
                                    <li class="active">{{ $key }}</li>
                                @else
                                    <li><a href="{{ url(@$value['url']) }}">{{ $key }}</a></li>
                                @endif
                            @endforeach
                        @endif
                    </ul>
                    @endif
                </div>
            </div>

    </div><!-- End apply-now -->

    <!-- <div id="home-course" class="container">
    </div> -->    
@endsection

@section('main-content')

<br>
<div class="container ">

        @if(@count($exchange_cat) > 0)
        @foreach($exchange_cat as $item)
            {!! $item->name !!}
        @endforeach
    @endif 

        <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">   
                    <div class="tabbable-line">
                        
                        <ul class="nav nav-tabs nav-justified " id="exTabJust" role="tablist">             

                            <li class="nav-item">
                                <a class="nav-link active" id="exprogram-tab-just" data-toggle="tab" href="#exprogram" role="tab" aria-controls="exprogram-just" aria-selected="true">
                                    <span> {!! trans('main/exchange_program.text_exprogram') !!} </span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link " id="exdetail-tab-just" data-toggle="tab" href="#exdetail" role="tab" aria-controls="exdetail-just" aria-selected="false">
                                    <span>   {!! trans('main/exchange_program.text_exdetail') !!} </span>
                                </a>
                            </li>
                           
                            

                            <li class="nav-item">
                                <a class="nav-link" id="exstudent-tab-just" data-toggle="tab" href="#exstudent" role="tab" aria-controls="exstudent-just" aria-selected="false">
                                    <span> {!! trans('main/exchange_program.text_exstudent')!!} </span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" id="exteacher-tab-just" data-toggle="tab" href="#exteacher" role="tab" aria-controls="exteacher-just" aria-selected="false">
                                    <span> {{ trans('main/exchange_program.text_exteacher') }} </span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" id="exuniversity-tab-just" data-toggle="tab" href="#exuniversity" role="tab" aria-controls="exuniversity-just" aria-selected="false">
                                    <span> {{ trans('main/exchange_program.text_exuniversity') }} </span>
                                </a>
                            </li>
    
                        </ul>
    
                        <div class="tab-content card pt-5" id="exTabContentJust" >
                                
    
                                <div class="tab-pane fade show active" id="exprogram" role="tabpanel" aria-labelledby="exprogram-tab-just">
                                        <h5></h5>
                                    @if(count($exchange_bycat_2['exchange_cat']) > 0 )
                                    @foreach($exchange_bycat_2['exchange_cat']->chunk(4) as $chunk) {{-- method chunk : แบ่ง object เป็น row row ละ 4 คอลัมน์ --}}
                                            <div class='row'>
                                                @foreach($chunk as $item)                                                  
                                                    <div class="home-news-item " >
                                                            <div>
                                                                @if($item->link )
                                                                    <a href="{{ $item->link}}" target="_blank">
                                                                        <div style="background-image: url({{ $item->image }})">
                                                                            <img src="{{ $item->image }}" class="img-fluid" style="visibility: hidden" />
                                                                        </div>
                                                                    </a>
                                                                @else
                                                                    <a href="{{ url(Config::get('url.main.exchange_programs').'/'.$item->id.'/detail') }}">
                                                                    
                                                                        <div style="background-image: url({{ $item->image }})">
                                                                            <img src="{{ $item->image }}" class="img-fluid" style="visibility: hidden" />
                                                                        </div>
                                                                                                
                                                                    </a> 
                                                                @endif
                                                            </div>

                                                            @if($item->link )
                                                                <a href="{{$item->link}}" target="_blank">
                                                                    <h1 class='truncate_text'>{{ $item->name }}  </h1>                          
                                                                </a> 
                                                            @else
                                                                <a href="{{ url(Config::get('url.main.exchange_programs').'/'.$item->id.'/detail') }}">
                                                                    <h1 class='truncate_text'>{{ $item->name }}  </h1>                              
                                                                </a> 
                                                            @endif
                                                            <div class='poston' style='font-size:0.75em;'>
                                                                    
                                                                <i class='fa fa-clock-o fa'></i> {{ date('d-M-Y', strtotime(@$item->publish_start)) }}
                                                                
                                                            </div>
                            
                                                    </div> 
                                                @endforeach 
                                            </div> <br>
                                           
                                        @endforeach 
    
                                        <div class='row'>
                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style='text-align:right'>
                                                <div class="pager">
                                                    <div class="pageinfo">
                                                        <span>{{ trans('backoffice/common.text_all_results') }} {{$exchange_bycat_2['exchange_cat']->total() }} {{ trans('backoffice/common.text_item(s)') }}</span>
                                                        <span>{{ trans('backoffice/common.text_page') }} {{ $exchange_bycat_2['exchange_cat']->currentPage () }} {{ trans('backoffice/common.text_from') }} {{ $exchange_bycat_2['exchange_cat']->lastPage() }} {{ trans('backoffice/common.text_page(s)') }}</span>
                                                    </div>		
                                                </div>	      
                                            </div>
                                        </div>
    
                                        <div class='row'>
                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style='text-align:center'>
                                                {{$exchange_bycat_2['exchange_cat']->links('vendor.pagination.bootstrap-4')}}
                                            </div>
                                        </div> 
    
                                    @else 
    
                                        <div class='row'>
                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style='text-align:center'>
                                                {{trans('main/common.text_not_found_data')}}
                                            </div>
                                        </div>
                                                                       
                                    @endif {{-- @if($catagory[$i]->article_category_id == '1' ) --}}
                               
    
                                </div> {{-- end div id port  --}}   
    
                            {{--  ================================================================================================================================  --}}
                            <div class="tab-pane fade " id="exdetail" role="tabpanel" aria-labelledby="exdetail-tab-just">
                                    <h5></h5> 
                                    @if(count($exchange_bycat_1['exchange_cat']) > 0 )
                                    @foreach($exchange_bycat_1['exchange_cat']->chunk(4) as $chunk) {{-- method chunk : แบ่ง object เป็น row row ละ 4 คอลัมน์ --}}
                                            <div class='row'>
                                                @foreach($chunk as $item)                                                  
                                                <div class="home-news-item " >
                                                        <div>
                                                            @if($item->link )
                                                                <a href="{{ $item->link}}" target="_blank">
                                                                    <div style="background-image: url({{ $item->image }})">
                                                                        <img src="{{ $item->image }}" class="img-fluid" style="visibility: hidden" />
                                                                    </div>
                                                                </a>
                                                            @else
                                                                <a href="{{ url(Config::get('url.main.exchange_programs').'/'.$item->id.'/detail') }}">
                                                                
                                                                    <div style="background-image: url({{ $item->image }})">
                                                                        <img src="{{ $item->image }}" class="img-fluid" style="visibility: hidden" />
                                                                    </div>
                                                                                            
                                                                </a> 
                                                            @endif
                                                        </div>

                                                        @if($item->link )
                                                            <a href="{{$item->link}}" target="_blank">
                                                                <h1 class='truncate_text'>{{ $item->name }}  </h1>                          
                                                            </a> 
                                                        @else
                                                            <a href="{{ url(Config::get('url.main.exchange_programs').'/'.$item->id.'/detail') }}">
                                                                <h1 class='truncate_text'>{{ $item->name }}  </h1>                              
                                                            </a> 
                                                        @endif
                                                        <div class='poston' style='font-size:0.75em;'>
                                                                
                                                            <i class='fa fa-clock-o fa'></i> {{ date('d-M-Y', strtotime(@$item->publish_start)) }}
                                                            
                                                        </div>
                        
                                                </div> 
                                                @endforeach 
                                            </div> <br>
                                           
                                        @endforeach 
    
                                        <div class='row'>
                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style='text-align:right'>
                                                <div class="pager">
                                                    <div class="pageinfo">
                                                        <span>{{ trans('backoffice/common.text_all_results') }} {{$exchange_bycat_1['exchange_cat']->total() }} {{ trans('backoffice/common.text_item(s)') }}</span>
                                                        <span>{{ trans('backoffice/common.text_page') }} {{ $exchange_bycat_1['exchange_cat']->currentPage () }} {{ trans('backoffice/common.text_from') }} {{ $exchange_bycat_1['exchange_cat']->lastPage() }} {{ trans('backoffice/common.text_page(s)') }}</span>
                                                    </div>		
                                                </div>	      
                                            </div>
                                        </div>
    
                                        <div class='row'>
                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style='text-align:center'>
                                                {{ $exchange_bycat_1['exchange_cat']->links('vendor.pagination.bootstrap-4')}}
                                            </div>
                                        </div> 
    
                                    @else 
    
                                        <div class='row'>
                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style='text-align:center'>
                                                {{trans('main/common.text_not_found_data')}}
                                            </div>
                                        </div>
                                                                       
                                    @endif {{-- @if($catagory[$i]->article_category_id == '1' ) --}}
                                </div> 
    
                           
            
                              
                            {{--  ================================================================================================================================  --}}     
    
                                <div class="tab-pane fade " id="exstudent" role="tabpanel" aria-labelledby="exstudent-tab-just">
                                        <h5></h5>
                                        @if(count($exchange_bycat_3['exchange_cat']) > 0 )
                                        @foreach($exchange_bycat_3['exchange_cat']->chunk(4) as $chunk) {{-- method chunk : แบ่ง object เป็น row row ละ 4 คอลัมน์ --}}
                                                <div class='row'>
                                                    @foreach($chunk as $item)                                                  
                                                    <div class="home-news-item " >
                                                            <div>
                                                                @if($item->link )
                                                                    <a href="{{ $item->link}}" target="_blank">
                                                                        <div style="background-image: url({{ $item->image }})">
                                                                            <img src="{{ $item->image }}" class="img-fluid" style="visibility: hidden" />
                                                                        </div>
                                                                    </a>
                                                                @else
                                                                    <a href="{{ url(Config::get('url.main.exchange_programs').'/'.$item->id.'/detail') }}">
                                                                    
                                                                        <div style="background-image: url({{ $item->image }})">
                                                                            <img src="{{ $item->image }}" class="img-fluid" style="visibility: hidden" />
                                                                        </div>
                                                                                                
                                                                    </a> 
                                                                @endif
                                                            </div>

                                                            @if($item->link )
                                                                <a href="{{$item->link}}" target="_blank">
                                                                    <h1 class='truncate_text'>{{ $item->name }}  </h1>                          
                                                                </a> 
                                                            @else
                                                                <a href="{{ url(Config::get('url.main.exchange_programs').'/'.$item->id.'/detail') }}">
                                                                    <h1 class='truncate_text'>{{ $item->name }}  </h1>                              
                                                                </a> 
                                                            @endif
                                                            <div class='poston' style='font-size:0.75em;'>
                                                                    
                                                                <i class='fa fa-clock-o fa'></i> {{ date('d-M-Y', strtotime(@$item->publish_start)) }}
                                                                
                                                            </div>
                            
                                                    </div> 
                                                    @endforeach 
                                                </div> <br>
                                               
                                            @endforeach 
        
                                            <div class='row'>
                                                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style='text-align:right'>
                                                    <div class="pager">
                                                        <div class="pageinfo">
                                                            <span>{{ trans('backoffice/common.text_all_results') }} {{$exchange_bycat_3['exchange_cat']->total() }} {{ trans('backoffice/common.text_item(s)') }}</span>
                                                            <span>{{ trans('backoffice/common.text_page') }} {{ $exchange_bycat_3['exchange_cat']->currentPage () }} {{ trans('backoffice/common.text_from') }} {{ $exchange_bycat_3['exchange_cat']->lastPage() }} {{ trans('backoffice/common.text_page(s)') }}</span>
                                                        </div>		
                                                    </div>	      
                                                </div>
                                            </div>
        
                                            <div class='row'>
                                                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style='text-align:center'>
                                                    {{$exchange_bycat_3['exchange_cat']->links('vendor.pagination.bootstrap-4')}}
                                                </div>
                                            </div> 
        
                                        @else 
        
                                            <div class='row'>
                                                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style='text-align:center'>
                                                    {{trans('main/common.text_not_found_data')}}
                                                </div>
                                            </div>
                                                                           
                                        @endif {{-- @if($catagory[$i]->article_category_id == '1' ) --}}
                                </div> {{-- end div id co operative --}}

                            {{--  ================================================================================================================================  --}} 
                                
                                <div class="tab-pane fade" id="exteacher" role="tabpanel" aria-labelledby="exteacher-tab-just">
                                        <h5></h5>
                                        @if(count($exchange_bycat_4['exchange_cat']) > 0 )
                                        @foreach($exchange_bycat_4['exchange_cat']->chunk(4) as $chunk) {{-- method chunk : แบ่ง object เป็น row row ละ 4 คอลัมน์ --}}
                                                <div class='row'>
                                                    @foreach($chunk as $item)                                                  
                                                    <div class="home-news-item " >
                                                            <div>
                                                                @if($item->link )
                                                                    <a href="{{ $item->link}}" target="_blank">
                                                                        <div style="background-image: url({{ $item->image }})">
                                                                            <img src="{{ $item->image }}" class="img-fluid" style="visibility: hidden" />
                                                                        </div>
                                                                    </a>
                                                                @else
                                                                    <a href="{{ url(Config::get('url.main.exchange_programs').'/'.$item->id.'/detail') }}">
                                                                    
                                                                        <div style="background-image: url({{ $item->image }})">
                                                                            <img src="{{ $item->image }}" class="img-fluid" style="visibility: hidden" />
                                                                        </div>
                                                                                                
                                                                    </a> 
                                                                @endif
                                                            </div>

                                                            @if($item->link )
                                                                <a href="{{$item->link}}" target="_blank">
                                                                    <h1 class='truncate_text'>{{ $item->name }}  </h1>                          
                                                                </a> 
                                                            @else
                                                                <a href="{{ url(Config::get('url.main.exchange_programs').'/'.$item->id.'/detail') }}">
                                                                    <h1 class='truncate_text'>{{ $item->name }}  </h1>                              
                                                                </a> 
                                                            @endif
                                                            <div class='poston' style='font-size:0.75em;'>
                                                                    
                                                                <i class='fa fa-clock-o fa'></i> {{ date('d-M-Y', strtotime(@$item->publish_start)) }}
                                                                
                                                            </div>
                            
                                                    </div> 
                                                    @endforeach 
                                                </div> <br>
                                               
                                            @endforeach 
        
                                            <div class='row'>
                                                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style='text-align:right'>
                                                    <div class="pager">
                                                        <div class="pageinfo">
                                                            <span>{{ trans('backoffice/common.text_all_results') }} {{$exchange_bycat_4['exchange_cat']->total() }} {{ trans('backoffice/common.text_item(s)') }}</span>
                                                            <span>{{ trans('backoffice/common.text_page') }} {{ $exchange_bycat_4['exchange_cat']->currentPage () }} {{ trans('backoffice/common.text_from') }} {{ $exchange_bycat_4['exchange_cat']->lastPage() }} {{ trans('backoffice/common.text_page(s)') }}</span>
                                                        </div>		
                                                    </div>	      
                                                </div>
                                            </div>
        
                                            <div class='row'>
                                                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style='text-align:center'>
                                                    {{$exchange_bycat_4['exchange_cat']->links('vendor.pagination.bootstrap-4')}}
                                                </div>
                                            </div> 
        
                                        @else 
        
                                            <div class='row'>
                                                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style='text-align:center'>
                                                    {{trans('main/common.text_not_found_data')}}
                                                </div>
                                            </div>
                                                                           
                                        @endif {{-- @if($catagory[$i]->article_category_id == '1' ) --}}
                                </div> {{-- end div id ex scholar --}}
    
                           {{--  ================================================================================================================================  --}}  
                           
                                <div class="tab-pane fade" id="exuniversity" role="tabpanel" aria-labelledby="exuniversity-tab-just">
                                        <h5></h5>
                                        @if(count($exchange_bycat_5['exchange_cat']) > 0 )
                                        @foreach($exchange_bycat_5['exchange_cat']->chunk(4) as $chunk) {{-- method chunk : แบ่ง object เป็น row row ละ 4 คอลัมน์ --}}
                                                <div class='row'>
                                                    @foreach($chunk as $item)                                                  
                                                    <div class="home-news-item " >
                                                            <div>
                                                                @if($item->link )
                                                                    <a href="{{ $item->link}}" target="_blank">
                                                                        <div style="background-image: url({{ $item->image }})">
                                                                            <img src="{{ $item->image }}" class="img-fluid" style="visibility: hidden" />
                                                                        </div>
                                                                    </a>
                                                                @else
                                                                    <a href="{{ url(Config::get('url.main.exchange_programs').'/'.$item->id.'/detail') }}">
                                                                    
                                                                        <div style="background-image: url({{ $item->image }})">
                                                                            <img src="{{ $item->image }}" class="img-fluid" style="visibility: hidden" />
                                                                        </div>
                                                                                                
                                                                    </a> 
                                                                @endif
                                                            </div>

                                                            @if($item->link )
                                                                <a href="{{$item->link}}" target="_blank">
                                                                    <h1 class='truncate_text'>{{ $item->name }}  </h1>                          
                                                                </a> 
                                                            @else
                                                                <a href="{{ url(Config::get('url.main.exchange_programs').'/'.$item->id.'/detail') }}">
                                                                    <h1 class='truncate_text'>{{ $item->name }}  </h1>                              
                                                                </a> 
                                                            @endif
                                                            <div class='poston' style='font-size:0.75em;'>
                                                                    
                                                                <i class='fa fa-clock-o fa'></i> {{ date('d-M-Y', strtotime(@$item->publish_start)) }}
                                                                
                                                            </div>
                            
                                                    </div> 
                                                    @endforeach 
                                                </div> <br>
                                               
                                            @endforeach 
        
                                            <div class='row'>
                                                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style='text-align:right'>
                                                    <div class="pager">
                                                        <div class="pageinfo">
                                                            <span>{{ trans('backoffice/common.text_all_results') }} {{$exchange_bycat_5['exchange_cat']->total() }} {{ trans('backoffice/common.text_item(s)') }}</span>
                                                            <span>{{ trans('backoffice/common.text_page') }} {{ $exchange_bycat_5['exchange_cat']->currentPage () }} {{ trans('backoffice/common.text_from') }} {{ $exchange_bycat_5['exchange_cat']->lastPage() }} {{ trans('backoffice/common.text_page(s)') }}</span>
                                                        </div>		
                                                    </div>	      
                                                </div>
                                            </div>
        
                                            <div class='row'>
                                                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style='text-align:center'>
                                                    {{$exchange_bycat_5['exchange_cat']->links('vendor.pagination.bootstrap-4')}}
                                                </div>
                                            </div> 
        
                                        @else 
        
                                            <div class='row'>
                                                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style='text-align:center'>
                                                    {{trans('main/common.text_not_found_data')}}
                                                </div>
                                            </div>
                                                                           
                                        @endif {{-- @if($catagory[$i]->article_category_id == '1' ) --}}
                                </div> {{-- end div id resherch --}}
                           {{--  ================================================================================================================================  --}}  
    
                        </div> {{-- end div tab --}}
    
                    </div>  {{--end tabbable-line --}}
                </div>
            </div> {{-- end div row--}}                  
</div>{{-- end containner --}}

@endsection


@section('style')
<link rel="stylesheet" href="{{ url('lib/swiper-4.4.0/css/swiper.min.css') }}">

@endsection

@section('script')

<script src="{{ url('lib/swiper-4.4.0/js/swiper.min.js') }}"></script>
 <!-- jQuery library -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

 <!-- Popper JS -->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

 <!-- Latest compiled JavaScript -->
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
 
 <script>

        $(document).ready(function(){
            
            //_รับค่าตัวแปรที่ส่งมาตอนกดเปลี่ยนหน้า เพื่อคง tab ที่เลือกไว้
            var cat_id = "<?php echo $exchange_category_id;?>"
    
            //_ถ้าว่างแสดงว่าเลือก tab ข่าวทั้งหมดอยู่
            if(cat_id == '' || cat_id == '2' ){
                cat_id = 1;
            }

          
           
            //console.log(cat_id);
            //_id tag ที่เกี่ยวข้อง ของ แต่ละ tab
            //cat_id = cat_id+1;
            // var i=0;
            var aryCategory = ['#exprogram-tab-just,#exprogram' //0
                                ,'#exdetail-tab-just,#exdetail'//1
                                ,'#exstudent-tab-just,#exstudent'//2
                                ,'#exteacher-tab-just,#exteacher'//3
                                ,'#exuniversity-tab-just,#exuniversity'//4
                            ];
            
            //aryCategory.forEach(function(selector_item , index) {
            aryCategory.forEach(function(selector_item , index) {
                //console.log(index);

             
                
                if(cat_id == (index+1)){
                    console.log(index);
                    $(selector_item).addClass('active show');
                }else{
                    $(selector_item).removeClass('active show');
                }
            });
        });
    
    </script> 
    
 

@endsection

