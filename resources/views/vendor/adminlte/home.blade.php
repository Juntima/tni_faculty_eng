@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('backoffice/dashboard.text_dashboard') }} 
@endsection

@section('contentheader_title')
	<i class="fa fa-dashboard"></i> {{ trans('backoffice/dashboard.text_dashboard') }}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">

		<div class="row">

		<?php 
            	if( array_intersect(['article/access', 'article/modify'] , Auth::user()->roles()->pluck('name')->toArray()) ) :
            ?>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box">
				<a href="{{ url(Config::get('url.backoffice.articles')) }}">
					<span class="info-box-icon bg-navy"><i class="fa fa-newspaper-o"></i></span>
				</a>

				<div class="info-box-content">
					<span class="info-box-text">{{ trans('backoffice/dashboard.text_news') }}</span>
					<span class="info-box-number">{{number_format($article_number)}}</span>
				</div>
				<!-- /.info-box-content -->
				</div>
				<!-- /.info-box -->
			</div>
			<!-- /.col -->
			<?php endif; ?>

			<?php 
            	if( array_intersect(['banner/access', 'banner/modify'] , Auth::user()->roles()->pluck('name')->toArray()) ) :
            ?>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box">
				<a href="{{ url(Config::get('url.backoffice.banners')) }}">
					<span class="info-box-icon bg-olive"><i class="fa fa-picture-o" ></i></span>
				</a>

				<div class="info-box-content">
					<span class="info-box-text">{{ trans('backoffice/dashboard.text_banner') }}</span>
					<span class="info-box-number">{{number_format($banner_number)}}</span>
				</div>
				<!-- /.info-box-content -->
				</div>
				<!-- /.info-box -->
			</div>
			<!-- /.col -->
			<?php endif; ?>

			<?php 
            	if( array_intersect(['banner_highlight/access', 'banner_highlight/modify'] , Auth::user()->roles()->pluck('name')->toArray()) ) :
            ?>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box">
				<a href="{{ url(Config::get('url.backoffice.banners_highlight')) }}">
					<span class="info-box-icon bg-maroon"><i class="fa fa-tags" aria-hidden="true"></i></span>
				</a>

				<div class="info-box-content">
					<span class="info-box-text">{{ trans('backoffice/dashboard.text_banner_highlight') }}</span>
					<span class="info-box-number">{{number_format($banner_highlight_number)}}</span>
				</div>
				<!-- /.info-box-content -->
				</div>
				<!-- /.info-box -->
			</div>
			<!-- /.col -->
			<?php endif; ?>

			<?php 
            	if( array_intersect(['event/access', 'event/modify'] , Auth::user()->roles()->pluck('name')->toArray()) ) :
            ?>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box">
				<a href="{{ url(Config::get('url.backoffice.events')) }}">
					<span class="info-box-icon bg-orange"><i class="fa fa-calendar"></i></span>
				</a>

				<div class="info-box-content">
					<span class="info-box-text">{{ trans('backoffice/dashboard.text_event_update') }}</span>
					<span class="info-box-number">{{number_format($event_number)}}</span>
				</div>
				<!-- /.info-box-content -->
				</div>
				<!-- /.info-box -->
			</div>
			<!-- /.col -->
			<?php endif; ?>

			<?php 
            	if( array_intersect(['exchange_program/access', 'exchange_program/modify'] , Auth::user()->roles()->pluck('name')->toArray()) ) :
            ?>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box">
					<a href="{{ url(Config::get('url.backoffice.exchange_programs')) }}">
					<span class="info-box-icon bg-blue"><i class="fa fa-graduation-cap" aria-hidden="true"></i></span>
				</a>

				<div class="info-box-content">
					<span class="info-box-text">{{ trans('backoffice/dashboard.text_exchange_program') }}</span>
					<span class="info-box-number">{{number_format($exchange_program_number)}}</span>
				</div>
				<!-- /.info-box-content -->
				</div>
				<!-- /.info-box -->
			</div>
			<!-- /.col -->
			<?php endif; ?>

			<?php 
            	if( array_intersect(['co_operative/access', 'co_operative/modify'] , Auth::user()->roles()->pluck('name')->toArray()) ) :
            ?>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box">
				<a href="{{ url(Config::get('url.backoffice.cooperatives')) }}">
					<span class="info-box-icon bg-green"><i class="fa fa-suitcase" aria-hidden="true"></i></span>
				</a>
				<div class="info-box-content">
					<span class="info-box-text">{{ trans('backoffice/dashboard.text_co_operative') }}</span>
					<span class="info-box-number">{{number_format($co_operative_number)}}</span>
				</div>
				<!-- /.info-box-content -->
				</div>
				<!-- /.info-box -->
			</div>
			<!-- /.col -->
			<?php endif; ?>

			<!-- fix for small devices only -->
			<div class="clearfix visible-sm-block"></div>

			<?php 
            	if( array_intersect(['student_portfolio/access', 'student_portfolio/modify'] , Auth::user()->roles()->pluck('name')->toArray()) ) :
            ?>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box">
				<a href="{{ url(Config::get('url.backoffice.student_portfolios')) }} ">
					<span class="info-box-icon bg-red"><i class="fa fa-trophy" aria-hidden="true"></i></span>
				</a>

				<div class="info-box-content">
					<span class="info-box-text">{{ trans('backoffice/dashboard.text_student_portfolio') }}</span>
					<span class="info-box-number">{{ number_format($student_portfolio_number)}}</span>
				</div>
				<!-- /.info-box-content -->
				</div>
				<!-- /.info-box -->
			</div>
			<!-- /.col -->
			<?php endif; ?>

			<!-- fix for small devices only -->
			<div class="clearfix visible-sm-block"></div>

			<?php 
            	if( array_intersect(['video/access', 'video/modify'] , Auth::user()->roles()->pluck('name')->toArray()) ) :
            ?>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box">
					<a href="{{ url(Config::get('url.backoffice.videos')) }}">
					<span class="info-box-icon bg-yellow"><i class="fa fa-video-camera" aria-hidden="true"></i></span>
				</a>

				<div class="info-box-content">
					<span class="info-box-text">{{ trans('backoffice/dashboard.text_tni_channel') }}</span>
					<span class="info-box-number">{{ number_format($video_number)}}</span>
				</div>
				<!-- /.info-box-content -->
				</div>
				<!-- /.info-box -->
			</div>
			<!-- /.col -->
			<?php endif; ?>

			<?php 
            	if( array_intersect(['ebochure/access', 'ebochure/modify'] , Auth::user()->roles()->pluck('name')->toArray()) ) :
            ?>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box">
					<a href="{{ url(Config::get('url.backoffice.ebochures')) }}">
					<span class="info-box-icon bg-aqua"><i class="fa fa-map" aria-hidden="true"></i></span>
				</a>

				<div class="info-box-content">
					<span class="info-box-text">{{ trans('backoffice/dashboard.text_e_bochure') }}</span>
					<span class="info-box-number">{{ number_format($ebochure_number)}}</span>
				</div>
				<!-- /.info-box-content -->
				</div>
				<!-- /.info-box -->
			</div>
			<!-- /.col -->
			<?php endif; ?>
			
			<?php 
            	if( array_intersect(['admission/access', 'admission/modify'] , Auth::user()->roles()->pluck('name')->toArray()) ) :
            ?>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box">
				<a href="{{ url(Config::get('url.backoffice.admissions')) }}">
					<span class="info-box-icon bg-lime"><i class="fa fa-registered"></i></span>
				</a>

				<div class="info-box-content">
					<span class="info-box-text">{{ trans('backoffice/dashboard.text_admission') }}</span>
					<span class="info-box-number">{{ number_format($admission_number) }}</span>
				</div>
				<!-- /.info-box-content -->
				</div>
				<!-- /.info-box -->
			</div>
			<!-- /.col -->
			<?php endif; ?>

			<?php 
            	if( array_intersect(['finance/access', 'finance/modify'] , Auth::user()->roles()->pluck('name')->toArray()) ) :
            ?>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box">
				<a href="{{ url(Config::get('url.backoffice.finances')) }}">
					<span class="info-box-icon bg-purple "><i class="fa fa-calculator"></i></span>
				</a>

				<div class="info-box-content">
					<span class="info-box-text">{{ trans('backoffice/dashboard.text_finance') }}</span>
					<span class="info-box-number">{{ number_format($finance_number) }}</span>
				</div>
				<!-- /.info-box-content -->
				</div>
				<!-- /.info-box -->
			</div>
			<!-- /.col -->
			<?php endif; ?>		


		</div>
	</div>
@endsection

@section('script')

@endsection
