<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    @if ($user->image)
                    <img src="{{ url('storage/image/' . $user->image) }}" class="avatar-thumbnail img-circle" alt="User Image" />
                    @else 
                    <img src="{{ url('img/user-avatar.jpg') }}" class="avatar-thumbnail img-circle" alt="User Image" />
                    @endif
                </div>
                <div class="pull-left info">
                    <p id="user-name-display" title="{{ Auth::user()->name }}">{{ Auth::user()->name }}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('backoffice/common.text_online') }}</a>
                </div>
            </div>
        @endif

        <form action="{{ url(Config::get('url.backoffice.articles')) }}" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="find" class="form-control" placeholder="{{ __('backoffice/common.text_search') }}..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
        </form>

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">{{ __('backoffice/common.text_menu') }}</li>
            <!-- Optionally, you can add icons to the links -->
            <li class="{{ @$dashboard_page_active }}">
                <a href="{{ url(Config::get('url.backoffice.dashboard')) }}">
                    <i class='fa fa-tachometer'></i> <span>{{ __('backoffice/common.text_dashboard') }}</span>
                </a>
            </li>

            <?php 
                if(array_intersect(['article/access', 'article/modify', 'article_category/access', 'article_category/modify'], 
                Auth::user()->roles()->pluck('name')->toArray())):
            ?>
            <li class="treeview {{ @$article_page_active }}">
                <a href="#"><i class='fa fa-newspaper-o'></i> <span>{{ __('backoffice/common.text_news') }}</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <?php  if(array_intersect(['article/access'], Auth::user()->roles()->pluck('name')->toArray())): ?>
                    <li><a href="{{ url(Config::get('url.backoffice.articles')) }}">{{ __('backoffice/common.text_news') }}</a></li>
                    <?php endif; ?>
                    <?php  if(array_intersect(['article/modify'], Auth::user()->roles()->pluck('name')->toArray())){ ?>
                    <li><a href="{{ url(Config::get('url.backoffice.articles_create')) }}">{{ __('backoffice/common.text_create_news') }}</a></li>
                    <?php } ?>
                    <?php  if(array_intersect(['article_category/access'], Auth::user()->roles()->pluck('name')->toArray())): ?>
                    <li><a href="{{ url(Config::get('url.backoffice.article_categories')) }}">{{ __('backoffice/common.text_news_categories') }}</a></li>
                    <?php endif; ?>
                    <?php  if(array_intersect(['article_category/modify'], Auth::user()->roles()->pluck('name')->toArray())){ ?>
                    <li><a href="{{ url(Config::get('url.backoffice.article_categories_create')) }}">{{ __('backoffice/common.text_create_news_category') }}</a></li>
                    <?php } ?>
                </ul>
            </li>
            <?php endif; ?>

            <?php 
                if(array_intersect(['banner/access', 'banner/modify'], 
                Auth::user()->roles()->pluck('name')->toArray())):
            ?>
            <li class="treeview {{ @$banner_page_active }}">
                <a href="#"><i class="fa fa-picture-o" aria-hidden="true"></i> <span>{{ __('backoffice/common.text_banner') }}</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <?php  if(array_intersect(['banner/access'], Auth::user()->roles()->pluck('name')->toArray())): ?>
                    <li><a href="{{ url(Config::get('url.backoffice.banners')) }}">{{ __('backoffice/common.text_banner') }}</a></li>
                    <?php endif; ?>
                    <?php  if(array_intersect(['banner/modify'], Auth::user()->roles()->pluck('name')->toArray())){ ?>
                    <li><a href="{{ url(Config::get('url.backoffice.banners_create')) }}">{{ __('backoffice/common.text_create_banner') }}</a></li>
                    <?php } ?>
                </ul>
            </li>
            <?php endif; ?>

            <?php 
            if(array_intersect(['banner_highlight/access', 'banner_highlight/modify'], 
            Auth::user()->roles()->pluck('name')->toArray())):
            ?>
            <li class="treeview {{ @$banner_highlight_page_active }}">
                <a href="#"><i class="fa fa-tags" aria-hidden="true"></i> <span>{{ __('backoffice/common.text_banner_highlight') }}</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <?php  if(array_intersect(['banner_highlight/access'], Auth::user()->roles()->pluck('name')->toArray())): ?>
                    <li><a href="{{ url(Config::get('url.backoffice.banners_highlight')) }}">{{ __('backoffice/common.text_banner_highlight') }}</a></li>
                    <?php endif; ?>
                    <?php  if(array_intersect(['banner_highlight/modify'], Auth::user()->roles()->pluck('name')->toArray())){ ?>
                    <li><a href="{{ url(Config::get('url.backoffice.banners_highlight_create')) }}">{{ __('backoffice/common.text_create_banner_highlight') }}</a></li>
                    <?php } ?>
                </ul>
            </li>
            <?php endif; ?>
        
            <?php 
            if(array_intersect(['event/access', 'event/modify'], 
            Auth::user()->roles()->pluck('name')->toArray())):
        ?>
        <li class="treeview {{ @$event_page_active }}">
            <a href="#"><i class="fa fa-calendar" aria-hidden="true"></i> <span>{{ __('backoffice/common.text_event') }}</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
                <?php  if(array_intersect(['event/access'], Auth::user()->roles()->pluck('name')->toArray())): ?>
                <li><a href="{{ url(Config::get('url.backoffice.events')) }}">{{ __('backoffice/common.text_event') }}</a></li>
                <?php endif; ?>
                <?php  if(array_intersect(['event/modify'], Auth::user()->roles()->pluck('name')->toArray())){ ?>
                <li><a href="{{ url(Config::get('url.backoffice.events_create')) }}">{{ __('backoffice/common.text_create_event') }}</a></li>
                <?php } ?>
            </ul>
        </li>
        <?php endif; ?>


            <?php  //exchange_programs
                 if(array_intersect(['exchange_program/access', 'exchange_program/modify', 'exchange_category/access', 'exchange_category/modify'], 
                 Auth::user()->roles()->pluck('name')->toArray())):
           ?>
           <li class="treeview {{ @$exchange_program_page_active}}" >
               <a href="#"><i class="fa fa-graduation-cap" aria-hidden="true"></i> <span>{{ __('backoffice/common.text_exchange_program') }}</span> <i class="fa fa-angle-left pull-right"></i></a>
               <ul class="treeview-menu">
                   <?php  if(array_intersect(['exchange_program/access'], Auth::user()->roles()->pluck('name')->toArray())): ?>
                   <li><a href="{{ url(Config::get('url.backoffice.exchange_programs')) }}">{{ __('backoffice/common.text_exchange_program') }}</a></li>
                   <?php endif; ?>
                   <?php  if(array_intersect(['exchange_program/modify'], Auth::user()->roles()->pluck('name')->toArray())){ ?>
                   <li><a href="{{ url(Config::get('url.backoffice.exchange_programs_create')) }}">{{ __('backoffice/common.text_create_exchange_program') }}</a></li>
                   <?php } ?>
                   <?php  if(array_intersect(['exchange_category/access'], Auth::user()->roles()->pluck('name')->toArray())): ?>
                    <li><a href="{{ url(Config::get('url.backoffice.exchange_categories')) }}">{{ __('backoffice/common.text_exchange_program_categories') }}</a></li>
                    <?php endif; ?>
                    <?php  if(array_intersect(['exchange_category/modify'], Auth::user()->roles()->pluck('name')->toArray())){ ?>
                    <li><a href="{{ url(Config::get('url.backoffice.exchange_categories_create')) }}">{{ __('backoffice/common.text_create_exchange_program_category') }}</a></li>
                    <?php } ?>
               </ul>
           </li>
           <?php endif; ?>


           <?php  //cooperatives
                 if(array_intersect(['co_operative/access', 'co_operative/modify'], 
                 Auth::user()->roles()->pluck('name')->toArray())):
            ?>
          <li class="treeview {{ @$Co_Operative_page_active }}">
              <a href="#"><i class="fa fa-suitcase" aria-hidden="true"></i> <span>{{ __('backoffice/common.text_co_operative') }}</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                  <?php  if(array_intersect(['co_operative/access'], Auth::user()->roles()->pluck('name')->toArray())): ?>
                  <li><a href="{{ url(Config::get('url.backoffice.cooperatives')) }}">{{ __('backoffice/common.text_co_operative') }}</a></li>
                  <?php endif; ?>
                  <?php  if(array_intersect(['co_operative/modify'], Auth::user()->roles()->pluck('name')->toArray())){ ?>
                  <li><a href="{{ url(Config::get('url.backoffice.cooperatives_create')) }}">{{ __('backoffice/common.text_create_co_operative') }}</a></li>
                  <?php } ?>
              </ul>
          </li>
          <?php endif; ?>

            <?php  //student portfolio
                 if(array_intersect(['student_portfolio/access', 'student_portfolio/modify'], 
                 Auth::user()->roles()->pluck('name')->toArray())):
            ?>
            <li class="treeview {{@$student_portfolio_page_active}}">
                <a href="#"><i class="fa fa-trophy" aria-hidden="true"></i> <span>{{ __('backoffice/common.text_student_portfolio') }}</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <?php  if(array_intersect(['student_portfolio/access'], Auth::user()->roles()->pluck('name')->toArray())): ?>
                    <li><a href="{{ url(Config::get('url.backoffice.student_portfolios')) }}">{{ __('backoffice/common.text_student_portfolio') }}</a></li>
                    <?php endif; ?>
                    <?php  if(array_intersect(['student_portfolio/modify'], Auth::user()->roles()->pluck('name')->toArray())){ ?>
                    <li><a href="{{ url(Config::get('url.backoffice.student_portfolios_create')) }}">{{ __('backoffice/common.text_create_student_portfolio') }}</a></li>
                    <?php } ?>
                </ul>
            </li>
            <?php endif; ?>

            <?php  //TNI Channel
                if(array_intersect(['event/access', 'event/modify'], 
                Auth::user()->roles()->pluck('name')->toArray())):
            ?>
            <li class="treeview {{@$video_page_active}}" >
                <a href="#"><i class="fa fa-video-camera" aria-hidden="true"></i> <span>{{ __('backoffice/common.text_tni_channel') }}</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <?php  if(array_intersect(['video/access'], Auth::user()->roles()->pluck('name')->toArray())): ?>
                    <li><a href="{{ url(Config::get('url.backoffice.videos')) }}">{{ __('backoffice/common.text_tni_channel') }}</a></li>
                    <?php endif; ?>
                    <?php  if(array_intersect(['video/modify'], Auth::user()->roles()->pluck('name')->toArray())){ ?>
                    <li><a href="{{ url(Config::get('url.backoffice.videos_create')) }}">{{ __('backoffice/common.text_create_tni_channel') }}</a></li>
                    <?php } ?>
                </ul>
            </li>
            <?php endif; ?>

            <?php  //TNI Ebochure
                if(array_intersect(['ebochure/access', 'ebochure/modify'], 
                Auth::user()->roles()->pluck('name')->toArray())):
            ?>
            <li class="treeview {{@$ebochure_page_active}}">
                <a href="#"><i class="fa fa-map" aria-hidden="true"></i> <span>{{ __('backoffice/common.text_ebochure') }}</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <?php  if(array_intersect(['ebochure/access'], Auth::user()->roles()->pluck('name')->toArray())): ?>
                    <li><a href="{{ url(Config::get('url.backoffice.ebochures')) }}">{{ __('backoffice/common.text_ebochure') }}</a></li>
                    <?php endif; ?>
                    <?php  if(array_intersect(['ebochure/modify'], Auth::user()->roles()->pluck('name')->toArray())){ ?>
                    <li><a href="{{ url(Config::get('url.backoffice.ebochures_create')) }}">{{ __('backoffice/common.text_create_ebochure') }}</a></li>
                    <?php } ?>
                </ul>
            </li>
            <?php endif; ?>

            <?php 
                //_Admission
                if(array_intersect(['admission/access', 'admission/modify'], 
                Auth::user()->roles()->pluck('name')->toArray())):
            ?>
            <li class="treeview {{ @$admission_page_active }}">
                <a href="#"><i class='fa fa-registered'></i> <span>{{ __('backoffice/common.text_admission') }}</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <?php  if(array_intersect(['admission/access'], Auth::user()->roles()->pluck('name')->toArray())): ?>
                    <li><a href="{{ url(Config::get('url.backoffice.admissions')) }}">{{ __('backoffice/admissions.text_admission_lists') }}</a></li>
                    <?php endif; ?>
                    <?php  if(array_intersect(['admission/modify'], Auth::user()->roles()->pluck('name')->toArray())){ ?>
                    <li><a href="{{ url(Config::get('url.backoffice.admissions_create')) }}">{{ __('backoffice/common.text_create_admission') }}</a></li>
                    <?php } ?>
                </ul>
            </li>    
            <?php endif; ?>

            <?php 
            //_finance
            if(array_intersect(['finance/access', 'finance/modify'], 
            Auth::user()->roles()->pluck('name')->toArray())):
        ?>
        <li class="treeview {{ @$finance_page_active }}">
            <a href="#"><i class="fa fa-calculator"></i><span>{{ __('backoffice/common.text_finance') }}</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
                <?php  if(array_intersect(['finance/access'], Auth::user()->roles()->pluck('name')->toArray())): ?>
                <li><a href="{{ url(Config::get('url.backoffice.finances')) }}">{{ __('backoffice/finance.text_finance_lists') }}</a></li>
                <?php endif; ?>
                <?php  if(array_intersect(['finance/modify'], Auth::user()->roles()->pluck('name')->toArray())){ ?>
                <li><a href="{{ url(Config::get('url.backoffice.finances_create')) }}">{{ __('backoffice/common.text_create_finance') }}</a></li>
                <?php } ?>
            </ul>
        </li>    
        <?php endif; ?>



            <?php
            //_Users 
                if(array_intersect(['user/access', 'user/modify'], 
                Auth::user()->roles()->pluck('name')->toArray())):
            ?>
            <li class="treeview {{ @$user_page_active }}">
                <a href="#"><i class='fa fa-user'></i> <span>{{ __('backoffice/common.text_users') }}</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <?php  if(array_intersect(['user/access'], Auth::user()->roles()->pluck('name')->toArray())): ?>
                    <li><a href="{{ url(Config::get('url.backoffice.users')) }}">{{ __('backoffice/common.text_users') }}</a></li>
                    <?php endif; ?>
                    <?php  if(array_intersect(['user/modify'], Auth::user()->roles()->pluck('name')->toArray())){ ?>
                    <li><a href="{{ url(Config::get('url.backoffice.users_create')) }}">{{ __('backoffice/common.text_create_user') }}</a></li>
                    <?php } ?>
                </ul>
            </li>
            <?php endif; ?>

          


            


         </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
