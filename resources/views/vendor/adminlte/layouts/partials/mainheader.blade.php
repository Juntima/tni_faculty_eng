<!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    <a href="{{ url(Config::get('url.backoffice.dashboard')) }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini hidden-xs"><b>T</b>NI</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg hidden-xs"><b>Admin</b>TNI </span>
        <span class="logo-lg visible-xs"><b>Admin</b>TNI </span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">{{ trans('adminlte_lang::message.togglenav') }}</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <img src="{{ url('language/' . $language->directory . '/' . $language->image) }}" /> {{ trans('backoffice/common.text_language') }}
                    </a>
                    <ul class="dropdown-menu">
                        @foreach($languages as $l)
                        <li class="header">
                            <a href="{{ url(Config::get('url.backoffice.language') . '/?code=' . $l->code) }}">
                                <img src="{{ url('language/' . $l->directory . '/' . $l->image) }}" /> {{ $l->name }}
                            </a>
                        </li>
                        @endforeach  
                    </ul>
                </li>
                  
                @if (Auth::guest())
                    <li><a href="{{ url('/register') }}">{{ trans('adminlte_lang::message.register') }}</a></li>
                    <li><a href="{{ url('/login') }}">{{ trans('adminlte_lang::message.login') }}</a></li>
                @else
                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            @if ($user->image)
                            <img src="{{ url('storage/image/' . $user->image) }}" class="avatar-thumbnail user-image"  alt="User Image" />
                            @else 
                            <img src="{{ url('img/user-avatar.jpg') }}" class="avatar-thumbnail user-image"  alt="User Image" />
                            @endif
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">{{ Auth::user()->name }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                @if ($user->image)
                                <img src="{{ url('storage/image/' . $user->image) }}" class="avatar-thumbnail img-circle"  alt="User Image" />
                                @else 
                                <img src="{{ url('img/user-avatar.jpg') }}" class="avatar-thumbnail img-circle" alt="User Image" />
                                @endif
                                <p>
                                    {{ Auth::user()->name }}
                                    <small>
                                        {{ trans('adminlte_lang::message.login') }} 
                                        @if(Auth::user()->last_login)
                                        {{ Auth::user()->last_login->toDayDateTimeString() }}
                                        @endif
                                    </small>
                                </p>
                            </li>
                            <!-- Menu Body -->

                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="{{ url(Config::get('url.backoffice.profile')) }}" class="btn btn-default btn-flat">{{ __('backoffice/common.text_profile') }}</a>
                                </div>
                                <div class="pull-right">
                                    <a href="{{ url('/logout') }}" class="btn btn-default btn-flat"
                                       onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                                 {{ __('backoffice/common.text_logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                        <input type="submit" value="logout" style="display: none;">
                                    </form>

                                </div>
                            </li>
                        </ul>
                    </li>
                @endif

                <!-- Control Sidebar Toggle Button -->
                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>
            </ul>
        </div>
    </nav>

    <!-- 
        Destinare Backoffice version 1.0.0
        Copyright (C) 2018, Developed by Korn <kornthebkk@gmail.com>
    -->
</header>
