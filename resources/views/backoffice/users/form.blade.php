<div class="panel panel-default">
        <div class="panel-body">

           @include('backoffice.partials.error-lists')

            <div class="col-sm-12">

                <div class="form-group">
                    <label for="name">{{ trans('backoffice/users.text_name') }}* : </label>							
                    <div class="input-group">
                        <i aria-hidden="true" class="input-group-addon fa fa-user"></i>
                    {{ Form::text('name', null, ['maxlength' => '100', 'class' => 'form-control', 'placeholder' => 'Name', 'required']) }}
                    </div>
                </div>

                <div class="form-group">
                    <label for="email">{{ trans('backoffice/users.text_email') }}* : </label>	
                    <div class="input-group">
                        <i aria-hidden="true" class="input-group-addon fa fa-envelope"></i>
                    {{ Form::text('email', null, ['maxlength' => '255', 'class' => 'form-control', 'placeholder' => 'E-Mail', 'required']) }}
                    </div>
                </div>

                <div class="form-group">
                    @if(isset($item->id))
                    <label for="password">{{ trans('backoffice/users.text_password') }} : </label>	
                    @else
                    <label for="password">{{ trans('backoffice/users.text_password') }}* : </label>
                    @endif
                    <div class="input-group">
                        <i aria-hidden="true" class="input-group-addon fa fa-key"></i>
                        {{ Form::password('password', ['maxlength' => '50', 'class' => 'form-control', 'placeholder' => 'Password']) }}
                    </div>
                </div>

                <div class="form-group">
                    @if(isset($item->id))
                    <label for="password_confirmation">{{ trans('backoffice/users.text_password_confirmation') }} : </label>
                    @else
                    <label for="password_confirmation">{{ trans('backoffice/users.text_password_confirmation') }}* : </label>
                    @endif
                    <div class="input-group">
                        <i aria-hidden="true" class="input-group-addon fa fa-key"></i>
                        {{ Form::password('password_confirmation', ['maxlength' => '50', 'class' => 'form-control', 'placeholder' => 'Password Confirmation']) }}
                    </div>
                </div>

                <div class="form-group">
                    <label for="status">{{ trans('backoffice/common.text_status') }} : </label>
                    <div class="input-group">
                        <i aria-hidden="true" class="input-group-addon fa fa-check"></i>
                        <select name="status" id="status" class="form-control" required>
                            <option value="1" @if((!isset($item) && old('status') == 1) || (isset($item) && (int)$item->status == 1)) selected="selected" @endif>{{ trans('backoffice/common.text_enable') }}</option>
                            <option value="0" @if((!isset($item) && old('status') == 0) || (isset($item) && (int)$item->status == 0)) selected="selected" @endif>{{ trans('backoffice/common.text_disable') }}</option>
                        </select>
                        
                    </div>
                </div>

                <div class="form-group">
                    <div style="margin-bottom: 5px">{{ trans('backoffice/users.text_image_profile') }}</div>
                    <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail">
                        @if(@$item->thumb)
                        <img src="{{ $item->thumb }}" data-placeholder="{{ url('storage/image/cache/no_image-100x100.png') }}" />
                        @else
                        <img src="{{ url('storage/image/cache/no_image-100x100.png') }}" data-placeholder="{{ url('storage/image/cache/no_image-100x100.png') }}" />
                        @endif
                    </a>
                    <input type="hidden" name="image" id="input-image" value="{{ @$item->image }}" />
                </div>
                
                <div class="form-group">
                    <div class="form-group">
                        <i class="fa fa-lock"></i> <strong>{{ trans('backoffice/users.text_user_privileges') }} :</strong> 
                        <span class="article-category privileges badge badge-pill badge-primary">
                            <label data-toggle="tooltip" title="{{ trans('backoffice/users.text_all_privileges') }}">
                                <input type="checkbox" id="privilegeAll" 
                                @if(count($roles) == count($roleUserData)) checked="checked" @endif />
                                {{ trans('backoffice/users.text_all_privileges') }}
                            </label>
                        </span>
                        </div>

                    @foreach($roles as $role)
                    <span class="article-category privileges badge badge-pill badge-primary privileges-item">
                        <label class="privilege">
                            <input type="checkbox" name="roles[]" id="input-role{{ $role->id }}" value="{{ $role->id }}"
                            
                            @if(in_array($role->id, $roleUserData))
                            checked="checked"
                            @endif
                            
                            > {{ $role->name }}
                        </label>
                    </span>
                    @endforeach
                </div>
                <br>

            </div>
        </div>
</div>

@section('script')
<script>
        $('.article-category input[type="checkbox"]:checked').parent().parent().css('opacity', 1.0);
        $('.article-category input[type="checkbox"]').click(function(){
            if($(this).prop('checked')){
                $(this).parent().parent().css('opacity', 1.0);
            }else{
                $(this).parent().parent().css('opacity', 0.6);
            }
        });

        $('#privilegeAll').click(function() {
            //console.log($('#privilegeAll').prop('checked'));
            if($('#privilegeAll').prop('checked')){
                $('.article-category input[type="checkbox"]').prop('checked', true);
                $('.privileges-item input[type="checkbox"]').parent().parent().css('opacity', 1.0);
            }else{
                $('.article-category input[type="checkbox"]').prop('checked', false);
                $('.privileges-item input[type="checkbox"]').parent().parent().css('opacity', 0.6);
            }
        });

        $('#formSubmit').submit(function() {
            $('.btn-save, .btn-cancel').prop('disabled', true);
            $('.btn-save').html('{{ trans("backoffice/common.text_saving")}}...');
            //return false;
        });
    </script>
@endsection