<ul class="nav nav-tabs">
    <li class="active"><a href="#tab-general" data-toggle="tab">{{ trans('backoffice/common.text_general_tab') }}</a></li>
    <li><a href="#tab-data" data-toggle="tab">{{ trans('backoffice/common.text_data_tab') }}</a></li>
    <li><a href="#tab-image" data-toggle="tab">{{ trans('backoffice/common.text_images_tab') }}</a></li>
</ul>

<div class="tab-content">

    <div class="tab-pane active" id="tab-general">
        
        <div class="tab-pane active" id="tab-general">
            <ul class="nav nav-tabs" id="language">
                @foreach($languages as $language)
                <li>
                    <a href="#language{{ $language->id }}" data-toggle="tab">
                        <img src="{{ url('language/' . $language->directory . '/' . $language->code . '.png') }}" title="{{ $language->name }}" /> {{ $language->name }}
                    </a>
                </li>
                @endforeach
            </ul>
        </div>

        <div class="tab-content">

            @foreach($languages as $language)

            @if($language->id == 1)
            <div class="tab-pane active" id="language{{ $language->id }}">
            @else
            <div class="tab-pane" id="language{{ $language->id }}">
            @endif  
                
                <div class="form-group required">
                    <label for="input-name{{ $language->id }}">{{ trans('backoffice/common.text_name') }}<span>*</span></label>
                    <input type="text" name="article_descriptions[{{$language->id}}][name]" value="{{ @$item->description[$language->id]->name }}" id="input-name{{ $language->id }}" maxlength="255" class="form-control" placeholder="{{ trans('backoffice/common.text_name') }}" />

                    @if($errors->any() && $errors->first('article_descriptions.' . $language->id . '.name'))
                    <div class="text-danger">{{ $errors->first('article_descriptions.' . $language->id . '.name') }}</div>
                    @endif
                </div>

                <div class="form-group required">
                    <label for="input-link{{ $language->id }}">{{ trans('backoffice/articles.text_link') }}<span  title="" > **</span></label>
                    <textarea name="article_descriptions[{{$language->id}}][link]" id="input-link{{ $language->id }}" class="form-control" placeholder="{{ trans('backoffice/common.text_link') }}">{{ @$item->description[$language->id]->link }}</textarea>
                </div>

                <div class="form-group">
                    <label for="input-description{{ $language->id }}">{{ trans('backoffice/common.text_description') }}</label>
                    <textarea name="article_descriptions[{{$language->id}}][description]" id="input-description{{ $language->id }}" data-toggle="summernote" data-lang="th-TH" class="form-control" placeholder="{{ trans('backoffice/common.text_description') }}">{{ @$item->description[$language->id]->description }}</textarea>
                </div>
        
                <div class="form-group required">
                     <label for="input-meta-title{{ $language->id }}">{{ trans('backoffice/common.text_meta_title') }}<span>*</span></label>
                    <input type="text" name="article_descriptions[{{$language->id}}][meta_title]" value="{{ @$item->description[$language->id]->meta_title }}" id="input-meta-title{{ $language->id }}" maxlength="255" class="form-control" placeholder="{{ trans('backoffice/common.text_meta_title') }}" />

                    @if($errors->any() && $errors->first('article_descriptions.' . $language->id . '.meta_title'))
                    <div class="text-danger">{{ $errors->first('article_descriptions.' . $language->id . '.meta_title') }}</div>
                    @endif
                </div>    

                <div class="form-group">
                    <label for="input-meta-description{{ $language->id }}">{{ trans('backoffice/common.text_meta_description') }}</label>
                    <input type="text" name="article_descriptions[{{$language->id}}][meta_description]" value="{{ @$item->description[$language->id]->meta_description }}" id="input-meta-description{{ $language->id }}" maxlength="255" class="form-control" placeholder="{{ trans('backoffice/common.text_meta_description') }}" />
                </div>  

                <div class="form-group">
                    <label for="input-meta-keyword{{ $language->id }}"><span data-toggle="tooltip" title="" data-original-title="{{ trans('backoffice/common.text_serperate_comma') }}">{{ trans('backoffice/common.text_meta_keyword') }}</span></label>
                    <textarea name="article_descriptions[{{$language->id}}][meta_keyword]" id="input-meta-keyword{{ $language->id }}" class="form-control" placeholder="{{ trans('backoffice/common.text_meta_keyword') }}">{{ @$item->description[$language->id]->meta_keyword }}</textarea>
                </div>

                <div class="form-group">
                    <label for="input-tag{{ $language->id }}"><span data-toggle="tooltip" title="" data-original-title="{{ trans('backoffice/common.text_serperate_comma') }}">Tags</span></label>
                    <textarea name="article_descriptions[{{$language->id}}][tag]" id="input-tag{{ $language->id }}" class="form-control" placeholder="{{ trans('backoffice/common.text_tags') }}">{{ @$item->description[$language->id]->tag }}</textarea>
                </div>

               <!--  <div class="form-group required">
                    <label for="input-link{{ $language->id }}">{{ trans('backoffice/articles.text_link') }}<span  title="" > *</span></label>
                    <textarea name="article_descriptions[{{$language->id}}][link]" id="input-link{{ $language->id }}" class="form-control" placeholder="{{ trans('backoffice/common.text_link') }}">{{ @$item->description[$language->id]->link }}</textarea>
                </div> --> 
    
            </div>
            @endforeach

        </div>
        
    </div>

    <div class="tab-pane" id="tab-data">
        <div class="form-group">
            <label for="input-article-categories">{{ trans('backoffice/common.text_link_to_category') }}</label><br />
            @foreach($article_categories as $category)

            <span class="article-category privileges badge badge-pill badge-primary">
                <label class="privilege">
                    <input type="checkbox" name="article_categories[]" id="input-article-categories{{ $category->article_category_id }}" value="{{ $category->article_category_id }}"
                    
                    @if(isset($item) && in_array($category->article_category_id, $item->article_categories_selected))
                    checked="checked"
                    @endif
                    
                    > {{ $category->name }}
                </label>
            </span>

            @endforeach
        </div>

       

        <div class="form-group required">
            <label for="input-publish-start{{ $language->id }}">{{ trans('backoffice/common.text_publish_start') }}</label>
            <input type="text" name="publish_start" value="" id="input-publish-start{{ $language->id }}" class="form-control" placeholder="{{ trans('backoffice/common.text_publish_start') }}" />
            
            @if($errors->any() && $errors->first('publish_start'))
            <div class="text-danger">{{ $errors->first('publish_start') }}</div>
            @endif
        </div>

        <div class="form-group">
            <label for="input-publish-stop{{ $language->id }}">{{ trans('backoffice/common.text_publish_stop') }}</label>
            <input type="text" name="publish_stop" value="" id="input-publish-stop{{ $language->id }}" class="form-control" placeholder="{{ trans('backoffice/common.text_publish_start') }}" />
        </div>

        <div class="form-group">
            <label for="input-status{{ $language->id }}">{{ trans('backoffice/common.text_status') }}</label>
            {{ Form::select('status', ['1' => 'Enable', '0' => 'Disable'], null, ['id' => 'input-status' . $language->id, 'class' => 'form-control'])}}
        </div>
    </div>

    <div class="tab-pane" id="tab-image">
        
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
              <thead>
                <tr>
                  <td class="text-left">{{ trans('backoffice/common.text_cover_image') }} : 975x685 pixel</td>
                </tr>
              </thead>
              <tbody>
                <tr>
                    <td class="text-left">
                        <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail">
                            @if(@$item->thumb)
                            <img src="{{ $item->thumb }}" data-placeholder="{{ url('storage/image/cache/no_image-100x100.png') }}" />
                            @else
                            <img src="{{ url('storage/image/cache/no_image-100x100.png') }}" data-placeholder="{{ url('storage/image/cache/no_image-100x100.png') }}" />
                            @endif
                        </a>
                        <input type="hidden" name="image" id="input-image" value="{{ @$item->image }}" />
                    </td>
                </tr>
              </tbody>
        </table>

        <div class="table-responsive">
            <table id="images" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <td class="text-left">{{ trans('backoffice/common.text_additional_image')}} : 975x475 pixel</td>
                    <td class="text-right">{{ trans('backoffice/common.text_sort_order')}}</td>
                    <td></td>
                </tr>
                </thead>
                <tbody>
                
                @php
                 $image_row = 0;   
                @endphp

                @if(isset($item->additional_image))
                @foreach($item->additional_image as $article_image)
                <tr id="image-row{{ $image_row }}">
                    <td class="text-left">
                        <a href="" id="thumb-image{{ $image_row }}" data-toggle="image" class="img-thumbnail">
                            <img src="{{ $article_image['thumb'] }}" alt="" title="" data-placeholder="{{ $placeholder }}" />
                        </a>
                        <input type="hidden" name="article_image[{{ $image_row }}][image]" value="{{ $article_image['image'] }}" id="input-image{{ $image_row }}" />
                    </td>
                    <td class="text-right">
                        <input type="number" name="article_image[{{ $image_row }}][sort_order]" value="{{ $article_image['sort_order'] }}" placeholder="{{ trans('backoffice/common.text_sort_order')}}" class="form-control" />
                    </td>
                    <td class="text-left">
                        <button type="button" onclick="$('#image-row{{ $image_row }}').remove();" data-toggle="tooltip" title="{{ trans('backoffice/common.text_delete')}}" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>
                    </td>
                </tr>
                @php
                    $image_row++;
                @endphp
                @endforeach
                @endif
                </tbody>
                
                <tfoot>
                <tr>
                    <td colspan="2"></td>
                    <td class="text-left"><button type="button" onclick="addImage();" data-toggle="tooltip" title="{{ trans('backoffice/common.text_add_image')}}" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                </tr>
                </tfoot>
            </table>
        </div>

    </div>

</div>

@section('script')
<script type="text/javascript" src="{{ url('lib/summernote/summernote.js') }}"></script>
<link href="{{ url('lib/summernote/summernote.css') }}" rel="stylesheet" />
<script type="text/javascript" src="{{ url('lib/summernote/summernote-image-attributes.js') }}"></script>
<!-- <script type="text/javascript" src="{{ url('lib/summernote/summernote-ext-browser.js') }}"></script> -->
<script type="text/javascript" src="{{ url('lib/summernote/main.js?ver=1.0') }}"></script>

<link rel="stylesheet" href="{{ url('lib/jquery-datetimepicker/build/jquery.datetimepicker.min.css') }}">
<script src="{{ url('lib/jquery-datetimepicker/build/jquery.datetimepicker.full.js') }}"></script>

<script>
    $('#language a:first').tab('show');

    $('#submitForm').submit(function(){
        $('.btn-save, .btn-cancel').prop('disabled', true);
        $('.btn-save').html('กำลังบันทึกข้อมูล...');
        //return false;
    });

    $('input[name=publish_start]').datetimepicker({ 
        format: 'Y-m-d H:i:s',
        @if(@$item->publish_start)
        value: '{{ $item->publish_start }}',
        @else
        value: '{{ date("Y-m-d H:i:s") }}',
        @endif
    });

    $('input[name=publish_stop]').datetimepicker({ 
        format: 'Y-m-d H:i:s',
        @if(@$item->publish_stop)
        value: '{{ $item->publish_stop }}',
        @endif
    });
</script>

<script>
var image_row = {{ $image_row }};

function addImage() {
    html  = '<tr id="image-row' + image_row + '">';
    html += '  <td class="text-left"><a href="" id="thumb-image' + image_row + '"data-toggle="image" class="img-thumbnail"><img src="{{ $placeholder }}" alt="" title="" data-placeholder="{{ $placeholder }}" /></a><input type="hidden" name="article_image[' + image_row + '][image]" value="" id="input-image' + image_row + '" /></td>';
    html += '  <td class="text-right"><input type="number" name="article_image[' + image_row + '][sort_order]" value="0" placeholder="{{ trans('backoffice/common.text_sort_order')}}" class="form-control" /></td>';
    html += '  <td class="text-left"><button type="button" onclick="$(\'#image-row' + image_row  + '\').remove();" data-toggle="tooltip" title="{{ trans('backoffice/common.text_delete_image')}}" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
    html += '</tr>';

    $('#images tbody').append(html);

    image_row++;
}
</script> 

<script>
    $('.article-category input[type="checkbox"]:checked').parent().parent().css('opacity', 1.0);
    $('.article-category input[type="checkbox"]').click(function(){
        if($(this).prop('checked')){
            $(this).parent().parent().css('opacity', 1.0);
        }else{
            $(this).parent().parent().css('opacity', 0.6);
        }
    });
</script>
@endsection

