<ul class="nav nav-tabs">
        <li class="active"><a href="#tab-general" data-toggle="tab">{{ trans('backoffice/common.text_general_tab') }}</a></li>
        <li><a href="#tab-data" data-toggle="tab">{{ trans('backoffice/common.text_data_tab') }}</a></li>
    </ul>
    
    <div class="tab-content">
    
        <div class="tab-pane active" id="tab-general">
            
            <div class="tab-pane active" id="tab-general">
                <ul class="nav nav-tabs" id="language">
                    @foreach($languages as $language)
                    <li>
                        <a href="#language{{ $language->id }}" data-toggle="tab">
                            <img src="{{ URL::to('language/' . $language->directory . '/' . $language->code . '.png') }}" title="{{ $language->name }}" /> {{ $language->name }}
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
    
            <div class="tab-content">
    
                @foreach($languages as $language)
    
                @if($language->id == 1)
                <div class="tab-pane active" id="language{{ $language->id }}">
                @else
                <div class="tab-pane" id="language{{ $language->id }}">
                @endif  
                    
                    <div class="form-group required">
                        <label for="input-name{{ $language->id }}">{{ trans('backoffice/common.text_name') }}<span>*</span></label>
                        <input type="text" name="exchange_category_descriptions[{{$language->id}}][name]" value="{{ @$item->description[$language->id]->name }}" id="input-name{{ $language->id }}" maxlength="255" class="form-control" placeholder="{{ trans('backoffice/common.text_name') }}" />
    
                        @if($errors->any() && $errors->first('exchange_category_descriptions.' . $language->id . '.name'))
                        <div class="text-danger">{{ $errors->first('exchange_category_descriptions.' . $language->id . '.name') }}</div>
                        @endif
                    </div>
            
                    <div class="form-group">
                        <label for="input-description{{ $language->id }}">{{ trans('backoffice/common.text_description') }}</label>
                        <textarea name="exchange_category_descriptions[{{$language->id}}][description]" id="input-description{{ $language->id }}" data-toggle="summernote" data-lang="th-TH" class="form-control" placeholder="Description">{{ @$item->description[$language->id]->description }}</textarea>
                    </div>
            
                    <div class="form-group required">
                         <label for="input-meta-title{{ $language->id }}">{{ trans('backoffice/common.text_meta_title') }}<span>*</span></label>
                        <input type="text" name="exchange_category_descriptions[{{$language->id}}][meta_title]" value="{{ @$item->description[$language->id]->meta_title }}" id="input-meta-title{{ $language->id }}" maxlength="255" class="form-control" placeholder="{{ trans('backoffice/common.text_meta_title') }}" />
    
                        @if($errors->any() && $errors->first('exchange_category_descriptions.' . $language->id . '.meta_title'))
                        <div class="text-danger">{{ $errors->first('exchange_category_descriptions.' . $language->id . '.meta_title') }}</div>
                        @endif
                    </div>    
    
                    <div class="form-group">
                        <label for="input-meta-description{{ $language->id }}">{{ trans('backoffice/common.text_meta_description') }}</label>
                        <input type="text" name="exchange_category_descriptions[{{$language->id}}][meta_description]" value="{{ @$item->description[$language->id]->meta_description }}" id="input-meta-description{{ $language->id }}" maxlength="255" class="form-control" placeholder="{{ trans('backoffice/common.text_meta_description') }}" />
                    </div>  
    
                    <div class="form-group">
                        <label for="input-meta-keyword{{ $language->id }}"><span data-toggle="tooltip" title="" data-original-title="{{ trans('backoffice/common.text_serperate_comma') }}">Meta Keyword</span></label>
                        <textarea name="exchange_category_descriptions[{{$language->id}}][meta_keyword]" id="input-meta-keyword{{ $language->id }}" class="form-control" placeholder="{{ trans('backoffice/common.text_meta_keyword') }}">{{ @$item->description[$language->id]->meta_keyword }}</textarea>
                    </div>
        
                </div>
                @endforeach
    
            </div>
            
        </div>
    
        <div class="tab-pane" id="tab-data">
            <div class="form-group">
                <label for="input-image">{{ trans('backoffice/common.text_image') }}</label><br />
                <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail">
                    @if(@$item->thumb)
                    <img src="{{ $item->thumb }}" data-placeholder="{{ URL::to('storage/image/cache/no_image-100x100.png') }}" />
                    @else
                    <img src="{{ URL::to('storage/image/cache/no_image-100x100.png') }}" data-placeholder="{{ URL::to('storage/image/cache/no_image-100x100.png') }}" />
                    @endif
                </a>
                <input type="hidden" name="image" id="input-image" value="{{ @$item->image }}" />
            </div>
    
            <div class="form-group">
                <label for="input-sort-order{{ $language->id }}">{{ trans('backoffice/common.text_sort_order') }}</label>
                <input type="number" name="sort_order" value="{{ @$item->sort_order }}" id="input-sort_order{{ $language->id }}" class="form-control" placeholder="Sort Order" />
            </div>
    
            <div class="form-group">
                <label for="input-status{{ $language->id }}">{{ trans('backoffice/common.text_status') }}</label>
                {!! Form::select("status", ['1' => 'Enable', '0' => 'Disable'], null, ['id' => 'input-status' . $language->id, 'class' => 'form-control']) !!}
            </div>
        </div>
    
    </div>
    
    @section('script')
    <script type="text/javascript" src="{{ URL::to('lib/summernote/summernote.js') }}"></script>
    <link href="{{ URL::to('lib/summernote/summernote.css') }}" rel="stylesheet" />
    <script type="text/javascript" src="{{ URL::to('lib/summernote/summernote-image-attributes.js') }}"></script>
    <script type="text/javascript" src="{{ URL::to('lib/summernote/main.js?ver=1.0') }}"></script>
    
    <script>
        $('#language a:first').tab('show');
    
        $('#submitForm').submit(function(){
            $('.btn-save, .btn-cancel').prop('disabled', true);
            $('.btn-save').html('{{ trans('backoffice/common.text_saving') }}...');
            //return false;
        }); 
    </script>
    @endsection
    
    