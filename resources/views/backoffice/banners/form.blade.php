<div class="form-group required">
    <label for="input-name">{{ trans('backoffice/common.text_name') }}<span>*</span></label>
    <input type="text" name="name" value="<?=old('name', @$item->name)?>" id="input-name" class="form-control" placeholder="{{ trans('backoffice/common.text_name') }}" />
    
    @if($errors->any() && $errors->first('name'))
    <div class="text-danger">{{ $errors->first('name') }}</div>
    @endif
</div>

<div class="form-group required">
    <label for="input-image">{{ trans('backoffice/common.text_image') }} 1140x450 pixel<span>*</span></label><br />
    <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail">
        @if(@$item->thumb)
        <img src="{{ $item->thumb }}" data-placeholder="{{ URL::to('storage/image/cache/no_image-100x100.png') }}" />
        @else
        <img src="{{ URL::to('storage/image/cache/no_image-100x100.png') }}" data-placeholder="{{ URL::to('storage/image/cache/no_image-100x100.png') }}" />
        @endif
    </a>
    <input type="hidden" name="image" id="input-image" value="{{ @$item->image }}" />
</div>

<div class="form-group required">
    <label for="input-background_color">{{ trans('backoffice/banners.text_background_color') }}<span>*</span></label>
    <input type="text" name="background_color" value="<?=old('background_color', @$item->background_color)?>" id="input-background_color" class="form-control jscolor" placeholder="{{ trans('backoffice/banners.text_background_color') }}" />
    
    @if($errors->any() && $errors->first('background_color'))
    <div class="text-danger">{{ $errors->first('background_color') }}</div>
    @endif
</div>

<div class="form-group required">
    <label for="input-publish-start">{{ trans('backoffice/common.text_publish_start') }}<span>*</span></label>
    <input type="text" name="publish_start" value="" id="input-publish-start" class="form-control" placeholder="{{ trans('backoffice/common.text_publish_start') }}" />
    
    @if($errors->any() && $errors->first('publish_start'))
    <div class="text-danger">{{ $errors->first('publish_start') }}</div>
    @endif
</div>

<div class="form-group">
    <label for="input-publish-stop">{{ trans('backoffice/common.text_publish_stop') }}</label>
    <input type="text" name="publish_stop" value="" id="input-publish-stop" class="form-control" placeholder="{{ trans('backoffice/common.text_publish_start') }}" />
</div>

<div class="form-group">
    <label for="input-sort-order">{{ trans('backoffice/common.text_sort_order') }}</label>
    <input type="number" name="sort_order" value="<?=old('sort_order', @$item->sort_order)?>" id="input-sort_order" class="form-control" placeholder="Sort Order" />
</div>

<div class="form-group">
    <label for="input-status">{{ trans('backoffice/common.text_status') }}</label>
    {!! Form::select("status", ['1' => 'Enable', '0' => 'Disable'], null, ['id' => 'input-status' . $language->id, 'class' => 'form-control']) !!}
</div>

@section('script')
<link rel="stylesheet" href="{{ url('lib/jquery-datetimepicker/build/jquery.datetimepicker.min.css') }}">
<script src="{{ url('lib/jquery-datetimepicker/build/jquery.datetimepicker.full.js') }}"></script>
<script src="{{ url('lib/jscolor.js') }}"></script>

<script>
    $('#submitForm').submit(function(){
        $('.btn-save, .btn-cancel').prop('disabled', true);
        $('.btn-save').html('{{ trans('backoffice/common.text_saving') }}...');
        //return false;
    }); 

    $('input[name=publish_start]').datetimepicker({ 
        format: 'Y-m-d H:i:s',
        @if(old('publish_start', @$item->publish_start))
        value: '{{ old("publish_start", @$item->publish_start) }}',
        @else
        value: '{{ date("Y-m-d H:i:s") }}',
        @endif
    });

    $('input[name=publish_stop]').datetimepicker({ 
        format: 'Y-m-d H:i:s',
        @if(old('publish_stop', @$item->publish_stop))
        value: '{{ old("publish_stop", @$item->publish_stop) }}',
        @endif
    });
</script>
@endsection

