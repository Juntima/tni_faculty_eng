@extends('adminlte::page')

@section('htmlheader_title')
{{ trans('backoffice/ebochures.text_ebochure_lists') }}
@endsection
@section('contentheader_title')
<i class="fa fa-map" aria-hidden="true"></i> {{ trans('backoffice/ebochures.text_ebochure_lists') }}
@endsection
  
@section('main-content')
	<div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                
                @if(in_array('ebochure/modify', Auth::user()->roles()->pluck('name')->toArray()))
                <div class="text-right">
                    <a href="{{ url(Config::get('url.backoffice.ebochures_create')) }}" data-toggle="tooltip" title="{{ trans('backoffice/common.text_create') }}" class="btn btn-success"><i class="fa fa-plus"></i></a>
                </div>
                <br />
                @endif
                    
                <div class="row">
                    <div class="col-md-6 col-sm-6">
    
                    {{ Form::open(['method' => 'GET']) }}
                        <div class="input-group">
                        <input type="text" name="find" value="{{ @$find }}" class="form-control" placeholder="{{ trans('backoffice/common.text_search') }}..." maxlength="255" />
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-default" data-toggle="tooltip" title="{{ trans('backoffice/common.text_search') }}"><i class="fa fa-search" aria-hidden="true"></i></button>
                            <a href="{{ url(Config::get('url.backoffice.ebochures')) }}" data-toggle="tooltip" title="{{ trans('backoffice/common.text_clear_search') }}" class="btn btn-default"><i class="fa fa-reply"></i></a>
                        </span>
                        </div><!-- /input-group -->
                    {{ Form::close() }}
    
                    </div><!-- /.col-md-6 -->

                </div><!-- /.row -->
                <br>
        
                <div class="panel panel-default">
        
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th width="5%">{{ trans('backoffice/ebochures.text_id') }}</th>
                                <th width="10%">{{ trans('backoffice/ebochures.text_image_cover') }}</th>  
                                <th width="10%">{{ trans('backoffice/ebochures.text_image_highlight') }}</th>           
                                <th width="25%">{{ trans('backoffice/ebochures.text_name') }}</th>                                                                                                              
                                <th width="10%" class="hidden-xs">{{ trans('backoffice/common.text_created_at') }}</th>
                                
                                <th width="10%" class="hidden-xs">{{ trans('backoffice/common.text_created_by') }}</th>
                                
                                <th width="10%" class="hidden-xs">{{ trans('backoffice/common.text_updated_at') }}</th>

                                <th width="10%" class="hidden-xs">{{ trans('backoffice/common.text_updated_by') }}</th>
 
                                <th width="15%">&nbsp;</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($items as $i => $item )
                            <tr class="{{ $item->status ? '' : 'textDeactive' }}">
                                <td scope="row">
                                    {{ sprintf('%03d', $item->ebochure_id) }}
                                </td>
                                <td>
                                    <img src="{{ $item->image }}" class="img-thumbnail" / >
                                 </td> 
                                 <td>
                                    <img src="{{ $item->image_highlight }}" class="img-thumbnail" / >
                                 </td> 
                                <td>
                                    {{ $item->name }}
                                </td>                              
                               
                                <td class="hidden-xs created-set">
                                    {{ $item->ebochure->created_at->format('d M Y') }}
                                </td>

                                <td class="hidden-xs created-set">
                                    {{$username[$i]['created_by']['name']}}
                                    {{--$item->user --}}
                                </td>

                                <td class="hidden-xs created-set">
                                   {{ $item->ebochure->updated_at->format('d M Y') }}
                            
                                </td>

                                <td class="hidden-xs created-set">
                                    {{--$username[$i]['updated_by']['name']--}}
                                    {{$username[$i]['updated_by']['name']}}
                                </td>
                                <td align="center">
                                    <div class="action">
                                        {{ Form::open(['id' => 'deleteForm', 'url' => Config::get('url.backoffice.ebochures') . '/' . $item->id, 'method' => 'delete'])}}

                                        @if(in_array('ebochure/modify', Auth::user()->roles()->pluck('name')->toArray()))
                                        <a data-toggle="tooltip" title="{{ trans('backoffice/common.text_edit') }}" href="{{ url(Config::get('url.backoffice.ebochures') . '/' . $item->id . '/edit') }}" class="btn btn-primary">
                                            <i class="fa fa-cog" aria-hidden="true"></i>
                                        </a>
                                        @endif

                                        @if(in_array('ebochure/modify', Auth::user()->roles()->pluck('name')->toArray()))
                                        <button data-toggle="tooltip" title="{{ trans('backoffice/common.text_delete') }}" type="button" class="btn btn-danger btn-delete">
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                        </button>
                                        @endif

                                        {{ Form::close() }}
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>

                    </table>
                </div>
               <div class="pager">
                    <div class="pageinfo">
                            <span>{{ trans('backoffice/common.text_all_results') }} {{ @$items->total() }} {{ trans('backoffice/common.text_item(s)') }}</span>
                            <span>{{ trans('backoffice/common.text_page') }} {{ $items->currentPage () }} {{ trans('backoffice/common.text_from') }} {{ $items->lastPage() }} {{ trans('backoffice/common.text_page(s)') }}</span>
                    </div>
                    <div>
                        {{ $items->appends(['find' => $find])->links() }}
                    </div>					
                </div>	 

            </div>
        </div>
	</div>
@endsection


    @section('script')
        <script>
        @if(Session::has('success'))
            alertToat("{{ Session::get('success') }}");
        @endif
        @if(Session::has('error'))
            alertToat("{{ Session::get('error') }}", "red");
        @endif

        /* $('.btn-delete').click(function(){
            swalConfirm('{{ trans('backoffice/common.text_confirm_delete') }}', '{{ trans('backoffice/common.text_confirm_delete_message') }}')
        .then(function(result){
        if(result){
            $(_this).parent().submit();
            }
            });
        }); */
        $('.btn-delete').click(function(){
            var _this = this;
            swalConfirm('{{ trans('backoffice/common.text_confirm_delete') }}', '{{ trans('backoffice/common.text_confirm_delete_message') }}')
            .then(function(result){
                if(result){
                    $(_this).parent().submit();
                }
            });
        });
        </script>
    @endsection

