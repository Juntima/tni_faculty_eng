@extends('main.layouts.app')

@section('htmlheader_title'){{ trans('main/home.text_htmltitle') }}@endsection

@section('htmlheader_description'){{ trans('main/common.htmlheader_description') }}@endsection

@section('htmlheader_keywords'){{ trans('main/common.htmlheader_keywords') }}@endsection

@section('content-header')

    <div id="header" class="nav-item">

            <div class="container">
                <!-- <div class="second-header">{{ trans('main/history.text_history') }}</div> -->
                <div class="second-header">
                    @if(@$breadcrumb && count($breadcrumb) > 0)
                    <ul class="breadcrumb">
                        @if(@$breadcrumb)
                            @foreach(@$breadcrumb as $key => $value)
                                @if(@$value['active'] == 'active')
                                    <li class="active">{!! $key !!}</li>
                                @else
                                    <li><a href="{{ url(@$value['url']) }}">{!! $key !!}</a></li>
                                @endif
                            @endforeach
                        @endif
                    </ul>
                    @endif
                </div>
            </div>

    </div>
    
    <!-- <div id="home-course" class="container">

    </div> -->


    <div class="container">

        <div class="swiper-container" >

            <!-- <img src="{{ url('image/catalog/mockup/mockup-banner-5.png') }}"  /> -->
            <div id="history-menu" class="container">
                <!-- <div class="row">
                    <div class="col-xs-4  col-md-4 col-lg-4">

                        @if($latest_video)
                            <div class="history-item">
                                <a href="#">
                                    <img src="{{ $latest_video->image }}" class="img-fluid" />
                                    {{-- <h2>{{ $latest_video->name }}</h2> --}}
                                    <img src="{{ url('img/play-overlay.png') }}" class="play-overlay" />
                                </a>
                            </div>
                                {{-- <a href="#" class="btn-more pull-right">{{ trans('main/home.text_more') }}</a> --}}
                        @endif
                                
                        @if($latest_video)
                            <div class="history-item">
                                <a href="#">
                                    <img src="{{ $latest_video->image }}" class="img-fluid" />
                                    {{-- <h2>{{ $latest_video->name }}</h2> --}}
                                    <img src="{{ url('img/play-overlay.png') }}" class="play-overlay" />
                                </a>
                            </div>
                                {{-- <a href="#" class="btn-more pull-right">{{ trans('main/home.text_more') }}</a> --}}
                        @endif
                    </div>

                    <div class="col-xs-4  col-md-4 col-lg-4">
                                {{-- <h1>{{ trans('main/home.text_tni_channel') }}</h1> --}}

                        @if($latest_video)
                            <div class="history-item">
                                <a href="#">
                                    <img src="{{ $latest_video->image }}" class="img-fluid" />
                                        {{-- <h2>{{ $latest_video->name }}</h2> --}}
                                    <img src="{{ url('img/play-overlay.png') }}" class="play-overlay" />
                                </a>
                            </div>
                                {{-- <a href="#" class="btn-more pull-right">{{ trans('main/home.text_more') }}</a> --}}
                            @endif
                            @if($latest_video)
                            <div class="history-item">
                                <a href="#">
                                    <img src="{{ $latest_video->image }}" class="img-fluid" />
                                    {{-- <h2>{{ $latest_video->name }}</h2> --}}
                                    <img src="{{ url('img/play-overlay.png') }}" class="play-overlay" />
                                </a>
                            </div>
                                {{-- <a href="#" class="btn-more pull-right">{{ trans('main/home.text_more') }}</a> --}}
                        @endif
                    </div>
                   
                    <div class="col-xs-4  col-md-4 col-lg-4">
                            
                         

                            <div class="col-xs-14  col-md-14 col-lg-14">
                                <div class="history-item">
                                    <a href="#">
                                        <div class="card text-white" >
                                            <div class="card-body danger">ทุนการศึกษา</div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            
                            <div class="col-xs-14  col-md-14 col-lg-14">
                                <div class="history-item">
                                    <a href="#">
                                        <div class="card text-white" >
                                            <div class="card-body danger">ค่าเล่าเรียน</div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            
                            <div class="col-xs-14  col-md-14 col-lg-14">
                                <div class="history-item">
                                    <a href="#">
                                        <div class="card text-white" >
                                            <div class="card-body danger">ค่าเล่าเรียน</div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                       

                        </div>
                               
                    </div> {{-- end col --}}
                            
                </div> {{-- end row --}} -->

                <div class='row' style='margin:auto'>
                        <div class="col-xs-8 col-md-8 col-lg-8 " >
                            <div class='row history_thumb_show' >
                                <div class="col-xs-6 col-md-6 col-lg-6 banner_caption" >
                                   
                                    <div class='tranparent_color_container ' >
                                        <a href='#tni_history'>
                                            <img src="{{ $thumb_image[0]['image'] }}" class="img-fluid" > 
                                            <div class='tranparent_color_cover'>    
                                                <div class='centered_bottom'>{{ trans('main/history.text_history') }}</div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-6 col-lg-6 shake">
                                    <a href='#tni_resolution' >
                                        <div class='custom-card banner_caption'>
                                            <span class='centered'><i class="fa fa-handshake-o"></i></i></span>                                        
                                            <span class='centered_bottom'>{{ trans('main/history.text_resolution_vision_mission') }}</span>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class='row history_thumb_show'>
                                <div class="col-xs-6 col-md-6 col-lg-6 shake" >
                                    <a href='#tni_character'>
                                        <div class='custom-card banner_caption' style='background-color:#737373'>
                                            <span class='centered'><i class="fa fa-users"></i></i></span>                                        
                                            <span class='centered_bottom'>{{ trans('main/history.text_character') }}</span>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-xs-6 col-md-6 col-lg-6 banner_caption" >
                                    <div class='tranparent_color_container'>
                                        <a href='#tni_value'>
                                            <img src="{{ $thumb_image[1]['image'] }}" class="img-fluid" />
                                            <div class='tranparent_color_cover'>
                                                <div class='centered_bottom'>{{ trans('main/history.text_value') }}</div>
                                            </div>
                                        </a>       
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4 col-md-4 col-lg-4 " >
                            <a href='#symbolic_color' style='color:inherit;text-decoration:none'>
                                <div class='row history_thumb_show shake'>
                                    <div class="col-xs-12 col-md-12 col-lg-12 custom-card2 " >
                                    {{ trans('main/history.text_symbolic_color') }}
                                    </div>
                                </div>
                            </a>
                            <a href='#principal_buddha_image' style='color:inherit;text-decoration:none'>
                            <div class='row history_thumb_show shake'>
                                <div class="col-xs-12 col-md-12 col-lg-12 custom-card2">
                                    {{ trans('main/history.text_principal_buddha_image_tree') }}
                                </div>
                            </div>
                            </a>

                            <a href='#tree' style='color:inherit;text-decoration:none'>
                                <div class='row history_thumb_show shake' >
                                    <div class="col-xs-12 col-md-12 col-lg-12 custom-card2">
                                        {{ trans('main/history.text_monument_feature') }}
                                    </div>
                                </div> 
                            </a>
                        </div>
                </div>
                
            </div> <!-- id-history-menu -->

        </div>

        <div id="history-line">{{ trans('main/home.text_history') }}</div>
        <div id="history-second-line"></div>

    </div>


@endsection

@section('main-content')

    <div id="history-tni" class="container" >
        <h1 id='tni_history'>{{ trans('main/history.text_history_tni') }}</h1>
        <h4>{{ trans('main/history.text_content_institution_history') }}</h4>
    </div>

    <div id="history-tni" class="container">
            <h1>{{ trans('main/history.text_philosophy') }}</h1>
            <h4>{{ trans('main/history.text_content_philosophy') }}</h4>

    </div>
    <div id="history-tni" class="container">
        <h1>{{ trans('main/history.text_around_tni') }}</h1>
    </div>

    <div id="hero-banner">
            <div class="container">

                <div class="swiper-container">
                    <div class="swiper-wrapper">

                        {{-- @foreach($home_hero_banner as $banner) --}}
                        @foreach($banner_image as $banner)
                            <div class="swiper-slide" data-background_color="{{ $banner->background_color }}">
                                @if($banner->link)
                                <a href="{{ $banner->link }}" target="_blank">
                                    <img src="{{ $banner->image }}" class="img-fluid" alt="{{ $banner->title }}" />
                                </a>
                                @else
                                <img src="{{ $banner->image }}" class="img-fluid" alt="{{ $banner->title }}" />
                                @endif
                            </div>
                        @endforeach

                    </div>

                    <div class="swiper-pagination"></div>
                </div>

            </div>
    </div>

    <div id="history-tni" class="container">
        <h1 id='tni_resolution'>{{ trans('main/history.text_resolution') }}</h1>
        <h4>{{ trans('main/history.text_content_resolution') }}</h4>
    </div>

    <div id="history-tni" class="container">
        <h1>{{ trans('main/history.text_vision') }}</h1>                
        <h4>{{ trans('main/history.text_content_vision') }}</h4>
    </div>
    <div id="history-tni" class="container">
        <h1>{{ trans('main/history.text_mission') }}</h1>
        <ol class='custom_list' >
            @foreach(trans('main/history.text_content_mission') as $text)
                <li>{{$text}}</li>
            @endforeach
        </ol>
    </div>

    <!-- อัตลักษณ์ของบัณฑิต -->
    <div id="history-tni" class="container">
        <h1 id='tni_character'>{{ trans('main/history.text_graduate_identity') }}</h1>
        <h4>{{ trans('main/history.text_content_graduate_identity') }}</h4>
    </div>
    <div id="history-tni" class="container">
        <h1 id=''>{{ trans('main/history.text_institute_identity') }}</h1>
        <h4>{{ trans('main/history.text_content_institute_identity') }}</h4>
    </div>
    <div id="history-tni" class="container">
        <h1>{{ trans('main/history.text_institute_highlight_focus') }}</h1>
        <ol class='custom_list' >
            @foreach(trans('main/history.text_content_institute_highlight_focus') as $text)
                <li>{{$text}}</li>
            @endforeach
        </ol>
    </div>

    <!-- ค่านิยมหลัก -->
    <div id="history-tni" class="container">
        <h1 id='tni_value'>{{ trans('main/history.text_value') }}</h1>
        <ol class='custom_list' >
            @foreach(trans('main/history.text_content_value') as $text)
                <li>{{$text}}</li>
            @endforeach
        </ol>
    </div>
   


    <!-- สัญลักษณ์และสีประจำสถาบัน -->
    <div id="history-tni" class="container">
        <h1 id='symbolic_color'>{{ trans('main/history.text_symbolic_color') }}</h1>
        <h4>
            <img src="{{ @$illustration_image[0]['image'] }}" class='img-fluid'>
        </h4>
        <h4>
            <b>{!! trans('main/history.text_content_symbolic_1') !!}</b>
        </h4>
        <h4>
            <b>{!! trans('main/history.text_content_symbolic_2') !!}</b>
        </h4>
        <ol class='custom_list' >
            @foreach(trans('main/history.text_content_symbolic_3') as $text)
                <li>{!! $text !!}</li>
            @endforeach
        </ol>

        <h4>
            <b>{!! trans('main/history.text_content_symbolic_4') !!}</b>
        </h4>
        <ol class='custom_list' >
            @foreach(trans('main/history.text_content_symbolic_5') as $text)
                <li>{!! $text !!}</li>
            @endforeach
        </ol>

        <h4>
            <b>{!! trans('main/history.text_content_color_1') !!}</b>
        </h4>
        <h4>
            <b>{!! trans('main/history.text_content_color_2') !!}</b>
        </h4>
        <ol class='custom_list' >
            @foreach(trans('main/history.text_content_color_3') as $text)
                <li>{!! $text !!}</li>
            @endforeach
        </ol>
    </div>

    <!-- พระประธานประจำสถาบัน -->
    <div id="history-tni" class="container">
        <h1 id='principal_buddha_image'>{{ trans('main/history.text_content_principal_buddha_image_1') }}</h1>
        <h4>
            <img src="{{ @$illustration_image[1]['image'] }}" class='mx-auto d-block img-thumbnail' style='max-width:20%'>
        </h4><br>
        <h4>
            {!! trans('main/history.text_content_principal_buddha_image_2') !!}
        </h4>
    </div>

    <!-- ต้นไม้ประจำสถาบัน -->
    <div id="history-tni" class="container">
        <h1 id='tree'>{{ trans('main/history.text_content_tree_1') }}</h1>
        <h4 style='margin:auto;text-align:center;'>
            <img src="{{ @$illustration_image[2]['image'] }}"  style='max-width:21%' class='img-thumbnail'>
            <img src="{{ @$illustration_image[3]['image'] }}" style='max-width:40%' class='img-thumbnail'>
        </h4> <br>
        <h4>
            {!! trans('main/history.text_content_tree_2') !!}
        </h4>
    </div>

    <!-- อนุสาวรีย์ผู้ก่อตั้ง -->
    <div id="history-tni" class="container">
        <h1 id='principal_buddha_image'>{{ trans('main/history.text_monument_feature') }}</h1>
        <h4>
            <img src="{{ @$illustration_image[4]['image'] }}" class='mx-auto d-block img-thumbnail' style='max-width:40%'>
        </h4><br>
        <h4>
            {!! trans('main/history.text_content_monument_feature') !!}
        </h4>
    </div>   
@endsection

@section('style')
<link rel="stylesheet" href="{{ url('lib/swiper-4.4.0/css/swiper.min.css') }}">

@endsection

@section('script')
<script src="{{ url('lib/swiper-4.4.0/js/swiper.min.js') }}"></script>
<script>
var homeHeroSwiper = new Swiper('#hero-banner .swiper-container', {
    autoplay: {
        delay: 5000,
        disableOnInteraction: false
    },
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    on: {
        init: function() {
            var background_color = $('#hero-banner .swiper-wrapper>div:nth-child(1)').data('background_color');
            $('#hero-banner').css('background', background_color).addClass('transition-none');
        },
        slideChange: function(e) {
            var background_color = $('#hero-banner .swiper-wrapper>div:nth-child(' + (homeHeroSwiper.activeIndex + 1) + ')').data('background_color');
            $('#hero-banner').css('background', background_color).removeClass('transition-none').addClass('transition-slow');
        }
    }
});

var homeNewsSwiper = new Swiper('#home-news .swiper-container', {
    loop: true,
    effect: 'fade',
    autoplay: {
        delay: 10000,
        disableOnInteraction: false
    },
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
});
</script>

@endsection


