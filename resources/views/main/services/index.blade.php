@extends('main.layouts.app')

@section('htmlheader_title'){{ trans('main/home.text_htmltitle') }}@endsection

@section('htmlheader_description'){{ trans('main/common.htmlheader_description') }}@endsection

@section('htmlheader_keywords'){{ trans('main/common.htmlheader_keywords') }}@endsection


@section('content-header')

@php
    {{--_รับค่าตัวแปรที่ส่งมาตอนกดเปลี่ยนหน้า ใช้ที่ Jquery ด้านล่าง --}}
    $article_category_id = @$_GET['category'];
@endphp

    <div id="header" class="nav-item">

           <!-- <div class="container">
                <div class="second-header">{{ trans('main/allnews.text_news_activity') }}</div>
            </div> -->
            <div class="container">
                <div class="second-header">
                    @if(@$breadcrumb && count($breadcrumb) > 0)
                    <ul class="breadcrumb">
                        @if(@$breadcrumb)
                            @foreach(@$breadcrumb as $key => $value)
                                @if(@$value['active'] == 'active')
                                    <li class="active">{!! $key !!}</li>
                                @else
                                    <li><a href="{{ url(@$value['url']) }}">{!! $key !!}</a></li>
                                @endif
                            @endforeach
                        @endif
                    </ul>
                    @endif
                </div>
            </div>

    </div><!-- End apply-now -->
    <div id="home-course" class="container">

    </div>

@endsection


@section('main-content')
 <div class="container ">
<!-- 
    <div class="row">  
        <div class="col-md-12 col-sm-12 col-xs-12">   
            <div class="tabbable-line">
            
            </div>
        </div>        
    </div>
    @if(@count($article_cat) > 0)
        @foreach($article_cat as $item)
            {!! $item->name !!}
        @endforeach
    @endif -->


        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 mt-4">   
                <div class="tabbable-line">
                    
                    <ul class="nav nav-tabs nav-justified " id="myTabJust" role="tablist">                            
                        <li class="nav-item">
                            <a class="nav-link active" id="student-tab-just" data-toggle="tab" href="#student" role="tab" aria-controls="student-just" aria-selected="true">
                                 <span>{{ trans('main/allnews.text_student') }} </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="aca-service-tab-just" data-toggle="tab" href="#aca-service" role="tab" aria-controls="aca-service-just" aria-selected="false">
                                {{ trans('main/allnews.text_academic_service') }}
                            </a>
                        </li>
               
                    </ul>

                    <div class="tab-content card pt-5" id="myTabContentJust" >
                            <div class="tab-pane fade show active" id="student" role="tabpanel" aria-labelledby="student-tab-just">
                                <h5></h5>
                                @if(count($articles_bycat_2['articles']) > 0 )
                                    @foreach($articles_bycat_2['articles']->chunk(4) as $chunk) {{-- method chunk : แบ่ง object เป็น row row ละ 4 คอลัมน์ --}}
                                        <div class='row'>
                                            @foreach($chunk as $item)       

                                                <div class="home-news-item" >
                                                
                                                        <div> 
                                                            @if($item->link )
                                                            <a href="{{ $item->link}}" target="_blank">
                                                            
                                                                <div style="background-image: url({{ $item->image }})">
                                                                    <img src="{{ $item->image }}" class="img-fluid" style="visibility: hidden" />
                                                                </div>
                                                                                         
                                                            </a> 
                                                            @else
                                                            <a href="{{ url(Config::get('url.main.services').'/'.$item->id.'/detail') }}">
                                                            
                                                                    <div style="background-image: url({{ $item->image }})">
                                                                        <img src="{{ $item->image }}" class="img-fluid" style="visibility: hidden" />
                                                                    </div>
                                                                                             
                                                            </a> 
                                                            @endif

                                                        </div>
                                                        @if($item->link )
                                                            <a href="{{ $item->link}}" target="_blank">
                                                                <h1 class='truncate_text'>{{ $item->name }}  </h1>
                            
                                                            </a> 
                                                        @else
                                                            <a href="{{ url(Config::get('url.main.services').'/'.$item->id.'/detail') }}">
                                                                <h1 class='truncate_text'>{{ $item->name }}  </h1>
                            
                                                            </a> 
                                                        @endif
                                                        <div class='poston' style='font-size:0.75em;'>
                                                                
                                                            <i class='fa fa-clock-o fa'></i> {{ date('d-M-Y', strtotime(@$item->publish_start)) }}
                                                            
                                                        </div>
                        
                                                </div> 
                                            @endforeach 
                                        </div> <br>
                                       
                                        @endforeach 

                                        <div class='row'>
                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style='text-align:right'>
                                                <div class="pager">
                                                    <div class="pageinfo">
                                                        <span>
                                                        {{ trans('backoffice/common.text_all_results') }} {{ $articles_bycat_2['articles']->total() }} {{ trans('backoffice/common.text_item(s)') }}
                                                        </span>
                                                        <span>
                                                        {{ trans('backoffice/common.text_page') }} {{ $articles_bycat_2['articles']->currentPage () }} {{ trans('backoffice/common.text_from') }} {{ $articles_bycat_2['articles']->lastPage() }} {{ trans('backoffice/common.text_page(s)') }}
                                                        </span>
                                                    </div>		
                                                </div>	      
                                            </div>
                                        </div>
    
                                        <div class='row'>
                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style='text-align:center'>
                                                {{$articles_bycat_2['articles']->links('vendor.pagination.bootstrap-4')}}
                                                
                                                {{-- $articles_bycat_2['articles']->appends(array_except(Request::query(),'test_2'))->links('vendor.pagination.bootstrap-4') --}}
                                            </div>
                                        </div> 
    
                                    @else 
    
                                        <div class='row'>
                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style='text-align:center'>
                                                {{trans('main/common.text_not_found_data')}}
                                            </div>
                                        </div>
                                                                       
                                    @endif {{-- @if($catagory[$i]->article_category_id == '1' ) --}}
                            </div> 

                         {{-- ================================================================================================================================  --}}  
        
                            <div class="tab-pane fade" id="aca-service" role="tabpanel" aria-labelledby="aca-service-tab-just">
                                <h5></h5>
                                @if(count($articles_bycat_7['articles']) > 0 )
                                    @foreach($articles_bycat_7['articles']->chunk(4) as $chunk) {{-- method chunk : แบ่ง object เป็น row row ละ 4 คอลัมน์ --}}
                                        <div class='row'>
                                            @foreach($chunk as $item)
                                              
                                                <div class="home-news-item" >
                                                   
                                                    
                                                        <div> 
                                                            @if($item->link )
                                                            <a href="{{ $item->link}}" target="_blank">
                                                            
                                                                <div style="background-image: url({{ $item->image }})">
                                                                    <img src="{{ $item->image }}" class="img-fluid" style="visibility: hidden" />
                                                                </div>
                                                                                         
                                                            </a> 
                                                            @else
                                                            <a href="{{ url(Config::get('url.main.services').'/'.$item->id.'/detail') }}">
                                                            
                                                                    <div style="background-image: url({{ $item->image }})">
                                                                        <img src="{{ $item->image }}" class="img-fluid" style="visibility: hidden" />
                                                                    </div>
                                                                                             
                                                            </a> 
                                                            @endif

                                                        </div>
                                                        @if($item->link )
                                                            <a href="{{ $item->link}}" target="_blank">
                                                                <h1 class='truncate_text'>{{ $item->name }}  </h1>
                            
                                                            </a> 
                                                        @else
                                                            <a href="{{ url(Config::get('url.main.services').'/'.$item->id.'/detail') }}">
                                                                <h1 class='truncate_text'>{{ $item->name }}  </h1>
                            
                                                            </a> 
                                                        @endif
                                                        <div class='poston' style='font-size:0.75em;'>
                                                                
                                                            <i class='fa fa-clock-o fa'></i> {{ date('d-M-Y', strtotime(@$item->publish_start)) }}
                                                            
                                                        </div>
                        
                                                </div> 
                                            @endforeach 
                                        </div> <br>
                                       
                                        @endforeach 

                                        <div class='row'>
                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style='text-align:right'>
                                                <div class="pager">
                                                    <div class="pageinfo">
                                                        <span>
                                                        {{ trans('backoffice/common.text_all_results') }} {{ $articles_bycat_7['articles']->total() }} {{ trans('backoffice/common.text_item(s)') }}
                                                        </span>
                                                        <span>
                                                        {{ trans('backoffice/common.text_page') }} {{ $articles_bycat_7['articles']->currentPage () }} {{ trans('backoffice/common.text_from') }} {{ $articles_bycat_7['articles']->lastPage() }} {{ trans('backoffice/common.text_page(s)') }}
                                                        </span>
                                                    </div>		
                                                </div>	      
                                            </div>
                                        </div>
    
                                        <div class='row'>
                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style='text-align:center'>
                                                {{$articles_bycat_7['articles']->links('vendor.pagination.bootstrap-4')}}
                                                
                                                {{-- $articles_bycat_2['articles']->appends(array_except(Request::query(),'test_2'))->links('vendor.pagination.bootstrap-4') --}}
                                            </div>
                                        </div> 
    
                                    @else 
    
                                        <div class='row'>
                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style='text-align:center'>
                                                {{trans('main/common.text_not_found_data')}}
                                            </div>
                                        </div>
                                                                       
                                    @endif {{-- @if($catagory[$i]->article_category_id == '1' ) --}}
                                </div> {{-- end div id port  --}}

                      
                 
                    </div> {{-- end div tab --}}

                </div> 
            </div>
        </div> {{-- end div row--}}

    </div>{{-- end containner --}}

<br>
@endsection

@section('style')
<link rel="stylesheet" href="{{ url('lib/swiper-4.4.0/css/swiper.min.css') }}">

@endsection

@section('script')

<script src="{{ url('lib/swiper-4.4.0/js/swiper.min.js') }}"></script>
 <!-- jQuery library -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

 <!-- Popper JS -->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

 <!-- Latest compiled JavaScript -->
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

<script>

    $(document).ready(function(){
        
        //_รับค่าตัวแปรที่ส่งมาตอนกดเปลี่ยนหน้า เพื่อคง tab ที่เลือกไว้
        var cat_id = "<?php echo $article_category_id;?>"

        //_ถ้าว่างแสดงว่าเลือก tab ข่าวทั้งหมดอยู่
        if(cat_id == ''){
            cat_id = 0;
        }  
       
        //console.log(cat_id);
        //_id tag ที่เกี่ยวข้อง ของ แต่ละ tab
        var aryCategory = ['#student-tab-just,#student' 
                            ,'#aca-service-tab-just,#aca-service'//1
                            // ,'#student-tab-just,#student'//2
                            // // ,'#co_operative-tab-just,#co_operative'
                            // // ,'#exchange_scholarship-tab-just,#exchange_scholarship'
                            // ,'#research-tab-just,#research'
                        ];
        
        aryCategory.forEach(function(selector_item , index) {
            
            if(cat_id == index){
                $(selector_item).addClass('active show');
            }else{
                $(selector_item).removeClass('active show');
            }
        });
    });

</script> 

@endsection

