@extends('main.layouts.app')

@section('htmlheader_title'){{ trans('main/home.text_htmltitle') }}@endsection

@section('htmlheader_description'){{ trans('main/common.htmlheader_description') }}@endsection

@section('htmlheader_keywords'){{ trans('main/common.htmlheader_keywords') }}@endsection



@section('content-header')
<?php
//_ตัวแปรรับมาจากปุ่มในหน้าโฮม (ปุ่มใต้แบนเนอร์หลัก)
$faculty_clicked = isset($_GET['f']) ? $_GET['f'] : '' ;
$master_clicked = isset($_GET['m']) ? $_GET['m'] : '' ;
?>
    <div id="header" class="nav-item">
            <!-- <div class="container">
                <div class="second-header">{{ trans('main/home.text_faculty') }}</div>
            </div> -->
            <div class="container">
                <div class="second-header">
                    @if(@$breadcrumb && count($breadcrumb) > 0)
                    <ul class="breadcrumb">
                        @if(@$breadcrumb)
                            @foreach(@$breadcrumb as $key => $value)
                                @if(@$value['active'] == 'active')
                                    <li class="active">{!! $key !!}</li>
                                @else
                                    <li><a href="{{ url(@$value['url']) }}">{!! $key !!}</a></li>
                                @endif
                            @endforeach
                        @endif
                    </ul>
                    @endif
                </div>
            </div>
    </div><!-- End apply-now -->

@endsection

@section('main-content')

<div id="fac-line-it-white" class="container"> <!-- ชื่อตณะ และรายละเอียด -->
    <h1>{{ trans('main/faculty_it.text_name') }}</h1>
    <h5 class="fac_detail"> {{ trans('main/faculty_it.text_detail') }} </h5>
    <a href="https://it.tni.ac.th/about-it/" target="_blank">
        <h5 style="color:#302b82;font-weight: 600; text-align:center;"><b>{{ trans('main/faculty_it.text_more') }}</h5>
    </a>
</div>

<div id="fac-line-it"> <!-- ชื่อปริญญาและสาขาวิชา -->
    <div class="container">
        <div class="row" >
            <div class="col-xs-6 col-md-6 col-lg-6">
                <div class="fac-item">
                    <img src="img/faculty/it/it.jpg" class="img-fluid" />
                </div>
            </div>
            <div class="col-xs-6  col-md-6 col-lg-6">
                <div class="fac-item">                                           
                    <h2 style="text-align:left;">{{ trans('main/faculty_it.text_program_name') }}</h2>
                    @if(count($name_course) > 0 )
                        @for($i=0;$i<count($name_course);$i++)
                            <h4 style="text-align:left; ">{{$name_course[$i]['topic'] }}</h4>
                            <h4 style="text-align:left;" >
                                <ul style="list-style:circle;">
                                    <li>{{$name_course[$i]['name'] }}</li>
                                </ul>                    
                            </h4>
                        @endfor                             
                    @endif
                </div>
            </div>
        </div>
    </div> 
</div>

<div id="fac-line-it-white"> <!-- โอกาสในการประกอบอาชีพ -->
    
    <h1>{{trans('main/faculty_it.text_fjob')}}</h1>
    <div class="container">
        <div class="row" >
            <div class="col-xs-6 col-md-6 col-lg-6">
                <div class="fac-item">
                    <img src="img/faculty/it/it.jpg" class="img-fluid" />
                </div>
            </div>
            <div class="col-xs-6  col-md-6 col-lg-6">
                <div class="fac-item" >               
                    <h5 style="color:#767676; text-align:justify">
                        <ol>  
                            @foreach(trans('main/faculty_it.text_job_detail') as $job_list)
                                <li>{{$job_list}}</li>
                            @endforeach 
                        </ol>                    
                    </h5>                
                </div>
            </div>
        </div>
    </div> 
</div>

<div id="fac-line-it-gray"> <!-- โครงสร้างหลักสูตร -->
    <h1>{{trans('main/faculty_it.text_struct')}}</h1>
    <div class="container">
            <div class="row" >             
                <div class="col-xs-12  col-md-12 col-lg-12"> 
                    <div class="fac-item" >
                        <table class="table table-striped">                  
                            <tbody class="unit ">
                                <tr class="table-light " >
                                    <td style="text-align:left"><b> {{trans('main/faculty_it.text_main_couse_name')}} </b></td>
                                    <td><b>{{trans('main/faculty_it.text_main_couse_unit')}}<b></td>
                                    <td><b>{{trans('main/faculty_it.text_unit')}}<b></td>
                                </tr>

                                @if(count($struct_detail) > 0 )                             
                                        @for($i=0;$i<count($struct_detail);$i++)
                                            <tr class="table-default" id="det">
                                                <td id="topic">{!!$struct_detail[$i]['name'] !!} </td>
                                                <td>{{ $struct_detail[$i]['detail'] }} </td>
                                                <td>{{trans('main/faculty_it.text_unit')}}</td>
                                            </tr>
                                        @endfor                        
                                @endif                           
                            </tbody>               
                        </table>                       
                    </div>
                </div>
    
            </div><!-- end row -->
    </div>
</div>

<div id="fac-line-it-white"><!-- แผนการศึกษา -->
    <h1>{{trans('main/faculty_it.text_program')}}</h1>
    <div class="container">   
        <div class="row" >      
<?php
    $text_sum = trans('main/faculty_it.text_sum') ;
    $text_sub_id = trans('main/faculty_it.text_sub_id');
    $text_sub_name = trans('main/faculty_it.text_sub_name');
    $text_unit = trans('main/faculty_it.text_unit') ;   
 
$fac_template1 = <<<EOT

<div class="col-xs-6  col-md-6 col-lg-6"> 
                    <div class="fac-item" >
                        <table class="table table-striped">                  
                            <tbody class="unit ">
                                <tr class="table-defult " >
                                    <td style="text-align:center;font-size:26px;" colspan="3"><b> %s </b></td>
                                </tr>

                                <tr class="table-light " >
                                    <td style="text-align:left;"><b> {$text_sub_id} </b></td>
                                    <td style="text-align:left;"><b> {$text_sub_name} </b></td>
                                    <td style="text-align:center;"><b> {$text_unit} </b></td>
                                </tr>
EOT;

$fac_template2 = <<<EOT
                               
                                 <tr class="table-light" >
                                    <td style="text-align:left;">%s</td>
                                    <td style="text-align:left;color:#302b82">%s</td>
                                    <td style="text-align:center;">%s</td>                                               
                                </tr>

EOT;

$fac_template3 = <<<EOT
                             
                                <tr class="table-light" >
                                    <td style="text-align:right;" colspan="2"><b> {$text_sum} </b></td> 
                                    <td style="text-align:center;" ><b>  %s  </b></td>                                                                                         
                                </tr>      
                            </tbody>               
                        </table>                       
                    </div>
</div>

EOT;
// --- ปี1 เทอม 1 --- //
$fac_info_result = sprintf($fac_template1 , trans('main/faculty_it.text_year1_semester1'));    
                    
                    if(count($subject_y1s1) > 0 ){                                             
                        for($i=0;$i<count($subject_y1s1);$i++){                           
$fac_info_result .= sprintf($fac_template2 
                                , $subject_y1s1[$i]['sub_id']                              
                                ,$subject_y1s1[$i]['sub_name']
                                ,$subject_y1s1[$i]['sub_unit']                                                                                 
                                );
                        }  
                    }                      
                                                         
$fac_info_result.= sprintf($fac_template3 ,trans('main/faculty_it.text_unit_sum'));
                                
                                  
// --- ปี1 เทอม 2 --- //
$fac_info_result .= sprintf($fac_template1 , trans('main/faculty_it.text_year1_semester2') );
 
                    if(count($subject_y1s2) > 0 ){                                             
                        for($i=0;$i<count($subject_y1s2);$i++){                          
$fac_info_result .= sprintf($fac_template2 
                                , $subject_y1s2[$i]['sub_id']                              
                                ,$subject_y1s2[$i]['sub_name']
                                ,$subject_y1s2[$i]['sub_unit']                                                                                 
                                );
                        }  
                    }                           
                                                         
$fac_info_result.= sprintf($fac_template3,trans('main/faculty_it.text_unit_sum') );
                                
                                 


// แสดงผลทั้งหมด
    echo $fac_info_result;

?> 
               
        </div><!-- end row -->
    </div>
</div>


































<div id="fac-line-footer" hidden>
<h2> {{ trans('main/faculty.text_alumni')}}</h2>
            <div class="swiper-container">
                <div class="swiper-wrapper" id="alumni">

                    <div class="swiper-slide" >
                        <img src="img/alumni/a1.jpg"  id ="img-graduate"  >
                        <h4 style="color:#fff; font-weight: 600;">
                            <i class="fa fa-quote-left"></i>
                                 &nbsp;   {{ trans('main/faculty.text_comment1')}}  &nbsp;
                            <i class="fa fa-quote-right"></i>  <br><br>
                            {{-- <h5 style="color:#fff; font-weight: 600;"> {{ trans('main/faculty.text_job1')}} </h5> --}}
                        </h4>   <br/>
                        <h4 style="color:#fff; font-weight: 600; ">{{ trans('main/faculty.text_major1')}} </h4> <br><br>
                        <h5 style="color:#fff; "> {{ trans('main/faculty.text_name1')}}   </h5>
                    </div>
                    
                    <div class="swiper-slide" >
                        <img src="img/alumni/a2.jpg"  id ="img-graduate"  >
                        <h4 style="color:#fff; font-weight: 600;">
                            <i class="fa fa-quote-left"></i>
                                 &nbsp;  {{ trans('main/faculty.text_comment2')}}  &nbsp;
                            <i class="fa fa-quote-right"></i>  <br><br>
                            {{-- <h5 style="color:#fff; font-weight: 600;">{{ trans('main/faculty.text_job2')}}</h5> --}}
                        </h4>   <br/>
                        <h4 style="color:#fff; font-weight: 600; ">{{ trans('main/faculty.text_major2')}} </h4> <br><br>
                        <h5 style="color:#fff; ">{{ trans('main/faculty.text_name2')}}</h5>
                    </div>

                    <div class="swiper-slide" >
                        <img src="img/alumni/a3.jpg"  id ="img-graduate"  >
                        <h4 style="color:#fff; font-weight: 600;">
                            <i class="fa fa-quote-left"></i>
                                 &nbsp;  {{ trans('main/faculty.text_comment3')}}  &nbsp;
                            <i class="fa fa-quote-right"></i>  <br><br>
                            {{-- <h5 style="color:#fff; font-weight: 600;"> {{ trans('main/faculty.text_job3')}} </h5> --}}
                        </h4>   <br/>
                        <h4 style="color:#fff; font-weight: 600; ">{{ trans('main/faculty.text_major3')}}</h4> <br><br>
                        <h5 style="color:#fff; "> {{ trans('main/faculty.text_name3')}}</h5>
                    </div>
                    <!-- add 19-12-2019 juntima -->
                    <div class="swiper-slide" >
                        <img src="img/alumni/a4.jpg"  id ="img-graduate"  >
                        <h4 style="color:#fff; font-weight: 600;">
                            <i class="fa fa-quote-left"></i>
                                 &nbsp;  {{ trans('main/faculty.text_comment4')}}  &nbsp;
                            <i class="fa fa-quote-right"></i>  <br><br>
                            {{-- <h5 style="color:#fff; font-weight: 600;"> {{ trans('main/faculty.text_job3')}} </h5> --}}
                        </h4>   <br/>
                        <h4 style="color:#fff; font-weight: 600; ">{{ trans('main/faculty.text_major4')}}</h4> <br><br>
                        <h5 style="color:#fff; "> {!! trans('main/faculty.text_name4')!!}</h5>
                    </div>

                    <div class="swiper-slide" >
                        <img src="img/alumni/a5.jpg"  id ="img-graduate"  >
                        <h4 style="color:#fff; font-weight: 600;">
                            <i class="fa fa-quote-left"></i>
                                 &nbsp;  {{ trans('main/faculty.text_comment5')}}  &nbsp;
                            <i class="fa fa-quote-right"></i>  <br><br>
                            {{-- <h5 style="color:#fff; font-weight: 600;"> {{ trans('main/faculty.text_job3')}} </h5> --}}
                        </h4>   <br/>
                        <h4 style="color:#fff; font-weight: 600; ">{{ trans('main/faculty.text_major5')}}</h4> <br><br>
                        <h5 style="color:#fff; "> {!!trans('main/faculty.text_name5')!!}</h5>
                    </div>

                    <div class="swiper-slide" >
                        <img src="img/alumni/a6.jpg"  id ="img-graduate"  >
                        <h4 style="color:#fff; font-weight: 600;">
                            <i class="fa fa-quote-left"></i>
                                 &nbsp;  {{ trans('main/faculty.text_comment6')}}  &nbsp;
                            <i class="fa fa-quote-right"></i>  <br><br>
                            {{-- <h5 style="color:#fff; font-weight: 600;"> {{ trans('main/faculty.text_job3')}} </h5> --}}
                        </h4>   <br/>
                        <h4 style="color:#fff; font-weight: 600; ">{{ trans('main/faculty.text_major6')}}</h4> <br><br>
                        <h5 style="color:#fff; "> {!! trans('main/faculty.text_name6')!!}</h5>
                    </div>
                    
                </div>
                <div class="swiper-pagination-graduate"></div>
            </div>
</div>    
 

@endsection

@section('style')
<link rel="stylesheet" href="{{ url('lib/swiper-4.4.0/css/swiper.min.css') }}">

@endsection

@section('script')
<script src="{{ url('lib/swiper-4.4.0/js/swiper.min.js') }}"></script>
 <!-- jQuery library -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

 <!-- Popper JS -->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

 <!-- Latest compiled JavaScript -->
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script>
var homeHeroSwiper = new Swiper('#fac-line-footer .swiper-container', {
    loop: true,
    autoplay: {
        delay: 5000,
        disableOnInteraction: false
    },
    pagination: {
        el: '.swiper-pagination-graduate',
        clickable: true,
    },

});

</script>

<style>
    [id^='fac_thumbnal'],[id^='level_']{
          cursor:pointer;
    }   
</style>



@endsection

