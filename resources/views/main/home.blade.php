@extends('main.layouts.app')

@section('htmlheader_title'){{ trans('main/home.text_htmltitle') }}@endsection

@section('htmlheader_description'){{ trans('main/common.htmlheader_description') }}@endsection

@section('htmlheader_keywords'){{ trans('main/common.htmlheader_keywords') }}@endsection



@section('content-header')
<div id="hero-banner" >
        <div class="container">
            
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    
                    @foreach($home_hero_banner as $banner)
                        <div class="swiper-slide" data-background_color="{{ $banner->background_color }}">
                            @if($banner->link)
                            <a href="{{ $banner->link }}" target="_blank">
                                <img src="{{ $banner->image }}" class="img-fluid" alt="{{ $banner->title }}" />
                               
                            </a>
                            
                            @else
                            <img src="{{ $banner->image }}" class="img-fluid" alt="{{ $banner->title }}" />
                            
                            @endif
                        </div>
                    @endforeach
    
                </div>
    
                <div class="swiper-pagination"></div>
            </div>
    
        </div>
</div> 

<div id="home-course" > <!--course-->
    <div class="container">
    
    <div id="major">  
        <div class="row">
            <div class="col-12 col-md-12 col-xl-12 col-lg-12 mt-5">     
                <a href="{{ url(Config::get('url.main.major_ae')) }}" class="shake"><img src="img/icon/07.png" class="mb-3">     <br> AE  </a>
                <a href="{{ url(Config::get('url.main.major_le')) }}" class="shake"><img src="img/icon/06-IE.png" class="mb-3">  <br> LE  </a>
                <a href="{{ url(Config::get('url.main.major_ee')) }}" class="shake"><img src="img/icon/03.png" class="mb-3">     <br> EE  </a>
                <a href="{{ url(Config::get('url.main.major_ie')) }}" class="shake"><img src="img/icon/04.png" class="mb-3">     <br> IE  </a>        
                <a href="{{ url(Config::get('url.main.major_ce')) }}" class="shake"><img src="img/icon/05-CE.png" class="mb-3">  <br> CE  </a>
                <a href="{{ url(Config::get('url.main.major_dge')) }}" class="shake"><img src="img/icon/02.png" class="mb-3">     <br> DGE (INTER)</a>
                <a href="{{ url(Config::get('url.main.major_met')) }}" class="shake"><img src="img/icon/01-MET.png" class="mb-3"> <br> MET (MASTER) </a>
            </div>
        </div>
   
    </div><br>
    <h5 style="font-weight: 600;text-align:center; " >
        <a href="{{ url(Config::get('url.main.major')) }}"  id="more">{{ trans('main/home.text_fac_more') }}</a>
    </h5><br>
</div>
</div>


    <div  id="home-news-event" class="container mt-5">
        <div class="row">
            <div class="col-sm-12 col-md-8 col-lg-9 col-xl-9">
                <div class="news_home">

                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="section_title text-center mb-80" id="home-news" >
                                    <h1 >{{ trans('main/home.text_highlight_activities') }}</h1>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            @for($i=0; $i < count($articles); $i++)
                                <div class="col-xl-6 col-md-6 col-lg-6">
                                    <div class="news_list d-flex align-items-center">                            
                                        <div> 
                                            @if($articles[$i]->link )
                                                <a href="{{ $articles[$i]->link }}" target="_blank"> 
                                                    <img src="{{ $articles[$i]->image }}"  class="thumb" alt=""> 
                                                </a>                                     
                                             @else
                  
                                                <a href="{{ url(Config::get('url.main.news').'/'.$articles[$i]->id.'/detail') }}">                                              
                                                    <img src="{{ $articles[$i]->image }}"  class="thumb" alt=""> 
                                                </a>                                        
                                            @endif  
                                        </div>

                                        <div class='truncate_text_event' id="news-topic">
                                            @if($articles[$i]->link )                                         
                                                <a href="{{ $articles[$i]->link }}" target="_blank"  >
                                                    <span class="badge badge-pill badge-danger"> ข่าวประกาศ</span>
                                                    <h5 class="mr-5">{{ $articles[$i]->name }}</h5>
                                                    <p class="mr-5">                                                       
                                                       <i class='fa fa-clock-o fa'></i> {{ date('d-M-Y', strtotime( $articles[$i]->publish_start)) }}                                                      
                                                   </p>  
                                                </a>
                                            @else
                                                <a href="{{ url(Config::get('url.main.news').'/'.$articles[$i]->id.'/detail') }}" >                                                
                                                   <span class="badge badge-pill badge-danger"> ข่าวประกาศ</span>
                                                    <h5 class="mr-5">{{ $articles[$i]->name }}</h5>
                                                     <p class="mr-5">                                                        
                                                        <i class='fa fa-clock-o fa'></i> {{ date('d-M-Y', strtotime( $articles[$i]->publish_start)) }}                                                        
                                                    </p>                                           
                                                </a>
                                            @endif                                        
                                        </div> 

                                    </div> <!-- end div news_list -->
                                </div> <!-- end col-6 -->

                            @endfor

                        </div> <!-- end row -->
                    </div> <!-- end container-->

                </div> <!-- end div news_home -->
                    <a href="{{ url(Config::get('url.main.news')) }}" class="btn-more pull-right">{{ trans('main/home.text_news_more' )}}</a>   {{--  ปุ่ม more  --}}
                    
            </div> <!-- end div col-->
           
            <div class="col-sm-12 col-md-4 col-lg-3 col-xl-3" >
                <div id="home-event">
                    <h1>{{ trans('main/home.text_event_update') }}</h1>
                    @if(count($event_update) > 0)
                        <ul class="timeline">
                            @foreach($event_update as $event)
                            <li>
                                @if($event->link )
                                    <a href="{{ $event->link }}" target="_blank">{{ $event->month }}</a>
                                    <a href="{{ $event->link }}" target="_blank" class='truncate_text_event'>{{ $event->name }}<br>&nbsp;</a>
                                @else
                                    <a href="{{ url(Config::get('url.main.events').'/'.$event->id.'/detail') }}">{{ $event->month }}</a>
                                    <a href="{{ url(Config::get('url.main.events').'/'.$event->id.'/detail') }}" class='truncate_text_event'>{{ $event->name }}<br>&nbsp;</a>
                                @endif
                                <div class="timeline-day">{{ $event->day }}</div>
                            </li>
                            @endforeach
                        </ul>
                    @else
                        <div style='text-align:center;margin-top:10px'><b>{{ trans('main/common.text_not_found_data') }}</b></div>
                    @endif
                    <a href="{{ url(Config::get('url.main.events')) }}" class="btn-more pull-right">{{ trans('main/home.text_event_more' )}}</a>
                </div>
            </div><!--- end col --->

        </div> <!--- end row --->
   
    </div> <!--- end container  --->
@endsection


@section('main-content')

<div id="home-nav-pec">
    <div class="container">
        <div class="row">
            @foreach($nav_pec as $pec)
            <div class="home-nav-pec-item col-sm-12 col-md-4 col-lg-4 col-xl-4">
                <div>
                    <!-- <div style="background-image: url({{ $pec->image }})">
                        @if($pec->link)
                        <a href="{{ $pec->link }}">
                            <img src="{{ $pec->image }}" class="img-fluid" style="visibility: hidden;"/>
                        </a>
                        @else
                        <img src="{{ $pec->image }}" class="img-fluid" />
                        @endif
                    </div> -->

                    @if($pec->link)
                        <a href="{{ $pec->link }}">
                            <div style="background-image: url({{ $pec->image }})">
                                <img src="{{ $pec->image }}" class="img-fluid" style="visibility: hidden;"/>
                            </div>
                        </a>
                    @else
                        <div style="background-image: url({{ $pec->image }})">
                            <img src="{{ $pec->image }}" class="img-fluid" style="visibility: hidden;"/>
                        </div>
                    @endif


                </div>

                @if($pec->link)
                <a href="{{ $pec->link }}" ><h1>{{ $pec->name }}</h1></a>
                @else
                <h1>{{ $pec->name }}</h1>
                @endif
            </div>
            @endforeach
        </div>
    </div>
</div>


<div id="home-learn2know" hidden>
    <div class="container">
        <div>
            <h1>{{ trans('main/home.text_learn2know_heading') }}</h1>
            <h2>{{ trans('main/home.text_learn2know_subheading') }}</h2>
            <p>{{ trans('main/home.text_learn2know_p1') }}</p>
            <p>{{ trans('main/home.text_learn2know_p2') }}</p>
        </div>
        <a class="shake" href="http://learn2know.tni.ac.th/main/" target="_blank">{{ trans('main/home.text_click_here') }}</a>
    </div>
</div>

<div id="home-present" >
    <div class="container">
            <div class="row">
                    <div class="col-sm-12 col-md-8 col-lg-8 col-xl-8">
                        <h1>{{ trans('main/home.text_eng_presentation') }}</h1>
                    
                            <div class="video-item" >
                                <iframe width="100%" height="350" src="https://www.youtube.com/embed/6pMn0AQDYhs" 
                                    frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
                                    allowfullscreen>
                                </iframe>
                                {{-- <h2>{{ trans('main/home.text_thai_version') }}</h2>  --}}
                            </div>
                    </div>
                    <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4" id="m_present">
    
                            <a href="https://www.youtube.com/channel/UCOMVmuT0gRQxBGAFBDgqBLg" target="_blank" class="btn-all" >
                                    <i class="far fa-play-circle fa-2x "></i>{{ trans('main/home.text_more') }}
                            </a>

                            <div class="card " id="dt_present">               
                                <div class="video-item" >
                                    <iframe width="100%" height="150%" src="https://www.youtube.com/embed/FoisB0NKtHs" 
                                        frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
                                        allowfullscreen>
                                    </iframe>
                                   
                                    {{-- <h2>{{ trans('main/home.text_thai_version') }}</h2>  --}}
                                </div>                                                                 
                                    <span >

                                            <a href="https://www.youtube.com/channel/UCOMVmuT0gRQxBGAFBDgqBLg" target="_blank" >                                  
                                                    {{ trans('main/home.text_more') }} <i class="far fa-play-circle fa-1x "></i>
                                            </a>
                                    </span>
                            </div>
               
                    </div>
            </div> {{-- row --}}
    </div>
</div>

<div id="home-channel" class="container">
    <div class="row" >
        <div class="col-xs-6 col-sm-12 col-md-12 col-lg-12" >
            <h1>{{ trans('main/home.text_eng_channel') }}</h1>
            @if(count($channel) > 0 )
                @for($i=0; $i<4; $i++)
                    <div class="row">  
                        @for($i=0;$i<count($channel);$i++)
                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="history-item shake">
                            <a class="btn btn-block btn-social btn-facebook" href="{{ $channel[$i]['url'] }}" target="_blank">
                                <span class="fa fa-facebook"></span> {{ $channel[$i]['name'] }}
                              </a>
                      
                        </div>
                    </div>
                      
                        @endfor
                    </div>    
                @endfor   
                <br>
                {{-- <a href="{{ url(Config::get('url.main.ebrochures')) }}" class="btn-more pull-right" >{{ trans('main/home.text_more') }}</a> --}}
            @endif
         </div>
      

        

    </div>  
</div>

@endsection

@section('style')
<link rel="stylesheet" href="{{ url('lib/swiper-4.4.0/css/swiper.min.css') }}">

@endsection

@section('script')
<script src="{{ url('lib/swiper-4.4.0/js/swiper.min.js') }}"></script>

{{-- <script src="{{ url('lib/jsized.snow/jsized.snow.min.js') }}"></script>  // snow-effect  add 27-12-62 --}}
{{-- <script>
    /**
     * This function takes 2 arguments
     * First is the path to the directory with snowflake images
     * Second is the maximum number of snowflakes, please do not
     * set this number above 60 as it will impact the performance
     */
    createSnow('jsized.snow/', 60);
</script> --}}


<script>
var homeHeroSwiper = new Swiper('#hero-banner .swiper-container', {
    autoplay: {
        delay: 5000,
        disableOnInteraction: false
    },
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    on: {
        init: function() {
            var background_color = $('#hero-banner .swiper-wrapper>div:nth-child(1)').data('background_color');
            $('#hero-banner').css('background', background_color).addClass('transition-none');
        },
        slideChange: function(e) {
            var background_color = $('#hero-banner .swiper-wrapper>div:nth-child(' + (homeHeroSwiper.activeIndex + 1) + ')').data('background_color');
            $('#hero-banner').css('background', background_color).removeClass('transition-none').addClass('transition-slow');
        }
    }
});

var homeNewsSwiper = new Swiper('#home-news .swiper-container', {
    loop: true,
    effect: 'fade',
    autoplay: {
        delay: 10000,
        disableOnInteraction: false
    },
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
});
</script>

@endsection