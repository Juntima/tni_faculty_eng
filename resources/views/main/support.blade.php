@extends('main.layouts.app')

@section('htmlheader_title'){{ trans('main/home.text_htmltitle') }}@endsection

@section('htmlheader_description'){{ trans('main/common.htmlheader_description') }}@endsection

@section('htmlheader_keywords'){{ trans('main/common.htmlheader_keywords') }}@endsection

@section('content-header')

    <div id="header" class="nav-item">

            <div class="container">
                <!-- <div class="second-header">{{ trans('main/history.text_board_and_executives') }}</div> -->
                <div class="second-header">
                    @if(@$breadcrumb && count($breadcrumb) > 0)
                    <ul class="breadcrumb">
                        @if(@$breadcrumb)
                            @foreach(@$breadcrumb as $key => $value)
                                @if(@$value['active'] == 'active')
                                    <li class="active">{!! $key !!}</li>
                                @else
                                    <li><a href="{{ url(@$value['url']) }}">{!! $key !!}</a></li>
                                @endif
                            @endforeach
                        @endif
                    </ul>
                    @endif
                </div>
            </div>

    </div>
    
@endsection

@section('main-content')

<div class="container" >

   
    <div class="home-news-more">
      
            <div class="row">  
                   
                        <div   class="col-sm-6 col-md-6 col-lg-6 col-xl-6" >
                        
                            <div class="card card-polaroid" style="width: 15rem;">
                                <img src="{{ trans('main/support.text_img1') }}" class="card-img-top" alt="...">
                                <div class="card-body" id="support">
                                    <h4 class="mt-1 mb-1"><b>{{ trans('main/support.text_name_th1') }}</b></h4>
                                    <p class="mb-2">{{ trans('main/support.text_name_position1') }}</p>
                                  {{-- <a href="#" class="btn btn-primary">Go somewhere</a> --}}
                                </div>
                              </div>

                        </div> 

                        {{-- <div   class="col-sm-6 col-md-6 col-lg-6 col-xl-6" >
                        
                            <div class="card card-polaroid" style="width: 15rem;">
                                <img src="{{ trans('main/support.text_img1') }}" class="card-img-top" alt="...">
                                <div class="card-body" id="support">
                                    <h4 class="mt-1 mb-1"><b>{{ trans('main/support.text_name_th1') }}</b></h4>
                                    <p class="mb-2">{{ trans('main/support.text_name_position1') }}</p>
                                  
                                </div>
                              </div>

                        </div>  --}}

                        
                     
            </div>    
    </div>

</div>


@endsection

@section('style')
<link rel="stylesheet" href="{{ url('lib/swiper-4.4.0/css/swiper.min.css') }}">

@endsection

@section('script')
<script src="{{ url('lib/swiper-4.4.0/js/swiper.min.js') }}"></script>

@endsection


