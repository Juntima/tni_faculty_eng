@extends('main.layouts.app')

@section('htmlheader_title'){{ trans('main/home.text_htmltitle') }}@endsection

@section('htmlheader_description'){{ trans('main/common.htmlheader_description') }}@endsection

@section('htmlheader_keywords'){{ trans('main/common.htmlheader_keywords') }}@endsection


@section('content-header')



@section('content-header')
<div id="background-new-detail">
    <div id="header" class="nav-item">

           <!-- <div class="container">
                <div class="second-header">{{ trans('main/allnews.text_news_activity') }}</div>
            </div> -->
            <div class="container">
                <div class="second-header">
                    @if(@$breadcrumb && count($breadcrumb) > 0)
                    <ul class="breadcrumb">
                        @if(@$breadcrumb)
                            @foreach(@$breadcrumb as $key => $value)
                                @if(@$value['active'] == 'active')
                                    <li class="active">{!! $key !!}</li>
                                @else
                                    <li><a href="{{ url(@$value['url']) }}">{!! $key !!}</a></li>
                                @endif
                            @endforeach
                        @endif
                    </ul>
                    @endif
                </div>

                 {{-- <div class='poston'>
                    <i class='fa fa-clock-o fa'></i> {{date('d-M-Y H:m:s', strtotime(@$item->publish_start)) }}
                        | 
                    <i class="fa fa-eye"></i> {{ @$item->viewed }}
                 </div>    --}}

            </div>

    </div><!-- End apply-now -->

</div>   
        
@endsection

@section('main-content')

<br>
    <div class="container">

        <h3 style="color: #0f3b9e "><b>Tags :  {{$tags}}</b> </h3>
        <br>
        <div class="tags">
                <div class='row'>
                    <div class='col-lg-12'>
                        @if(count($articles) > 0 )
                                @foreach($articles as $article) 

                                    <div id="tags" >          
                                        <a href="{{ url(Config::get('url.main.news').'/'.$article->id.'/detail') }}" target="_blank" >                              
                                            <h4 class='truncate_text shake'> 
                                                {!! str_replace($tags,"<font style='background-color:yellow'><b>$tags</b></font>",$article->name)!!} 
                                            </h4>
                                        </a> 
                                    </div>

                                    @foreach(explode(',', $article->tag) as $text_tags) {{-- แยกเครื่องหมาย , ที่คั่นในฟิล tag ใน DB เก็บในตัวแปร tags --}}
                                        @if(empty($text_tags))
                                             {{""}}
                                        @else
                                            <span class="tags badge badge-pill badge-primary"><a href="{{ url(Config::get('url.main.tags').'?tag='.$text_tags) }}">{{$text_tags}}</a></span>  
                                        @endif   
                                    @endforeach
                                    <hr>        

                                        {{-- <a href="{{ url(Config::get('url.main.tag').'?tag='.$text_tag) }}"></a> --}}
                                @endforeach
                                <div class='row'>
                                    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style='text-align:right'>
                                        <div class="pager">
                                            <div class="pageinfo">
                                                <span>{{ trans('backoffice/common.text_all_results') }} {{ @$articles->total() }} {{ trans('backoffice/common.text_item(s)') }}</span>
                                                <span>{{ trans('backoffice/common.text_page') }} {{ $articles->currentPage () }} {{ trans('backoffice/common.text_from') }} {{ $articles->lastPage() }} {{ trans('backoffice/common.text_page(s)') }}</span>
                                            </div>		
                                        </div>	      
                                    </div>
                                </div>

                                <div class='row'>
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style='text-align:center'>
                                            {{$articles->links('vendor.pagination.bootstrap-4')}}
                                        </div>
                                    </div> 

                            @else 

                                <div class='row'>
                                    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style='text-align:center'>
                                            {{trans('main/common.text_not_found_data')}}
                                    </div>
                                </div>
                                                                   
                            @endif {{-- @if($catagory[$i]->article_category_id == '1' ) --}}
                    </div>   
                </div> 
                       
        </div>
        <br>

    </div> {{-- end div --}}
   
</div> 

@endsection

@section('style')
<link rel="stylesheet" href="{{ url('lib/swiper-4.4.0/css/swiper.min.css') }}">

<!-- Add fancyBox main CSS files -->
<link rel="stylesheet"  href="{{ url('lib/fancybox/source/jquery.fancybox.css?v=2.1.5') }}" type="text/css" media="screen" />

<!-- Optionally add helpers - button, thumbnail and/or media -->
<link rel="stylesheet" href="{{ url('lib/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5') }}" type="text/css" media="screen" />
<link rel="stylesheet" href="{{ url('lib/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7') }}" type="text/css" media="screen" />
@endsection


@section('script')

<script src="{{ url('lib/swiper-4.4.0/js/swiper.min.js') }}"></script>
 <!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

 <!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

 <!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

<!-- _fancy box (simple) -->
    <!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="{{ url('lib/fancybox/lib/jquery.mousewheel-3.0.6.pack.js') }}"></script>

<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="{{ url('lib/fancybox/source/jquery.fancybox.js?v=2.1.5') }}"></script>

<!-- Optionally add helpers - button, thumbnail and/or media -->

<script type="text/javascript" src="{{ url('lib/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5') }}"></script>
<script type="text/javascript" src="{{ url('lib/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6') }}"></script>

<script type="text/javascript" src="{{ url('lib/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7') }}"></script>

<script>
    $(document).ready(function() {
        
        //_Simple image gallery. Uses default settings
        //$('.fancybox').fancybox();

        $(".fancybox-button").fancybox({
            prevEffect		: 'none',
            nextEffect		: 'none',
            closeBtn		: false,
            helpers		: {
                title	: { type : 'inside' },
                buttons	: {}
            }
        });

    });

</script>

@endsection

    