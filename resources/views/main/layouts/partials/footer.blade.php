<br />
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-6 col-lg-3 col-xl-3">
                <h1>{{ trans('main/footer.text_quick_link') }}</h1>
                <ul>
                    <li><a href="https://www.tni.ac.th/home/admission" target="_blank">{{ trans('main/footer.text_apply_now') }}</a><li>
                    <li><a href="https://reg.tni.ac.th/registrar/calendar.asp?avs396432680=1" target="_blank">{{ trans('main/footer.text_calendar') }}</a><li>
                    {{-- <li><a href="http://e-learning.tni.ac.th/" target="_blank">{{ trans('main/footer.text_e_learning') }}</a><li> --}}
                    {{-- <li><a href="https://icc.tni.ac.th/dict/" target="_blank">{{ trans('main/footer.text_e_dictionary') }}</a><li> --}}
                    {{-- <li><a href="{{ url(Config::get('url.main.weblink')) }}" target="_blank">{{ trans('main/footer.text_web_link') }}</a><li> --}}
                    {{-- <li><a href="http://webmail.tni.ac.th/" target="_blank">{{ trans('main/footer.text_webmail') }}</a><li> --}}
                    {{-- <li><a href="http://www.jobtni.com/" target="_blank">{{ trans('main/footer.text_jobtni') }}</a><li> --}}
                    {{-- <li><a href="{{ url(Config::get('url.main.careers')) }}"  target="_blank">{{ trans('main/footer.text_career_tni') }}</a><li>     --}}
                    {{-- <li><a href="https://journal.tni.ac.th/main/" target="_blank">{{ trans('main/footer.text_journal') }}</a><li> --}}
                </ul>

                <h1 class="footer-faculty mt-2">{{ trans('main/footer.text_faculty') }}</h1>
                <ul>
                    {{-- <li><a href="https://eng.tni.ac.th/" target="_blank">{{ trans('main/footer.text_engineering') }}</a><li> --}}
                    <li><a href="https://it.tni.ac.th/" target="_blank">{{ trans('main/footer.text_it') }}</a><li>
                    <li><a href="https://ba.tni.ac.th/2018/" target="_blank">{{ trans('main/footer.text_ba') }}</a><li>
                    <li><a href="https://cgel.tni.ac.th/2018/" target="_blank">{{ trans('main/footer.text_cgel') }}</a><li>
                    <li><a href="https://grad.tni.ac.th/" target="_blank">{{ trans('main/footer.text_grad') }}</a><li>
                    <li><a href="http://inter.tni.ac.th/" target="_blank">{{ trans('main/footer.text_internation_program') }}</a><li>
                </ul>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-3 col-xl-3">
                <h1>{{ trans('main/footer.text_tni') }}</h1>
                <ul>
                    <li class="footer-fa footer-address">
                        {{ trans('main/footer.text_adress') }}
                    <li>
                    <li class="footer-fa footer-tel">
                        {!! trans('main/footer.text_tel') !!}
                    <li>
                    <li class="footer-fa footer-web">
                        <a href="https://www.tni.ac.th/home/" target="_blank"> {{ trans('main/footer.text_tni_web') }} </a>
                    <li>
                    <li class="footer-fa footer-email">
                        <a href="mailto:tniinfo@tni.ac.th">{{ trans('main/footer.text_email') }}</a>
                    <li>
                    <li class="footer-fa footer-direct">
                        <a href="{{ url(Config::get('url.main.hotline')) }}" >{{ trans('main/footer.text_direct_chancellor') }}</a>
                    <li>
                </ul>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-3 col-xl-3">
                <h1>{{ trans('main/footer.text_eng') }}</h1>
                <ul>
                    <li class="footer-fa footer-address">
                        {{ trans('main/footer.text_eng_address') }}
                    <li>
                    <li class="footer-fa footer-tel">
                        {!! trans('main/footer.text_eng_tel') !!}
                    <li>
                  
                    {{-- <li class="footer-fa footer-email">
                        <a href="mailto:tniinfo@tni.ac.th">{{ trans('main/footer.text_email') }}</a>
                    <li> --}}
                    {{-- <li class="footer-fa footer-direct">
                        <a href="{{ url(Config::get('url.main.hotline')) }}" >{{ trans('main/footer.text_direct_chancellor') }}</a>
                    <li> --}}
                </ul>

                {{-- <h1 class="footer-download">{{ trans('main/footer.text_donwload') }}</h1>
                <ul>
                    <li><a href="#" target="_blank">{{ trans('main/footer.text_sigil_program') }}</a><li>
                </ul> --}}
            </div>
            <div class="footer-map col-sm-6 col-md-6 col-lg-3 col-xl-3">
                <h1>{{ trans('main/footer.text_map') }}</h1>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d968.9175275071767!2d100.62893112922994!3d13.738411602133247!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x311d61c72894ebd1%3A0xf696dbcd5e032b3a!2sFaculty%20of%20Engineering%2C%20Thai-Nichi%20Institute%20of%20Technology!5e0!3m2!1sen!2sth!4v1581567584716!5m2!1sen!2sth" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                
                <div class="footer-social">
                    <a href="https://www.facebook.com/TNIfacultyIT/" target="_blank" class="footer-social-facebook">Facebook</a>
                    {{-- <a href="https://twitter.com/tniadmissions" target="_blank" class="footer-social-twitter">Twitter</a>
                    <a href="https://plus.google.com/110037289499151553368" target="_blank" class="footer-social-google">Google+</a> --}}
                    <a href="https://www.youtube.com/user/tnipr" target="_blank" class="footer-social-youtube">Youtube</a>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-copyright">
        {{ trans('main/footer.text_copyright') }}
    </div>
</footer>

<a href="#top" id="go2top"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>