<div id="header-logo">
 
    <div class='container ' id="tni-logo">
        <a href="{{ url('/') }}" class="btn-logo" >
                
            <img src="{{ url('img/logo-tni-eng.png') }}" class="img-fluid " /> 
       
        </a>
  
        <div class="row" id="dt">
            <div class="col-6 text-left">
                <a class="navbar-brand" href="{{ url('/') }}" >
                
                    <img src="{{ url('img/logo-eng.png') }}" class="img-fluid " /> 
                    {{-- <img src="{{ url('img/logo-eng.png') }}" class="img-fluid " /> --}}
                </a>
            </div>
            <div class="col-6 text-right">
                <a class="navbar-brand" href="{{ url('/') }}" >
                
                    <img src="{{ url('img/logo-tni.png') }}" class="img-fluid " /> 
                    {{-- <img src="{{ url('img/logo-eng.png') }}" class="img-fluid " /> --}}
                </a>
            </div>
        </div>
      
     
        {{-- <div style="border:1px solid red" id='test_logo'>LOGO TEST</div> --}}
         
        
    </div>
</div>


<div id="header" class="nevmenu">
    <nav class="navbar navbar-expand-lg navbar-light bg-light container " >
        
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fa fa-bars" aria-hidden="true"></i>
        </button>        

        <div class="collapse navbar-collapse " id="navbarNavDropdown" >
            <ul class="navbar-nav mr-auto"></ul>
            <ul class="navbar-nav "  >
                <li class="nav-item d-block d-xl-none d-lg-none">
                    <div class="language">
                    <ul>
                        <!-- @foreach($languages as $l)
                            @if($l->id == $language->id)
                                <li><a class="active" href="{{ url(Config::get('url.main.language') . '/?code=' . $l->code) }}">{{ $l->code }}</a></li>
                            @else
                                <li><a href="{{ url(Config::get('url.main.language') . '/?code=' . $l->code) }}">{{ $l->code }}</a></li>
                            @endif
                        @endforeach -->
                        @php
                            $active_class = "";
                            $url          = "";
                            $target       = "";
                        @endphp  

                        @foreach($languages as $l)
                        
                            @if($l->id == 1) {{-- if choose Thai language--}}
                                @php $url = url(Config::get('url.main.language') . '/?code=' . $l->code); @endphp
                            @elseif($l->id == 2) {{-- if choose Eng language--}}
                                @php 
                                    // $url    = "https://admission.tni.ac.th/old/web/TNI2014-en/";
                                    $url    = "#";
                                    $target = "target=_blank";
                                @endphp
                            @elseif($l->id == 3) {{-- if choose Jap language--}}
                                @php
                                    // $url    = "https://admission.tni.ac.th/old/web/TNI2014-jp/";
                                    $url    = "#";
                                    $target = "target=_blank";
                                @endphp 
                            @endif

                            @if($l->id == $language->id)
                                @php $active_class = "class=active"; @endphp
                            @else
                                @php $active_class = ""; @endphp
                            @endif
                            
                            <li><a href="{{$url}}" {{$active_class}} {{$target}}> {{ $l->code }}</a></li>
                        @endforeach
                        
                    </ul>
                    </div>
                </li>

                <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="{{ url(Config::get('url.main.home')) }}">    
                        {{ trans('main/navbar.text_home') }}
                    </a>
                </li>
                
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#">    
                        {{ trans('main/navbar.text_about') }}
                    </a>

                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink1">
                        <a class="dropdown-item" href="{{ url(Config::get('url.main.message_eng_dean')) }}" >{{ trans('main/navbar.text_dean_info') }}</a>
                        <a class="dropdown-item" href="{{ url(Config::get('url.main.mission_and_vision')) }}">{{ trans('main/navbar.text_mission') }}</a>
                        <a class="dropdown-item" href="{{ url(Config::get('url.main.management_structure')) }}">{{ trans('main/navbar.text_manage_struct') }}</a>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="{{ url(Config::get('url.main.major')) }}" >{{ trans('main/navbar.text_faculty') }}  </a>
                </li>

                 <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" > {{ trans('main/navbar.text_member') }} </a>
                       
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink1">
                        <a class="dropdown-item" href="https://workload.tni.ac.th/teacher/eng.php" target="_blank">{{ trans('main/navbar.text_teacher') }}</a>
                        <a class="dropdown-item" href="{{ url(Config::get('url.main.support')) }}">{{ trans('main/navbar.text_support') }}</a>
                    </div>
                </li>


                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="{{ url(Config::get('url.main.eng_lab_research_room')) }} ">
                        {{ trans('main/navbar.text_lab_research_room') }}
                    </a>
                </li>
            
                <li class="nav-item dropdown">
                    <a class="nav-link" href="{{ url(Config::get('url.main.services')) }}">{{ trans('main/navbar.text_service') }}</a>
              
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link" href="{{ url(Config::get('url.main.contact')) }}">
                        {{ trans('main/navbar.text_contact') }}
                    </a>
                </li>

            </ul>
        </div>
        
    </nav>
  
    <div class="language container d-none d-lg-block d-xl-block">
        <ul>
            <!-- @foreach($languages as $l)
                @if($l->id == $language->id)
                <li><a class="active" href="{{ url(Config::get('url.main.language') . '/?code=' . $l->code) }}">{{ $l->code }}</a></li>
                @else
                <li><a href="{{ url(Config::get('url.main.language') . '/?code=' . $l->code) }}">{{ $l->code }}</a></li>
                @endif
            @endforeach -->
                
                @php
                    $active_class = "";
                    $url          = "";
                    $target       = "";
                @endphp  

                @foreach($languages as $l)
                
                    @if($l->id == 1) {{-- if choose Thai language--}}
                        @php $url = url(Config::get('url.main.language') . '/?code=' . $l->code); @endphp
                    @elseif($l->id == 2) {{-- if choose Eng language--}}
                        @php 
                            // $url    = "https://admission.tni.ac.th/old/web/TNI2014-en/";
                            $url    = "#";
                            $target = "target=_blank";
                        @endphp
                    @elseif($l->id == 3) {{-- if choose Jap language--}}
                        @php
                            // $url    = "https://admission.tni.ac.th/old/web/TNI2014-jp/";
                            $url    = "#";
                            $target = "target=_blank";
                        @endphp 
                    @endif

                    @if($l->id == $language->id)
                        @php $active_class = "class=active"; @endphp
                    @else
                        @php $active_class = ""; @endphp
                    @endif
                    
                    <li><a href="{{$url}}" {{$active_class}} {{$target}}> {{ $l->code }}</a></li>
                @endforeach

        </ul>

        <!-- <div class="search-form">
            <input type="search" placeholder="{{ trans('main/common.text_search') }}" />
        </div> -->
    </div>
</div><!-- End header -->

<div id="apply-now">
    <div class="container">
        <div><a href="tel:+6627632605">0-2763-2605</a></div>
        
        <div class="shake">
            <a href="http://m.me/TNIadmissioncenter" target="_blank">
                <!-- <i class="fa fa-facebook-official"></i> -->
                <!-- <i class="fab fa-facebook-square"></i> -->
                <i class="fab fa-facebook-messenger"></i>
            </a>
        </div>
        
        <div class="shake">
            <a href="http://line.me/ti/p/@TNISAC" target="_blank" >
                <i class="fab fa-line"></i>
            </a>
        </div>
        <!-- <div class="shake"><a href="http://line.me/ti/p/@TNISAC" target="_blank" ><img src="{{ url('img/LINE_SOCIAL_Square_resize.png') }}"></a></div> -->
        
        <div><a href="https://www.tni.ac.th/home/admission" target="_blank" class="shake">{{ trans('main/common.text_apply_now') }}</a></div>
       
        
    </div>
</div><!-- End apply-now -->



<div hidden> <!-- add -->
<div id="header">
    <nav class="navbar navbar-expand-lg navbar-light bg-light container">
        <a class="navbar-brand" href="{{ url('/') }}">
            <img src="{{ url('img/logo.png') }}" class="img-fluid" />
          </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fa fa-bars" aria-hidden="true"></i>
        </button>        

        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav mr-auto"></ul>
            <ul class="navbar-nav">
                <li class="nav-item d-block d-xl-none d-lg-none">
                    <div class="language">
                    <ul>
                        <!-- @foreach($languages as $l)
                            @if($l->id == $language->id)
                                <li><a class="active" href="{{ url(Config::get('url.main.language') . '/?code=' . $l->code) }}">{{ $l->code }}</a></li>
                            @else
                                <li><a href="{{ url(Config::get('url.main.language') . '/?code=' . $l->code) }}">{{ $l->code }}</a></li>
                            @endif
                        @endforeach -->
                        @php
                            $active_class = "";
                            $url          = "";
                            $target       = "";
                        @endphp  

                        @foreach($languages as $l)
                        
                            @if($l->id == 1) {{-- if choose Thai language--}}
                                @php $url = url(Config::get('url.main.language') . '/?code=' . $l->code); @endphp
                            @elseif($l->id == 2) {{-- if choose Eng language--}}
                                @php 
                                    $url    = "https://admission.tni.ac.th/old/web/TNI2014-en/";
                                    $target = "target=_blank";
                                @endphp
                            @elseif($l->id == 3) {{-- if choose Jap language--}}
                                @php
                                    $url    = "https://admission.tni.ac.th/old/web/TNI2014-jp/";
                                    $target = "target=_blank";
                                @endphp 
                            @endif

                            @if($l->id == $language->id)
                                @php $active_class = "class=active"; @endphp
                            @else
                                @php $active_class = ""; @endphp
                            @endif
                            
                            <li><a href="{{$url}}" {{$active_class}} {{$target}}> {{ $l->code }}</a></li>
                        @endforeach
                        
                    </ul>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ trans('main/navbar.text_about') }}
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink1">
                        <a class="dropdown-item" href="{{ url(Config::get('url.main.history')) }}">{{ trans('main/history.text_intro') }}</a>
                        <a class="dropdown-item" href="{{ url(Config::get('url.main.board')) }}">{{ trans('main/navbar.text_personnel') }}</a>
                        <a class="dropdown-item" href="{{ url(Config::get('url.main.teacher')) }}">{{ trans('main/navbar.text_teacher') }}</a>
                        <a class="dropdown-item" href="{{ url(Config::get('url.main.facilities')) }}">{{ trans('main/navbar.text_learngin_support') }}</a>
                        
                        {{-- <a class="dropdown-item" href="#">{{ trans('main/navbar.text_popularity') }}</a> --}}
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="{{ url(Config::get('url.main.faculty')) }}" >
                        {{ trans('main/navbar.text_faculty') }}
                    </a>
                    <div class="dropdown-menu" >
                        {{-- <a class="dropdown-item" href="#">{{ trans('main/navbar.text_engineering') }}</a>
                        <a class="dropdown-item" href="#">{{ trans('main/navbar.text_information_of_tachnology') }}</a>
                        <a class="dropdown-item" href="#">{{ trans('main/navbar.text_business_administration') }}</a>
                        <a class="dropdown-item" href="#">{{ trans('main/navbar.text_collage_of_general_education_and_language') }}</a>
                        <a class="dropdown-item" href="#">{{ trans('main/navbar.text_graduate_school') }}</a>
                        <a class="dropdown-item" href="#">{{ trans('main/navbar.text_international_programs') }}</a> --}}
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="{{ url(Config::get('url.main.department')) }}" >
                        {{ trans('main/navbar.text_department') }}
                    </a>
                    <div class="dropdown-menu" >
                            {{-- <a class="dropdown-item" href="#">{{ trans('main/navbar.text_admissions_center') }}</a>
                            <a class="dropdown-item" href="#">{{ trans('main/navbar.text_international_public_relations_and_scholarships') }}</a>
                            <a class="dropdown-item" href="#">{{ trans('main/navbar.text_business_administrationAdministration_department') }}</a>
                            <a class="dropdown-item" href="#">{{ trans('main/navbar.text_academic_affairs_department') }}</a>
                            <a class="dropdown-item" href="#">{{ trans('main/navbar.text_student_affairs_department') }}</a>
                            <a class="dropdown-item" href="#">{{ trans('main/navbar.text_Cooperative_education_and_employment_center') }}</a>
                            <a class="dropdown-item" href="#">{{ trans('main/navbar.text_Information_and_communication_center') }}</a>
                            <a class="dropdown-item" href="#">{{ trans('main/navbar.text_Research_and_academic_services') }}</a>
                            <a class="dropdown-item" href="#">{{ trans('main/navbar.text_international_programs') }}</a>
                            <a class="dropdown-item" href="#">{{ trans('main/navbar.text_international_programs') }}</a> --}}
                            
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="{{ url(Config::get('url.main.contact')) }}">
                        {{ trans('main/navbar.text_contact') }}
                    </a>
                </li>
            </ul>
        </div>
        
    </nav>
  
    <div class="language container d-none d-lg-block d-xl-block">
        <ul>
            <!-- @foreach($languages as $l)
                @if($l->id == $language->id)
                <li><a class="active" href="{{ url(Config::get('url.main.language') . '/?code=' . $l->code) }}">{{ $l->code }}</a></li>
                @else
                <li><a href="{{ url(Config::get('url.main.language') . '/?code=' . $l->code) }}">{{ $l->code }}</a></li>
                @endif
            @endforeach -->
                
                @php
                    $active_class = "";
                    $url          = "";
                    $target       = "";
                @endphp  

                @foreach($languages as $l)
                
                    @if($l->id == 1) {{-- if choose Thai language--}}
                        @php $url = url(Config::get('url.main.language') . '/?code=' . $l->code); @endphp
                    @elseif($l->id == 2) {{-- if choose Eng language--}}
                        @php 
                            $url    = "https://admission.tni.ac.th/old/web/TNI2014-en/";
                            $target = "target=_blank";
                        @endphp
                    @elseif($l->id == 3) {{-- if choose Jap language--}}
                        @php
                            $url    = "https://admission.tni.ac.th/old/web/TNI2014-jp/";
                            $target = "target=_blank";
                        @endphp 
                    @endif

                    @if($l->id == $language->id)
                        @php $active_class = "class=active"; @endphp
                    @else
                        @php $active_class = ""; @endphp
                    @endif
                    
                    <li><a href="{{$url}}" {{$active_class}} {{$target}}> {{ $l->code }}</a></li>
                @endforeach

        </ul>

        <!-- <div class="search-form">
            <input type="search" placeholder="{{ trans('main/common.text_search') }}" />
        </div> -->
    </div>
</div><!-- End header -->

<div id="apply-now">
    <div class="container">
        <div><a href="tel:+6627632605">0-2763-2605</a></div>
        
        <div class="shake">
            <a href="http://m.me/TNIadmissioncenter" target="_blank">
                <!-- <i class="fa fa-facebook-official"></i> -->
                <!-- <i class="fab fa-facebook-square"></i> -->
                <i class="fab fa-facebook-messenger"></i>
            </a>
        </div>
        
        <div class="shake">
            <a href="http://line.me/ti/p/@TNISAC" target="_blank" >
                <i class="fab fa-line"></i>
            </a>
        </div>
        <!-- <div class="shake"><a href="http://line.me/ti/p/@TNISAC" target="_blank" ><img src="{{ url('img/LINE_SOCIAL_Square_resize.png') }}"></a></div> -->
        
        <div><a href="{{ url(Config::get('url.main.admission'))}}" class="shake">{{ trans('main/common.text_apply_now') }}</a></div>
       
        
    </div>
</div><!-- End apply-now -->
</div>