<!DOCTYPE html>
<html lang="{{ $language->code }}">
@include('main.layouts.partials.htmlheader')

<body>
<div id="wrapper">
    
    @include('main.layouts.partials.mainheader')

    <div id="content-header">
    @yield('content-header')
    </div><!-- /.content-header -->

    <div id="content-wrapper">
    @yield('main-content')
    </div><!-- /.content-wrapper -->

    @include('main.layouts.partials.footer')

</div><!-- ./wrapper -->

<script src="{{ url('lib/jquery-3.2.1.min.js') }}"></script>
<script src="{{ url('lib/popper.min.js') }}"></script>
<script src="{{ url('lib/bootstrap-4.0.0/js/bootstrap.min.js') }}"></script>
<script src="{{ url('js/front.js?ver=1.2') }}"></script>
{{-- <script src="{{ url('lib/sakura-effect.js') }}"></script>   <!-- add juntima 27-12-62 --> --}}

@yield('script')

</body>
</html>

