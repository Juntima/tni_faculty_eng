@extends('main.layouts.app')

@section('htmlheader_title'){{ trans('main/home.text_htmltitle') }}@endsection

@section('htmlheader_description'){{ trans('main/common.htmlheader_description') }}@endsection

@section('htmlheader_keywords'){{ trans('main/common.htmlheader_keywords') }}@endsection

@section('content-header')

    <div id="header" class="nav-item">

            <div class="container">
                <!-- <div class="second-header">{{ trans('main/history.text_history') }}</div> -->
                <div class="second-header">
                    @if(@$breadcrumb && count($breadcrumb) > 0)
                    <ul class="breadcrumb">
                        @if(@$breadcrumb)
                            @foreach(@$breadcrumb as $key => $value)
                                @if(@$value['active'] == 'active')
                                    <li class="active">{!! $key !!}</li>
                                @else
                                    <li><a href="{{ url(@$value['url']) }}">{!! $key !!}</a></li>
                                @endif
                            @endforeach
                        @endif
                    </ul>
                    @endif
                </div>
            </div>

    </div>
    
   
    <div class="container">
        <br>
        
        <div id="history-line">{!! trans('main/history.text_manage_struct') !!}</div>
        <div id="history-second-line"></div>

    </div>


@endsection

@section('main-content')

   
    <div id="history-tni" class="container"> <!--พันธกิจ-->
        <h1>{{ trans('main/history.text_manage_struct') }}</h1>
        <ol class='custom_list' >
            {{-- @foreach(trans('main/history.text_manage_struct') as $text)
                <li>{{$text}}</li>
            @endforeach --}}
        </ol>
    </div>

    <div id="history-tni" class="container">
       <hr class="mt-5" color="#ededed ">
    </div>

    {{-- <div id="history-tni" class="container"> --}}
    <div id="end-apply">
        <div class="container">

            <div class="mr-3 shake"><a href="{{ url(Config::get('url.main.major')) }}" >สาขาที่เปิดรับสมัคร</a> </div>
                
            <div id="eng-apply"><a></a> </div>
         
            <div class="ml-3"><a href="https://www.tni.ac.th/home/admission" target="_blank" class="shake">{{ trans('main/common.text_apply_now') }}</a></div>
            
            
            
        </div>
    </div><!-- End apply-now -->
    {{-- </div> --}}
@endsection





@section('style')
<link rel="stylesheet" href="{{ url('lib/swiper-4.4.0/css/swiper.min.css') }}">

@endsection

@section('script')
<script src="{{ url('lib/swiper-4.4.0/js/swiper.min.js') }}"></script>
<script>
var homeHeroSwiper = new Swiper('#hero-banner .swiper-container', {
    autoplay: {
        delay: 5000,
        disableOnInteraction: false
    },
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    on: {
        init: function() {
            var background_color = $('#hero-banner .swiper-wrapper>div:nth-child(1)').data('background_color');
            $('#hero-banner').css('background', background_color).addClass('transition-none');
        },
        slideChange: function(e) {
            var background_color = $('#hero-banner .swiper-wrapper>div:nth-child(' + (homeHeroSwiper.activeIndex + 1) + ')').data('background_color');
            $('#hero-banner').css('background', background_color).removeClass('transition-none').addClass('transition-slow');
        }
    }
});

var homeNewsSwiper = new Swiper('#home-news .swiper-container', {
    loop: true,
    effect: 'fade',
    autoplay: {
        delay: 10000,
        disableOnInteraction: false
    },
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
});
</script>

@endsection


