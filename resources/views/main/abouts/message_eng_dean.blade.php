@extends('main.layouts.app')

@section('htmlheader_title'){{ trans('main/home.text_htmltitle') }}@endsection

@section('htmlheader_description'){{ trans('main/common.htmlheader_description') }}@endsection

@section('htmlheader_keywords'){{ trans('main/common.htmlheader_keywords') }}@endsection

@section('content-header')

    <div id="header" class="nav-item">

            <div class="container">
                <!-- <div class="second-header">{{ trans('main/history.text_history') }}</div> -->
                <div class="second-header">
                    @if(@$breadcrumb && count($breadcrumb) > 0)
                    <ul class="breadcrumb">
                        @if(@$breadcrumb)
                            @foreach(@$breadcrumb as $key => $value)
                                @if(@$value['active'] == 'active')
                                    <li class="active">{!! $key !!}</li>
                                @else
                                    <li><a href="{{ url(@$value['url']) }}">{!! $key !!}</a></li>
                                @endif
                            @endforeach
                        @endif
                    </ul>
                    @endif
                </div>
            </div>

    </div>
    
   
    <div class="container">
        <br>
        
        <div id="history-line">{!! trans('main/history.text_topic_hilight') !!}</div>
        <div id="history-second-line"></div>

    </div>


@endsection

@section('main-content')

    <div id="history-tni" class="container" >
        <h4 class="mt-3  text-justify">{{ trans('main/history.text_content_hilight') }}</h4> <!--คณะวิศวกรรมศาสตร์ TNI มี ดี ยั ง ไ ง ? -->
    </div>

    <div id="history-tni" class="container"> <!--คณบดีคณะวิศวกรรมศาสตร์ -->
        <h1 class="mt-3">{{ trans('main/history.text_dean_eng') }}</h1>
        <h4 class="mt-3">
            <img src="img/history/dean_eng.jpg" class="mx-auto d-block  img-thumbnail" style="max-width:20%">
        </h4><br>
        <h4 class="text-center">{!! trans('main/history.text_dean_name')!!}</h4>
    </div>

    <div id="history-tni" class="container text-justify">
            <h1>{{ trans('main/history.text_history') }}</h1> <!--ความเป็นมาของคณะวิศวกรรมศาสตร์ -->
            <h4>{{ trans('main/history.text_content_history1') }}</h4>
            {{-- <h4>{{ trans('main/history.text_content_history2') }}</h4>
            <h4>{{ trans('main/history.text_content_history3') }}</h4> --}}
    </div>
    
    <div id="history-tni" class="container"> <!--ปรัชญา -->
        <h1 id='tni_resolution'>{{ trans('main/history.text_philosophy') }}</h1>
        <h4>{{ trans('main/history.text_content_philosophy') }}</h4>
    </div>

   
    <div id="history-tni" class="container">
       <hr class="mt-5" color="#ededed ">
    </div>

    {{-- <div id="history-tni" class="container"> --}}
    <div id="end-apply">
        <div class="container">

            <div class="mr-3 shake"><a href="{{ url(Config::get('url.main.major')) }}" >สาขาที่เปิดรับสมัคร</a> </div>
                
            <div id="eng-apply"><a></a> </div>
         
            <div class="ml-3"><a href="https://www.tni.ac.th/home/admission" target="_blank" class="shake">{{ trans('main/common.text_apply_now') }}</a></div>
            
            
            
        </div>
    </div><!-- End apply-now -->
    {{-- </div> --}}
@endsection





@section('style')
<link rel="stylesheet" href="{{ url('lib/swiper-4.4.0/css/swiper.min.css') }}">

@endsection

@section('script')
<script src="{{ url('lib/swiper-4.4.0/js/swiper.min.js') }}"></script>
<script>
var homeHeroSwiper = new Swiper('#hero-banner .swiper-container', {
    autoplay: {
        delay: 5000,
        disableOnInteraction: false
    },
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    on: {
        init: function() {
            var background_color = $('#hero-banner .swiper-wrapper>div:nth-child(1)').data('background_color');
            $('#hero-banner').css('background', background_color).addClass('transition-none');
        },
        slideChange: function(e) {
            var background_color = $('#hero-banner .swiper-wrapper>div:nth-child(' + (homeHeroSwiper.activeIndex + 1) + ')').data('background_color');
            $('#hero-banner').css('background', background_color).removeClass('transition-none').addClass('transition-slow');
        }
    }
});

var homeNewsSwiper = new Swiper('#home-news .swiper-container', {
    loop: true,
    effect: 'fade',
    autoplay: {
        delay: 10000,
        disableOnInteraction: false
    },
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
});
</script>

@endsection


