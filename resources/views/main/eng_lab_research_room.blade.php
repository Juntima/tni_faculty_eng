@extends('main.layouts.app')

@section('htmlheader_title'){{ trans('main/home.text_htmltitle') }}@endsection

@section('htmlheader_description'){{ trans('main/common.htmlheader_description') }}@endsection

@section('htmlheader_keywords'){{ trans('main/common.htmlheader_keywords') }}@endsection



@section('content-header')

    <div id="header" class="nav-item">

            <!-- <div class="container">
                <div class="second-header">{{ trans('main/history.text_it_classroom') }}</div>
            </div> -->
            <div class="container">
                <div class="second-header">
                    @if(@$breadcrumb && count($breadcrumb) > 0)
                    <ul class="breadcrumb">
                        @if(@$breadcrumb)
                            @foreach(@$breadcrumb as $key => $value)
                                @if(@$value['active'] == 'active')
                                    <li class="active">{!! $key !!}</li>
                                @else
                                    <li><a href="{{ url(@$value['url']) }}">{!! $key !!}</a></li>
                                @endif
                            @endforeach
                        @endif
                    </ul>
                    @endif
                </div>
            </div>

    </div><!-- End apply-now -->
    <div id="home-course" class="container">

    </div>

    
@endsection


@section('main-content')
<br>

<div id="fac-line-eng-white"> <!-- โอกาสในการประกอบอาชีพ -->
    
    <h1>{{trans('main/history.text_eng_lab_room')}}</h1>
    <div id="history-tni" class="container">
        <hr class="mt-1 mb-5" color="#ededed ">
     </div>

    <div class="container">
        <div class="row">

<?php
$text_more = trans('main/faculty.text_more');
            
$fac_template = <<<EOT
            <div class="col-6 col-xs-6 col-md-6 col-lg-3">         
              <div class="card-polaroid" >
                <img src="%s" alt="Snow" style="width:%s">
                <div class="container">
                    
                    <p class="mb-3"> %s </p>
                </div>
              </div>
            </div>

EOT;

$fac_info_result = sprintf($fac_template   
                                ,'img/classroom/Workshop_2.jpg' 
                                ,'100%'
                                ,trans('main/history.text_lab_1')
                               );
$fac_info_result .= sprintf($fac_template   
                                ,'img/classroom/Automotive.png' 
                                ,'100%'
                                ,trans('main/history.text_lab_2')
                               );
$fac_info_result .= sprintf($fac_template   
                                ,'img/classroom/cad.png' 
                                ,'100%'
                                ,trans('main/history.text_lab_3')
                               );
$fac_info_result .= sprintf($fac_template   
                                ,'img/classroom/chemistry-lab.png' 
                                ,'100%'
                                ,trans('main/history.text_lab_4')
                               );
$fac_info_result .= sprintf($fac_template   
                                ,'img/classroom/cnclab.png' 
                                ,'100%'
                                ,trans('main/history.text_lab_5')
                               );
$fac_info_result .= sprintf($fac_template   
                                ,'img/classroom/digital-lab.jpg' 
                                ,'100%'
                                ,trans('main/history.text_lab_6')
                               );
$fac_info_result .= sprintf($fac_template   
                                ,'img/classroom/dynamo-lab.jpg' 
                                ,'100%'
                                ,trans('main/history.text_lab_7')
                               );
$fac_info_result .= sprintf($fac_template   
                                ,'img/classroom/electric-eng.jpg' 
                                ,'100%'
                                ,trans('main/history.text_lab_8')
                               );
$fac_info_result .= sprintf($fac_template   
                                ,'img/classroom/electric.jpg' 
                                ,'100%'
                                ,trans('main/history.text_lab_9')
                               );
$fac_info_result .= sprintf($fac_template   
                                ,'img/classroom/embedded-lab.jpg' 
                                ,'100%'
                                ,trans('main/history.text_lab_10')
                               );
$fac_info_result .= sprintf($fac_template   
                                ,'img/classroom/material-eng-lab.jpg' 
                                ,'100%'
                                ,trans('main/history.text_lab_11')
                               );
$fac_info_result .= sprintf($fac_template   
                                ,'img/classroom/material-eng.jpg' 
                                ,'100%'
                                ,trans('main/history.text_lab_11')
                               );
$fac_info_result .= sprintf($fac_template   
                                ,'img/classroom/meLab.jpg' 
                                ,'100%'
                                ,trans('main/history.text_lab_12')
                               );
$fac_info_result .= sprintf($fac_template   
                                ,'img/classroom/metrology-lab.png' 
                                ,'100%'
                                ,trans('main/history.text_lab_12')
                               );
$fac_info_result .= sprintf($fac_template   
                                ,'img/classroom/mold-die-lab.jpg' 
                                ,'100%'
                                ,trans('main/history.text_lab_13')
                               );
$fac_info_result .= sprintf($fac_template   
                                ,'img/classroom/molddieLab2.jpg' 
                                ,'100%'
                                ,trans('main/history.text_lab_13')
                               );
$fac_info_result .= sprintf($fac_template   
                                ,'img/classroom/physics.png' 
                                ,'100%'
                                ,trans('main/history.text_lab_14')
                               );
$fac_info_result .= sprintf($fac_template   
                                ,'img/classroom/plc-lab.png' 
                                ,'100%'
                                ,trans('main/history.text_lab_15')
                               );
$fac_info_result .= sprintf($fac_template   
                                ,'img/classroom/Tool-room_1.jpg' 
                                ,'100%'
                                ,trans('main/history.text_lab_16')
                               );
$fac_info_result .= sprintf($fac_template   
                                ,'img/classroom/Tool-room_2.jpg' 
                                ,'100%'
                                ,trans('main/history.text_lab_16')
                               );

echo $fac_info_result;                               
?>
            </div>
        </div>           
    </div>
</div> 

<!-- ห้องวิจัย-->
<div id="fac-line-eng-white"> 
  <h1>{{trans('main/history.text_eng_research_room')}}</h1>
  <div id="history-tni" class="container">
      <hr class="mt-1 mb-5" color="#ededed ">
   </div>
</div>

<div class="container">
  <div class="row">
    <div class="col-12">
      <div id="accordion1">
<?php

$fac_template = <<<EOT
                <div class="card-header" id="%s">
                    <h4 class="mb-0" >
                        <a class="collapsed" tyle="text-indent:%s;" data-toggle="collapse" data-target="%s" aria-expanded="%s" aria-controls="%s">                           
                            <img src="%s" class="img-thumbnail" style="width:%s">
                            <span class="ml-3"> %s </span>
                        </a>
                    </h4>
                </div>
                <div id="%s" class="%s" data-parent="#accordion1" aria-labelledby="%s">
                    <div class="card-body faq">
                        %s            
                    </div>  
                </div>   

                <br>
        
EOT;
        
$fac_info_result = sprintf($fac_template   ,'faq-1' ,'10%' ,'#faq1' ,'true'  ,'faq1'
                                        ,'img/research_room/1.jpg' ,'50%'
                                        ,trans('main/history.text_research_1')
                                        ,'faq1' ,'collapse show','faq-1'
                                        ,trans('main/history.text_research_det_1')                        
                                       );
$fac_info_result .= sprintf($fac_template   ,'faq-2' ,'10%' ,'#faq2' ,'fals'  ,'faq2'
                                        ,'img/research_room/2.png','50%'
                                        ,trans('main/history.text_research_2')
                                        ,'faq2' ,'collapse','faq-2'
                                        ,trans('main/history.text_research_det_2')                        
                                       );
$fac_info_result .= sprintf($fac_template   ,'faq-3' ,'10%' ,'#faq3' ,'fals'  ,'faq3'
                                        ,'img/research_room/3.jpg','50%'
                                        ,trans('main/history.text_research_3')
                                        ,'faq3' ,'collapse','faq-3'
                                        ,trans('main/history.text_research_det_3')                        
                                       );
$fac_info_result .= sprintf($fac_template   ,'faq-4' ,'10%' ,'#faq4' ,'fals'  ,'faq4'
                                        ,'img/research_room/4.jpg','50%'
                                        ,trans('main/history.text_research_4')
                                        ,'faq4' ,'collapse','faq-4'
                                        ,trans('main/history.text_research_det_4')                        
                                       );
$fac_info_result .= sprintf($fac_template   ,'faq-5' ,'10%' ,'#faq5' ,'fals'  ,'faq5'
                                        ,'img/research_room/5.jpg','50%'
                                        ,trans('main/history.text_research_5')
                                        ,'faq5' ,'collapse','faq-5'
                                        ,trans('main/history.text_research_det_5')                        
                                       );
$fac_info_result .= sprintf($fac_template   ,'faq-6' ,'10%' ,'#faq6' ,'fals'  ,'faq6'
                                        ,'img/research_room/6.png','50%'
                                        ,trans('main/history.text_research_6')
                                        ,'faq6' ,'collapse','faq-6'
                                        ,trans('main/history.text_research_det_6')                        
                                       );
$fac_info_result .= sprintf($fac_template   ,'faq-7' ,'10%' ,'#faq7' ,'fals'  ,'faq7'
                                        ,'img/research_room/7.jpg','50%'
                                        ,trans('main/history.text_research_7')
                                        ,'faq7' ,'collapse','faq-7'
                                        ,trans('main/history.text_research_det_7')                        
                                       );
$fac_info_result .= sprintf($fac_template   ,'faq-8' ,'10%' ,'#faq8' ,'fals'  ,'faq8'
                                        ,'img/research_room/8.jpg','50%'
                                        ,trans('main/history.text_research_8')
                                        ,'faq8' ,'collapse','faq-8'
                                        ,trans('main/history.text_research_det_8')                        
                                       );
     
echo $fac_info_result;                               
?>
       
      </div> {{-- end accordion1 --}}

    </div>
  </div>
</div>


<br>
@endsection

@section('style')
<link rel="stylesheet" href="{{ url('lib/swiper-4.4.0/css/swiper.min.css') }}">
@endsection

@section('script')

<script src="{{ url('lib/swiper-4.4.0/js/swiper.min.js') }}"></script>
 <!-- jQuery library -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

 <!-- Popper JS -->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

 <!-- Latest compiled JavaScript -->
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

@endsection

