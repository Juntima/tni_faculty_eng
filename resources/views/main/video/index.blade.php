@extends('main.layouts.app')

@section('htmlheader_title'){{ trans('main/home.text_htmltitle') }}@endsection

@section('htmlheader_description'){{ trans('main/common.htmlheader_description') }}@endsection

@section('htmlheader_keywords'){{ trans('main/common.htmlheader_keywords') }}@endsection



@section('content-header')

    <div id="header" class="nav-item">

           <!-- <div class="container">
                <div class="second-header">{{ trans('main/home.text_tni_channel') }}</div>
            </div> -->
            <div class="container">
                <div class="second-header">
                    @if(@$breadcrumb && count($breadcrumb) > 0)
                    <ul class="breadcrumb">
                        @if(@$breadcrumb)
                            @foreach(@$breadcrumb as $key => $value)
                                @if(@$value['active'] == 'active')
                                    <li class="active">{!! $key !!}</li>
                                @else
                                    <li><a href="{{ url(@$value['url']) }}">{!! $key !!}</a></li>
                                @endif
                            @endforeach
                        @endif
                    </ul>
                    @endif
                </div>
            </div>

    </div><!-- End apply-now -->

    <!-- <div id="home-course" class="container">
    </div> -->    
@endsection

@section('main-content')

<br>
<div class="container ">
       
        @if(count($videos) > 0 )
        
            @foreach($videos->chunk(4) as $chunk) {{-- method chunk : แบ่ง object เป็น row row ละ 4 คอลัมน์ --}}
                <div class='row'>
                    @foreach($chunk as $item)
                    <div class="news-nav-pec-item col-sm-3 col-md-3 col-lg-3 col-xl-3" id="news-nav-pec" style="width:100%; hight:20%;">
                        <div style="background-image:#000;" >                                    
                            <a href="{{ url(Config::get('url.main.tni_channel').'/'.$item->id.'/detail') }}"> 
                                <img src="{{$item->image}}" class="img-fluid"> 
                            </a>   
                            
                             {{-- <iframe class="embed-responsive-item" src="{{$item->video_link}}" allowfullscreen></iframe>  --}}
                             
                           
                            
                                                                   
                        </div>

                        <a href="{{ url(Config::get('url.main.tni_channel').'/'.$item->id.'/detail') }}"> 
                            <h1 class="truncate_text">
                                {{$item->name}}<br><br>
                                <!-- <span style="color:#767676;"> {{date('d-m-Y', strtotime($item->publish_start)) }}</span> -->
                                <div class='poston' style='font-size:0.75em;'>
                                    <i class='fa fa-clock-o fa'></i> {{ date('d-M-Y', strtotime(@$item->publish_start)) }}
                                </div> 
                            </h1>
                            <br>      
                        </a>
                    </div>
                    @endforeach 
                </div>
            @endforeach 

            <div class='row'>
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style='text-align:right'>
                    <div class="pager">
                        <div class="pageinfo">
                            <span>{{ trans('backoffice/common.text_all_results') }} {{ @$videos->total() }} {{ trans('backoffice/common.text_item(s)') }}</span>
                            <span>{{ trans('backoffice/common.text_page') }} {{ $videos->currentPage () }} {{ trans('backoffice/common.text_from') }} {{ $videos->lastPage() }} {{ trans('backoffice/common.text_page(s)') }}</span>
                        </div>		
                    </div>	      
                </div>
            </div>

            <div class='row'>
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style='text-align:center'>
                    {{$videos->links('vendor.pagination.bootstrap-4')}}
                </div>
            </div>
        @else

            <div class='row'>
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style='text-align:center'>
                    {{trans('main/common.text_not_found_data')}}
                </div>
            </div>
            
        @endif



      


                        
</div>{{-- end containner --}}

@endsection


@section('style')
<link rel="stylesheet" href="{{ url('lib/swiper-4.4.0/css/swiper.min.css') }}">

@endsection

@section('script')

<script src="{{ url('lib/swiper-4.4.0/js/swiper.min.js') }}"></script>
 <!-- jQuery library -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

 <!-- Popper JS -->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

 <!-- Latest compiled JavaScript -->
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

 

@endsection

