@extends('main.layouts.app')

@section('htmlheader_title'){{ trans('main/home.text_htmltitle') }}@endsection

@section('htmlheader_description'){{ trans('main/common.htmlheader_description') }}@endsection

@section('htmlheader_keywords'){{ trans('main/common.htmlheader_keywords') }}@endsection



@section('content-header')

    <div id="header" class="nav-item">
            <!-- <div class="container">
                <div class="second-header">{{ trans('main/home.text_major') }}</div>
            </div> -->
            <div class="container">
                <div class="second-header">
                    @if(@$breadcrumb && count($breadcrumb) > 0)
                    <ul class="breadcrumb">
                        @if(@$breadcrumb)
                            @foreach(@$breadcrumb as $key => $value)
                                @if(@$value['active'] == 'active')
                                    <li class="active">{!! $key !!}</li>
                                @else
                                    <li><a href="{{ url(@$value['url']) }}">{!! $key !!}</a></li>
                                @endif
                            @endforeach
                        @endif
                    </ul>
                    @endif
                </div>
            </div>
    </div><!-- End apply-now -->
    <div id="fac-line-eng"> 
        <div class="container">
            <div class="row" >
                <div class="col-xs-6 col-md-6 col-lg-6">
                    <div class="fac-item">
                        <img src="img/major/eng3.png" class="img-fluid" />
                    </div>
                </div>
                <div class="col-xs-6  col-md-6 col-lg-6">
                    <div class="fac-item">                                           
                         
                            <h4 style="text-align:left; "> {{trans('main/major.text_bachelor')}} </h4>
                            <h4 style="text-align:left;" >
                                <ul style="list-style:circle;">
                                    <li><a class="shake"  id="fac" href="{{ url(Config::get('url.main.major_ae')) }}">{{trans('main/major.text_ae')}}</a></li>
                                    <li><a class="shake"  id="fac" href="{{ url(Config::get('url.main.major_le')) }}">{{trans('main/major.text_le')}}</a></li>
                                    <li><a class="shake"  id="fac" href="{{ url(Config::get('url.main.major_ce')) }}">{{trans('main/major.text_ce')}}</a></li>
                                    <li><a class="shake"  id="fac" href="{{ url(Config::get('url.main.major_ie')) }}">{{trans('main/major.text_ie')}}</a></li>   
                                    <li><a class="shake"  id="fac" href="{{ url(Config::get('url.main.major_ee')) }}">{{trans('main/major.text_ee')}}</a></li>                                    
                                </ul>                    
                            </h4>

                            <h4 style="text-align:left; ">{{trans('main/major.text_master')}}  </h4>
                            <h4 style="text-align:left;" >
                                <ul style="list-style:circle;">
                                    <li><a class="shake"  id="fac" href="{{ url(Config::get('url.main.major_met')) }}">{{trans('main/major.text_met')}}</a></li> 
                                </ul>                    
                            </h4>

                            <h4 style="text-align:left; "> {{trans('main/major.text_inter')}}  </h4>
                            <h4 style="text-align:left;" >
                                <ul style="list-style:circle;">
                                    <li><a class="shake"  id="fac" href="{{ url(Config::get('url.main.major_dge')) }}">{{trans('main/major.text_dge')}}</a></li>
                                </ul>                    
                            </h4>                                                            
                    </div>
                </div>
            </div>
        </div> 
    </div>

    

@endsection

@section('main-content')

<?php

$text_more = trans('main/major.text_more');

$fac_template1 = <<<EOT

<div id="fac-line-%s">
    <div class="container">
        <div class="row" >
            <div class="col-xs-6 col-md-6 col-lg-6">
                <div class="fac-item">
                    <img src="%s" class="img-fluid" />
                </div>
            </div>
            <div class="col-xs-6  col-md-6 col-lg-6">
                <div class="fac-item" >

                    <h2>%s</h2>
                    
                    <h2>%s</h2>
                    <h5 style="color:#767676; text-align:justify">
                         %s
                    </h5>

                    <h2>%s</h2>
                    <h5 style="color:#767676; text-align:justify">
                        <ol>  
                             %s
                        </ol>
                    </h5>

                    <a href="%s">
                        <h5 style="color:%s;font-weight: 600; text-align:right;"><b>{$text_more}</h5>
                    </a>
                </div>
            </div>
        </div>
    </div> 
</div>

EOT;
$fac_template2 = <<<EOT

<div id="fac-line-%s">
<div class="container">
            <div class="row" >
                    <div class="col-xs-6 col-md-6 col-lg-6">
                         <div class="fac-item">
                            <h2>%s</h2>
                    
                    <h2>%s</h2>
                    <h5 style="color:#767676; text-align:justify">
                         %s
                    </h5>

                    <h2>%s</h2>
                    <h5 style="color:#767676; text-align:justify">
                        <ol>  
                            %s
                        </ol>
                    </h5>
                            <a href="%s">
                                <h5 style="color:%s;font-weight: 600;  text-align:right;">{$text_more}</h5>
                            </a>
                        </div>
                    </div>

                    <div class="col-xs-6 col-md-6 col-lg-6">
                        <div class="fac-item">
                            <img src="%s" class="img-fluid" />
                        </div>
                    </div>
            </div>
</div>
</div>

EOT;

?>
<div id="fac_info"> 
    <?php
     
            $fac_info_result = sprintf($fac_template1 ,'eng-white'  //ae
                                ,'img/major/ae.png'
                                ,trans('main/major.text_ae')                            
                                ,trans('main/major.text_hilight_ae')
                                ,trans('main/major.text_hilight_detail_ae')
                                ,trans('main/major.text_job_ae')
                                ,trans('main/major.text_job_detail_ae')
                                ,url(Config::get('url.main.major_ae')) 
                                ,'#610000' ); 

            $fac_info_result.= sprintf($fac_template2 ,'eng-gray'  //le
                                ,trans('main/major.text_le')                          
                                ,trans('main/major.text_hilight_le')
                                ,trans('main/major.text_hilight_detail_le')
                                ,trans('main/major.text_job_le')
                                ,trans('main/major.text_job_detail_le')
                                ,url(Config::get('url.main.major_le')) 
                                ,'#610000' 
                                ,'img/major/ae.png');    
            $fac_info_result .= sprintf($fac_template1 ,'eng-white' //_ce
                                ,'img/major/ae.png' 
                                ,trans('main/major.text_ce')                         
                                ,trans('main/major.text_hilight_ce')
                                ,trans('main/major.text_hilight_detail_ce')
                                ,trans('main/major.text_job_ce')
                                ,trans('main/major.text_job_detail_ce')
                                ,url(Config::get('url.main.major_ce')) 
                                ,'#610000' );         
            $fac_info_result .= sprintf($fac_template2 ,'eng-gray'   //_ie                            
                                ,trans('main/major.text_ie')                         
                                ,trans('main/major.text_hilight_ie')
                                ,trans('main/major.text_hilight_detail_ie')
                                ,trans('main/major.text_job_ie')
                                ,trans('main/major.text_job_detail_ie')
                                ,url(Config::get('url.main.major_ie')) 
                                ,'#610000' 
                                ,'img/major/ae.png' );            
            $fac_info_result .= sprintf($fac_template1 ,'eng-white' //ee
                                ,'img/major/ae.png' 
                                ,trans('main/major.text_ee')                         
                                ,trans('main/major.text_hilight_ee')
                                ,trans('main/major.text_hilight_detail_ee')
                                ,trans('main/major.text_job_ee')
                                ,trans('main/major.text_job_detail_ee')
                                ,url(Config::get('url.main.major_ee')) 
                                ,'#610000' );            
            $fac_info_result .= sprintf($fac_template2 ,'eng-gray' //met                              
                                ,trans('main/major.text_met')                         
                                ,trans('main/major.text_hilight_met')
                                ,trans('main/major.text_hilight_detail_met')
                                ,trans('main/major.text_job_met')
                                ,trans('main/major.text_job_detail_met')
                                ,url(Config::get('url.main.major_met')) 
                                ,'#610000' 
                                ,'img/major/ae.png' );            
            $fac_info_result .= sprintf($fac_template1 ,'eng-white'  //dge
                                ,'img/major/ae.png'
                                ,trans('main/major.text_dge')                         
                                ,trans('main/major.text_hilight_dge')
                                ,trans('main/major.text_hilight_detail_dge')
                                ,trans('main/major.text_job_dge')
                                ,trans('main/major.text_job_detail_dge')
                                ,url(Config::get('url.main.major_dge')) 
                                ,'#610000' );                            

           

echo $fac_info_result;
    ?>
</div>



<div id="fac-line-footer" > <!-- ศิษย์เก่า-->
    <h2> {{ trans('main/major.text_alumni')}}</h2>
                <div class="swiper-container">
                    <div class="swiper-wrapper" id="alumni">
<?php 
$fac_template1 = <<<EOT
                        <div class="swiper-slide" >
                                <img src="%s"  id ="img-graduate"  >
                                    <h4 style="color:#fff; font-weight: 600;">
                                        <i class="fa fa-quote-left"></i>
                                            &nbsp; %s  &nbsp;
                                        <i class="fa fa-quote-right"></i>  <br><br>
                                    
                                    </h4> <br/>
                                    <h4 style="color:#fff; font-weight: 600; "> %s </h4> <br><br>
                                    <h5 style="color:#fff; "> %s </h5>
                        </div>
                
                   
EOT;
    
$fac_info_result = sprintf($fac_template1 
                                    ,'img/alumni/alum.jpg' 
                                    ,trans('main/major.text_comment4')
                                    ,trans('main/major.text_majors4')
                                    ,trans('main/major.text_name4')
                                ); 
$fac_info_result.= sprintf($fac_template1   
                                    ,'img/alumni/alum.jpg' 
                                    ,trans('main/major.text_comment5')
                                    ,trans('main/major.text_majors5')
                                    ,trans('main/major.text_name5')                              
                                );     
$fac_info_result.= sprintf($fac_template1   
                                    ,'img/alumni/alum.jpg' 
                                    ,trans('main/major.text_comment6')
                                    ,trans('main/major.text_majors6')
                                    ,trans('main/major.text_name6')                              
                                );                                                             
    
echo $fac_info_result;                              
?>
    
                    </div>
    
                    <div class="swiper-pagination-graduate"></div>
    
                </div>
 
 
 
@endsection

@section('style')
<link rel="stylesheet" href="{{ url('lib/swiper-4.4.0/css/swiper.min.css') }}">

@endsection

@section('script')
<script src="{{ url('lib/swiper-4.4.0/js/swiper.min.js') }}"></script>
 <!-- jQuery library -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

 <!-- Popper JS -->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

 <!-- Latest compiled JavaScript -->
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script>
var homeHeroSwiper = new Swiper('#fac-line-footer .swiper-container', {
    loop: true,
    autoplay: {
        delay: 5000,
        disableOnInteraction: false
    },
    pagination: {
        el: '.swiper-pagination-graduate',
        clickable: true,
    },

});

</script>





@endsection

