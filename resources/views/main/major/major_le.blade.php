@extends('main.layouts.app')

@section('htmlheader_title'){{ trans('main/home.text_htmltitle') }}@endsection

@section('htmlheader_description'){{ trans('main/common.htmlheader_description') }}@endsection

@section('htmlheader_keywords'){{ trans('main/common.htmlheader_keywords') }}@endsection



@section('content-header')

    <div id="header" class="nav-item">
            <!-- <div class="container">
                <div class="second-header">{{ trans('main/home.text_faculty') }}</div>
            </div> -->
            <div class="container">
                <div class="second-header">
                    @if(@$breadcrumb && count($breadcrumb) > 0)
                    <ul class="breadcrumb">
                        @if(@$breadcrumb)
                            @foreach(@$breadcrumb as $key => $value)
                                @if(@$value['active'] == 'active')
                                    <li class="active">{!! $key !!}</li>
                                @else
                                    <li><a href="{{ url(@$value['url']) }}">{!! $key !!}</a></li>
                                @endif
                            @endforeach
                        @endif
                    </ul>
                    @endif
                </div>
            </div>
    </div><!-- End apply-now -->

@endsection

@section('main-content')

<div id="fac-line-eng-white" class="container"> <!-- ชื่อตณะ และรายละเอียด -->
    <h1>{{ trans('main/major_le.text_name') }}</h1>
    <h5 class="fac_detail"> {!! trans('main/major_le.text_detail') !!} </h5>
    
</div>

<div id="fac-line-eng-header"> <!---<div ชื่อปริญญาและสาขาวิชา -->
    <div class="container">
        <div class="row" >
            <div class="col-xs-6 col-md-6 col-lg-6">
                <div class="fac-item">
                    <img src="img/major/ae.jpg" class="img-fluid" />
                </div>
            </div>
            <div class="col-xs-6  col-md-6 col-lg-6">
                <div class="major-item">                                           
                    <h2 style="text-align:left;">{{ trans('main/major_le.text_program_name') }}</h2>
                    @if(count($name_course) > 0 )
                        @for($i=0;$i<count($name_course);$i++)
                            <h4 style="text-align:left; ">{{$name_course[$i]['topic'] }}</h4>
                            <h4 style="text-align:left;" >
                                <ul style="list-style:circle;">
                                    <li>{{$name_course[$i]['name'] }}</li>
                                </ul>                    
                            </h4>
                        @endfor                             
                    @endif
                </div>
            </div>
        </div>
    </div> 
</div>

<div id="fac-line-eng-white"> <!-- โอกาสในการประกอบอาชีพ -->
    
    <h1>{{trans('main/major_le.text_fjob')}}</h1>
    <div class="container">
        <div class="row" >
            <div class="col-xs-6 col-md-6 col-lg-6">
                <div class="fac-item">
                    <img src="img/major/ae.jpg" class="img-fluid" />
                </div>
            </div>
            <div class="col-xs-6  col-md-6 col-lg-6">
                <div class="fac-item" >               
                    <h5 style="color:#767676; text-align:justify">
                        <ol>  
                            @foreach(trans('main/major_le.text_job_detail') as $job_list)
                                <li>{{$job_list}}</li>
                            @endforeach 
                        </ol>                    
                    </h5>                
                </div>
            </div>
        </div>
    </div> 
</div>

<div id="fac-line-eng-gray"> <!-- โครงสร้างหลักสูตร -->
    <h1>{{trans('main/major_le.text_struct')}}</h1>
    <div class="container">
            <div class="row" >             
                <div class="col-xs-12  col-md-12 col-lg-12"> 
                    <div class="fac-item" >
                        <table class="table table-striped">                  
                            <tbody class="unit ">
                                <tr class="table-light " >
                                    <td style="text-align:left"><b> {{trans('main/major_le.text_main_couse_name')}} </b></td>
                                    <td><b>{{trans('main/major_le.text_main_couse_unit')}}<b></td>
                                    <td><b>{{trans('main/major_le.text_unit')}}<b></td>
                                </tr>

                                @if(count($struct_detail) > 0 )                             
                                        @for($i=0;$i<count($struct_detail);$i++)
                                            <tr class="table-default" id="det">
                                                <td id="topic">{!!$struct_detail[$i]['name'] !!} </td>
                                                <td>{{ $struct_detail[$i]['detail'] }} </td>
                                                <td>{{trans('main/major_le.text_unit')}}</td>
                                            </tr>
                                        @endfor                        
                                @endif                           
                            </tbody>               
                        </table>                       
                    </div>
                </div>
    
            </div><!-- end row -->
    </div>
</div>
<div id="fac-line-eng-white"><!-- แผนการศึกษา -->
    <h1>{{trans('main/major_le.text_program')}}</h1>
    <div class="container ">   
        <div class="row" >
            <div class="col-md-12">      
                <nav>
                    <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-y1-tab" data-toggle="tab" href="#nav-y1" role="tab" aria-controls="nav-y1" aria-selected="true">{{trans('main/major_le.text_year1')}}</a>
                        <a class="nav-item nav-link" id="nav-y2-tab" data-toggle="tab" href="#nav-y2" role="tab" aria-controls="nav-y2" aria-selected="false">{{trans('main/major_le.text_year2')}}</a>
                        <a class="nav-item nav-link" id="nav-y3-tab" data-toggle="tab" href="#nav-y3" role="tab" aria-controls="nav-y3" aria-selected="false">{{trans('main/major_le.text_year3')}}</a>
                        <a class="nav-item nav-link" id="nav-y4-tab" data-toggle="tab" href="#nav-y4" role="tab" aria-controls="nav-y4" aria-selected="false">{{trans('main/major_le.text_year4')}}</a>
                    </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">
<?php

    $text_sum = trans('main/major_le.text_sum') ;
    $text_sub_id = trans('main/major_le.text_sub_id');
    $text_sub_name = trans('main/major_le.text_sub_name');
    $text_unit = trans('main/major_le.text_unit') ;   

$fac_template1 = <<<EOT

        <div class="%s" id="%s" role="tabpanel" aria-labelledby="%s">
            <div class="row">
                <div class="col-xs-6  col-md-6 col-lg-6"> 
                    <div class="fac-item" >
                        <table class="table table-striped">                  
                            <tbody class="unit ">
                                <tr class="table-defult " >
                                    <td style="text-align:center;font-size:26px;" colspan="3"><b> %s </b></td>
                                </tr>

                                <tr class="table-light " >
                                    <td style="text-align:left;"><b> {$text_sub_id} </b></td>
                                    <td style="text-align:left;"><b> {$text_sub_name} </b></td>
                                    <td style="text-align:center;"><b> {$text_unit} </b></td>
                                </tr>
EOT;

$fac_template2 = <<<EOT
                               
                                 <tr class="table-light" >
                                    <td style="text-align:left;">%s</td>
                                    <td style="text-align:left;color:#610000">%s</td>
                                    <td style="text-align:center;">%s</td>                                               
                                </tr>

EOT;

$fac_template3 = <<<EOT
                             
                                <tr class="table-light" >
                                    <td style="text-align:right;" colspan="2"><b> {$text_sum} </b></td> 
                                    <td style="text-align:center;" ><b>  %s  </b></td>                                                                                         
                                </tr>      
                            </tbody>               
                        </table>                       
                    </div>
                </div>
         

EOT;

$fac_template4 = <<<EOT

      
                <div class="col-xs-6  col-md-6 col-lg-6"> 
                    <div class="fac-item" >
                        <table class="table table-striped">                  
                            <tbody class="unit ">
                                <tr class="table-defult " >
                                    <td style="text-align:center;font-size:26px;" colspan="3"><b> %s </b></td>
                                </tr>

                                <tr class="table-light " >
                                    <td style="text-align:left;"><b> {$text_sub_id} </b></td>
                                    <td style="text-align:left;"><b> {$text_sub_name} </b></td>
                                    <td style="text-align:center;"><b> {$text_unit} </b></td>
                                </tr>
EOT;

$fac_template5 = <<<EOT
                               
                                 <tr class="table-light" >
                                    <td style="text-align:left;">%s</td>
                                    <td style="text-align:left;color:#610000">%s</td>
                                    <td style="text-align:center;">%s</td>                                               
                                </tr>

EOT;

$fac_template6 = <<<EOT
                             
                                <tr class="table-light" >
                                    <td style="text-align:right;" colspan="2"><b> {$text_sum} </b></td> 
                                    <td style="text-align:center;" ><b>  %s  </b></td>                                                                                         
                                </tr>      
                            </tbody>               
                        </table>                       
                    </div>
                </div>

            </div>
        </div>

EOT;



// --- ปี1 เทอม 1 --- //
$fac_info_result = sprintf($fac_template1 , "tab-pane fade show active" , "nav-y1" ," nav-y1-tab" ,
                     trans('main/major_le.text_year1_semester1'));    
                    
                    if(count($subject_y1s1) > 0 ){                                             
                        for($i=0;$i<count($subject_y1s1);$i++){                           
$fac_info_result .= sprintf($fac_template2 
                                ,$subject_y1s1[$i]['sub_id']                              
                                ,$subject_y1s1[$i]['sub_name']
                                ,$subject_y1s1[$i]['sub_unit']                                                                                 
                            );
                        }  
                    }                      
                                                         
$fac_info_result.= sprintf($fac_template3 ,trans('main/major_le.text_sub_y1s1_unit_sum'));
                                
// --- ปี1 เทอม 2 --- //
$fac_info_result .= sprintf($fac_template4 , trans('main/major_le.text_year1_semester2') );
 
                    if(count($subject_y1s2) > 0 ){                                             
                        for($i=0;$i<count($subject_y1s2);$i++){                          
$fac_info_result .= sprintf($fac_template5 
                                ,$subject_y1s2[$i]['sub_id']                              
                                ,$subject_y1s2[$i]['sub_name']
                                ,$subject_y1s2[$i]['sub_unit']                                                                                 
                        );
                        }  
                    }                           
                                                         
$fac_info_result.= sprintf($fac_template6,trans('main/major_le.text_sub_y1s2_unit_sum'));

//-------------------------------------------------------------------------------------------------

// --- ปี2 เทอม 1 --- //
$fac_info_result .= sprintf($fac_template1 , "tab-pane fade" , "nav-y2" ,"nav-y2-tab" ,
                     trans('main/major_le.text_year2_semester1'));    
                    
                    if(count($subject_y2s1) > 0 ){                                             
                        for($i=0;$i<count($subject_y2s1);$i++){                           
$fac_info_result .= sprintf($fac_template2 
                                ,$subject_y2s1[$i]['sub_id']                              
                                ,$subject_y2s1[$i]['sub_name']
                                ,$subject_y2s1[$i]['sub_unit']                                                                                 
                            );
                        }  
                    }                      
                                                         
$fac_info_result.= sprintf($fac_template3 ,trans('main/major_le.text_sub_y2s1_unit_sum'));
                                                                
// --- ปี2 เทอม 2 --- //
$fac_info_result .= sprintf($fac_template4 , trans('main/major_le.text_year2_semester2') );
 
                    if(count($subject_y2s2) > 0 ){                                             
                        for($i=0;$i<count($subject_y2s2);$i++){                          
$fac_info_result .= sprintf($fac_template5 
                                ,$subject_y2s2[$i]['sub_id']                              
                                ,$subject_y2s2[$i]['sub_name']
                                ,$subject_y2s2[$i]['sub_unit']                                                                                 
                        );
                        }  
                    }                           
                                                         
$fac_info_result.= sprintf($fac_template6,trans('main/major_le.text_sub_y2s2_unit_sum') );

//-------------------------------------------------------------------------------------------------

// --- ปี3 เทอม 1 --- //
$fac_info_result .= sprintf($fac_template1 , "tab-pane fade" , "nav-y3" ,"nav-y3-tab" ,
                     trans('main/major_le.text_year3_semester1'));    
                    
                    if(count($subject_y3s1) > 0 ){                                             
                        for($i=0;$i<count($subject_y3s1);$i++){                           
$fac_info_result .= sprintf($fac_template2 
                                ,$subject_y3s1[$i]['sub_id']                              
                                ,$subject_y3s1[$i]['sub_name']
                                ,$subject_y3s1[$i]['sub_unit']                                                                                 
                            );
                        }  
                    }                      
                                                         
$fac_info_result.= sprintf($fac_template3 ,trans('main/major_le.text_sub_y3s1_unit_sum'));
                                                                
// --- ปี3 เทอม 2 --- //
$fac_info_result .= sprintf($fac_template4 , trans('main/major_le.text_year3_semester2') );
 
                    if(count($subject_y3s2) > 0 ){                                             
                        for($i=0;$i<count($subject_y3s2);$i++){                          
$fac_info_result .= sprintf($fac_template5 
                                ,$subject_y3s2[$i]['sub_id']                              
                                ,$subject_y3s2[$i]['sub_name']
                                ,$subject_y3s2[$i]['sub_unit']                                                                                 
                        );
                        }  
                    }                           
                                                         
$fac_info_result.= sprintf($fac_template6,trans('main/major_le.text_sub_y3s2_unit_sum') );

//-------------------------------------------------------------------------------------------------

// --- ปี4 เทอม 1 --- //
$fac_info_result .= sprintf($fac_template1 , "tab-pane fade" , "nav-y4" ,"nav-y4-tab" ,
                     trans('main/major_le.text_year4_semester1'));    
                    
                    if(count($subject_y4s1) > 0 ){                                             
                        for($i=0;$i<count($subject_y4s1);$i++){                           
$fac_info_result .= sprintf($fac_template2 
                                ,$subject_y4s1[$i]['sub_id']                              
                                ,$subject_y4s1[$i]['sub_name']
                                ,$subject_y4s1[$i]['sub_unit']                                                                                 
                            );
                        }  
                    }                      
                                                         
$fac_info_result.= sprintf($fac_template3 ,trans('main/major_le.text_sub_y4s1_unit_sum'));
                                                                
// --- ปี4 เทอม 2 --- //
$fac_info_result .= sprintf($fac_template4 , trans('main/major_le.text_year4_semester2') );
 
                    if(count($subject_y4s2) > 0 ){                                             
                        for($i=0;$i<count($subject_y4s2);$i++){                          
$fac_info_result .= sprintf($fac_template5 
                                ,$subject_y4s2[$i]['sub_id']                              
                                ,$subject_y4s2[$i]['sub_name']
                                ,$subject_y4s2[$i]['sub_unit']                                                                                 
                        );
                        }  
                    }                           
                                                         
$fac_info_result.= sprintf($fac_template6,trans('main/major_le.text_sub_y4s2_unit_sum') );

//-------------------------------------------------------------------------------------------------



// แสดงผลทั้งหมด
echo $fac_info_result;

?>                  
               </div><!-- end tabs-->
            </div><!-- end col-12 -->
        </div><!-- end row -->

    </div>
</div>

<!-- donload doc PDF-->
<div class="d-flex justify-content-center" id="download-doc">
    <a class="btn btn-lg shake" href="{{ trans('main/major_le.text_link_pdf_file') }}">{{ trans('main/major_le.text_download_doc') }}</a>
</div>


@endsection

@section('style')
<link rel="stylesheet" href="{{ url('lib/swiper-4.4.0/css/swiper.min.css') }}">

@endsection

@section('script')
<script src="{{ url('lib/swiper-4.4.0/js/swiper.min.js') }}"></script>
 <!-- jQuery library -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

 <!-- Popper JS -->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

 <!-- Latest compiled JavaScript -->
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script>
var homeHeroSwiper = new Swiper('#fac-line-footer .swiper-container', {
    loop: true,
    autoplay: {
        delay: 5000,
        disableOnInteraction: false
    },
    pagination: {
        el: '.swiper-pagination-graduate',
        clickable: true,
    },

});

</script>

<style>
    [id^='fac_thumbnal'],[id^='level_']{
          cursor:pointer;
    }   
</style>



@endsection
