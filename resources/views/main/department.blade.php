@extends('main.layouts.app')

@section('htmlheader_title'){{ trans('main/home.text_htmltitle') }}@endsection

@section('htmlheader_description'){{ trans('main/common.htmlheader_description') }}@endsection

@section('htmlheader_keywords'){{ trans('main/common.htmlheader_keywords') }}@endsection



@section('content-header')

    <div id="header" class="nav-item">

            <!-- <div class="container">
                <div class="second-header">{{ trans('main/department.text_department') }}</div>
            </div> -->
            <div class="container">
                <div class="second-header">
                    @if(@$breadcrumb && count($breadcrumb) > 0)
                    <ul class="breadcrumb">
                        @if(@$breadcrumb)
                            @foreach(@$breadcrumb as $key => $value)
                                @if(@$value['active'] == 'active')
                                    <li class="active">{!! $key !!}</li>
                                @else
                                    <li><a href="{{ url(@$value['url']) }}">{!! $key !!}</a></li>
                                @endif
                            @endforeach
                        @endif
                    </ul>
                    @endif
                </div>
            </div>

    </div><!-- End apply-now -->
    <div id="home-course" class="container">

    </div>

    
@endsection


@section('main-content')
<br>
<div class="container">
        <div class="row" id="card-dep">
                <div class="col-xs-6 col-sm-12 col-md-12 col-lg-12" >

                    <div class="row" id="card-dep">

                        <div class="col-xs-3 col-sm-6 col-md-3 col-lg-3">
                            <div class="history-item" >
                                <a href="https://admission.tni.ac.th/home/2017/main" target="_blank">
                                    <div class="card text-white shake" >
                                        <div class="card-body dep" id="dep" style="font-size:25px; line-height: 1.5em;">
                                            <i class="fa fa-user-plus fa-3x"></i><br><?php echo trans('main/department.text_regiter_center'); ?>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div> {{-- ศูนย์รับสมัคร --}}
                        <div class="col-xs-3 col-sm-6 col-md-3 col-lg-3">
                            <div class="history-item">
                                <a href="https://coe-isi.tni.ac.th/" target="_blank">
                                    <div class="card text-white shake" >
                                        <div class="card-body dep" id="dep" style="font-size:25px; line-height: 1.5em;">
                                            <i class="fa fa-cogs fa-3x"></i><br>{{ trans('main/department.text_coe') }}
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div> {{-- coe-isi --}}   
                              
                        <div class="col-xs-3 col-sm-6 col-md-3 col-lg-3">
                            <div class="history-item">
                                <a href="{{ url(Config::get('url.main.exchange_programs')) }}" target="_blank">
                                    <div class="card text-white shake" >
                                        <div class="card-body dep" id="dep">
                                            <i class="fa fa-bullhorn fa-3x"></i><br>{{ trans('main/department.text_public_relations') }}
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div> {{-- ประชาสัมพันธ์ --}}                 
                        <div class="col-xs-3 col-sm-6 col-md-3 col-lg-3">
                            <div class="history-item">
                                <a href="{{ url(Config::get('url.main.finances')) }}" >
                                    <div class="card text-white shake">                                       
                                        <div class="card-body dep " id="dep">
                                            <i class="fa fa-users fa-3x"></i><br>{!!trans('main/department.text_adminsitrator')!!}
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>{{-- บริหาร --}}
                    
                    </div> {{-- end div row 1 --}}

                    <div class="row" id="card-dep">

                        <div class="col-xs-3  col-sm-6 col-md-3 col-lg-3">
                            <div class="history-item">
                                <a href="https://coop.tni.ac.th/2014/new/main/" target="_blank">
                                    <div class="card text-white shake" >
                                        <div class="card-body dep" id="dep">
                                           <i class="fa fa-suitcase fa-3x"></i><br><br>{{ trans('main/department.text_cooperative_career') }}
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>   {{-- สหกิจ --}}   
                        <div class="col-xs-3 col-sm-6 col-md-3 col-lg-3">
                            <div class="history-item">
                                <a href="https://icc.tni.ac.th/" target="_blank">
                                    <div class="card text-white shake">                                       
                                        <div class="card-body dep " id="dep">
                                            <i class="fa fa-desktop fa-3x"></i><br><br>{{ trans('main/department.text_icc') }}
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div> {{-- icc --}}                                            
                        <div class="col-xs-3 col-sm-6 col-md-3 col-lg-3">
                            <div class="history-item">
                                <a href="https://sites.google.com/a/tni.ac.th/fay-wichakar/?pli=1" target="_blank">
                                    <div class="card text-white shake">                                       
                                        <div class="card-body dep " id="dep">
                                            <i class="fa fa-line-chart fa-3x"></i><br><br>{{ trans('main/department.text_academic') }}
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div> {{-- วิชาการ --}}
                        <div class="col-xs-3 col-sm-6 col-md-3 col-lg-3">
                            <div class="history-item" >
                                <a href="https://studentaffairs.tni.ac.th/home/" target="_blank">
                                    <div class="card text-white shake" >
                                        <div class="card-body dep" id="dep">
                                            <i class="fas fa-user-graduate fa-3x"></i><br><br>{{ trans('main/department.text_studen_affairs') }}
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div> {{-- กิจการนศ. --}}

                    </div>{{-- end div row 2 --}}

                    <div class="row" id="card-dep">

                        <div class="col-xs-3 col-sm-6 col-md-3 col-lg-3">
                            <div class="history-item" >
                                <a href="https://sites.google.com/a/tni.ac.th/tni_library/library-2018" target="_blank">
                                    <div class="card text-white shake" >
                                        <div class="card-body dep" id="dep">
                                            <i class="fa fa-book fa-3x"></i><br><br><?php echo trans('main/department.text_library'); ?>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div> {{-- วิทยบริการ --}}                        
                        <div class="col-xs-3 col-sm-6 col-md-3 col-lg-3">
                            <div class="history-item">
                                <a href="https://www.tci-thaijo.org/index.php/TNIJournal/index" target="_blank">
                                    <div class="card text-white shake" >
                                        <div class="card-body dep" id="dep">
                                                <i class="fas fa-newspaper fa-3x"></i><br>{!! trans('main/department.text_journal_en') !!}
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div> {{-- บริการวิชาการ --}}
                        <div class="col-xs-3 col-sm-6 col-md-3 col-lg-3">
                            <div class="history-item" >  
                                <a href="https://www.tci-thaijo.org/index.php/TNIJournalBA/index" target="_blank">
                                    <div class="card text-white shake" >
                                        <div class="card-body dep" id="dep" style="font-size:24.8px; line-height: 1.5em;">
                                            <i class="fa fa-newspaper-o fa-3x"></i><br>{{trans('main/department.text_journal_ba')}}
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div> {{-- วารสารวิชาการ --}}                       
                        <div class="col-xs-3 col-sm-6 col-md-3 col-lg-3">
                            <div class="history-item">
                                <a href="https://research.tni.ac.th/new/main/" target="_blank">
                                    <div class="card text-white shake">                                       
                                        <div class="card-body dep " id="dep">
                                                <i class="fas fa-scroll fa-3x"></i><br><br>{{ trans('main/department.text_research_and_academic_services') }}
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div> {{-- วิจัย --}}

                    </div> {{-- end div row 3 --}}
                   
                    <div class="row"  id="card-dep">
                        <div class="col-xs-3 col-sm-6 col-md-3 col-lg-3">
                            <div class="history-item">
                                <a href="https://csd.tni.ac.th/" target="_blank">
                                    <div class="card text-white shake" >
                                        <div class="card-body dep" id="dep">
                                            <i class="fa fa-leanpub fa-3x"></i><br><br>{{ trans('main/department.text_community_services') }}
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div> {{-- บริการวิชาการ --}}
                    </div>

                </div> {{-- end div col-12 --}}   
        </div>   {{-- end row card --}}

</div>{{-- end containner --}}
    

<br>
@endsection

@section('style')
<link rel="stylesheet" href="{{ url('lib/swiper-4.4.0/css/swiper.min.css') }}">
@endsection

@section('script')

<script src="{{ url('lib/swiper-4.4.0/js/swiper.min.js') }}"></script>
 <!-- jQuery library -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

 <!-- Popper JS -->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

 <!-- Latest compiled JavaScript -->
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

 <style>
        [id^='card'],[id^='accordion']{
          cursor:pointer;
        }
</style>



@endsection

