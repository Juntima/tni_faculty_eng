<?php

return[
    'text_admissions' => 'Admission (bachelor degree)',
    'text_admission_lists' => 'Admission Lists (bachelor degree)',
    'text_create_admission' => 'Crerate Admission (bachelor degree)',
    'text_edit_admission' => 'Edit Admission (bachelor degree)',
    'text_id' => 'ID',
    'text_image_cover' => 'Cover Image', //หน้ารวม
    'text_image_highlight' => 'Highlight Image', //highlight
    'text_name' => 'Name',
    'text_link' => 'Link',
    'text_pdf_link' => 'PDF Link',

];
?>