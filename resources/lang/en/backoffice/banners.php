<?php

    return [
        'text_banner' => 'Banner',
        'text_banner_lists' => 'Banners Lists',
        'text_create_banner' => 'Create Banner',
        'text_background_color' => 'Background',
        'text_edit_banner' => 'Edit Banner',

        //Banner highlight

        'text_banner_highlight' => 'Banner Highlight',
        'text_banner_highlight_lists' => 'Banner Highlight List',
        'text_create_banner_highlight' => 'Create Banner Highlight ',
        'text_edit_banner_highlight' => 'Edit Banner Highlight',
    ];