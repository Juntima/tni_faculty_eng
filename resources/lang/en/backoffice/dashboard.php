<?php

return [

    'text_dashboard' => 'DASHBOARD',
    'text_news' => 'NEWS',
    'text_event_update' => 'EVENT UPDATE',
    'text_exchange_program' => 'EXCHANGE PROGRAM',
    'text_co_operative' => 'CO-OPERATIVE',
    'text_student_portfolio' => 'STUDENT PORTFOLIO',
    'text_tni_channel' => 'TNI CHANNEL',
    'text_e_bochure' => 'E-BOCHURES',
    'text_banner' => 'BANNERS',
    'text_information' => 'INFORMATIONS',
    'text_admission' => 'ADMISSIONS'
];
