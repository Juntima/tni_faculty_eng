<?php

$arr_texts1[] = "จัดการ การศึกษาระดับอุดมศึกษาในสาขาวิชาชีพเฉพาะทางชั้นสูงที่เป็นความต้องการของ ภาคธุรกิจและ ภาคอุตสาหกรรม โดยมุ่งเน้นความเป็นเลิศทางวิชาการ การปฏิบัติ และการประยุกต์ใช้จริง";
$arr_texts1[] = "พัฒนานักศึกษาให้มีความรู้คู่คุณธรรม คิดเป็นทำเป็น มีความรับผิดชอบในการทำงานเป็นแบบอย่างที่ดีและมีจิตสำนึกต่อสังคม";
$arr_texts1[] = "ดำเนินการวิจัย สร้างสรรค์และพัฒนาเทคโนโลยีและองค์ความรู้ใหม่ๆ เพื่อสนับสนุนการเรียนการสอน การพัฒนาภาคธุรกิจและอุตสาหกรรม";
$arr_texts1[] = "ถ่ายทอดความรู้และเทคโนโลยีชั้นสูง เพื่อเสริมสร้างความสามารถในการแข่งขันของภาคธุรกิจและอุตสาหกรรม";
$arr_texts1[] = "ทำนุบำรุง ส่งเสริม เผยแพร่ และแลกเปลี่ยนศิลปวัฒนธรรมและภูมิปัญญาไทย";

//_จุดเด่นและจุดเน้นของสถาบันฯ
$arr_texts2[] = "ความสัมพันธ์และความร่วมมือไทย-ญี่ปุ่น";
$arr_texts2[] = "การพัฒนานักศึกษาตามหลักโมโนซุคุริ";

//_ค่านิยมหลัก
$arr_texts3[] = "Monodzukuri (คิดเป็น ทำเป็น)";
$arr_texts3[] = "Kaizen (ปรับปรุงอย่างต่อเนื่อง)";
$arr_texts3[] = "Hansei (Self-Reflection พิจารณาและปรับปรุงตนเองก่อน)";
$arr_texts3[] = "Honest (ซื่อสัตย์ ไม่คดโกง ไม่เอาเปรียบผู้อื่น)";
$arr_texts3[] = "Respect (ให้เกียรติตนเองและผู้อื่น)";
$arr_texts3[] = "Public-Interest Conscious (ตระหนักถึงประโยชน์ส่วนรวม)";

    return [
        'text_intro'            => '(google trans)Introduce TNI',

        'text_history'                    => 'History of the institution, Philosophy',                               //_menu
        'text_resolution_vision_mission' => 'Resolution,Vision,Mission',                                            //_menu
        'text_character'                  => 'อัตลักษณ์ของบัณฑิต,เอกลักษณ์ของสถาบันฯ จุดเด่นและจุดเน้นของสถาบันฯ',   //_menu
        'text_value'                      => 'Value',                                                          //_menu
        'text_symbolic_color'             => 'สัญลักษณ์และสีประจำสถาบัน',                                            //_menu
        'text_icon_tree'                  => 'พระประธานประจำสถาบัน,ต้นไม้ประจำสถาบัน',                               //_menu
        'text_monument_feature'           => 'อนุสาวรีย์ผู้ก่อตั้ง,ลักษณะเด่นของสถาบัน',                             //_menu
        
        'text_history_tni' => 'History of the institution',
        'text_philosophy'  => 'Philosophy of TNI',
        'text_around_tni'  => 'Around TNI',
        'text_resolution'  => 'Resolution',
        'text_vision'      => 'Vision',
        'text_mission'     => 'Mission',

        'text_graduate_identity'         => 'TNI Graduates Key competence',
        'text_institute_identity'        => 'Institute Identity',
        'text_institute_highlight_focus' => 'Highlight & Focus',
        
        'text_content_institution_history' => "(demo : google trans)Technology Promotion Association (Thai-Japanese) or TBA is an institution established by cooperation And the sacrifice of a group of people who had studied and practiced in Japan with His Excellency Sommai Hoontrakul as Chairman of the Founding Committee And received great help from Ajarn Ngokichi Hosumi, former Chairman of the Japan-Thai Economic Cooperation Association Committee MPs have received support for activities from the Ministry of Economy, Trade and Industry (METI), Japan from the past to the present. The TBA was formally established on January 24, 1973 with the objective of promoting and transferring new knowledge and technology. To Thai personnel Throughout the past period, the operation of the TBA With continuous expansion And build reputation in many areas, such as training in technology and management seminars Foreign language teaching is Japanese, English and Chinese. Providing calibration services for industrial instrumentation and environmental analysis. Publishing new books and journals in technology and management And consulting with many industrial establishments From experience and expertise in various academic training fields For enterprises in the industrial sector Together with being a center for a variety of specialist fields Therefore, the TPA has plans to establish higher education institutions that create personnel with Specializing in specialized technologies In order to enter personnel for Thai industrial establishments and in 2005, TPA has implemented a project to establish higher education institutions in the name of \"Thai-Japanese Institute of Technology\" by using the abbreviation TBA or English name \"Thai-Nichi Institute of Technology\" using the abbreviation TNI and on September 29, 2006 Institute Has been granted permission from the Ministry of Education to be a new higher education institution And began teaching in the 1st semester of the academic year 2007 in various courses Both undergraduate and graduate degrees Focused on engineering technology Information and management needed for Thai business and industry" ,

        'text_content_philosophy' => "Technology Promotion Association (Thailand-Japan), the TNI's founder, has been operated under the philosophy of \"disseminating knowledge, building economic base\" reflecting the clear direction and goal in the operation and services of TPA thoroughly over 45 years which as a bridge of technology dissemination on management and engineering from Japan to the Thai personnel widely, TPA has taken a major part in cultivating human resources to be competent in knowledge and skills in order to help develop the Kingdom's economy.

        TNI establishing committee contemplated that the philosophy of TPA and TNI regarding organizational operation must be in accordance with each other in order to proliferate the value of activities and services and expand the servicing role by aiming TNI to be an academic institute focusing on creating and developing the Kingdom's personnel, a resource for creating new knowledge and a channel for disseminating knowledge to society, especially industrial sector. Therefore, the TNI philosophy for operation is for \"Developing knowledge, enriching industry for economy and society\"" ,



        'text_content_resolution' => "\"สถาบันเทคโนโลยีไทย-ญี่ปุ่น มุ่งมั่นเป็นสถาบันอุดมศึกษาชั้นนำของประเทศที่เป็นศูนย์กลางทางวิชาการและ
        วิชาชีพเฉพาะทางชั้นสูง เพื่อเป็นแหล่งสร้างและพัฒนาบุคลากรในด้านเทคโนโลยีอุตสาหกรรมและเทคโนโลยี
        การบริหารจัดการที่ทันสมัย มีความเป็นเลิศทางวิชาการ การประยุกต์ และการเผยแพร่องค์ความรู้แก่สังคมโดยยึดมั่นในคุณธรรมและจิตสำนึกต่อสังคม\"" ,//_ปณิธาน

        'text_content_vision' => "Thai-Nichi wants to become one of the leading academic institutions in Thailand which provides high level of knowledge in specialized fields and produce highly qualified graduates in industrial and management technology who can adequately apply and disseminate those technologies to the society with integrity and accountability" ,

        'text_content_mission'                   => $arr_texts1 ,
        'text_content_graduate_identity'         => '"รอบรู้ในศาสตร์ เก่งการจัดการ มุ่งมั่นและสร้างสรรค์ด้วยวิถีไทย-ญี่ปุ่น พร้อมทักษะการสื่อสาร มีคุณธรรมและจิตสำนึกต่อสังคม"',
        'text_content_institute_identity'        => '"พัฒนาคนและวิทยาการ เพื่อเสริมสร้างธุรกิจอุตสาหกรรมด้วยวิถีไทย-ญี่ปุ่น"',
        'text_content_institute_highlight_focus' => $arr_texts2,
        'text_content_value'                     => $arr_texts3,


        // Board -----------------------------------------------------
        'text_board_and_executives' => 'Board and Executives of Thai-Nichi Institute of Technology',
        'text_board' => 'Board of Thai-Nichi Institute of Technology Council',
        'text_executives' => 'Executives of Thai-Nichi Institute of Technology',

        // list board ================================================
        'text_name1' =>'Dr. Supong Chayutsahakij',
        'text_position1' =>'Chairman' ,
        'text_graduate1' =>'Vice Chairman of The Executive Board Bangkok Expressway and Metro Public Co., Ltd. D.B.A. (Management) Phranakhon Rajabhat University, Thailand ', 
      
         'text_name2' => 'Assoc.Prof.Dr. Sucharit Koontanakulvong' ,
         'text_position2' => 'Vice Chairman of TNI Council' ,
         'text_graduate2' =>'D.Eng. (Agricultural Engineering) Kyoto University, Japan',

         'text_name3' => 'Assoc.Prof.Dr. Virach Aphimeteetamrong' ,
         'text_position3' => 'Board Member' ,
         'text_graduate3' =>'Chairman of Dr. Virach and Associates Public Accounting Firm Ph.D. (Finance), University of Illinois, USA',

         'text_name4' => 'Dr. Damri Sukhothanang' ,
         'text_position4' => 'Board Member' ,
         'text_graduate4' =>'Former Permanent Secretariat, Ministry of Industry Ph.D. (Ceramic Engineering) University of Missouri at Rolla, USA',

         'text_name5' => 'Mr. Thavorn Chalassathien ' ,
         'text_position5' => 'Board Member' ,
         'text_graduate5' =>'Director of Administration Denso International (THAILAND) Co., Ltd. Ind.Edu. (Mechanical Technology) Rajamangala University of Technology Krungthep',

         'text_name6' => 'Mr.Teetitorn Chullapram' ,
         'text_position6' => 'Board Member' ,
         'text_graduate6' =>'Managing Director of the I-Tech Systems Co., Ltd. B.Eng. (Electronics) Chulalongkorn University',

         'text_name7' => 'Assoc.Prof. Pranee Jongsutjarittam ' ,
         'text_position7' => 'Board Member' ,
         'text_graduate7' =>'Former Vice Dean of Research and Academic Services Department, Kasetsart University M.I.A. (Japanese Literature) Tsukuba University, Japan',

         'text_name8' => 'Assoc.Prof.Dr. Bandhit Rojarayanont' ,
         'text_position8' => 'Board Member and TNI President' ,
         'text_graduate8' =>'President of Thai-Nichi Institute of Technology (TNI) D.Eng. (Electrical and Electronics Engineering)Tokyo Institute of Technology, Japan',

         'text_name9' => 'Mr. Chirapan Oulapathorn ' ,
         'text_position9' => 'Board Member' ,
         'text_graduate9' =>'President of the Sumipol Corporation Ltd. Master of Management, Sasin Graduate Institute of Business Administration of Chulalongkorn University',

         'text_name10' => 'Dr. Surapan  Meknavin' ,
         'text_position10' => 'Board Member' ,
         'text_graduate10' =>'President  of Technology Promotion Association (Thailand-Japan) Managing Director of the Guru Square Co.,Ltd.',

         'text_name11' => 'Assoc.Prof.Dr. Mangkorn  Rodprapakorn' ,
         'text_position11' => 'Board Member' ,
         'text_graduate11' =>'Executive Director of Technology Promotion Association (Thailand-Japan) Ph.D. (Genetic Resources Technology) Kyushu University, Japan',

         'text_name12' => 'Dr.Sumate Yamnoon' ,
         'text_position12' => 'Board Member' ,
         'text_graduate12' =>'Ph.D. (Applied Statistics and Research Methods) University of Northern Colorado, USA',

         'text_name13' => 'Assoc.Prof. Pichit  Lumyong' ,
         'text_position13' => 'Board Member' ,
         'text_graduate13' =>'วศ.บ. ไฟฟ้า สถาบันเทคโนโลยีเจ้าคุณทหารลาดกระบัง วศ.ม. ไฟฟ้า สถาบันเทคโนโลยีเจ้าคุณทหารลาดกระบัง',

         'text_name14' => ' Dr. Krissanapong Kirtikara' ,
         'text_position14' => 'Board Member' ,
         'text_graduate14' =>'Adviser to the University, King Mongkut\'s University of Technology Thonburi (KMUTT) Ph.D. (Electrical Engineering) University of Glasgow, UK',

         'text_name15' => 'Assoc.Prof. Pornanong Budsaratragoon' ,
         'text_position15' => 'Board Member' ,
         'text_graduate15' =>'DBA. (Finance), จุฬาลงกรณ์มหาวิทยาลัย  MBA. (MIS), University of Dallas B.A. (การจัดการเชิงปริมาณ), จุฬาลงกรณ์มหาวิทยาลัย',

         'text_name16' => 'Asst.Prof. Rungsun Lertnaisat' ,
         'text_position16' => 'Board Member (ผู้แทนคณาจารย์)' ,
         'text_graduate16' =>'MBA (Marketing), Kyoto University, Japan',

         'text_name17' => 'Mr. Chartchan Phodhiphad ' ,
         'text_position17' => 'Secretary' ,
         'text_graduate17' =>'Director of Office of the President M.Sc. (Human Resource and Organization Development), NIDA',


         // executive =========================================================================
  
         'text_ename1' => 'Assoc.Prof.Dr.Bandhit Rojarayanont' ,
         'text_eposition1' => 'President Acting Vice President of Administration Affairs' ,
         'text_egraduate1' =>'Ph.D. (Electrical and Electronics Engineering), Tokyo Institute of Technology, Japan',

         'text_ename2' => 'Assoc.Prof.Dr.Pichit Sukcharoenpong' ,
         'text_eposition2' => 'Vice President of Research and Academic Services and Dean of Graduate School' ,
         'text_egraduate2' =>'Ph.D. (Industrial Engineering and Management), Asian Institute of Technology, Thailand',

         'text_ename3' => 'Mrs.Pornanong Niyomka Horikawa' ,
         'text_eposition3' => 'Vice President of International, Public Relations and Scholarships' ,
         'text_egraduate3' =>'M.Econ. (Economics), Hitotsubashi University, Japan',

         'text_ename4' => 'Assoc.Prof.Dr.Choompol Antarasena' ,
         'text_eposition4' => 'Acting Vice President of Student Affairs Dean of Engineering Faculty' ,
         'text_egraduate4' =>'Ph.D. (Electronics), INSA Touluse, France',

         'text_ename5' => 'Assoc.Prof.Dr.Ruttikorn Varakulsiripunth' ,
         'text_eposition5' => 'Dean of Information Technology Faculty' ,
         'text_egraduate5' =>'Ph.D. (Electrical and Communications Engineering), Tohoku University, Japan',

         'text_ename6' => 'Asst.Prof.Rungsun Lertnaisat' ,
         'text_eposition6' => 'Dean of Business Administration Faculty' ,
         'text_egraduate6' =>'MBA (Marketing), Kyoto University, Japan',

         'text_ename7' => 'Asst.Prof.Dr.Wanwimon Roungtheera' ,
         'text_eposition7' => 'Dean of College of General Education and Languages' ,
         'text_egraduate7' =>'Ph.D.(Japanese Language and Culture), Osaka University of Foreign Studies, Japan',

         'text_ename8' => 'Mr.Chartcharn Phodhiphad' ,
         'text_eposition8' => 'Director of the Office of the President' ,
         'text_egraduate8' =>'M.Sc.(Human Resource and Organization Development) , National Institute of  Development Administration',

         'text_ename9' => 'Ms.Suwannapa Ruengsilkonlakarn' ,
         'text_eposition9' => 'Director of the Administration Department' ,
         'text_egraduate9' =>'B.A.(Political Science), Chulalongkorn University',

         'text_ename10' => 'Mr.Narong Anankavanich' ,
         'text_eposition10' => 'Director of Academic Affairs Department' ,
         'text_egraduate10' =>'MS. (Mechanical Engineering), University of Florida, U.S.A.',

         'text_ename11' => 'Mr.Surawat Yingsawad' ,
         'text_eposition11' => 'Director of Student Affairs Department' ,
         'text_egraduate11' =>'M.A.(Physical Education), Kasetsart University ',

         'text_ename12' => 'Ms.Supaporn Hempongsopa' ,
         'text_eposition12' => 'Director of International, Public Relations and Scholarships' ,
         'text_egraduate12' =>'M.Econ.(Economics), Ramkhamhaeng University',
    ];
?>