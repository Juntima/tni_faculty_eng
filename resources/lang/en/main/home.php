<?php

    return [
        'text_htmltitle' => 'Thai-Nichi Instutute of Technology',
        'text_course_heading' => 'THAI-NICHI INSTITUTE OF TECHNOLOGY',
        'text_bechelor' => 'Bechelor\'s Degree',
        'text_master' => 'Master\'s Degree',
        'text_international_program' => 'International Program',
        'text_scholarship' => 'Scholarship',
        'text_exchange_program' => 'Exchange Program',
        'text_highlight_activities' => 'HIGHLIGHT & ACTIVITIES',
        'text_news_more' => 'More',
        'text_event_update' => 'EVENT UPDATE',
        'text_event_more' => 'More',
        'text_student_portfolio' => 'Student Portfolio',
        'text_co_operative' => 'Co-Operative',
        'text_click_here' => 'Click Here',
        'text_learn2know_heading' => 'Learn2know',
        'text_learn2know_subheading' => 'Learn from listening. Know the practice.',
        'text_learn2know_p1' => 'Learning from listening Through Situations',
        'text_learn2know_p2' => 'Variety of situations to practice in English & Japanese',
        'text_tni_channel' => 'TNI CHANNEL',
        'text_e_bochure' => 'E-BOCHURE',
        'text_more' => 'More',
        'text_home' => 'Home',

    ];