<?php

    return [
        
        'text_more' => '+  More...',
        'text_major' => 'Major',

        'text_alumni' => 'ALUMNI',

        'text_major1' => 'Information Technology Program',
        'text_job1' => 'นักวิชาการคอมพิวเตอร์',
        'text_comment1' => ' " การศึกษาดีมีอยู่ที่นี่ " ',
        'text_name1' => 'Ms. Name Lastname',

        'text_major2' => 'สาขาวิชาวิศวกรรมอุตสาหการ',
        'text_job2' => 'นักวิชาการอุตสากรรม',
        'text_comment2' => ' " การศึกษาดีมีอยู่ที่นี่ " ',
        'text_name2' => 'Mr. Name Lastname ',

        'text_major3' => 'สาขาบริหารธุรกิจ ภาษาญี่ปุ่น',
        'text_job3' => 'นักธุรกิจ',
        'text_comment3' => ' " การศึกษาดีมีอยู่ที่นี่ " ',
        'text_name3' => 'Ms. Name Lastname',
        
      
    ];