<?php

    return [
        'text_news_activity' => 'news_activity',
        
        'text_allnews' => 'ข่าวทั้งหมด',
        'text_admission' => 'รับสมัครนักศึกษา',
        'text_student' => 'นักศึกษา',
        'text_co_operative' => 'สหกิจศึกษา',
        'text_exchange_scholarship' => 'ทุน/โครงการแลกเปลี่ยน',
        'text_research' => 'งานวิจัย',
       
    ];