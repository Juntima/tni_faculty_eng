<?php

return [

    // Text
    'text_language' => '言語',
    'text_menu' => 'メニュー',
    'text_online' => 'オンライン',
    'text_search' => '検索',
    'text_clear_search' => '検索のクリア',
    'text_dashboard' => 'ダッシュボード',
    'text_news' => 'ニュース',
    'text_create_news' => 'ニュースを作成',
    'text_news_categories' => 'ニュースカテゴリ',
    'text_create_news_category' => 'ニュースカテゴリを作成する',
    'text_banner' => 'バナー',
    'text_create_banner' => 'バナーの作成',
    'text_users' => 'ユーザー',
    'text_create_user' => 'ユーザーを作成する',
    'text_logout' => 'ログアウト',
    'text_profile' => '私のプロフィール',
    'text_all_results' => 'すべての結果',
    'text_item(s)' => 'item(s)',
    'text_page' => 'ページ',
    'text_from' => 'から',
    'text_page(s)' => 'page(s)',
    'text_confirm_delete' => '削除を確認する。',
    'text_confirm_delete_message' => 'Once deleted, you will not be able to recover this data.',
    'text_save' => 'リュウ',
    'text_saving' => '保存する',
    'text_cancel' => 'キャンセル',
    'text_status' => 'ステータス',
    'text_enable' => '有効にする',
    'text_disable' => '無効にする',
    'text_save_successful' => '成功を保存します。',
    'text_delete_successful' => '削除が成功しました。',
    'text_edit' => '編集',
    'text_delete' => '削除',
    'text_create' => 'データの作成',
    'text_created_at' => '作成者',
    'text_updated_at' => '更新日',
    'text_id' => 'ID',
    'text_touch' => '最初に更新',
    'text_email' => 'メール',
    'text_password' => 'パスワード',
    'text_password_confirmation' => 'パスワードの確認',
    
    'text_admission' => '入場',
    'text_create_admission'=>"入場許可を作成する",


    // Tabs
    'text_general_tab' => 'General',
    'text_data_tab' => 'Data',
    'text_images_tab' => 'Images',

    // Form
    'text_name' => 'Name',
    'text_description' => 'Description',
    'text_meta_title' => 'Meta Title',
    'text_meta_description' => 'Meta Description',
    'text_meta_keyword' => 'Meta Keyword',
    'text_serperate_comma' => 'Separate words with a comma.',
    'text_image' => 'Image',
    'text_sort_order' => 'Sort Order',
    'text_tags' => 'Tags',
    'text_link_to_category' => 'Link to Category',
    'text_publish_start' => 'Publish Start',
    'text_publish_stop' => 'Publish Stop',
    'text_cover_image' => 'Cover Image',
    'text_additional_image' => 'Additional Image',
    'text_add_image' => 'Add Image',


    //error
    'error_unauthorized' => 'You do not have access to this page. Please contact the administrator.',
];
