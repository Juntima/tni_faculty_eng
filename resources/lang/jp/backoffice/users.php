<?php

return [

    'text_user_lists' => 'User Lists',
    'text_user' => 'User',
    'text_search' => 'Search',
    'text_clear_search' => 'Clear Search',
    'text_create_user' => 'Create User',
    'text_edit_user' => 'Edit User',
    'text_delete' => 'Delete',
    'text_id' => 'ID',
    'text_name' => 'Name',
    'text_email' => 'E-Mail',
    'text_latest_access' => 'Latest Access',
    'text_password' => 'Password',
    'text_password_confirmation' => 'Password Confirmation',
    'text_image_profile' => 'Image Profile',
    'text_user_privileges' => 'User Privileges',
    'text_all_privileges' => 'All Privileges',

];
