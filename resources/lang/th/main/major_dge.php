<?php
//โอกาสในการประกอบอาชีพ
 $arr_texts[] = "วิศวกรออกแบบระบบอัตโนมัติในอุตสาหกรรม (SI Engineer)";
 $arr_texts[] = "วิศวกรออกแบบและพัฒนาผลิตภัณฑ์ (R&D Engineer)";
 $arr_texts[] = "เจ้าของกิจการด้านนวัตกรรมการผลิตสมัยใหม่ (New Enterprise)";


    return [
        'text_more' => '+  ดูรายละเอียดเพิ่มเติม',
        'text_download_doc' => 'ดาวโหลดเอกสารหลักสูตร(PDF)',

        'text_name' => 'วิศวกรรมดิจิทัล',
        'text_detail' => '<b>หลักสูตรวิศวกรรมศาสตรบัณฑิต สาขาวิชาวิศวกรรมลีนออโตเมชั่นและบูรณาการระบบ </b> เป็นหลักสูตรที่หลักสูตรมุ่งสร้างองค์ความรู้ภายใต้ 3M ได้แก่ Manufacturing / Management / Mechatronics ซึ่งนักศึกษาจะได้รับการเรียนรู้พร้อมทักษะวิชาชีพที่สำคัญ ได้แก่
        1. มีความเข้าใจศาสตร์พื้นฐานด้านวิศวกรรมการผลิต ซึ่งเป็นหัวใจสำคัญของการเป็นวิศวกร เริ่มต้นตั้งแต่ การออกแบบ การผลิต จนถึงการควบคุมคุณภาพให้ได้ตามข้อกำหนด
        2. มีความรอบรู้และสามารถบูรณาระบบการผลิตที่มีความเชื่อมโยงด้วยเทคนิคการลดความสูญเสียแบบลีน การประยุกต์ใช้ระบบอัตโนมัตและหุ่นยนต์เพื่อให้เกิดสายการผลิตแบบอัตโนมัติในอุตสาหกรรม', //ปรัชญาหลักสูตร
        
        'text_program_name' => 'ชื่อปริญญาและสาขาวิชา',

        'text_topic1' => 'ชื่อเต็ม (ภาษาไทย)',
        'text_name1' => 'วิศวกรรมศาสตรบัณฑิต (วิศวกรรมลีนออโตเมชั่นและบูรณาการระบบ)',

        'text_topic2' => 'ชื่อย่อ (ภาษาไทย)',
        'text_name2' => 'วศ.บ. (วิศวกรรมลีนออโตเมชั่นและบูรณาการระบบ)',

        'text_topic3' => 'ชื่อเต็ม (ภาษาอังกฤษ)',
        'text_name3' => 'Bachelor of Engineering (Lean Automation and System Integration Engineering)',

        'text_topic4' => 'ชื่อย่อ (ภาษาอังกฤษ)',
        'text_name4' => 'B.Eng. (Lean Automation and System Integration Engineering)',
        
        //โอกาสในการประกอบอาชีพ
        'text_fjob' => 'โอกาสในการประกอบอาชีพ',
        'text_job_detail' => $arr_texts,

        //โครงสร้างหลักสูตร
        'text_struct' => 'โครงสร้างหลักสูตร',
        'text_main_unit' =>' หน่วยกิต' ,  

             //-- เปลี่ยนตรงนี้ --
        'text_main_couse_name' =>'จำนวนหน่วยกิตรวมตลอดหลักสูตร (ไม่น้อยกว่า)' , 
        'text_main_couse_unit' =>'141',

        //รายวิชาย่อย 
        'text_unit' =>' หน่วยกิต' ,

        //-- เปลี่ยนตรงนี้ --
            // หมวดวิชาศึกษาทั่วไป
        'text_couse_name1' =>'<b>(1) หมวดวิชาศึกษาทั่วไป  </b>' , 
        'text_couse_unit1' =>'34',

        'text_couse_name2' =>'1.1 กลุ่มวิชามนุษยศาสตร์' , 
        'text_couse_unit2' =>'3',

        'text_couse_name3' =>'1.2 กลุ่มวิชาสังคมศาสตร์ ' , 
        'text_couse_unit3' =>'3',

        'text_couse_name4' =>'1.3 กลุ่มวิชาวิทยาศาสตร์และคณิตศาสตร์ ' , 
        'text_couse_unit4' =>'4',

        'text_couse_name5' =>'1.4 กลุ่มวิชาภาษา' , 
        'text_couse_unit5' =>'24',

            // วิชาเฉพาะ
        'text_couse_name6' =>'<b> (2) หมวดวิชาเฉพาะ </b>' , 
        'text_couse_unit6' =>'101',

        'text_couse_name7' =>'2.1 กลุ่มวิชาพื้นฐาน' , 
        'text_couse_unit7' =>'56',

        'text_couse_name8' =>'2.2 กลุ่มวิชาบังคับสาขา ' , 
        'text_couse_unit8' =>'30',

        'text_couse_name9' =>'2.3 กลุ่มวิชาฝึกปฏิบัติทางวิศวกรรมการผลิต  ' , 
        'text_couse_unit9' =>'7',

        'text_couse_name10' =>'2.4 กลุ่มวิชาเลือกสาขา' , 
        'text_couse_unit10' =>'12',
       
            // วิชาเสรี
        'text_couse_name11' =>'<b> (3) หมวดวิชาเลือกเสรี  </b>' , 
        'text_couse_unit11' =>'6',


        //แผนการศึกษา
        'text_program' => 'แผนการศึกษา',

        'text_year1' => 'ปีที่ 1',
        'text_year2' => 'ปีที่ 2',
        'text_year3' => 'ปีที่ 3',
        'text_year4' => 'ปีที่ 4',
        
        'text_year1_semester1' => 'ปีที่ 1 ภาคการศึกษาที่ 1',
        'text_year1_semester2' => 'ปีที่ 1 ภาคการศึกษาที่ 2',

        'text_year2_semester1' => 'ปีที่ 2 ภาคการศึกษาที่ 1',
        'text_year2_semester2' => 'ปีที่ 2 ภาคการศึกษาที่ 2',

        'text_year3_semester1' => 'ปีที่ 3 ภาคการศึกษาที่ 1',
        'text_year3_semester2' => 'ปีที่ 3 ภาคการศึกษาที่ 2',

        'text_year4_semester1' => 'ปีที่ 4 ภาคการศึกษาที่ 1',
        'text_year4_semester2' => 'ปีที่ 4 ภาคการศึกษาที่ 2',

        'text_sub_id' => 'รหัสวิชา',
        'text_sub_name' => 'ชื่อวิชา',

        // --- เพิ่มรายวิชาตรงนี้ ---
        
        // ปี1 เทอม1
        'sub_y1s1_id1' => 'ENL-111',
        'sub_y1s1_name1' => 'ภาษาอังกฤษเพื่อการสื่อสาร 1' ,
        'sub_y1s1_unit1' => '2' ,

        'sub_y1s1_id2' => 'JPN-101',
        'sub_y1s1_name2' => 'ภาษาญี่ปุ่นธุรกิจ1' ,
        'sub_y1s1_unit2' => '3' ,

        'sub_y1s1_id3' => 'ENG-101',
        'sub_y1s1_name3' => 'เขียนแบบวิศวกรรม' ,
        'sub_y1s1_unit3' => '3' ,

        'sub_y1s1_id4' => 'ENG-111',
        'sub_y1s1_name4' => 'ฟิสิกส์ทั่วไป' ,
        'sub_y1s1_unit4' => '3' ,

        'sub_y1s1_id5' => 'ENG-112',
        'sub_y1s1_name5' => 'ปฏิบัติการฟิสิกส์ทั่วไป' ,
        'sub_y1s1_unit5' => '1' ,

        'sub_y1s1_id6' => 'ENG-131',
        'sub_y1s1_name6' => 'แคลคูลัส 1' ,
        'sub_y1s1_unit6' => '3' ,

        'sub_y1s1_id7' => 'ENG-202',
        'sub_y1s1_name7' => 'การปฏิบัติพื้นฐานวิศวกรรม' ,
        'sub_y1s1_unit7' => '1' ,

        'sub_y1s1_id8' => 'ENG-102',
        'sub_y1s1_name8' => 'การโปรแกรมคอมพิวเตอร์สำหรับวิศวกร' ,
        'sub_y1s1_unit8' => '3' ,

        'text_sum' =>'รวม',
        
        'text_sub_y1s1_unit_sum' =>'19',  //หน่วยกิตรวม

        // ปี1 เทอม 2 
        'sub_y1s2_id1' => 'ENL-112',
        'sub_y1s2_name1' => 'พัฒนาทักษะทางภาษาอังกฤษ' ,
        'sub_y1s2_unit1' => '2' ,

        'sub_y1s2_id2' => 'JPN-102',
        'sub_y1s2_name2' => 'ภาษาญี่ปุ่นธุรกิจ 2' ,
        'sub_y1s2_unit2' => '3' ,

        'sub_y1s2_id3' => 'ENG-133',
        'sub_y1s2_name3' => 'กลศาสตร์วิศวกรรม' ,
        'sub_y1s2_unit3' => '3' ,

        'sub_y1s2_id4' => 'ENG-121',
        'sub_y1s2_name4' => 'เคมีทั่วไป' ,
        'sub_y1s2_unit4' => '3' ,

        'sub_y1s2_id5' => 'ENG-122',
        'sub_y1s2_name5' => 'ปฏิบัติการเคมีทั่วไป' ,
        'sub_y1s2_unit5' => '1' ,

        'sub_y1s2_id6' => 'ENG-132',
        'sub_y1s2_name6' => 'แคลคูลัส 2 ' ,
        'sub_y1s2_unit6' => '3' ,

        'sub_y1s2_id7' => 'ENG-103',
        'sub_y1s2_name7' => 'ฟิสิกส์วิศวกรรม' ,
        'sub_y1s2_unit7' => '3' ,

        'sub_y1s2_id8' => 'ENG-104',
        'sub_y1s2_name8' => 'ปฏิบัติการฟิสิกส์วิศวกรรม' ,
        'sub_y1s2_unit8' => '1' ,

        'text_sub_y1s2_unit_sum' =>'19', //หน่วยกิตรวม
     

        // ปี2 เทอม 1 
        'sub_y2s1_id1' => 'ENL-211',
        'sub_y2s1_name1' => 'ภาษาอังกฤษเพื่อการทำงาน' ,
        'sub_y2s1_unit1' => '2' ,

        'sub_y2s1_id2' => 'JPN-201',
        'sub_y2s1_name2' => 'ภาษาญี่ปุ่นธุรกิจ 3' ,
        'sub_y2s1_unit2' => '3' ,

        'sub_y2s1_id3' => 'ENG-203',
        'sub_y2s1_name3' => 'วัสดุวิศวกรรม' ,
        'sub_y2s1_unit3' => '3' ,

        'sub_y2s1_id4' => 'ENG-211',
        'sub_y2s1_name4' => 'สถิติวิศวกรรม' ,
        'sub_y2s1_unit4' => '3' ,

        'sub_y2s1_id5' => 'ENG-218',
        'sub_y2s1_name5' => 'วิศวกรรมไฟฟ้า' ,
        'sub_y2s1_unit5' => '3' ,

        'sub_y2s1_id6' => 'ENG-219',
        'sub_y2s1_name6' => 'ปฏิบัติการวิศวกรรมไฟฟ้า' ,
        'sub_y2s1_unit6' => '1' ,

        'sub_y2s1_id7' => 'ENG-216',
        'sub_y2s1_name7' => 'วิศวกรรมร้อนของไหล' ,
        'sub_y2s1_unit7' => '3' ,

        'sub_y2s1_id8' => 'XXX-xxx',
        'sub_y2s1_name8' => 'กลุ่มวิชาสังคมศาสตร์ และมนุษยศาสตร์' ,
        'sub_y2s1_unit8' => '3' ,

        'text_sub_y2s1_unit_sum' =>'21', //หน่วยกิตรวม

        // ปี2 เทอม 2 
        'sub_y2s2_id1' => 'JPN-202',
        'sub_y2s2_name1' => 'ภาษาญี่ปุ่นธุรกิจ 4' ,
        'sub_y2s2_unit1' => '3' ,

        'sub_y2s2_id2' => 'ENG-209',
        'sub_y2s2_name2' => 'ปฏิบัติการวิศวกรรมเครื่องกล' ,
        'sub_y2s2_unit2' => '1' ,

        'sub_y2s2_id3' => 'ENG-210',
        'sub_y2s2_name3' => 'กลศาสตร์วัสดุ' ,
        'sub_y2s2_unit3' => '3' ,

        'sub_y2s2_id4' => 'ENG-212',
        'sub_y2s2_name4' => 'กรรมวิธีการผลิต' ,
        'sub_y2s2_unit4' => '3' ,

        'sub_y2s2_id5' => 'ENG-213',
        'sub_y2s2_name5' => 'คณิตศาสตร์สำหรับวิศวกรรม' ,
        'sub_y2s2_unit5' => '3' ,

        'sub_y2s2_id6' => 'ENL-212',
        'sub_y2s2_name6' => 'เตรียมพร้อมสอบ TOIEC' ,
        'sub_y2s2_unit6' => '3' ,

        'sub_y2s2_id7' => 'ENG-217',
        'sub_y2s2_name7' => 'ปัญญาประดิษฐ์ในงานอุตสาหกรรมการผลิต' ,
        'sub_y2s2_unit7' => '3' ,

        'sub_y2s2_id8' => 'XXX-xxx',
        'sub_y2s2_name8' => 'กลุ่มวิชาสังคมศาสตร์ และมนุษยศาสตร์' ,
        'sub_y2s2_unit8' => '3' ,


        'text_sub_y2s2_unit_sum' =>'22', //หน่วยกิตรวม


         // ปี3 เทอม 1 
         'sub_y3s1_id1' => 'JPN-301',
         'sub_y3s1_name1' => 'ภาษาญี่ปุ่นธุรกิจ 5' ,
         'sub_y3s1_unit1' => '3' ,
 
         'sub_y3s1_id2' => 'LEN-303',
         'sub_y3s1_name2' => 'เครื่องมือกลอัตโนมัติ' ,
         'sub_y3s1_unit2' => '3' ,

         'sub_y3s1_id3' => 'LEN-301',
         'sub_y3s1_name3' => 'ปฏิบัติการวิศวกรรมการผลิตอัตโนมัติแบบลีน 1' ,
         'sub_y3s1_unit3' => '1' ,

         'sub_y3s1_id4' => 'LEN-304',
         'sub_y3s1_name4' => 'การจำลองและระบบควบคุม ' ,
         'sub_y3s1_unit4' => '3' ,

         'sub_y3s1_id5' => 'LEN-305',
         'sub_y3s1_name5' => 'หุ่นยนต์อุตสาหกรรมและการมองเห็นของเครื่องจักร' ,
         'sub_y3s1_unit5' => '3' ,

         'sub_y3s1_id6' => 'LEN-306',
         'sub_y3s1_name6' => 'การวางแผนและการควบคุมการผลิต' ,
         'sub_y3s1_unit6' => '3' ,

         'sub_y3s1_id7' => 'LEN-307',
         'sub_y3s1_name7' => 'การศึกษางาน' ,
         'sub_y3s1_unit7' => '3' ,
 
         'text_sub_y3s1_unit_sum' =>'19', //หน่วยกิตรวม

         // ปี3 เทอม 2 
         'sub_y3s2_id1' => 'LEN-302',
         'sub_y3s2_name1' => 'ปฏิบัติการวิศวกรรมการผลิตอัตโนมัติแบบลีน 2' ,
         'sub_y3s2_unit1' => '1' ,
 
         'sub_y3s2_id2' => 'LEN-308',
         'sub_y3s2_name2' => 'คอมพิวเตอร์ช่วยในอุตสาหกรรมการผลิต' ,
         'sub_y3s2_unit2' => '3' ,

         'sub_y3s2_id3' => 'LEN-309',
         'sub_y3s2_name3' => 'การวัดและเครื่องมือวัดอัจฉริยะ ' ,
         'sub_y3s2_unit3' => '3' ,

         'sub_y3s2_id4' => 'LEN-310',
         'sub_y3s2_name4' => 'ระบบการผลิตอัตโนมัติและการควบคุม' ,
         'sub_y3s2_unit4' => '3' ,

         'sub_y3s2_id5' => 'LEN-311',
         'sub_y3s2_name5' => 'วิศวกรรมความปลอดภัยและมาตรฐานสากล' ,
         'sub_y3s2_unit5' => '3' ,

         'sub_y3s2_id6' => 'LEN-312',
         'sub_y3s2_name6' => 'วิศวกรรมความปลอดภัยและมาตรฐานสากล' ,
         'sub_y3s2_unit6' => '3' ,

         'sub_y3s2_id7' => 'LEN-491',
         'sub_y3s2_name7' => 'เตรียมสหกิจศึกษา' ,
         'sub_y3s2_unit7' => '1' ,

         'sub_y3s2_id8' => 'XXX-xxx',
         'sub_y3s2_name8' => 'วิชาเลือกสาขา' ,
         'sub_y3s2_unit8' => '3' ,

         'text_sub_y3s2_unit_sum' =>'20', //หน่วยกิตรวม

          // ปี4 เทอม 1 
          'sub_y4s1_id1' => 'LEN-492',
          'sub_y4s1_name1' => 'สหกิจศึกษา' ,
          'sub_y4s1_unit1' => '6',
          'text_sub_y4s1_unit_sum' =>'6', //หน่วยกิตรวม
 
          // ปี4 เทอม 2 
          'sub_y4s2_id1' => 'LEN-401',
          'sub_y4s2_name1' => 'การประยุกต์ใช้ AI ในระบบการผลิต' ,
          'sub_y4s2_unit1' => '3' ,

          'sub_y4s2_id2' => 'LEN-402',
          'sub_y4s2_name2' => 'การบูรณาการระบบการผลิตแบบลีน' ,
          'sub_y4s2_unit2' => '3' ,

          'sub_y4s2_id3' => 'XXX-xxx',
          'sub_y4s2_name3' => 'วิชาเลือกสาขา' ,
          'sub_y4s2_unit3' => '3' ,

          'sub_y4s2_id4' => 'XXX-xxx',
          'sub_y4s2_name4' => 'วิชาเลือกเสรี' ,
          'sub_y4s2_unit4' => '3' ,

          'sub_y4s2_id5' => 'XXX-xxx',
          'sub_y4s2_name5' => 'วิชาเลือกเสรี' ,
          'sub_y4s2_unit5' => '3' ,

          'text_sub_y4s2_unit_sum' =>'15', //หน่วยกิตรวม

          
          'text_link_pdf_file' => 'file/curriculum_ae.pdf'  //path file document หลักสูตร


        //--- note ---

            // หลังจากใส่รายละเอียดรายวิชาแล้ว ให้นับจำนวนรายวิชาในแต่ละภาคเรียน แล้วนำจำนวนไปใส่ใน แต่ละ function ใน controller  Ex. MajorAeController

        //--- end note ---
       

   ];



 