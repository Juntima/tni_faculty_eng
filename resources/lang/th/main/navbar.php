<?php 

    return [
        // 'text_about' => 'แนะนำสถาบัน',
        // 'text_faculty' => 'คณะและหลักสูตร',
        // 'text_teacher' => 'ข้อมูลอาจารย์',
        // 'text_department' => 'หน่วยงานและบริการทางวิชาการ',
        // 'text_contact' => 'ติดต่อสถาบัน',

        // //about
        // 'text_history' => 'ประวัติความเป็นมา',
        // 'text_personnel' => 'ผู้บริหารและบุคลากร',
        // 'text_learngin_support' => 'สิ่งสนับสนุนการเรียน',
        // 'text_popularity' => 'ค่านิยมหลัก',
        'text_home' => 'หน้าหลัก',
        'text_about' => 'เกี่ยวกับคณะ',
            'text_dean_info' => 'สาส์นจากคณบดี',
            'text_mission' => 'วิสัยทัศน์ และ พันธกิจ',
            'text_manage_struct' => 'โครงสร้างการบริหารงาน',
        'text_faculty' => 'หลักสูตร',
        'text_member' => 'บุคลากร',
            'text_teacher' => 'คณาจารย์',
            'text_support' => 'ฝ่ายสนับสนุน',

        'text_lab_research_room' => 'ห้องปฏิบัติการและห้องวิจัย',
        // 'text_research_room' => 'ห้องวิจัย',
        'text_service' => 'บริการของเรา',
        'text_contact' => 'ติดต่อคณะ',
        // 'text_other' =>'อื่นๆ',
        'text_academic_service' => 'อบรมและบริการวิชาการ',
        
        // 'text_it' => 'Information Technology (IT)',
        // 'text_bi' => 'Business Information Technology (BI)',
        // 'text_mt' => 'Multimedia Technology (MT)',
        // 'text_dc' => 'Digital Technology in Mass Communication (DC)',
        // 'text_dsa' => 'Data Science and Analytics (DSA)',
        // 'text_mit' => 'Master of Scince Program Inforamtion Technology (MIT)',
        
        'text_it' => 'เทคโนโลยีสารสนเทศ', //Information Technology
        'text_bi' => 'เทคโนโลยีสารสนเทศทางธุรกิจ', //Business Information Technology
        'text_mt' => 'เทคโนโลยีมัลติมิเดีย ',//Multimedia Technology
        'text_dc' => 'เทคโนโลยีดิจิทัลทางสื่อสารมวลชน', //Digital Technology in Mass Communication
        'text_dsa' => 'วิทยาการข้อมูลและการวิเคราะห์เชิงลึก (INTER) ',//Data Science and Analytics
        'text_mit' => 'เทคโนโลยีสารสนเทศ (ปริญญาโท) ',//Master of Scince Program Inforamtion Technology

        //about
        'text_history' => 'ประวัติความเป็นมา',
        'text_personnel' => 'ผู้บริหารและบุคลากร',
        'text_learngin_support' => 'สิ่งสนับสนุนการเรียน',
        'text_popularity' => 'ค่านิยมหลัก',
        
       //อาจารย์และบุคลากร',

      
       'text_research' => 'วิจัย',
       'text_calendar' => 'ปฏิทิน'
    ];