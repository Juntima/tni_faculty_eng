<?php
    
    return [

        'text_intro' => 'ฝ่ายสนับสนุน',

        'text_name_th1' => 'คุณฐิติมา สมทอง',
        // 'text_name_en1' => 'Ms. Patchon Sangarun',
        'text_name_position1' => 'เลขานุการหลักสูตร MET',
        'text_contact1' => 'โทร. 02-763-2740 <br> สถานที่ติดต่อ: ห้อง A605 อาคาร A ชั้น 6 <br> สถาบันเทคโนโลยีไทย – ญี่ปุ่น' ,
        'text_img1' => 'img/member/thitima1.jpg',

        'text_name_th2' => 'คุณอาทิตยา ไทยยงค์',
        'text_name_en2' => 'Ms. Athitaya Thaiyong',
        'text_name_position2' => 'เลขานุการคณะเทคโนโลยีสารสนเทศ',
        'text_contact2' => 'โทร. 02-763-2740 <br> สถานที่ติดต่อ: ห้อง A605 อาคาร A ชั้น 6 <br> สถาบันเทคโนโลยีไทย – ญี่ปุ่น' ,
        'text_img2' => 'img/member/athitaya.jpg',
     
    ];

  
?>