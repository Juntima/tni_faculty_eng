<?php
return [
    'text_htmltitle' => 'คณะวิศวกรรมศาสตร์',
    'text_course_heading' => 'FACULTY OF ENGINEERING',
    'text_fac_more' => '+ ดูรายละเอียดเพิ่มเติม ',
    'text_it' => 'เทคโนโลยีสารสนเทศ', //Information Technology
    'text_bi' => 'เทคโนโลยีสารสนเทศทางธุรกิจ', //Business Information Technology
    'text_mt' => 'เทคโนโลยีมัลติมิเดีย ',//Multimedia Technology
    'text_dc' => 'เทคโนโลยีดิจิทัลทางสื่อสารมวลชน', //Digital Technology in Mass Communication
    'text_dsa' => 'วิทยาการข้อมูลและการวิเคราะห์เชิงลึก (INTER) ',//Data Science and Analytics
    'text_mit' => 'เทคโนโลยีสารสนเทศ (ปริญญาโท) ',//Master of Scince Program Inforamtion Technology
    'text_detail' => 'ตลอดระยะเวลา 13 ปีที่คณะวิศวกรรมศาสตร์ สถาบันเทคโนโลยีไทย-ญี่ปุ่น ได้ผลิตบัณฑิตสู่ภาคอุตสาหกรรมอย่างต่อเนื่องด้วยพันธกิจ ',
    'text_detail_mission' => 'สร้างวิศวกรนักคิด บัณฑิตนักปฏิบัติ ซื่อสัตย์ในจรรยาบรรณ มุ่งมั่นเพื่อสังคม',
    
    //facebook//
    'text_fb_ce' => 'Facebook CE',
    'text_fb_ie' => 'Facebook IE',
    'text_fb_le' => 'Facebook LE',
    // 'text_fb_dc' => 'Facebook DC',
    // 'text_fdsa' => 'DSA',
    // 'text_fmit' => 'MIT (Master Degree)',
    'text_ceUrl' =>'https://www.facebook.com/comengTNI/?__tn__=%2Cd%2CP-R&eid=ARD6qiHTcbzSxNd6eG9zF7smiFy3a7og1ERfBUKKo1ysq_YoHdTV3vR7CB1dz7AtpHjchzergVlcq6lh', 
    'text_ieUrl' =>'https://www.facebook.com/IE.ThaiNichi/', 
    'text_leUrl' =>'https://www.facebook.com/proengtni/?__tn__=%2Cd%2CP-R&eid=ARA332qH-ZLFPIMOGC_RbPezioKXtoVDeNGWeGoopjsMce9jQG9k9EV0JjCyE9JZcX7ebXHJ7TPaIZDL', 
    // 'text_mtUrl' =>'https://www.facebook.com/MT.ThaiNichi/', 
    
    'text_eng_channel' => 'ENG CHANNEL',
    // 'text_highlight_activities' => 'HIGHLIGHT & ACTIVITIES',
    'text_highlight_activities' => 'NEWS',

    'text_aca_service' => 'บริการวิชาการ',
    'text_student_activity' =>'กิจกรรมนักศึกษา',
    // 'text_labroom' => 'บริการวิชาการ',
    'text_research' =>'วิจัย',
    // 'text_eng_lab_research_room' =>'ห้องปฏิบัติการและห้องวิจัย',
    








    
    'text_exchange_program' => 'ทุนและโครงการแลกเปลี่ยน',
    'text_exchange_program_news' => 'ประกาศรับสมัครโครงการแลกเปลี่ยน',
    'text_news_more' => 'ข่าวทั้งหมด',
    'text_event_update' => 'EVENT UPDATE',
    'text_event_more' => 'ดูทั้งหมด',
    'text_student_portfolio' => 'ผลงานนักศึกษา',
    'text_co_operative' => 'สหกิจศึกษาและฝึกงาน',
    'text_click_here' => 'Click Here',
    'text_more' => 'ดูทั้งหมด',
    'text_history' => 'แนะนำสถาบันเทคโนโลยีไทย-ญี่ปุ่น',
    'text_history_tni' => 'ความเป็นมาของสถาบัน',
    'text_philosophy' => 'ปรัชญา',
    'text_around_tni' => 'รอบรั้วสถาบัน',
    'text_resolution' => 'ปณิธาน',
    'text_vision' => 'วิสัยทัศน์',
    'text_mission' => 'พันธกิจ',
    'text_faculty' => 'คณะและสาขา',
    'text_home' => '<i class="fas fa-home fa"></i> หน้าหลัก',

    //ร่วมงานกับเรา
    'text_career_tni' => 'ร่วมงานกับเรา',
    'text_application_form' =>'ดาวน์โหลดใบสมัคร',
    'text_download' =>'Download',
    'text_send_application' =>' " เมื่อกรอกใบสมัครเรียบร้อยแล้ว สามารถส่งใบสมัครมาได้ที่ <b style="color:#276fc1">hr@tni.ac.th</b> "',
    'text_travel' =>'<i style="color:red;">หรือ</i> สามารถเดินทางมาสมัครด้วยตนเอง ดังนี้ ',

    'text_travel_detail' =>'- เดินทางด้วยรถไฟฟ้า (BTS) ลงที่สถานีเอกมัย ต่อรถประจำทางสาย 133 จากหน้าวัดธาตุทอง ลงรถที่ป้ายหน้าซอยพัฒนาการ 37 <br>
    - เดินทางด้วยรถไฟฟ้ามหานคร (รถไฟใต้ดิน) ลงที่สถานีเพชรบุรี ต่อรถประจำทางสาย 11 บนถนนเพชรบุรีตัดใหม่ ลงรถที่ป้ายหน้าซอยพัฒนาการ 37 <br>
    - รถประจำทางที่ผ่านสถาบันฯ ได้แก่ สาย 11, 133, 206, ปอ.92 และ ปอ.517',

    'text_jobtni' => 'ข้อมูลตำแหน่งงาน',
    'text_link_jobtni' => 'www.jobtni.com',

    // web link 
    'text_organization' =>'องค์กรผู้ให้การสนับสนุนสถาบันเทคโนโลยีไทย-ญี่ปุ่น' ,

    // IT-Presentation 
    'text_thai_version' => 'Thai Version',
    'text_english_version' => 'English Version',
    'text_japan_version' => 'Japanese Version',
    'text_eng_presentation' =>'ENG PRESENTATION',
];
    

// return [
    //     'text_htmltitle' => 'สถาบันเทคโนโลยีไทย-ญี่ปุ่น',
    //     'text_course_heading' => 'THAI-NICHI INSTITUTE OF TECHNOLOGY',
    //     'text_bechelor' => 'ปริญญาตรี',
    //     'text_master' => 'ปริญญาโท',
    //     'text_international_program' => 'International Program',
    //     'text_scholarship' => 'ทุนการศึกษา',
    //     'text_exchange_program' => 'ทุนและโครงการแลกเปลี่ยน',
    //     'text_exchange_program_news' => 'ประกาศรับสมัครโครงการแลกเปลี่ยน',
    //     'text_highlight_activities' => 'HIGHLIGHT & ACTIVITIES',
    //     'text_news_more' => 'ข่าวทั้งหมด',
    //     'text_event_update' => 'EVENT UPDATE',
    //     'text_event_more' => 'ดูทั้งหมด',
    //     'text_student_portfolio' => 'ผลงานนักศึกษา',
    //     'text_co_operative' => 'สหกิจศึกษาและฝึกงาน',
    //     'text_click_here' => 'Click Here',
    //     'text_learn2know_heading' => 'เลียนให้รู้ : Learn2know',
    //     'text_learn2know_subheading' => 'เรียนจากการฟัง รู้จากการฝึกฝน',
    //     'text_learn2know_p1' => 'Learning from listening Through Situations',
    //     'text_learn2know_p2' => 'Variety of situations to practice in English & Japanese',
    //     'text_tni_channel' => 'TNI CHANNEL',
    //     'text_e_bochure' => 'E-BROCHURE',
    //     'text_more' => 'ดูทั้งหมด',
    //     'text_history' => 'แนะนำสถาบันเทคโนโลยีไทย-ญี่ปุ่น',
    //     'text_history_tni' => 'ความเป็นมาของสถาบัน',
    //     'text_philosophy' => 'ปรัชญา',
    //     'text_around_tni' => 'รอบรั้วสถาบัน',
    //     'text_resolution' => 'ปณิธาน',
    //     'text_vision' => 'วิสัยทัศน์',
    //     'text_mission' => 'พันธกิจ',
    //     'text_faculty' => 'คณะและสาขา',
    //     'text_home' => '<i class="fas fa-home fa"></i> หน้าหลัก',

    //     //ร่วมงานกับเรา
    //     'text_career_tni' => 'ร่วมงานกับเรา',
    //     'text_application_form' =>'ดาวน์โหลดใบสมัคร',
    //     'text_download' =>'Download',
    //     'text_send_application' =>' " เมื่อกรอกใบสมัครเรียบร้อยแล้ว สามารถส่งใบสมัครมาได้ที่ <b style="color:#276fc1">hr@tni.ac.th</b> "',
    //     'text_travel' =>'<i style="color:red;">หรือ</i> สามารถเดินทางมาสมัครด้วยตนเอง ดังนี้ ',

    //     'text_travel_detail' =>'- เดินทางด้วยรถไฟฟ้า (BTS) ลงที่สถานีเอกมัย ต่อรถประจำทางสาย 133 จากหน้าวัดธาตุทอง ลงรถที่ป้ายหน้าซอยพัฒนาการ 37 <br>
    //     - เดินทางด้วยรถไฟฟ้ามหานคร (รถไฟใต้ดิน) ลงที่สถานีเพชรบุรี ต่อรถประจำทางสาย 11 บนถนนเพชรบุรีตัดใหม่ ลงรถที่ป้ายหน้าซอยพัฒนาการ 37 <br>
    //     - รถประจำทางที่ผ่านสถาบันฯ ได้แก่ สาย 11, 133, 206, ปอ.92 และ ปอ.517',

    //     'text_jobtni' => 'ข้อมูลตำแหน่งงาน',
    //     'text_link_jobtni' => 'www.jobtni.com',

    //     // web link 
    //     'text_organization' =>'องค์กรผู้ให้การสนับสนุนสถาบันเทคโนโลยีไทย-ญี่ปุ่น' ,

    //     // TNI-Presentation 
    //     'text_thai_version' => 'Thai Version',
    //     'text_english_version' => 'English Version',
    //     'text_japan_version' => 'Japanese Version',
    //     'text_tni_presentation' =>'TNI PRESENTATION',
    // ];