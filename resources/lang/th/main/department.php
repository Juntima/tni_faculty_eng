<?php

    return [
        'text_department' => 'หน่วยงานและบริการทางวิชาการ ',
        'text_regiter_center' => 'ศูนย์รับสมัครนักศึกษา <br> สถาบันเทคโนโลยีไทย-ญี่ปุ่น',
        'text_public_relations' => 'ฝ่ายวิเทศสัมพันธ์ ประชาสัมพันธ์และจัดหาทุน' ,
        'text_adminsitrator' =>'ฝ่ายบริหาร<br>งานบัญชีและการเงิน',
        'text_academic' =>'ฝ่ายวิชาการ',
        'text_studen_affairs' =>'ฝ่ายกิจการนักศึกษา',
        'text_cooperative_career' =>'ศูนย์สหกิจศึกษาและจัดหางาน',
        'text_icc' =>'ศูนย์สารสนเทศและการสื่อสาร',
        'text_research_and_academic_services' =>'ฝ่ายวิจัยและบริการวิชาการ',      
        'text_journal_en' => 'TNI Journal of Engineering and Technology ',
        'text_journal_ba' => 'TNI Journal of Business Administration and Languages ',
        'text_community_services' => 'บริการวิชาการเพื่อสังคม',
        'text_library' => 'ศูนย์วิทยบริการ',
        'text_coe' => 'ศูนย์ความเชี่ยวชาญการบูรณาการระบบอัจฉริยะ (COE-ISI)',




        //finance
        'text_finance' => 'ฝ่ายบริหาร งานบัญชีและการเงิน',
  
    ];