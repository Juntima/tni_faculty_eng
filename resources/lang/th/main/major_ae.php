<?php
//โอกาสในการประกอบอาชีพ
 $arr_texts[] = "หลักสูตรวิศวกรรมศาสตรบัณฑิต สาขาวิชาวิศวกรรมยานยนต์ เป็นหลักสูตรที่มุ่งผลิตบัณฑิตให้เป็นผู้มีความรู้ทางด้านวิศวกรรมยานยนต์ 
 ด้านการออกแบบและพัฒนาชิ้นส่วนยานยนต์ ด้านกระบวนการผลิตและเทคโนโลยีวัสดุ ด้านการบริหารจัดการสายการผลิต 
 ด้านระบบควบคุมและระบบอัตโนมัติ และด้านการซ่อมบำรุงในอุตสาหกรรมยานยนต์ เพื่อให้เป็นวิศวกรที่มีความสามารถในการประยุกต์ความรู้ทางด้านวิศวกรรมศาสตร์ 
 สามารถวิเคราะห์ สังเคราะห์ และแก้ปัญหาในอุตสาหกรรมยานยนต์ เป็นผู้มีทักษะภาษาอังกฤษ และภาษาญี่ปุ่นเพื่อการสื่อสารได้เป็นอย่างดี รวมทั้งเป็นผู้มีคุณธรรมและจริยธรรม 
 เป็นแบบอย่างที่ดีในสังคม";
//  $arr_texts[] = "ด้านการสอบบัญชี";
//  $arr_texts[] = "ด้านการบัญชีบริหาร";
//  $arr_texts[] = "ด้านการภาษีอากร"; 
//  $arr_texts[] = "ด้านการวางระบบบัญชี";

    return [
        'text_more' => '+  ดูรายละเอียดเพิ่มเติม',
        'text_download_doc' => 'ดาวโหลดเอกสารหลักสูตร(PDF)',

        'text_name' => 'วิศวกรรมยานยนต์',
        'text_detail' => '<b>หลักสูตรวิศวกรรมศาสตรบัณฑิต สาขาวิชาวิศวกรรมยานยนต์ </b> เป็นหลักสูตรที่มุ่งผลิตบัณฑิตให้เป็นผู้มีความรู้ทางด้านวิศวกรรมยานยนต์ 
            ด้านการออกแบบและพัฒนาชิ้นส่วนยานยนต์ ด้านกระบวนการผลิตและเทคโนโลยีวัสดุ ด้านการบริหารจัดการสายการผลิต ด้านระบบควบคุมและระบบอัตโนมัติ 
            และด้านการซ่อมบำรุงในอุตสาหกรรมยานยนต์ เพื่อให้เป็นวิศวกรที่มีความสามารถในการประยุกต์ความรู้ทางด้านวิศวกรรมศาสตร์ สามารถวิเคราะห์ สังเคราะห์ 
            และแก้ปัญหาในอุตสาหกรรมยานยนต์ เป็นผู้มีทักษะภาษาอังกฤษ และภาษาญี่ปุ่นเพื่อการสื่อสารได้เป็นอย่างดี รวมทั้งเป็นผู้มีคุณธรรมและจริยธรรม เป็นแบบอย่างที่ดีในสังคม', //ปรัชญาหลักสูตร
        
        'text_program_name' => 'ชื่อปริญญาและสาขาวิชา',

        'text_topic1' => 'ชื่อเต็ม (ภาษาไทย)',
        'text_name1' => 'วิศวกรรมศาสตรบัณฑิต (วิศวกรรมยานยนต์)',

        'text_topic2' => 'ชื่อย่อ (ภาษาไทย)',
        'text_name2' => 'วศ.บ. (วิศวกรรมยานยนต์)',

        'text_topic3' => 'ชื่อเต็ม (ภาษาอังกฤษ)',
        'text_name3' => ' Bachelor of Engineering (Automotive Engineering)',

        'text_topic4' => 'ชื่อย่อ (ภาษาอังกฤษ)',
        'text_name4' => 'B.Eng. (Automotive Engineering)',
        
        //โอกาสในการประกอบอาชีพ
        'text_fjob' => 'โอกาสในการประกอบอาชีพ',
        'text_job_detail' => $arr_texts,

        //โครงสร้างหลักสูตร
        'text_struct' => 'โครงสร้างหลักสูตร',
        'text_main_unit' =>' หน่วยกิต' ,  

             //-- เปลี่ยนตรงนี้ --
        'text_main_couse_name' =>'จำนวนหน่วยกิตรวมตลอดหลักสูตร (ไม่น้อยกว่า)' , 
        'text_main_couse_unit' =>'144',

        //รายวิชาย่อย 
        'text_unit' =>' หน่วยกิต' ,

        //-- เปลี่ยนตรงนี้ --
            // หมวดวิชาศึกษาทั่วไป
        'text_couse_name1' =>'<b>(1) หมวดวิชาศึกษาทั่วไป  </b>' , 
        'text_couse_unit1' =>'33',

        'text_couse_name2' =>'1.1 กลุ่มวิชามนุษยศาสตร์' , 
        'text_couse_unit2' =>'3',

        'text_couse_name3' =>'1.2 กลุ่มวิชาสังคมศาสตร์ ' , 
        'text_couse_unit3' =>'3',

        'text_couse_name4' =>'1.3 กลุ่มวิชาวิทยาศาสตร์และคณิตศาสตร์ ' , 
        'text_couse_unit4' =>'3',

        'text_couse_name5' =>'1.4 กลุ่มวิชาภาษา' , 
        'text_couse_unit5' =>'24',

            // วิชาเฉพาะ
        'text_couse_name6' =>'<b> (2) หมวดวิชาเฉพาะ </b>' , 
        'text_couse_unit6' =>'105',

        'text_couse_name7' =>'2.1 กลุ่มวิชาพื้นฐาน' , 
        'text_couse_unit7' =>'60',

        'text_couse_name8' =>'2.2 กลุ่มวิชาบังคับสาขา ' , 
        'text_couse_unit8' =>'26 ',

        'text_couse_name9' =>'2.3 กลุ่มวิชาฝึกปฏิบัติทางวิศวกรรมยานยนต์  ' , 
        'text_couse_unit9' =>'7',

        'text_couse_name10' =>'2.4 กลุ่มวิชาเลือกสาขา' , 
        'text_couse_unit10' =>'12',
       
            // วิชาเสรี
        'text_couse_name11' =>'<b> (3) หมวดวิชาเลือกเสรี  </b>' , 
        'text_couse_unit11' =>'6',


        //แผนการศึกษา
        'text_program' => 'แผนการศึกษา',

        'text_year1' => 'ปีที่ 1',
        'text_year2' => 'ปีที่ 2',
        'text_year3' => 'ปีที่ 3',
        'text_year4' => 'ปีที่ 4',
        
        'text_year1_semester1' => 'ปีที่ 1 ภาคการศึกษาที่ 1',
        'text_year1_semester2' => 'ปีที่ 1 ภาคการศึกษาที่ 2',

        'text_year2_semester1' => 'ปีที่ 2 ภาคการศึกษาที่ 1',
        'text_year2_semester2' => 'ปีที่ 2 ภาคการศึกษาที่ 2',

        'text_year3_semester1' => 'ปีที่ 3 ภาคการศึกษาที่ 1',
        'text_year3_semester2' => 'ปีที่ 3 ภาคการศึกษาที่ 2',

        'text_year4_semester1' => 'ปีที่ 4 ภาคการศึกษาที่ 1',
        'text_year4_semester2' => 'ปีที่ 4 ภาคการศึกษาที่ 2',

        'text_sub_id' => 'รหัสวิชา',
        'text_sub_name' => 'ชื่อวิชา',

        // --- เพิ่มรายวิชาตรงนี้ ---
        
        // ปี1 เทอม1
        'sub_y1s1_id1' => 'ENL-101',
        'sub_y1s1_name1' => 'ภาษาอังกฤษเพื่อการสื่อสาร 1' ,
        'sub_y1s1_unit1' => '3' ,

        'sub_y1s1_id2' => 'JPN-101',
        'sub_y1s1_name2' => 'ภาษาญี่ปุ่นธุรกิจ1' ,
        'sub_y1s1_unit2' => '3' ,

        'sub_y1s1_id3' => 'ENG-111',
        'sub_y1s1_name3' => 'ฟิสิกส์ทั่วไป' ,
        'sub_y1s1_unit3' => '3' ,

        'sub_y1s1_id4' => 'ENG-112',
        'sub_y1s1_name4' => 'ปฏิบัติการฟิสิกส์ทั่วไป' ,
        'sub_y1s1_unit4' => '1' ,

        'sub_y1s1_id5' => 'ENG-121',
        'sub_y1s1_name5' => 'เคมีทั่วไป' ,
        'sub_y1s1_unit5' => '3' ,

        'sub_y1s1_id6' => 'ENG-122',
        'sub_y1s1_name6' => 'ปฏิบัติการเคมีทั่วไป' ,
        'sub_y1s1_unit6' => '1' ,

        'sub_y1s1_id7' => 'ENG-131',
        'sub_y1s1_name7' => 'แคลคูลัส 1' ,
        'sub_y1s1_unit7' => '3' ,

        'text_sum' =>'รวม',
        'text_sum_summer' =>'รวม (ภาคฤดูร้อน)',
        'text_sub_y1s1_unit_sum' =>'17(18-0-36)',  //หน่วยกิตรวม

        // ปี1 เทอม 2 
        'sub_y1s2_id1' => 'ENL-102',
        'sub_y1s2_name1' => 'ภาษาอังกฤษเพื่อการสื่อสาร 2' ,
        'sub_y1s2_unit1' => '3' ,

        'sub_y1s2_id2' => 'JPN-102',
        'sub_y1s2_name2' => 'ภาษาญี่ปุ่นธุรกิจ 2' ,
        'sub_y1s2_unit2' => '3' ,

        'sub_y1s2_id3' => 'ENG-132',
        'sub_y1s2_name3' => 'แคลคูลัส 2 ' ,
        'sub_y1s2_unit3' => '3' ,

        'sub_y1s2_id4' => 'ENG-101',
        'sub_y1s2_name4' => 'เขียนแบบวิศวกรรม' ,
        'sub_y1s2_unit4' => '3' ,

        'sub_y1s2_id5' => 'ENG-103',
        'sub_y1s2_name5' => 'ฟิสิกส์วิศวกรรม' ,
        'sub_y1s2_unit5' => '3' ,

        'sub_y1s2_id6' => 'ENG-104',
        'sub_y1s2_name6' => 'ปฏิบัติการฟิสิกส์วิศวกรรม ' ,
        'sub_y1s2_unit6' => '1' ,

        'sub_y1s2_id7' => 'ENG-105',
        'sub_y1s2_name7' => 'กลศาสตร์วิศวกรรม 1 ' ,
        'sub_y1s2_unit7' => '3' ,

        'sub_y1s2_id8' => '-',
        'sub_y1s2_name8' => '<b> ภาคเรียนที่ 3 </b>' ,
        'sub_y1s2_unit8' => '-' ,

        'sub_y1s2_id9' => 'ENG-102',
        'sub_y1s2_name9' => 'การโปรแกรมคอมพิวเตอร์สำหรับวิศวกร' ,
        'sub_y1s2_unit9' => '3' ,

        'sub_y1s2_id10' => 'ENG-202',
        'sub_y1s2_name10' => 'การปฏิบัติพื้นฐานวิศวกรรม' ,
        'sub_y1s2_unit10' => '1' ,

        'text_sub_y1s2_unit_sum' =>'19', //หน่วยกิตรวม
        'text_sub_y1s3_unit_sum' => '4', //หน่วยกิตรวม เทอม3

        // ปี2 เทอม 1 
        'sub_y2s1_id1' => 'ENL-201',
        'sub_y2s1_name1' => 'ภาษาอังกฤษเพื่อการสื่อสาร 3' ,
        'sub_y2s1_unit1' => '3' ,

        'sub_y2s1_id2' => 'JPN-201',
        'sub_y2s1_name2' => 'ภาษาญี่ปุ่นธุรกิจ 3' ,
        'sub_y2s1_unit2' => '3' ,

        'sub_y2s1_id3' => 'ENG-213',
        'sub_y2s1_name3' => 'คณิตศาสตร์สำหรับวิศวกรรม' ,
        'sub_y2s1_unit3' => '3' ,

        'sub_y2s1_id4' => 'ENG-203',
        'sub_y2s1_name4' => 'วัสดุวิศวกรรม' ,
        'sub_y2s1_unit4' => '3' ,

        'sub_y2s1_id5' => 'ENG-204',
        'sub_y2s1_name5' => 'กลศาสตร์วิศวกรรม 2' ,
        'sub_y2s1_unit5' => '3' ,

        'sub_y2s1_id6' => 'ENG-214',
        'sub_y2s1_name6' => 'เทอร์โมไดนามิกส์' ,
        'sub_y2s1_unit6' => '3' ,

        'sub_y2s1_id7' => 'AEN-301',
        'sub_y2s1_name7' => 'วิศวกรรมยานยนต์ 1' ,
        'sub_y2s1_unit7' => '3' ,

        'sub_y2s1_id8' => 'AEN-302',
        'sub_y2s1_name8' => 'ปฏิบัติการวิศวกรรมยานยนต์ 1' ,
        'sub_y2s1_unit8' => '1' ,

        'text_sub_y2s1_unit_sum' =>'22', //หน่วยกิตรวม

        // ปี2 เทอม 2 
        'sub_y2s2_id1' => 'JPN-202',
        'sub_y2s2_name1' => 'ภาษาญี่ปุ่นธุรกิจ 4' ,
        'sub_y2s2_unit1' => '3' ,

        'sub_y2s2_id2' => 'MSC-202',
        'sub_y2s2_name2' => 'สถิติและความน่าจะเป็น' ,
        'sub_y2s2_unit2' => '3' ,

        'sub_y2s2_id3' => 'ENG-206',
        'sub_y2s2_name3' => 'กลศาสตร์ของไหล' ,
        'sub_y2s2_unit3' => '3' ,

        'sub_y2s2_id4' => 'ENG-207',
        'sub_y2s2_name4' => 'วิศวกรรมไฟฟ้า' ,
        'sub_y2s2_unit4' => '3' ,

        'sub_y2s2_id5' => 'ENG-208',
        'sub_y2s2_name5' => 'ปฏิบัติการวิศวกรรมไฟฟ้า' ,
        'sub_y2s2_unit5' => '3' ,

        'sub_y2s2_id6' => 'ENG-209',
        'sub_y2s2_name6' => 'ปฏิบัติการวิศวกรรมเครื่องกล' ,
        'sub_y2s2_unit6' => '1' ,

        'sub_y2s2_id7' => 'ENG-210',
        'sub_y2s2_name7' => 'กลศาสตร์วัสดุ' ,
        'sub_y2s2_unit7' => '3' ,

        'sub_y2s2_id8' => 'AEN-303',
        'sub_y2s2_name8' => 'วิศวกรรมยานยนต์ 2' ,
        'sub_y2s2_unit8' => '3' ,

        'sub_y2s2_id9' => 'AEN-304',
        'sub_y2s2_name9' => 'ปฏิบัติการวิศวกรรมยานยนต์ 2' ,
        'sub_y2s2_unit9' => '1' ,


        'text_sub_y2s2_unit_sum' =>'21', //หน่วยกิตรวม


         // ปี3 เทอม 1 
         'sub_y3s1_id1' => 'JPN-301',
         'sub_y3s1_name1' => 'ภาษาญี่ปุ่นธุรกิจ 5' ,
         'sub_y3s1_unit1' => '3' ,
 
         'sub_y3s1_id2' => 'ENG-301',
         'sub_y3s1_name2' => 'เศรษฐศาสตร์วิศวกรรม' ,
         'sub_y3s1_unit2' => '3' ,

         'sub_y3s1_id3' => 'AEN-305',
         'sub_y3s1_name3' => 'การวัดทางวิศวกรรมยานยนต์' ,
         'sub_y3s1_unit3' => '3' ,

         'sub_y3s1_id4' => 'AEN-306',
         'sub_y3s1_name4' => 'การสั่นสะเทือนทางกล ' ,
         'sub_y3s1_unit4' => '3' ,

         'sub_y3s1_id5' => 'AEN-308',
         'sub_y3s1_name5' => 'การออกแบบเครื่องจักรกล' ,
         'sub_y3s1_unit5' => '3' ,

         'sub_y3s1_id6' => 'AEN-313',
         'sub_y3s1_name6' => 'กลศาสตร์เครื่องจักรกล' ,
         'sub_y3s1_unit6' => '3' ,

         'sub_y3s1_id7' => 'XXX-xxx',
         'sub_y3s1_name7' => 'วิชาเลือกสาขา' ,
         'sub_y3s1_unit7' => '3' ,
 
         'text_sub_y3s1_unit_sum' =>'21', //หน่วยกิตรวม

         // ปี3 เทอม 2 
         'sub_y3s2_id1' => 'ENG-212',
         'sub_y3s2_name1' => 'กรรมวิธีการผลิต' ,
         'sub_y3s2_unit1' => '3' ,
 
         'sub_y3s2_id2' => 'ENG-215',
         'sub_y3s2_name2' => 'วิศวกรรมและการจัดการอุตสาหกรรม' ,
         'sub_y3s2_unit2' => '3' ,

         'sub_y3s2_id3' => 'AEN-310',
         'sub_y3s2_name3' => 'การควบคุมอัตโนมัติ ' ,
         'sub_y3s2_unit3' => '3' ,

         'sub_y3s2_id4' => 'AEN-312',
         'sub_y3s2_name4' => 'เครื่องยนต์สันดาปภายใน' ,
         'sub_y3s2_unit4' => '3' ,

         'sub_y3s2_id5' => 'AEN-491',
         'sub_y3s2_name5' => 'เตรียมสหกิจศึกษา' ,
         'sub_y3s2_unit5' => '3' ,

         'sub_y3s2_id6' => 'XXX-xxx',
         'sub_y3s2_name6' => 'วิชาเลือกสาขา' ,
         'sub_y3s2_unit6' => '3' ,

         'text_sub_y3s2_unit_sum' =>'18', //หน่วยกิตรวม

          // ปี4 เทอม 1 
          'sub_y4s1_id1' => 'AEN-492',
          'sub_y4s1_name1' => 'สหกิจศึกษา' ,
          'sub_y4s1_unit1' => '6',
          'text_sub_y4s1_unit_sum' =>'6', //หน่วยกิตรวม
 
          // ปี4 เทอม 2 
          'sub_y4s2_id1' => 'XXX-xxx',
          'sub_y4s2_name1' => 'กลุ่มวิชาสังคมศาสตร์ และมนุษยศาสตร์' ,
          'sub_y4s2_unit1' => '3' ,

          'sub_y4s2_id2' => 'XXX-xxx',
          'sub_y4s2_name2' => 'กลุ่มวิชาสังคมศาสตร์ และมนุษยศาสตร์' ,
          'sub_y4s2_unit2' => '3' ,

          'sub_y4s2_id3' => 'XXX-xxx',
          'sub_y4s2_name3' => 'วิชาเลือกสาขา' ,
          'sub_y4s2_unit3' => '3' ,

          'sub_y4s2_id4' => 'XXX-xxx',
          'sub_y4s2_name4' => 'วิชาเลือกสาขา' ,
          'sub_y4s2_unit4' => '3' ,

          'sub_y4s2_id5' => 'XXX-xxx',
          'sub_y4s2_name5' => 'วิชาเลือกเสรี' ,
          'sub_y4s2_unit5' => '3' ,

          'sub_y4s2_id6' => 'XXX-xxx',
          'sub_y4s2_name6' => 'วิชาเลือกเสรี' ,
          'sub_y4s2_unit6' => '3' ,
  
 
          'text_sub_y4s2_unit_sum' =>'18', //หน่วยกิตรวม

          
          'text_link_pdf_file' => 'file/curriculum_ae.pdf'  //path file document หลักสูตร

        //--- note ---

            // หลังจากใส่รายละเอียดรายวิชาแล้ว ให้นับจำนวนรายวิชาในแต่ละภาคเรียน แล้วนำจำนวนไปใส่ใน แต่ละ function ใน controller  Ex. MajorAeController

        //--- end note ---
       

   ];



 