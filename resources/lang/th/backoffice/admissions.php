<?php

return[
    'text_admissions' => 'การรับสมัคร (ป.ตรี)',
    'text_admission_lists' => 'รายการการรับสมัคร (ป.ตรี)',
    'text_create_admission' => 'สร้างการรับสมัคร (ป.ตรี)',
    'text_edit_admission' => 'แก้ไขการรับสมัคร (ป.ตรี)',
    'text_id' => 'รหัส',
    'text_image_cover' => 'รูปหน้าปก', //หน้ารวม
    'text_image_highlight' => 'รูป Highlight', //highlight
    'text_name' => 'ประเภทการรับสมัคร',
    'text_link' => 'Link (url แบบฟอร์มใบสมัครออนไลน์)',
    'text_pdf_link' => 'PDF Link',

];
?>