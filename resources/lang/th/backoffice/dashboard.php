<?php

return [

    'text_dashboard' => 'แผงควบคุม',
    'text_news' => 'ข่าว',
    'text_event_update' => 'อีเวนท์อัปเดท',
    'text_exchange_program' => 'โครงการแลกเปลี่ยน',
    'text_co_operative' => 'สหกิจศึกษา',
    'text_student_portfolio' => 'ผลงานนักศึกษา',
    'text_tni_channel' => 'TNI CHANNEL',
    'text_e_bochure' => 'อี-โบชัวร์',
    'text_banner' => 'แบนเนอร์',
    'text_banner_highlight' => 'แบนเนอร์ไฮไลท์',
    'text_information' => 'ข้อมูลสถาบันฯ',
    'text_admission' => 'ข้อมูลการรับสมัคร',
    'text_finance' => 'บัญชีและการเงิน',
    
];
