<?php

    return [
        'text_banner' => 'แบนเนอร์',
        'text_banner_lists' => 'รายการแบนเนอร์',
        'text_create_banner' => 'สร้างแบนเนอร์',
        'text_background_color' => 'สีพื้นหลัง',
        'text_edit_banner' => 'แก้ไขแบนเนอร์',


        //Banner highlight

        'text_banner_highlight' => 'แบนเนอร์ไฮไลท์',
        'text_banner_highlight_lists' => 'รายการแบนเนอร์ไฮไลท์',
        'text_create_banner_highlight' => 'สร้างแบนเนอร์ไฮไลท์',
        // 'text_background_color' => 'สีพื้นหลัง',
        'text_edit_banner_highlight' => 'แก้ไขแบนเนอร์ไฮไลท์',
    ];