/*-----------------------------------------------------------------
-----------------------------INDEX---------------------------------
-------------------------------------------------------------------
	--varidation input--
	isDigit
	isDecimal
	isPhoneNo
	isEmail
	isTimeFormat
	isUsernameFormat
	varifyBeforeSubmit
	feedbackNotify ~work with boostrap (class has-error)

	--force or change--
	NumOnlyValue(event)
	lowerCase
	enterMoveNext(event , strEleId)

	--varidation datetime input--
	DateCheck(obj)
	DateFormat(obj)
	isValidDate(obj)
	isValidDate2(str)
	delDateFormat(obj) 
	
	--custom--
	readURLMultipleFiles ~preview image before upload-
	switchToggleChekcbox ~interface of checkbox
-------------------------------------------------------------------*/

/*//////////////////////////////////////////////////////////////////
-------------------------VARIDATION INPUT---------------------------
//////////////////////////////////////////////////////////////////*/

/*------------------------------------------------------
	function: check Digit
	exp: var n=isDigit(f.obj.value);
/*------------------------------------------------------*/
function isDigit(input) 
{
	var regExp = /^[1-9]$/;
	return regExp.test(input);
}

/*------------------------------------------------------
	function: check Decimal
	exp: var n=isDecimal(f.obj.value);
/*------------------------------------------------------*/
function isDecimal(input) 
{
	
	//var regExp = /^[1-9]([0-9]+)*(\.[0-9]{2})*$/;
	var regExp = /^(0|[1-9]([0-9]+)*(\.[0-9]{2})*)$/;
	return regExp.test(input);
	/*+++++++++++++++++++++++++++++++++++
	^0|[1-9]        = 1st digit must begin with 1-9 Or 0
	([0-9]+)*     = next digit ,+ = 1 or more , * = nothing or more char
	(\.[0-9]{2})* = digit after point must 2 digit 
	++++++++++++++++++++++++++++++++++++++++++*/
}


/*------------------------------------------------------
	function: check phone no
	exp: var n=isPhoneNo(f.obj.value);
/*------------------------------------------------------*/
function isPhoneNo(input)
{
	var regExp = /^0[0-9]{8,9}$/;
	return regExp.test(input);
}

/*------------------------------------------------------
	function: check email
	exp: var n=isEmail(f.obj.value);
/*------------------------------------------------------*/
function isEmail(input)
{
	//var regExp = /[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/igm;
	if(input != ''){
		var regExp = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
		return regExp.test(input);
	}else{
		return true;
	}
}	

/*------------------------------------------------------
	function: check Time format (H:i)
	exp: var n=isTimeFormat(f.obj.value);
/*------------------------------------------------------*/
function isTimeFormat(input)
{
	var regExp = /^([0-1][0-9]|2[0-3]):[0-5][0-9]$/;
	return regExp.test(input);
}


/*----------------------------------------------------------------
	function: check username format (eng alphabet and digit only)
	exp: var n=isUsernameFormat(f.obj.value);
/*----------------------------------------------------------------*/
function isUsernameFormat(input)
{
	//ขึ้นต้นด้วยอักษร eng เล็กหรือใหญ่ ตัวต่อไปอะไรก็ได้ (เฉพาะเลขกัลตัวอีกษร)ซึ่งไม่เกิน 5-19 ตัว so username mush have length between 6-20 digits
	//var regExp = /^[a-zA-Z][a-zA-Z0-9]{5,19}$/i; //#อ่อนหัด is i=insentive case not specified A-Z
	//var regExp = /^[a-z][a-z0-9_]{5,19}$/i; //#add underscore
	var regExp = /^[a-z][a-z0-9]{4,49}$/; //#--sensitive case
	
	return regExp.test(input);
}

/*---------------------------------------------------------------------------------
	Function: Blank Check , varify before submit (Input element only) 
	Input   : 1.jquery selector[selector1,selector2,...] (e.g.:"form input[type='text'] , ...")
			: 2.exceptEleId[selector1 selector2 ...] (ex:'txtEmail txtPhone'(option))
	Output  : error message
	* work with Bootstrap css : class="form-group" 
	** must set id of tag (div) that use above class by start with  frmgroup_... word
/*---------------------------------------------------------------------------------*/
function varifyBeforeSubmit(jqrySel , exceptEleId)
{	
	var msgErr = '';
	$(jqrySel).each(function(){
        if( !$.trim($(this).val()) )
        {   
            var eleId = $(this).attr('id');
            //alert(eleId+'-'+exceptEleId+'-'+exceptEleId.search(eleId));
            if( exceptEleId.search(eleId) < 0){ 
	            $("#frmgroup_"+eleId).addClass('has-error'); 
	            msgErr += '*กรุณาระบุ ' + $(this).attr('placeholder') + '<br>';
            }
        }
    });
    return msgErr;
}

/*---------------------------------------------------------------------------------
	Function: Show Feedback's bootstap Notify for specified case ex- notify when username duplication
	Input   : 1.eleId = element id that u want to check
			: 2.chkResult = result check dup (YES or NO ) 
			: 3.notifyMsg = error case 's notify message 
	Output  : Notify message and add bootstrap Class
	** For duplicate check Yes = dup
	--must--
	i.work with Bootstrap css : class="form-group" 
	ii.set id of tag (div) that use above class by start with  frmgroup_... word ex. frmgroup_txtUsername
	iii.and add  span tag:id = spanFeedback_xxx such as  spanFeedback__txtUsername
	iv.add span tag:id = spanErr
	v.ex: <div id='frmgroup_txtUsername' class='from-group'>
			<input type=text id=txtUsername >
			<span id=spanFeedback__txtUsername></span>
		  </div>
		  <span id=spanErr></span> => for notify msg
/*---------------------------------------------------------------------------------*/
function feedbackNotify(eleId , chkResult , notifyMsg)
{
	var frmGrpClass   = 'has-feedback';
    var feedBackClass = 'form-control-feedback glyphicon';

	if(chkResult == 'has_error')
    {
      //#--add Class for duplication notify-------------------------
      $("#frmgroup_"+eleId).addClass(frmGrpClass + ' has-error'); //#red line around input textbox
      $("#spanFeedback_"+eleId).addClass(feedBackClass + ' glyphicon-remove');//#

      //#--text notify--
      //$('#spanErr').html( $('#'+eleId).val() + " มีในระบบแล้ว กรุณาระบุใหม่" );
      $('#errorNotify').html( $('#'+eleId).val() + notifyMsg );
      //#--Disabled submit button--
      $('#btnSubmit').addClass('disabled');

    }else if(chkResult == 'has_success'){
      
      //#--remove error Class-------------------------
      $("#frmgroup_"+eleId).removeClass('has-error');
      $("#spanFeedback_"+eleId).removeClass('glyphicon-remove');

      //#--add Class for pass notify-------------------------
      $("#frmgroup_"+eleId).addClass(frmGrpClass + ' has-success');
      $("#spanFeedback_"+eleId).addClass(feedBackClass + ' glyphicon-ok');

      //#--Clear text notify--
      $('#errorNotify').html('');
      //#--Enable submit button--
      $('#btnSubmit').removeClass('disabled');
    }
}

/*//////////////////////////////////////////////////////////////////
-------------------FORCRE or CHANGE INPUT VALUE--------------------
//////////////////////////////////////////////////////////////////*/

/*------------------------------------------------------
	Function: check digit input only
	exp: onKeypress="numOnlyValue(event)"
	ref:https://www.w3schools.com/charsets/ref_html_ascii.asp
/*------------------------------------------------------*/
function numOnlyValue(event)
{
	if((event.keyCode > 47) && (event.keyCode < 58) || (event.keyCode == 8)){
			event.returnValue = true; 
	}else{
			event.returnValue = false;
	}
}

/*------------------------------------------------------
function: lower case
exp: onfocusOut
/*------------------------------------------------------*/
function lowerCase(eleId)
{
	//input.value = input.value.toLowerCase();
	//alert('ss');
	$('#'+eleId).val($('#'+eleId).val().toLowerCase());

}


/*---------------------------------------------------------------------------
	Function : press enter on input text then go to next specified input text
	Work with: Input , onKeydown event
	Input    : event (enter) , element id of next input text 
/*---------------------------------------------------------------------------*/
function enterMoveNext(event , strEleId)
{
	if(event.keyCode == "13") 
		$('#'+strEleId).focus();
}

/*---------------------------------------------------------------------------
	Function : Money Format Dispaly
	Input : xxxx.xx Or xxxx
	Output : x,xxx,xxx.xx
	CR:stackoverflow.com/questions/2901102/how-to-print-a-number-with-commas-as-thousands-separators-in-javascript
/*---------------------------------------------------------------------------*/
function numberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g , ",");
}

/*//////////////////////////////////////////////////////////////////
-------------------VARIDATION DATETIME INPUT------------------------
//////////////////////////////////////////////////////////////////*/

/*----------------------------------------------------------	
function ตรวจสอบความถูกต้องเกี่ยวกับวันที่
	1. ต้องป้อนครบ 8 ตัว
	2. ต้องถูกต้องตามปีปฎิทิน
	3. ต้องเริ่มต้นตั้งแต่วันที่ 01/01/2535 เป็นต้นไป
	input อยู่ในรูปแบบ ddmmyyyy หรือ dd/mm/yyyy
	ตัวอย่าง onFocusout = DateCheck(this) 
------------------------------------------------------------*/
/*function DateCheck(obj) {
	var msg = "";
	if (obj.value.length != 0) {	
		DateFormat(obj);
		if (obj.value.length != 10) {
			msg = "โปรดบันทึกเป็นตัวเลข 8 หลัก";
		} else if (!isValidDate(obj)) {
			msg = "ค่าวันที่ไม่ถูกต้อง โปรดใส่ค่าใหม่";
		} 
	}
	if (msg.length != 0) {
		obj.value = "";
		alert(msg);
		obj.focus();
		return false;
	}
	return true;
}*/

/*------------------------------------------------------	
function ใส่รูปแบบให้กับวันที่จาก "ddmmyyyy" เป็น "dd/mm/yyyy"
input อยู่ในรูปแบบ ddmmyyyy
-------------------------------------------------------*/
// function DateFormat(obj) {
// 	if (obj.value.length == 8) {
// 		obj.value = obj.value.substring(0,2) + "/" + obj.value.substring(2,4) + "/" + obj.value.substring(4,8);
// 	}
// }

/*------------------------------------------------------
function:ตรวจสอบว่า เป็นวันที่ตามปีปฎิทิน
input : input element (อยู่ในรูปแบบ dd/mm/yyyy)
------------------------------------------------------*/
/*function isValidDate(obj) {
	var msg = "";
	if (obj.value.length == 10) {
		var strDate = obj.value.substring(0,2);
		var strMonth = obj.value.substring(3,5) - 1;
		// var dateEntry = new Date((obj.value.substring(6,10) - 543), (obj.value.substring(3,5) - 1), obj.value.substring(0,2));
		var dateEntry = new Date((obj.value.substring(6,10)), (obj.value.substring(3,5) - 1), obj.value.substring(0,2));
		if (strDate == dateEntry.getDate() && strMonth == dateEntry.getMonth()) 
		{
			return true;
		} 
	}
	return false;
}*/

/*------------------------------------------------------
function: ตรวจสอบว่า เป็นวันที่ตามปีปฎิทิน 
input: String (input อยู่ในรูปแบบ dd/mm/yyyy)
------------------------------------------------------*/
/*function isValidDate2(str) 
{
	if (str.length == 10) {
		var strDate = str.substring(0,2);
		var strMonth = str.substring(3,5) - 1;
		var dateEntry = new Date((str.substring(6,10)), (str.substring(3,5) - 1), str.substring(0,2));
		if (strDate == dateEntry.getDate() && strMonth == dateEntry.getMonth()) 
		{
			return true;
		} 
	}
	return false;
}*/

/*------------------------------------------------------------------------	
function ทำการเปลี่ยนรูปแบบของ dd/mm/yyyy ให้อยู่ในรูปแบบ ddmmyyyy เพื่อทำการพิมพ์ใหม่
ตัวอย่าง onFocus='delDateFormat(this)' 
------------------------------------------------------------------------*/
/*function delDateFormat(obj) {
	var regExp = new RegExp("/",'g');
	obj.value = obj.value.replace( regExp, "" );
	obj.select();
}
*/

/*//////////////////////////////////////////////////////////////////
--------------------------CUSTOM-----------------------------------
//////////////////////////////////////////////////////////////////*/

//***** JS File size Check : thaicreate.com/php/forum/042940.html
 /*------------------------------------------------------------------------------------------------
    FUNCTION: for for Preview selected images 
    INPUT   : 1.input file (single/multiple) object , 2.element selector (for preview img)
    OUTPUT  : append image element to Div
    NOTE    : work with Boostrap class & custom css (imageDisplay) & Button for browse file
    e.g.:
    	~~html part~~
    	  <input type=file id='idinputfile' class=hidden> ==> must hidden
    	  <button id='btnBrowse'></button> ==> interface of input file
    	  <div id='panel'></div> ==> for preview
    	~~jquery part~~
    	$('#btnBrowse').click(function(){ 
    		$('#idinputfile').trigger('click') });  ==>browse file Click,then trigger input file
    	$('#idinputfile').change(function(){ call fx }); ==> then input file change ,let call fx
		so, input 1=$('#idinputfile') or this
				  2='#panel'
    [cr-thaicoding.com|ninenik.com]
 ----------------------------------------------------------------------------------------------------*/
function readURLMultipleFiles(input , panelEleSelector) 
{   
  //console.log(input.files);
  if(input.files.length > 0)
  {
    var tagImg = ''; 
    var k      = 0; //#for set img element id or name

    $(panelEleSelector).html(''); //#Reset Preview panel (div)

    for (var i = 0; i < input.files.length; i++) 
    {
        if (input.files && input.files[i])
        {
            var reader = new FileReader();
            //#Note for- reader.onload function will process in last step
            reader.onload = function (e) 
            {   
              k++;
              tagImg = "<img id=previewImg"+k;
              tagImg += " src="+e.target.result;
              tagImg += " alt='your image' class='thumbnail imageDisplay'>";
                
              $(panelEleSelector).append(tagImg); //#Preview image
              tagImg = ''; //#Reset for next image
            }
            reader.readAsDataURL(input.files[i]);
        }
    }//#loop
  }else{
    //#If not select any image,clear Preview Panel(div)
    $(panelEleSelector).html("<small class='noPreviewText'></small>");
  }
}//end fx



/*----------------------------------------------------------------------------------------------------
    FUNCTION: for check/uncheck (on Interface of checkbox element)
    INPUT   : 1.element id of element that use font-awsome icon class (as user interface of checkbox )
              2.fount-awsome class of 1 (include on-off icon)
              3.fount-awsome class : icon default (meaning Swicht On)
              4.element id of Checkbox (must hidden)
              5.(option) element id of some kind of text input (check=enable/uncheck=disable)
    OUTPUT  : Check Of Uncheck checkbox / Enable or Disable text input
    NOTE    : work with Bootstrap / Font-awsome lib
    e.g.     : 
    	  <i id='switch' class='fontawsome icon switch on'></i>
          <input type=checkbox id='chkbox' class=hidden>
          <textare id='ta' readonly>
          so, input are 1='#switch' , 2='fontawsome icon switch on fontawsome icon switch off'
                        3='fontawsome icon switch on' , 4='#chkbox' , 5='#ta'
  -------------------------------------------------------------------------------------------------------*/
  function switchToggleChekcbox( uiChkBoxEleSelector , uiChkBoxEleClass 
                                ,uiChkBoxEleClassSwitchDefault ,chkBoxEleSelector ,txtEleSlector = '')
  {
      event.preventDefault();//#prevent behavior of a href

      //#On or Off Switch (checkbox) Interface (use fontawsome class)
      $(uiChkBoxEleSelector).toggleClass(uiChkBoxEleClass);

      var isAvailable = $(uiChkBoxEleSelector).hasClass(uiChkBoxEleClassSwitchDefault);
      if(isAvailable) //#if default check On
      {
        $(chkBoxEleSelector).prop('checked', true); //#check on checkbox
       
        if(txtEleSlector != ''){
        	$(txtEleSlector).each(function(){
        		//$(this).prop('readonly', ''); //#Enable input text or textarea
        		$(this).removeClass('hidden'); //#Enable input text or textarea
        	});
        }

      }else{ //#if default check Off
        
        $(chkBoxEleSelector).prop('checked',false); //#Uncheck on checkbox
      
        if(txtEleSlector != ''){
        	$(txtEleSlector).each(function(){
        		//$(this).val('').prop('readonly', 'readonly'); //#Clear value & Disable input text or textarea; 
        		$(this).val('').addClass('hidden'); //#Clear value & Disable input text or textarea; 
        	});
        }

      }//isAvailable
        
  }