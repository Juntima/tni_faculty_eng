var alertToat = function(text = '', bgColor = '#5cb85c', title = 'ALERT') {
    $.toast({
        heading: title,
        text: text,
        icon: 'info',
        loader: true, // Change it to false to disable loader
        loaderBg: 'yellow', // To change the background
        hideAfter: 10000,
        bgColor: bgColor,
        position: 'bottom-right'
    })
};

// Highlight any found errors
$('.text-danger').each(function() {
    var element = $(this).parent();

    if (element.hasClass('form-group')) {
        element.addClass('has-error');
    }
});


var swalConfirm = function(title = 'Are you sure?', text = 'Once deleted, you will not be able to recover this data', icon = 'warning') {
    return swal({
            title: title,
            text: text,
            icon: icon,
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                return true;
            } else {
                return false;
            }
        });
};