$(document).ready(function() {
    $('a[href="#"]').attr('href', 'javascript:;');

    $("a[href='#top']").click(function() {
        $("html, body").animate({
            scrollTop: 0
        }, "slow");
        return false;
    });

    /**
     * Deteact scroll up / down
     */
    var iScrollPos = 0;
    $(window).scroll(function() {
        var iCurScrollPos = $(this).scrollTop();
        if (iCurScrollPos > iScrollPos) {
            //Scrolling Down
            $('a#go2top').fadeIn();
        } else {
            //Scrolling Up
            $('a#go2top').fadeOut();
        }
        iScrollPos = iCurScrollPos;
    });
});